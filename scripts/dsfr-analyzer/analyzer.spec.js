const { Analyzer } = require('./analyzer');

describe('Analyzer', () => {
  test('analyzeFile', async () => {
    const analyzer = new Analyzer();
    await analyzer
      .testAnalyzeFile('projects/ngx-dsfr-components/src/lib/components/card/card.component.html')
      .then((classNames) => {
        expect(classNames).toBeDefined();
        expect(classNames).toEqual(
          new Set([
            'fr-card__body',
            'fr-card__content',
            'fr-card__desc',
            'fr-card__detail',
            'fr-card__end',
            'fr-card__footer',
            'fr-card__header',
            'fr-card__img',
            'fr-card__start',
            'fr-card__title',
            'fr-ratio-16x9',
            'fr-ratio-1x1',
            'fr-ratio-2x3',
            'fr-ratio-32x9',
            'fr-ratio-3x2',
            'fr-ratio-3x4',
            'fr-ratio-4x3',
            'fr-responsive-img',
          ]),
        );
      });
  });

  test('parseContent', () => {
    const content = `
    getClasses(): string[] {
      const classes = ['fr-alert'];
      if (this.severity === DsfrSeverityConst.ERROR) classes.push(this.dsfr.c('fr-alert--error'));
      if (this.severity === DsfrSeverityConst.SUCCESS) classes.push(this.dsfr.c('fr-alert--success'));
      if (this.severity === DsfrSeverityConst.INFO) classes.push(this.dsfr.c('fr-alert--info'));
      if (this.severity === DsfrSeverityConst.WARNING) classes.push(this.dsfr.c('fr-alert--warning'));
      if (this.size === DsfrSizeConst.SM) classes.push(this.dsfr.c('fr-alert--sm'));
      return classes;
    }`;
    const analyzer = new Analyzer();
    const classNames = analyzer.testParseContent(content);
    expect(classNames).toBeDefined();
    expect(classNames).toEqual(
      new Set([
        'fr-alert',
        'fr-alert--error',
        'fr-alert--info',
        'fr-alert--sm',
        'fr-alert--success',
        'fr-alert--warning',
      ]),
    );
  });
});
