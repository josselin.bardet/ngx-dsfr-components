const { readFile, readdir } = require('fs/promises');
const { frCoreClassNames, frIconClassNames, riIconClassNames } = require('./classnames.js');

module.exports = { Analyzer };

/**
 * Parse et analyse les fichiers HTML d'un répertoire.
 * Indique les styles 'fr-' qui ne font pas partie du Dsfr
 */
function Analyzer() {
  const dsfrClassNames = frCoreClassNames.concat(frIconClassNames).concat(riIconClassNames);

  /**
   * Analyse le répertoire
   * @param path
   * @return {Promise<void>}
   */
  this.analyzeDir = (path) => {
    return new Promise((resolve, reject) => {
      readdir(path, { withFileTypes: true }).then(
        (entries) => {
          const allPromises = [];
          entries.forEach((entry) => {
            const entryPath = path + '/' + entry.name;
            if (entry.isFile()) {
              const extension = getExtension(entryPath);
              if (extension === 'html' || entryPath.endsWith('component.ts') || entryPath.endsWith('stories.ts'))
                allPromises.push(analyzeFile(entryPath));
            } else if (entry.isDirectory()) {
              allPromises.push(this.analyzeDir(entryPath));
            }
          });
          Promise.all(allPromises).then(
            () => resolve(),
            (err) => reject(err),
          );
        },
        (err) => reject(err),
      );
    });
  };

  /**
   * Analyse un fichier
   * @param path
   * @return {Promise<unknown>}
   */
  function analyzeFile(path) {
    return new Promise((resolve, reject) => {
      readFile(path).then(
        (content) => {
          console.info("- Fichier '" + path + "'");
          const classNames = analyzeContent(content);
          resolve(classNames);
        },
        (err) => reject(err),
      );
    });
  }

  /**
   * Analyse le contenu d'un fichier
   */
  function analyzeContent(content) {
    const classNames = parseContent(content.toString());
    checkClassNames(classNames);
    return classNames;
  }

  /**
   * Parse le contenu HTML et isole le nom des classes du document dans classNames
   * @param content
   */
  function parseContent(content) {
    const classNames = new Set();
    const regExpr = /[ '"](fr-)[^ '")]*/gs;
    const result = content.matchAll(regExpr);
    Array.from(result, (matchArr) => {
      classNames.add(matchArr[0].substring(1));
    });
    return classNames;
  }

  /**
   * Vérifie que les classes font partie du DSFR
   */
  function checkClassNames(classNames) {
    classNames.forEach((className) => {
      if (className?.startsWith('fr-')) {
        const index = dsfrClassNames.findIndex((e) => e === className);
        if (index === -1) console.error(`  /!\\ Classe '${className}' inconnue`);
      }
    });
  }

  // -- Tests -------------------------------------------------------------- */
  this.testAnalyzeFile = (path) => {
    return analyzeFile(path);
  };
  this.testParseContent = (content) => {
    return parseContent(content);
  };
}

function getExtension(fileName /*: string*/) {
  return fileName.split('.').pop();
}
