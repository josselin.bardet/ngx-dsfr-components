// const readline = require('readline');
const { Analyzer } = require('./analyzer');

module.exports = { dsfrAnalyzerMain };

function dsfrAnalyzerMain(args) {
  // const conversation = readline.createInterface({
  //   input: process.stdin,
  //   output: process.stdout,
  // });

  const dir = args?.dir || 'projects/ngx-dsfr-components/src/lib';
  // conversation.question(`Voulez-vous analyser le répertoire '${dir}' ? (Y/n) `, (response) => {
  //   conversation.close();
  //   if (response === 'y' || response === 'Y' || !response) {
  console.log(`Analyse du répertoire '${dir}'`);
  const analyzer = new Analyzer();
  analyzer.analyzeDir(dir).then(
    () => console.log('Le traitement est terminé'),
    (err) => console.error(err),
  );
  //   } else console.log('quit');
  // });
}
