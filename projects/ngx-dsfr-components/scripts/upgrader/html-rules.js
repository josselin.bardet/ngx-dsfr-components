const RULES = {
  /**
   * Le renommage des tags est le premier ensemble de règles passé, donc toutes les autres règles doivent référencer
   * les nouveaux tags. Les traitements 1.0 passent avant 1.1.
   */
  TAGS: [
    /* --- 1.0 --- */
    [1.0, 'dsfr-badge-group', 'dsfr-badges-group'],
    [1.0, 'dsfr-call-out', 'dsfr-callout'],
    [1.0, 'dsfr-consent', 'dsfr-consent-banner'],
    [1.0, 'dsfr-download-multiple', 'dsfr-downloads-group'],
    [1.0, 'dsfr-radio-button', 'dsfr-form-radio'],
    [1.0, 'dsfr-radiobutton-extended', 'dsfr-form-radio-rich'],
    [1.0, 'dsfr-scroll-to-top', 'dsfr-backtotop'],
    [1.0, 'dsfr-search', 'dsfr-search-bar'],
    [1.0, 'dsfr-tag-group', 'dsfr-tags-group'],
    [1.0, 'dsfr-toggle-switch', 'dsfr-form-toggle'],

    /* --- 1.1 --- */
    [1.1, 'dsfr-checkbox', 'dsfr-form-checkbox'],
    [1.1, 'dsfr-input', 'dsfr-form-input'],
    [1.1, 'dsfr-radio', 'dsfr-form-radio'],
    [1.1, 'dsfr-select', 'dsfr-form-select'],
  ],

  /** Renommage des attributs */
  ATTR_NAMES: [
    /* --- 1.0 --- */
    // Général
    [1.0, 'dsfr-', 'aide', 'hint'],
    [1.0, 'dsfr-', 'customThemeClass', 'customClass'],
    [1.0, 'dsfr-', 'href', 'link'],
    [1.0, 'dsfr-', 'tooltip', 'tooltipMessage'],
    // Spécifique
    [1.0, 'dsfr-accordion', 'ngModel', 'expanded'],
    [1.0, 'dsfr-alert', 'closeControlId', 'closable'],
    [1.0, 'dsfr-button', 'buttonAriaLabel', 'headingAriaLabel'],
    [1.0, 'dsfr-button', 'state', 'variant'],
    [1.0, 'dsfr-button', 'text', 'label'],
    [1.0, 'dsfr-button', 'titleButton', 'tooltipMessage'],
    [1.0, 'dsfr-buttons-group', 'breakpoint', 'inline'],
    [1.0, 'dsfr-buttons-group', 'justify', 'alignment'],
    [1.0, 'dsfr-buttons-group', 'buttonsEquisized', 'equisized'],
    [1.0, 'dsfr-breadcrumb', 'model', 'items'],
    [1.0, 'dsfr-card', 'badgesMedia', 'badges'], // FIXME Mettre media=true
    [1.0, 'dsfr-card', 'clickFullArea', 'enlargeLink'],
    [1.0, 'dsfr-card', 'title', 'heading'],
    [1.0, 'dsfr-card', 'mediaType', 'imageType'],
    [1.0, 'dsfr-card', 'objectFitValue', 'imageFit'],
    [1.0, 'dsfr-card', 'useActionArea', 'hasFooter'],
    [1.0, 'dsfr-form-checkbox', 'selectedOption', 'value'],
    [1.0, 'dsfr-download', 'cardDescription', 'fileDescription'],
    [1.0, 'dsfr-download', 'cardMode', 'variant'],
    [1.0, 'dsfr-download', 'hreflang', 'langCode'],
    [1.0, 'dsfr-download', 'linklang', 'langCode'],
    [1.0, 'dsfr-download', 'fileDescription', 'description'],
    [1.0, 'dsfr-download', 'fileMimeType', 'mimeType'],
    [1.0, 'dsfr-download', 'fileSizeOctet', 'sizeBytes'],
    [1.0, 'dsfr-download', 'metaDataDownload', 'assessFile'],
    [1.0, 'dsfr-downloads-group', 'titleGroup', 'heading'],
    [1.0, 'dsfr-footer', 'reboundLink', 'reboundLinks'],
    [1.0, 'dsfr-footer', 'presentationText', 'reboundLinks'],
    [1.0, 'dsfr-footer', 'institutionelLinks', 'institutionalLinks'],
    [1.0, 'dsfr-header', 'fastAccessLinks', 'headerToolsLinks'],
    [1.0, 'dsfr-header', 'siteName', 'serviceTitle'],
    [1.0, 'dsfr-header', 'baseline', 'serviceTagline'],
    [1.0, 'dsfr-header', 'imagePath', 'operatorImagePath'],
    [1.0, 'dsfr-header', 'imageAlt', 'operatorImageAlt'],
    [1.0, 'dsfr-header', 'verticalImage', 'operatorImageVertical'],
    [1.0, 'dsfr-header', 'searchbar', 'searchBar'],
    [1.0, 'dsfr-link', 'routerLinkActiveValue', 'routerLinkActive'],
    [1.0, 'dsfr-link', 'target', 'targetLink'],
    [1.0, 'dsfr-link', 'text', 'label'],
    [1.0, 'dsfr-link', 'routerLinkValue', 'routerLink'],
    [1.0, 'dsfr-link', 'hrefValue', 'link'],
    [1.0, 'dsfr-form-password', 'description', 'hint'],
    [1.0, 'dsfr-form-select', 'firstOption', 'placeHolder'],
    [1.0, 'dsfr-form-select', 'selectedOption', 'value'],
    [1.0, 'dsfr-quote', 'sourceURL', 'sourceUrl'],
    [1.0, 'dsfr-form-select', 'selectedOption', 'value'],
    [1.0, 'dsfr-skiplinks', 'skiplinks', 'links'],
    [1.0, 'dsfr-sidemenu', 'ariaLabelledby', 'ariaLabel'],
    [1.0, 'dsfr-sidemenu', 'model', 'menu'],
    [1.0, 'dsfr-table', 'bottomCaption', 'captionBottom'],
    [1.0, 'dsfr-table', 'borders', 'bordered'],
    [1.0, 'dsfr-table', 'rowBorders', 'bordered'],
    [1.0, 'dsfr-table', 'table', 'data'],
    [1.0, 'dsfr-tab', 'remixIcon', 'icon'],
    [1.0, 'dsfr-tag', 'url', 'link'],
    [1.0, 'dsfr-tag', 'tagTitle', 'tooltipMessage'],
    [1.0, 'dsfr-tile', 'content', 'description'],
    [1.0, 'dsfr-tile', 'title', 'heading'],
    [1.0, 'dsfr-form-toggle', 'hr', 'showSeparator'],
    [1.0, 'dsfr-form-toggle', 'checked', 'defaultChecked'],
    [1.0, 'dsfr-form-toggle', 'displayState', 'showCheckedHint'],
    [1.0, 'dsfr-form-toggle', 'showCheckedLabel', 'showCheckedHint'],
    [1.0, 'dsfr-form-toggle', 'stateChecked', 'checkedHintLabel'],
    [1.0, 'dsfr-form-toggle', 'dataLabelChecked', 'checkedHintLabel'],
    [1.0, 'dsfr-form-toggle', 'stateUnchecked', 'uncheckedHintLabel'],
    [1.0, 'dsfr-form-toggle', 'dataLabelUnchecked', 'uncheckedHintLabel'],
    [1.0, 'dsfr-form-toggle', 'tooltipMessage', 'hint'],
    [1.0, 'dsfr-form-toggle', 'defaultChecked', 'value'],
    [1.0, 'dsfr-toggles-group', 'hr', 'showSeparators'],
    [1.0, 'dsfr-toggles-group', 'hasBorderBottom', 'showSeparators'],

    /* --- 1.2 --- */
    [1.2, 'dsfr-form-select', 'placeHolder', 'placeholder'],
  ],

  ATTR_VALUES: [
    /* --- 1.0 --- */
    // Général
    [1.0, 'dsfr-highlight', 'borderWidth', 'small', 'SM'],
    [1.0, 'dsfr-highlight', 'borderWidth', 'medium', 'MD'],
    [1.0, 'dsfr-highlight', 'borderWidth', 'large', 'LG'],
    // Icônes fontawasome
    [1.0, 'dsfr-', 'icon', 'fa-arrow-right', 'ri-arrow-right-line'],
  ],

  ATTR_LOWERCASE: [[1.0, 'dsfr-', 'severity']],

  ATTR_DELETE: [
    /* --- 1.0 --- */
    // Forms
    [1.0, 'dsfr-form-', 'bindLabel'], // Pas de binding des propriétés
    [1.0, 'dsfr-form-', 'bindValue'],
    [1.0, 'dsfr-form-', 'bindAideRadio'],
    // Spécifique
    [1.0, 'dsfr-backtotop', 'link'], // (anciennement `href`)
    [1.0, 'dsfr-button', 'borderTertiary'], // Utilisez la propriété `variant`
    [1.0, 'dsfr-card', 'useHeadingAside'], // Utilisez les propriétés `detail` et `detailBottom` à la place
    [1.0, 'dsfr-card', 'useTitleAside'], // idem
    [1.0, 'dsfr-download', 'fileLanguage'], // Seul `langCode` est significatif
    [1.0, 'dsfr-download', 'buttonMarkup'], // Affichage d'un bouton si `link` n'est pas renseigné
    [1.0, 'dsfr-highlight', 'borderWidth'], // Non conforme au DSFR
    [1.0, 'dsfr-highlight', 'weightEdge'], // Non conforme au DSFR
    [1.0, 'dsfr-link', 'position'], // Pas d'icône à droite
    [1.0, 'dsfr-form-radio', 'addonTemplateRef'],
    [1.0, 'dsfr-form-select', 'compareWith'], // Non supporté
    [1.0, 'dsfr-sidemenu', 'overlay'],
    [1.0, 'dsfr-toggle', 'defaultChecked'], // Non conforme au DSFR
    [1.0, 'dsfr-tag', 'disabled'], // Non conforme au DSFR
    [1.0, 'dsfr-tag', 'iconPositionRight'],
    [1.0, 'dsfr-tile', 'underlined'], // Non conforme au DSFR

    /* --- 1.1 --- */
    [1.1, 'dsfr-modal', 'titleElementId'],
  ],

  STATEMENTS: [
    /* --- 1.0 --- */
    // Événements
    [1.0, 'dsfr-accordion', `expandedEvent`, `expandedChange`],
    [1.0, 'dsfr-accordion', '<ng-container body>', '<ng-container content>'],
    [1.0, 'dsfr-buttons-group', `<ng-container btns>`, `<ng-container>`, 'Container par défaut'],
    [1.0, 'dsfr-breadcrumb', `itemClick`, `linkSelect`],
    [1.0, 'dsfr-card', `[useGreyBackground]="true"`, `[customBackground]="'grey'"`],
    [1.0, 'dsfr-card', `[noArrow]="false"`, `[enlargeLink]="true"`],
    [1.0, 'dsfr-card', `[noArrow]="true"`, `[enlargeLink]="false"`],
    [1.0, 'dsfr-consent-banner', `acceptAllEvent`, `acceptAllSelect`],
    [1.0, 'dsfr-consent-banner', `refuseAllEvent`, `refuseAllSelect`],
    [1.0, 'dsfr-consent-banner', `openModalEvent`, `customizeSelect`],
    [1.0, 'dsfr-consent-banner', `confirmCustomizeEvent`, `confirmCustomizeSelect`],
    [1.0, 'dsfr-consent-banner', `changeAcceptFinalityEvent`, `finalityChange`],
    [1.0, 'dsfr-header', `linkEvent`, `linkSelect`],
    [1.0, 'dsfr-header', `searchPerformed`, `searchSelect`],
    [1.0, 'dsfr-form-input', `inputEmitter`, `input`],
    [1.0, 'dsfr-link', `linkEvent`, `linkSelect`],
    [1.0, 'dsfr-form-select', `selectItemEvent`, `itemSelect`],
    [1.0, 'dsfr-summary', `routeSelect`, `entrySelect`],
    [1.0, 'dsfr-tag', `linkEvent`, `tagSelect`],
    // Expressions
    [1.0, 'dsfr-accordion', '[(ngModel)]', '[expanded]'],
    [1.0, 'dsfr-buttons-group', '[inline]="true"', `[inline]="'inline'"`],
    [1.0, 'dsfr-callout', '[icon]="true"', 'icon="fr-fi-information-line"'],
    [1.0, 'dsfr-callout', '[icon]="false"', ''],
    [1.0, 'dsfr-callout', '<ng-container body>', '<ng-container>'],
    [1.0, 'dsfr-card', '[useGreyBackground]="true"', `[customBackground]="'grey'"`],
    [1.0, 'dsfr-download', '[buttonMarkup]="true"', `[variant]="'block'"`],
    [1.0, 'dsfr-form-toggle', '[labelLeft]="true"', `[labelPosition]="'left'"`],

    /* --- 1.2 --- */
    [1.2, 'dsfr-accordion', '<ng-container content>', '<ng-container>'], // Peut être supprimé
  ],
};

module.exports = { RULES };
