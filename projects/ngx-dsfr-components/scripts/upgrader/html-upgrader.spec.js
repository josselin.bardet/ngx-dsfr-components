const { beforeAll } = require('@jest/globals');
const { HtmlUpgrader } = require('./html-upgrader');

describe('HtmlUpgrader', () => {
  let upgrader;

  beforeAll(() => {
    upgrader = new HtmlUpgrader();
  });

  /* --- Contenu HTML ----------------------------------------------------------------------------------------------- */
  test('contentHtml', () => {
    const actual = `contentHtml
                    <dsfr-button [state]="'primary'" [icon]="'fa-arrow-left'" [iconPosition]="'right'" [text]="'Bouton primary with icon on left'" ></dsfr-button>
                    <dsfr-button [text]="'Bouton secondary icon fa right'" [state]="'secondary'" [icon]="'ri-arrow-right-line'" [iconPosition]="'right'"></dsfr-button>`;
    const expected = actual
      .replaceAll('[text]=', '[label]=')
      .replaceAll('[state]=', '[variant]=')
      .replaceAll('[titleButton]=', '[tooltipMessage]=');
    const received = upgrader.testUpgradeContent(actual);
    expect(received).toBe(expected);
  });

  /* --- Suppression du préfixe ngx --------------------------------------------------------------------------------- */
  test('prefixNgx', () => {
    const actual = `<ngx-dsfr-button id="1">Label</ngx-dsfr-button>
                    <ngx-dsfr-button id="2">Label</ngx-dsfr-button>`;
    const expected = actual.replaceAll('ngx-dsfr', 'dsfr');
    const received = upgrader.testDeletePrefixNgx(actual);
    expect(received).toBe(expected);
  });

  /* --- Renommage d'un tag ----------------------------------------------------------------------------------------- */
  test('tag', () => {
    const actual = `tag
                    <dsfr-badge-group [badges]="badgesData"></dsfr-badge-group>
                    <dsfr-badge-group [badges]="badgesData"></dsfr-badge-group>`;
    const expected = actual.replaceAll('dsfr-badge-group', 'dsfr-badges-group');
    const received = upgrader.testUpgradeTag(actual, [1.0, 'dsfr-badge-group', 'dsfr-badges-group']);
    expect(received).toBe(expected);
  });

  /* --- Renommage d'un attribut ------------------------------------------------------------------------------------ */
  test('attributeName', () => {
    const actual = `<dsfr-button [icon]="'fa-arrow-left'" [state]="'primary'" [iconPosition]="'right'" [text]="'Bouton 1'"></dsfr-button>
                 <dsfr-button [text]="'Bouton 2'" state="secondary" [icon]="'fa-arrow-right'" [iconPosition]="'right'"></dsfr-button>`;
    const expected = actual.replaceAll('state', 'variant');
    const received = upgrader.testUpgradeAttributeName(actual, [1.0, 'dsfr-button', 'state', 'variant']);
    expect(received).toBe(expected);
  });

  /* --- Valeur d'un attribut --------------------------------------------------------------------------------------- */
  test('attributeValue', () => {
    const actual = `<dsfr-highlight [label]="'1'" borderWidth="small"></dsfr-highlight>  
  <dsfr-highlight [borderWidth]="'small'" label="2" ></dsfr-highlight>`;
    const expected = actual.replaceAll('small', 'SM');
    const received = upgrader.testUpgradeAttributeValue(actual, [1.0, 'dsfr-highlight', 'borderWidth', 'small', 'SM']);
    expect(received).toBe(expected);
  });

  /* --- Valeur d'un attribut en minuscule -------------------------------------------------------------------------- */
  test('upgradeLowerCase', () => {
    const actual = `valueLowerCase début
              <dsfr-alert heading="Titre alerte 1" [headingLevel]="'H2'" message="Description alerte 1" severity="SUCCESS"></dsfr-alert>
              <dsfr-alert heading="Titre alerte 2" [headingLevel]="'H3'" severity="SUCCESS" message="Description alerte 2"></dsfr-alert>
              valueLowerCase fin`;
    const expected = actual.replaceAll('SUCCESS', 'success');
    const received = upgrader.testUpgradeLowerCase(actual, [1.0, 'dsfr-', 'severity']);
    expect(received).toBe(expected);
  });

  /* --- Suppression d'attribut ------------------------------------------------------------------------------------- */
  test('suppressAttribute', () => {
    const actual = `<dsfr-highlight [label]="'1'" [weightEdge]="'bold'"
></dsfr-highlight>  
  <dsfr-highlight weightEdge="large"    label="2" ></dsfr-highlight>`;
    const expected = `<dsfr-highlight [label]="'1'" ></dsfr-highlight>  
  <dsfr-highlight label="2" ></dsfr-highlight>`;
    const received = upgrader.testSuppressAttribute(actual, [1.0, 'dsfr-highlight', 'weightEdge']);
    expect(received).toBe(expected);
  });

  /* --- Remplacement d'une expression ------------------------------------------------------------------------------ */
  test('ReplaceStatement', () => {
    const actual = `<dsfr-accordion index="8" [(ngModel)]="expanded">`;
    const expected = actual.replace('[(ngModel)]', '[expanded]');
    let received = upgrader.testReplaceStatement(actual, [1.0, 'dsfr-accordion', '[(ngModel)]', '[expanded]']);
    received = upgrader.testReplaceStatement(received, [1.0, 'dsfr-callout', '[icon]="false"', '']);
    expect(received).toBe(expected);
  });

  test('statement_container', () => {
    const actual = `<dsfr-buttons-group><ng-container><ng-container btns></ng-container></ng-container></dsfr-buttons-group>`;
    const expected = actual.replace('<ng-container btns>', '<ng-container>');
    const received = upgrader.testReplaceStatement(actual, [
      1.0,
      'dsfr-buttons-group',
      `<ng-container btns>`,
      `<ng-container>`,
    ]);
    expect(received).toBe(expected);
  });

  test('statement_callout', () => {
    const actual = `<dsfr-callout heading="Callout 1" [icon]="false"></dsfr-callout>
                <dsfr-callout heading="Callout 2" [icon]="true"></dsfr-callout>`;
    const expected = actual
      .replaceAll('[icon]="true"', 'icon="fr-fi-information-line"')
      .replaceAll('[icon]="false"', '');
    let received = upgrader.testReplaceStatement(actual, [
      1.0,
      'dsfr-callout',
      '[icon]="true"',
      'icon="fr-fi-information-line"',
    ]);
    received = upgrader.testReplaceStatement(received, [1.0, 'dsfr-callout', '[icon]="false"', '']);
    expect(received).toBe(expected);
  });

  /* --- Evénements ------------------------------------------------------------------------------------------------- */
  test('event', () => {
    const actual = `<dsfr-tag id="1" (linkEvent)="onEvent()"></dsfr-tag>
                    <dsfr-tag id="2" (linkEvent)="onEvent()"></dsfr-tag>`;
    const expected = actual.replaceAll('linkEvent', 'tagSelect');
    const received = upgrader.testReplaceStatement(actual, [1.0, 'dsfr-tag', `linkEvent`, `tagSelect`]);
    expect(received).toBe(expected);
  });

  test('upgradeButton', () => {
    const actual = `<li>En +</li>   <li>
              <dsfr-button label='Btn 1'></dsfr-button>   </li>   <li>  
                         <dsfr-button label='Btn 2'></dsfr-button>
            </li>`;
    const expected = `<li>En +</li>   <dsfr-button label='Btn 1'></dsfr-button>   <dsfr-button label='Btn 2'></dsfr-button>`;
    let received = upgrader.testUpgradeButton(actual);
    expect(received).toBe(expected);
  });
});
