const { RULES } = require('./html-rules');

function HtmlUpgrader() {
  const REGEXP_FLAGS = 'sg';

  /* --- Contenu HTML ----------------------------------------------------------------------------------------------- */
  this.upgradeContent = (content) => {
    let result = content;
    result = deletePrefixNgx(result);
    result = upgradeRules(result, RULES.TAGS, upgradeTag);
    result = upgradeRules(result, RULES.ATTR_NAMES, upgradeAttributeName);
    result = upgradeRules(result, RULES.ATTR_VALUES, upgradeAttributeValue);
    result = upgradeRules(result, RULES.ATTR_LOWERCASE, upgradeLowerCase);
    result = upgradeRules(result, RULES.ATTR_DELETE, suppressAttribute);
    result = upgradeRules(result, RULES.STATEMENTS, replaceStatement);
    result = upgradeButton(result);
    return result;
  };

  function upgradeRules(content, rules, fnUpgrade) {
    let result = content;
    rules.forEach((rule) => {
      result = fnUpgrade(result, rule);
    });
    return result;
  }

  /* --- Suppression du préfixe ngx --------------------------------------------------------------------------------- */
  function deletePrefixNgx(content, rule) {
    return content.replaceAll('<ngx-dsfr-', '<dsfr-').replaceAll('</ngx-dsfr-', '</dsfr-');
  }

  /* --- Renommage d'un tag ----------------------------------------------------------------------------------------- */
  function upgradeTag(content, rule) {
    const generic = '<COMPONENT(\\s?[^>]*?>.*?<)\\/COMPONENT>';
    const regexpStr = generic.replaceAll('COMPONENT', rule[1]);
    const regexp = new RegExp(regexpStr, REGEXP_FLAGS);
    return content.replaceAll(regexp, `<${rule[2]}$1/${rule[2]}>`);
  }

  /* --- Renommage d'un attribut ------------------------------------------------------------------------------------ */
  function upgradeAttributeName(content, rule) {
    const generic = '(<COMPONENT[^>]*?)(ATTRIBUTE)([\\]]?=")';
    const regexpStr = generic.replace('COMPONENT', rule[1]).replace('ATTRIBUTE', rule[2]);
    const regexp = new RegExp(regexpStr, REGEXP_FLAGS);
    return content.replaceAll(regexp, `$1${rule[3]}$3`);
  }

  /* --- Valeur d'un attribut --------------------------------------------------------------------------------------- */
  function upgradeAttributeValue(content, rule) {
    // \x5B représente '['
    const generic = '(<COMPONENT[^>]*?\\x5B?ATTRIBUTE]?="\'?)VALUE(\'?")';
    const regexpStr = generic.replace('COMPONENT', rule[1]).replace('ATTRIBUTE', rule[2]).replace('VALUE', rule[3]);
    const regexp = new RegExp(regexpStr, REGEXP_FLAGS);
    return content.replaceAll(regexp, `$1${rule[4]}$2`);
  }

  /* --- Valeur d'un attribut en minuscule -------------------------------------------------------------------------- */
  function upgradeLowerCase(content, rule) {
    const fnLowercase = (match, p1, p2) => {
      return p1 + p2?.toLowerCase();
    };

    const generic = '(<COMPONENT[^>]*?ATTRIBUTE.*?=")([^"]*)';
    const regexpStr = generic.replace('COMPONENT', rule[1]).replace('ATTRIBUTE', rule[2]);
    const regexp = new RegExp(regexpStr, REGEXP_FLAGS);
    return content.replaceAll(regexp, fnLowercase);
  }

  /* --- Suppression d'attribut ------------------------------------------------------------------------------------- */
  function suppressAttribute(content, rule) {
    const generic = `(<COMPONENT[^>]*?)(\\[?ATTRIBUTE\\]?=".*?"\\s*)`;
    const regexpStr = generic.replace('COMPONENT', rule[1]).replace('ATTRIBUTE', rule[2]);
    const regexp = new RegExp(regexpStr, REGEXP_FLAGS);
    return content.replaceAll(regexp, '$1');
  }

  /* --- Remplacement d'une expression ------------------------------------------------------------------------------ */
  function replaceStatement(content, rule) {
    const generic = '(<COMPONENT.*?)(STATEMENT)';
    const statement = replaceSpecialCars(rule[2]);
    const regexpStr = generic.replace('COMPONENT', rule[1]).replace('STATEMENT', statement);
    const regexp = new RegExp(regexpStr, REGEXP_FLAGS);
    return content.replaceAll(regexp, `$1${rule[3]}`);
  }

  function replaceSpecialCars(regexpStr) {
    let result = regexpStr;
    const specialCars = ['[', ']', '(', ')'];
    specialCars.forEach((c) => (result = result.replaceAll(c, `\\${c}`)));
    return result;
  }

  /**
   *
   * @param content
   * @return {*}
   */
  function upgradeButton(content) {
    // Suppression des <li>
    const regexp = /<li>\s*(<dsfr-button[^>]*?>.*?<\/dsfr-button>)\s*<\/li>/gs;
    return content.replaceAll(regexp, '$1');
  }

  /* --- Tests ------------------------------------------------------------------------------------------------------ */
  this.testUpgradeContent = (content) => {
    return this.upgradeContent(content);
  };
  this.testDeletePrefixNgx = (content) => {
    return deletePrefixNgx(content);
  };
  this.testUpgradeTag = (content, rule) => {
    return upgradeTag(content, rule);
  };
  this.testUpgradeAttributeName = (content, rule) => {
    return upgradeAttributeName(content, rule);
  };
  this.testUpgradeAttributeValue = (content, rule) => {
    return upgradeAttributeValue(content, rule);
  };
  this.testUpgradeLowerCase = (content, rule) => {
    return upgradeLowerCase(content, rule);
  };
  this.testReplaceStatement = (content, rule) => {
    return replaceStatement(content, rule);
  };
  this.testSuppressAttribute = (content, rule) => {
    return suppressAttribute(content, rule);
  };
  this.testUpgradeButton = (content) => {
    return upgradeButton(content);
  };
}

module.exports = { HtmlUpgrader };
