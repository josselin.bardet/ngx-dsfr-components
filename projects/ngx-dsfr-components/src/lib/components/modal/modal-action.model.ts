import { DsfrButton } from '../button/button.model';

/** Action transformée en bouton dans le bas du dialogue modal. */
export interface DsfrModalAction
  extends Omit<DsfrButton, 'size' | 'iconPosition' | 'loader' | 'invertedOutlineContrast'> {
  /** Action appelée lors du 'click' sur le bouton. */
  callback: () => void; // eslint-disable-line @typescript-eslint/ban-types
}
