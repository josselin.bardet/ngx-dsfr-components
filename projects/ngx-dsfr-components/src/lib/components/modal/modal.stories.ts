import { argHeadingLevel, titleDecorator } from '.storybook/storybook-utils';
import { Meta, StoryFn, StoryObj, componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { DsfrSizeConst, HeadingModule } from '../../shared';
import { DsfrButtonModule } from '../button';
import { ModalWrapperComponent } from './modal-wrapper/modal-wrapper.component';
import { DsfrModalComponent } from './modal.component';

const meta: Meta = {
  title: 'COMPONENTS/Modal',
  component: DsfrModalComponent,
  decorators: [
    moduleMetadata({
      declarations: [ModalWrapperComponent],
      imports: [DsfrButtonModule, HeadingModule],
    }),
  ],
  parameters: {
    docs: {
      story: {
        inline: false,
        height: 300,
      },
    },
  },
  argTypes: {
    headingLevel: argHeadingLevel,
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) },
    disclose: { control: { type: 'EventEmitter' } },
    conceal: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrModalComponent>;

const title = 'Titre de la modale';
const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.`;
const loremIpsumLong = `Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.
<br/> <br/> Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.
<br/> <br/>Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.`;
const template = `
  <dsfr-modal 
    [dialogId]="dialogId"
    [titleModal]="titleModal"
    [actions]="actions"
    [size]="size"
    [headingLevel]="headingLevel"
    [autoCloseOnAction] = "autoCloseOnAction">
      ${loremIpsum}
  </dsfr-modal>`;

const templateLong = `
  <dsfr-modal 
    [dialogId]="dialogId"
    [titleModal]="titleModal"
    [actions]="actions"
    [size]="size"
    [headingLevel]="headingLevel"
    [autoCloseOnAction] = "autoCloseOnAction">
      ${loremIpsumLong}
  </dsfr-modal>`;

const openButton = function (buttonLabel = 'Modal', dialogId?: string) {
  return componentWrapperDecorator(
    (story) => `<dsfr-button label="${buttonLabel}" ariaControls="${dialogId}"></dsfr-button>${story}`,
  );
};

const DefaultValues: Story = {
  args: {
    actions: [],
    autoCloseOnAction: true,
    headingLevel: 'none',
    size: 'MD',
    titleModal: title,
  },
};

export const Default: Story = {
  decorators: [openButton('Modal simple', 'modalDefault'), titleDecorator('Modale simple')],

  args: {
    ...DefaultValues.args,
    dialogId: 'modalDefault',
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

export const Small: Story = {
  decorators: [openButton('Modal SM', 'modalSmall'), titleDecorator('Modale SM')],
  args: {
    ...DefaultValues.args,
    dialogId: 'modalSmall',
    size: 'SM',
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

export const Large: Story = {
  decorators: [openButton('Modal LG', 'modalLarge'), titleDecorator('Modale LG')],
  args: {
    ...DefaultValues.args,
    dialogId: 'modalLarge',
    size: 'LG',
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

export const Programmatic: StoryFn<DsfrModalComponent> = (args) => ({
  props: args,
  template: `
    <dsfr-modal #dialog5 titleModal="My Title" controlMode="dynamic">
        <div>La modale va être fermée programmatiquement dans trois secondes</div>
    </dsfr-modal>`,
});
Programmatic.decorators = [componentWrapperDecorator(ModalWrapperComponent)];
