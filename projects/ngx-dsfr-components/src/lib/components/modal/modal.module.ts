import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrButtonModule } from '../button';
import { DsfrModalComponent } from './modal.component';

@NgModule({
  declarations: [DsfrModalComponent],
  exports: [DsfrModalComponent],
  imports: [CommonModule, DsfrButtonModule],
})
export class DsfrModalModule {}
