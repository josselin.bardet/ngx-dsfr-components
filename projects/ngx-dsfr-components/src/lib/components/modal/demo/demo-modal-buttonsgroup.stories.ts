import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrModalModule } from '../modal.module';
import { DemoModalButtonsGroupComponent } from './demo-modal-buttonsgroup.component';

const meta: Meta = {
  title: 'COMPONENTS/Modal',
  component: DemoModalButtonsGroupComponent,
  decorators: [moduleMetadata({ imports: [DsfrModalModule, DsfrButtonModule, DsfrButtonsGroupModule] })],
};
export default meta;
type Story = StoryObj<DemoModalButtonsGroupComponent>;

export const OpenFromButtonsGroup: Story = {
  name: 'Open from button group',
  decorators: dsfrDecorator('Ouverture depuis un groupe de boutons'),
  args: {},
};
