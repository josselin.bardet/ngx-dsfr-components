import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../button';
import { DsfrModalModule } from '../modal.module';
import { DemoModalActionsComponent } from './demo-modal-actions.component';

const meta: Meta = {
  title: 'COMPONENTS/Modal',
  component: DemoModalActionsComponent,
  decorators: [moduleMetadata({ imports: [DsfrModalModule, DsfrButtonModule] })],
};
export default meta;
type Story = StoryObj<DemoModalActionsComponent>;

export const Actions: Story = {
  decorators: dsfrDecorator('Modale avec actions'),
  args: { toogleActionResetDisabled: true },
};
