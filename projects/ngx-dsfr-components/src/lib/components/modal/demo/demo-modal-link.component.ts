import { CommonModule } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { DsfrLinkModule } from '../../link';
import { DsfrModalAction } from '../modal-action.model';
import { DsfrModalComponent } from '../modal.component';
import { DsfrModalModule } from '../modal.module';

@Component({
  selector: 'demo-modal-link',
  templateUrl: './demo-modal-link.component.html',
  styles: ['dsfr-link {margin-right: 1rem}'],
  standalone: true,
  imports: [CommonModule, DsfrModalModule, DsfrLinkModule],
})
export class DemoModalLinkComponent {
  /** @internal */ @ViewChild('accountModal') accountModal!: DsfrModalComponent;
  /** @internal */ @ViewChild('logoutModal') logoutModal!: DsfrModalComponent;
  /** @internal */ actions: DsfrModalAction[] = [
    { label: 'Valider', callback: () => {} },
    { label: 'Annuler', callback: () => {}, variant: 'secondary' },
  ];

  /** @internal */
  onLinkSelect(route: string) {
    if (route === 'account') {
      // this.accountModal?.open(); // inutile avec DSFR 1.10
    } else if (route === 'logout') {
      // this.logoutModal?.open(); // inutile avec DSFR 1.10
    }
  }
}
