import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrModalAction } from '../modal-action.model';
import { DsfrModalModule } from '../modal.module';

@Component({
  selector: 'demo-modal-actions',
  template: `
    <dsfr-button label="Bouton 2 (open)" ariaControls="myModalId"></dsfr-button>

    <p>
      <code>toogleActionResetDisabled state: {{ toogleActionResetDisabled }}</code>
    </p>

    <dsfr-modal dialogId="myModalId" titleModal="Titre de la modale" [actions]="actions">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae sapien
        pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis
        volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.
      </p>
    </dsfr-modal>
  `,
  standalone: true,
  imports: [CommonModule, DsfrModalModule, DsfrButtonModule, DsfrButtonsGroupModule],
})
export class DemoModalActionsComponent {
  /**
   * @internal
   */
  public actions: DsfrModalAction[] = [
    { label: 'Label bouton', icon: 'fr-icon-checkbox-circle-line', callback: () => alert('Action') },
    {
      label: 'Reset',
      callback: () => alert('Action'),
      variant: 'secondary',
      tooltipMessage: 'My tooltip',
      type: 'reset',
      disabled: true,
    },
  ];

  private _toogleActionResetDisabled = true;

  public get toogleActionResetDisabled(): boolean {
    return this._toogleActionResetDisabled;
  }

  @Input()
  public set toogleActionResetDisabled(value: boolean) {
    this._toogleActionResetDisabled = value;
    this.actions[1].disabled = this._toogleActionResetDisabled;
  }
}
