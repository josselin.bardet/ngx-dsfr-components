import { Canvas, Controls, Meta } from '@storybook/blocks';
import * as Stories from './modal.stories';

<Meta of={Stories} title="Modal" />

# Modale (modal)

La modale permet de concentrer l’attention de l’utilisateur exclusivement sur une tâche ou un élément d’information,
sans perdre le contexte de la page en cours.

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/modale)

- _Module_ : `DsfModalModule`
- _Composant_ : `DsfrModalComponent`
- _Tag_ : `dsfr-modal`

## Fonctionnement

L'ouverture de la modale se fait à partir d'un lien ou d'un bouton en ajoutant des attributs `data-fr-opened` et
`aria-controls`.

🔥 En termes d'accessibilité, l'ouverture d'une modale à partir d'un lien n'est pas recommandée, il vaut mieux utiliser un bouton.

- Story : [Open From Link](/story/components-modal--open-from-link)

```html
<button type="button" class="fr-btn" data-fr-opened="false" aria-controls="fr-modal-1">Modal simple</button>
```

Ou à partir d'un button DSFR :

```html
<dsfr-button label="Modal simple" ariaControls="fr-modal-1"></dsfr-button>
```

Le contenu du dialog modal est transmis via le point d'injection (slot) **`content`**.

Mais aussi programmatiquement ([+ d'infos](#ouverture-programmatique)) :

```javascript
@ViewChild('myDialog') dialog: DsfrModalComponent;

openModal() {
  this.dialog.open();
}

closeModal() {
  this.dialog.close();
}
```

## Aperçu

<Canvas of={Stories.Default} />

## API

<Controls />

## Exemples

- Le contenu de la modale, hors titre, est affiché dans la balise `<ng-content />` par défaut.
- Le contenu peut être de tout type, formulaire, iframe, ...
- Après fermeture de la modale, l'événement `(conceal)`est émis.

### Taille

La dimension de la modale est définie par la propriété `size`, `MD` par défaut.

<Canvas of={Stories.Small} />
<Canvas of={Stories.Large} />

### Actions

- Des boutons peuvent être ajoutés dans la modale.
- La sélection d'une action ferme la modale, sauf si `autoCloseOnAction=false` (vrai par défaut).

### Ouverture avec un lien

🔥 Les règles d'accessibilité déconseillent l'ouverture depuis un lien, il est préférable d'utiliser un bouton.

Cf. Story : [Open from link](/story/components-modal--open-from-link)

Il faut indiquer quel élément ouvrir avec la propriété `ariaControls` (attribut `aria-controls`) du lien, par exemple :

```html
<dsfr-link
  icon="fr-fi-logout-box-r-line"
  iconPosition="left"
  size="SM"
  route="logout"
  ariaControls="logoutModal"
  (linkSelect)="onLinkSelect($event)">
  Se déconnecter
</dsfr-link>
```

👆 La propriété `route` est **obligatoire** pour que le lien soit actif (et pour prévenir la propagation de l'événement).

### Ouverture programmatique

Il est possible d'ouvrir une modale avec la méthode `open()`, exemple :

```javascript
@ViewChild(DsfrModalComponent) logoutModal!: DsfrModalComponent;

onLinkSelect(link: DsfrLink) {
  if (link.route === 'logout')
    this.logoutModal?.open();
  }
}
```

🔥 L'API JS DSFR a été modifiée en version 1.10.0 : la présence d'un élément de contrôle (bouton ou lien doté des
attributs `data-fr-opened` et `aria-controls`) est à présent obligatoire, et ce, même si vous pilotez programmatiquement
la modale. Sans élément de contrôle, l'appel à la méthode `open()` est devenu inopérant (cf. [issue Github](https://github.com/GouvernementFR/dsfr/issues/728)).

Si vous avez besoin de piloter exclusivement la modale via ses primitives open/close, c'est-à-dire sans aucun élément de
contrôle donc sans aucune intéraction utilisateur, vous devez activer un mode de contrôle spécial via la propriété
`controlMode=dynamic`. Dans ce mode, le composant va placer dans le DOM un bouton d'ouverture, masqué visuellement.
Ce bouton ne sert qu'à rendre opérationnelle la fonction `open()`.

🔥 Attention : si vous optez pour une manipulation exclusivement programmatique de la modale, vous devrez gérer
manuellement le positionnement du focus à la sortie de la modale en vous mettant à l'écoute de l'output `conceal`.

🔥 Attention : vous ne devez pas activer le mode `dynamic` si vous disposez d'un bouton de contrôle, ceci même si vous
souhaitez ouvrir ou fermer programmatiquement la modale.
