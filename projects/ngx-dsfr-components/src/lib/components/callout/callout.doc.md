La mise en avant permet à l’utilisateur de distinguer rapidement une information qui vient compléter le contenu
consulté.

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/mise-en-avant)

- _Module_ : `DsfrCallOutModule`
- _Composant_ : `DsfrCallOutComponent`
- _Tag_ : `dsfr-callout`

---

### ☝️ Points d'attention sur l'usage du wrapper Angular

- Nous avons prévu un point d'injection HTML (ng-content) pour gérer la description qui est <u>obligatoire</u>.
  Cela vous permet d'utiliser du formatage HTML au sein de votre description. Attention toutefois : il ne faut pas
  en abuser et faire n'importe quoi, l'idée étant juste de vous permettre de formatter superficiellement votre texte.
- Pour que votre description ait le style DSFR spécifié, il ne faut pas encapsuler votre texte dans un conteneur
  de type `<p>` ou `<div>`, si vous avez absolument besoin de faire de placer un conteneur il est à votre charge de
  repositionner sur celui-ci la classe CSS `fr-callout__text`.
