import { componentWrapperDecorator, Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrHeadingLevelConst } from '../../shared';
import { DsfrButtonModule } from '../button';
import { DsfrCalloutComponent } from './callout.component';
import descriptionMd from './callout.doc.md';
import { argHeadingLevel } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Callout',
  component: DsfrCalloutComponent,
  decorators: [moduleMetadata({ imports: [DsfrButtonModule] })],
  parameters: { docs: { description: { component: descriptionMd } } },
  argTypes: {
    headingLevel: argHeadingLevel,
  },
};
export default meta;

const Template: StoryFn<DsfrCalloutComponent> = (args) => ({
  props: args,
  template: `
    <dsfr-callout
      [heading] = "heading"
      [headingLevel] = "headingLevel"
      [icon] = "icon"
      [customClass] = "customClass"
    >
      Les parents d’enfants de 11 à 14 ans n’ont aucune démarche à accomplir : les CAF versent automatiquement
      l’ARS aux familles déjà allocataires qui remplissent les conditions.
    </dsfr-callout>`,
});

/** Default */
export const Default = Template.bind({});
Default.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Mise en avant avec texte seul</div>${story}`),
];

/** TitleAndText */
export const TitleAndText = Template.bind({});
TitleAndText.args = {
  heading: 'Titre mise en avant',
};
TitleAndText.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Mise en avant avec titre et texte</div>${story}`),
];

/** TitleButtonAndText */
export const TitleButtonAndText = Template.bind({});
TitleButtonAndText.args = {
  heading: 'Titre mise en avant',
};
TitleButtonAndText.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Mise en avant avec titre, bouton et texte</div>${story}`),
];

/** TitleButtonIconAndText */
export const TitleButtonIconAndText = Template.bind({});
TitleButtonIconAndText.args = {
  heading: 'Titre mise en avant',
  icon: 'ri-information-line',
};
TitleButtonAndText.decorators = [
  componentWrapperDecorator(
    (story) => `<div class="sb-title">Mise en avant avec titre, bouton, icône et texte</div>${story}`,
  ),
];

/** CustomTheme */
export const CustomTheme = Template.bind({});
CustomTheme.args = {
  heading: 'Titre mise en avant',
  customClass: 'fr-callout--green-emeraude',
};
CustomTheme.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Mise en avant accentuée</div>${story}`),
];

/** TitleNoSectionHeading */
export const TitleNoSectionHeading = Template.bind({});
TitleNoSectionHeading.args = {
  heading: 'Titre mise en avant',
  headingLevel: 'none',
};
TitleNoSectionHeading.decorators = [
  componentWrapperDecorator(
    (story) => `<div class="sb-title">Mise en avant avec titre sans titre de section</div>${story}`,
  ),
];

/** TitleH2 */
export const TitleH2 = Template.bind({});
TitleH2.args = {
  heading: 'Titre mise en avant',
  headingLevel: DsfrHeadingLevelConst.H2,
};
TitleH2.storyName = 'Title h2';
TitleH2.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Mise en avant avec titre de niveau h2</div>${story}`),
];
