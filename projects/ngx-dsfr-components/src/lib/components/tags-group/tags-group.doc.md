Les tags peuvent être utilisés à plusieurs dans des groupes de tags. Dans ce cas-là ils appliquent des espacements
préalablement définis par le DSFR.

Les tags `<dsfr-tag>` peuvent être placés dans un groupe `<dsfr-tags-group>`,
qui peut également recevoir un tableau de `DsfrTag` :

```typescript
/** Description d'un élément tag */
export interface DsfrTag {
  /** Libelle du tag. */
  label: string;
  /** Classe de l'icon  */
  icon?: string;
  /** Lien du tag à renseigner si le mode est 'clickable'. */
  href?: string;
  /** titre du tag */
  title?: string;
  /** Taille du tag (small ou médium). */
  small?: boolean;
  /*** Permet de personnaliser la couleur du tag. **/
  customClass?: string;
  /** Etat disabled du tag. */
  disabled?: boolean;
  /** Etat selected du tag. */
  selected?: boolean;
  /** Mode spécifique : default /selectable / clickable / deletable */
  mode?: DsfrTagMode;
}
```
