import { Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrTagModeConst, DsfrTagModule } from '../tag';
import { DsfrTagsGroupComponent } from './index';
import descriptionMd from './tags-group.doc.md';

const meta: Meta = {
  title: 'COMPONENTS/Tags group',
  component: DsfrTagsGroupComponent,
  parameters: { docs: { description: { component: descriptionMd } } },
  decorators: [
    moduleMetadata({
      imports: [DsfrTagModule],
    }),
  ],
  argTypes: {
    mode: { control: { type: 'inline-radio' }, options: Object.values(DsfrTagModeConst) },
    tagSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;

const Template: StoryFn<DsfrTagsGroupComponent> = (args) => ({
  props: args,
});

/** Default */
export const Default: StoryFn<DsfrTagsGroupComponent> = (args) => ({
  props: args,
  template: `
<dsfr-tags-group [mode]="mode" [tags]="tags">
  <dsfr-tag label= 'Tag non cliquable'></dsfr-tag>
  <dsfr-tag label= 'Tag sélectionnable' mode="selectable"></dsfr-tag>
  <dsfr-tag label= 'Tag sélectionné' mode="selectable" [selected]="true"></dsfr-tag>
  <dsfr-tag label= 'Tag cliquable with path' link="#"></dsfr-tag>
  <dsfr-tag label= 'Tag cliquable with href' route="#"></dsfr-tag>
  <dsfr-tag label= 'Tag personnalisé' link="#" customClass="fr-tag--green-emeraude"></dsfr-tag>
  <dsfr-tag label= 'Tag supprimable' mode="deletable"></dsfr-tag>
  <dsfr-tag label= 'Tag avec icône' icon="fr-icon-checkbox-circle-line"></dsfr-tag>
  <dsfr-tag label= 'Tag small' small="true"></dsfr-tag>
</dsfr-tags-group>
`,
});

/** GroupMode */
export const GroupMode: StoryFn<DsfrTagsGroupComponent> = (args) => ({
  props: args,
  template: `
<dsfr-tags-group [mode]="mode" [tags]="tags">
  <dsfr-tag icon='fr-icon-arrow-left-line' label='Tag avec icône'></dsfr-tag>
  <dsfr-tag [small]=true label='Tag small'></dsfr-tag>
  <dsfr-tag [selected]=true label='Tag supprimable'></dsfr-tag>
  <dsfr-tag [mode]="'deletable'" label='Tag supprimable' ></dsfr-tag>
  <dsfr-tag customClass='fr-tag--green-emeraude' label='Tag personnalisé' href='#'></dsfr-tag>
</dsfr-tags-group>
`,
});
GroupMode.args = {
  mode: DsfrTagModeConst.DELETABLE,
};

/** With Array */
export const WithArray = Template.bind({});
WithArray.args = {
  tags: [
    { label: 'Tag non cliquable' },
    { label: 'Tag sélectionnable', mode: 'selectable' },
    { label: 'Tag sélectionné', mode: 'selectable', selected: true },
    { label: 'Tag cliquable with path', link: '#' },
    { label: 'Tag cliquable with href', route: '#' },
    { label: 'Tag personnalisé', link: '#', customClass: 'fr-tag--green-emeraude' },
    { label: 'Tag supprimable', mode: 'deletable' },
    { label: 'Tag avec icône', icon: 'ri-checkbox-circle-line' },
    { label: 'Tag small', small: true },
  ],
};
