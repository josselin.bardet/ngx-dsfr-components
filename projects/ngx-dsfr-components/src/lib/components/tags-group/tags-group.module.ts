import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrTagModule } from '../tag';
import { DsfrTagsGroupComponent } from './tags-group.component';

@NgModule({
  declarations: [DsfrTagsGroupComponent],
  exports: [DsfrTagsGroupComponent],
  imports: [CommonModule, DsfrTagModule],
})
export class DsfrTagsGroupModule {}
