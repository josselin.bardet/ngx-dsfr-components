import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { DomUtils } from '../../shared';
import { DsfrTag, DsfrTagComponent, DsfrTagEvent, DsfrTagMode } from '../tag';

@Component({
  selector: 'dsfr-tags-group',
  templateUrl: './tags-group.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrTagsGroupComponent implements AfterViewInit, AfterContentInit {
  /** Modèle de présentation des tags. */
  @Input() tags: DsfrTag[] = [];

  /** Mode du groupe de tags (deletable / clickable / selectable). Surcharge le mode par tag. */
  @Input() mode: DsfrTagMode;

  /**
   * Événement émis suite au click sur un tag, le contenu de l'événement est soit le link, la route ou à défaut le label du tag.
   * @since 1.4.0, si l'id du tag est renseigné, l'événement émet un objet de type DsfrTagEvent.
   */
  @Output() tagSelect = new EventEmitter<string | DsfrTagEvent>();

  /** @internal */
  @ContentChildren(DsfrTagComponent) tagsComponent: QueryList<DsfrTagComponent>;

  constructor(private _elementRef: ElementRef) {}

  /** @internal */
  ngAfterViewInit() {
    DomUtils.surroundChildWithli(this._elementRef, 'dsfr-tag');
  }

  /** @internal */
  ngAfterContentInit(): void {
    if (this.mode && this.tagsComponent) {
      this.setGroupeMode();
    }
  }

  /**
   * Surcharge le mode de chaque tag avec le mode du groupe en cas d'utilisation de ng-content.
   * @internal
   */
  setGroupeMode() {
    this.tagsComponent.map((c) => (c.mode = this.mode));
  }

  /** @internal */
  onTagSelect(event: string | DsfrTagEvent) {
    this.tagSelect.emit(event);
  }
}
