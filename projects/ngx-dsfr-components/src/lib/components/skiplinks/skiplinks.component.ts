import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrAnchorLink } from '../../shared';

@Component({
  selector: 'dsfr-skiplinks',
  templateUrl: './skiplinks.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrSkipLinksComponent {
  /**
   * Tableau des liens.
   */
  @Input() links: DsfrAnchorLink[];
}
