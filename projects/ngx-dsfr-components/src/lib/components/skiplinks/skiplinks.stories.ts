import { RouterTestingModule } from '@angular/router/testing';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrAnchorLink } from '../../shared';
import { DsfrSkipLinksComponent } from './skiplinks.component';

const meta: Meta = {
  title: 'COMPONENTS/Skiplinks',
  component: DsfrSkipLinksComponent,
  decorators: [
    moduleMetadata({
      imports: [RouterTestingModule],
    }),
  ],
};
export default meta;

type Story = StoryObj<DsfrSkipLinksComponent>;

const links: DsfrAnchorLink[] = [
  { label: 'lien 1', link: '#' },
  { label: 'lien 2', link: '#' },
  { label: 'lien 3', fragment: '#test' },
];

/** Default */
export const Default: Story = {
  args: {
    links: links,
  },
};
