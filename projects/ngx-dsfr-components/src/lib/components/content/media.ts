export type DsfrMedia = 'image' | 'video';

export namespace DsfrMediaConst {
  export const IMAGE = 'image';
  export const VIDEO = 'video';
}
