import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { gridDecoratorLG, titleDecorator } from '../../../../../../.storybook/storybook-utils';
import { DsfrSizeConst } from '../../shared';
import { DsfrLinkModule } from '../link';
import { DsfrTranscriptionComponent } from '../transcription';
import { DsfrContentComponent } from './content.component';
import { DsfrMediaConst } from './media';
import { MARIANNE_SVG } from '.storybook/assets/marianne.svg';

const meta: Meta = {
  title: 'COMPONENTS/Content',
  component: DsfrContentComponent,
  decorators: [moduleMetadata({ imports: [DsfrTranscriptionComponent, DsfrLinkModule] })],
  argTypes: {
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) },
    ratio: { control: { type: 'inline-radio' }, options: ['16:9/2', '16:9', '3:2', '4:3', '1:1', '3:4', '2:3'] },
    type: { control: { type: 'inline-radio' }, options: Object.values(DsfrMediaConst) },
    linkSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrContentComponent>;

const legend = 'Description / Source';
const imagePath = 'img/placeholder.16x9.png';
const linkLabel = 'Label lien';
const loremIpsum =
  'Lorem ipsum dolor sit amet, consectetur adipiscing, <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">link test</a> incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.';
const video = 'https://www.youtube.com/embed/HyirpmPL43I';

const DefaultValues: Story = {
  args: {
    type: 'image',
    ratio: '16:9',
    size: 'MD',
  },
};

export const Default: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image édito')],
  args: {
    ...DefaultValues.args,
    source: imagePath,
    legend: legend,
    link: '?path=/story/components-content--default',
    linkLabel: linkLabel,
  },
};

export const Small: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image édito, petite taille')],
  args: {
    ...DefaultValues.args,
    size: 'SM',
    source: imagePath,
    legend: legend,
    route: 'maRoute',
    linkLabel: "route='maRoute'",
  },
};

export const Large: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image édito, grande taille')],
  args: {
    ...DefaultValues.args,
    size: 'LG',
    source: imagePath,
    legend: legend,
    routerLink: '/path',
    linkLabel: "routerLink='/path'",
  },
};

export const Ratio: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image edito ratio 32x9')],
  args: {
    ...DefaultValues.args,
    ratio: '16:9/2',
    source: imagePath,
    legend: legend,
    route: 'maRoute',
    linkLabel: linkLabel,
  },
};

export const SVG: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image en svg, porteur d’information')],
  args: {
    ...DefaultValues.args,
    legend: legend,
    route: 'maRoute',
    linkLabel: linkLabel,
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-content [legend]="legend" [link]="transcriptionLink" [linkLabel]="transcriptionLinkLabel">
  <ng-container svg>${MARIANNE_SVG}</ng-container>
</dsfr-content>`,
  }),
};

export const Transcription: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image avec une transcription (projetée)')],
  args: {
    ...DefaultValues.args,
    source: imagePath,
    legend: legend,
    route: 'maRoute',
    linkLabel: linkLabel,
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-content
      [type]="type"
      [legend]="legend"
      [ratio]="ratio"
      [source]="source"
      [linkLabel]="linkLabel"
      [link]="link"
      [linkTarget]="linkTarget"
      [route]="route"
      [routerLink]="routerLink"
      [transcriptionContent]="transcriptionContent"
      [transcriptionHeading]="transcriptionHeading"
      [alternate]="alternate"
      [tooltipMessage]="tooltipMessage">
  <dsfr-transcription transcription heading="Titre de l'image">
    <div>
      <p>${loremIpsum}</p>
      <ul>
        <li>list item</li>
        <li>list item</li>
        <li>list item</li>
      </ul>
    </div>
  </dsfr-transcription>
</dsfr-content>`,
  }),
};

export const TranscriptionText: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média image avec une transcription (définie par des propriétés)')],
  args: {
    ...DefaultValues.args,
    source: imagePath,
    legend: legend,
    route: 'maRoute',
    linkLabel: linkLabel,
    transcriptionContent: loremIpsum,
    transcriptionHeading: "Titre de l'image",
  },
};

export const Video: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média video édito, ratio d‘aspect 16/9 par défaut')],
  args: {
    ...DefaultValues.args,
    type: 'video',
    route: 'maRoute',
    linkLabel: linkLabel,
    source: video,
    legend: legend,
  },
};

export const VideoRatio: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Média video édito, ratio d‘aspect 4x3')],
  args: {
    ...DefaultValues.args,
    type: 'video',
    ratio: '4:3',
    route: 'maRoute',
    linkLabel: linkLabel,
    source: video,
    legend: legend,
  },
};
