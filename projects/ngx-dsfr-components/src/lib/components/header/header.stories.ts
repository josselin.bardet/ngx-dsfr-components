import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrLink } from '../../shared';
import { DISPLAY_MODAL_ID, DsfrDisplayModule } from '../display';
import { DsfrLinkModule } from '../link';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrSearchBarModule } from '../search-bar';
import { DsfrTranslateModule } from '../translate';
import { DsfrHeaderComponent } from './header.component';
import { DsfrHeaderMenuItem } from './header.model';
import { DsfrMegaMenuComponent } from './mega-menu/mega-menu.component';

const meta: Meta = {
  title: 'COMPONENTS/Header',
  component: DsfrHeaderComponent,
  decorators: [
    moduleMetadata({
      imports: [
        FormsModule,
        DsfrMegaMenuComponent,
        DsfrTranslateModule,
        DsfrSearchBarModule,
        RouterTestingModule,
        RouterTestingModule,
        ItemLinkComponent,
        DsfrDisplayModule,
        DsfrLinkModule,
      ],
    }),
  ],
  argTypes: {
    searchChange: { control: { type: 'EventEmitter' } },
    searchSelect: { control: { type: 'EventEmitter' } },
    linkSelect: { control: { type: 'EventEmitter' } },
    langChange: { control: { type: 'EventEmitter' } },
  },
};
export default meta;

type Story = StoryObj<DsfrHeaderComponent>;

const toolsLinks: DsfrLink[] = [
  { label: 'Créer un espace', icon: 'fr-icon-add-circle-line' },
  { label: 'Se connecter', route: '#', icon: 'fr-icon-lock-line' },
  {
    label: "S'enregistrer",
    icon: 'fr-icon-account-line',
    routerLink: '/',
    routerLinkActive: 'class-active',
  },
  { label: 'Un lien de trop', route: '#' },
];

const toolsLinksDisplay: DsfrLink[] = [
  { label: 'Créer un espace', icon: 'fr-icon-add-circle-line' },
  {
    mode: 'button',
    label: 'Thème',
    customClass: 'fr-icon-theme-fill',
    ariaControls: DISPLAY_MODAL_ID,
  },
  { label: 'Se connecter', route: '#', icon: 'fr-icon-lock-line' },
];

const menuDeroulantHeader: DsfrHeaderMenuItem[] = [
  { label: 'Accès rapide' },
  {
    label: 'Menu déroulant',
    subItems: [{ label: 'Accès routerLink', routerLink: '/' }, { label: 'Accès direct 3' }],
  },
  { label: 'Accès rapide actif', link: '#', active: true },
  { label: 'Menu déroulant 2', link: '#', subItems: [{ label: 'Accès routerLink' }, { label: 'Accès direct 3' }] },
  {
    label: 'Menu déroulant 3',
    link: '#',
    subItems: [{ label: 'Accès routerLink', target: '_self' }, { label: 'Accès direct 3' }],
  },
];

const megaMenuHeader: DsfrHeaderMenuItem[] = [
  { label: 'Accès direct', link: '#' },
  {
    label: 'Méga menu (actif)',
    active: true,
    megaMenu: {
      leader: {
        title: 'Titre éditorialisé',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.',
        link: { label: 'Voir toute la rubrique', route: '#' },
      },
      categories: [
        {
          label: 'Catégorie 1',
          link: '#',
          subItems: [
            { label: 'Lien 1', route: '#' },
            { label: 'Lien 3', route: '#' },
          ],
        },
        {
          label: 'Catégorie 2',
          subItems: [
            {
              label: 'Lien 1',
              route: '#',
              routerLinkActive: 'class-active',
              routerLinkActiveOptions: { exact: false },
            },
            {
              label: 'Lien actif',
              route: '#',
              active: true,
            },
            { label: 'Lien désactivé' },
          ],
        },
      ],
    },
  },
  {
    label: 'Méga menu 2',
    megaMenu: {
      categories: [
        {
          label: 'Catégorie 1',
          subItems: [{ label: 'Lien 1' }, { label: 'Lien 3' }],
        },
        {
          label: 'Catégorie 2',
          subItems: [
            { label: 'Lien 1' },
            { label: 'Lien 2', routerLink: '/' },
            { label: 'Lien 3' },
            { label: 'Lien 4' },
          ],
        },
        {
          label: 'Catégorie 3',
          subItems: [{ label: 'Lien 1' }, { label: 'Lien 2', routerLink: '/' }, { label: 'Lien 3' }],
        },
      ],
    },
  },
  { label: 'Accès direct 3', link: '#' },
];

const DEFAULT_SERVICE_TITLE = 'Nom du site / service';
const DEFAULT_SEVICE_TAGLINE = "tagline - précisions sur l'organisation";
const DEFAULT_LOGO_TOOLTIP = 'Informations complémentaires sur le site ou service';

/** Default */
export const Default: Story = {
  args: {},
};

/** With Services */
export const WithService: Story = {
  args: {
    serviceTitle: DEFAULT_SERVICE_TITLE,
    serviceTagline: DEFAULT_SEVICE_TAGLINE,
  },
};

/** Quick access */
export const QuickAccess: Story = {
  args: {
    serviceTitle: DEFAULT_SERVICE_TITLE,
    serviceTagline: DEFAULT_SEVICE_TAGLINE,
    headerToolsLinks: toolsLinks,
  },
};

/** With Search */
export const WithSearch: Story = {
  args: {
    serviceTitle: DEFAULT_SERVICE_TITLE,
    serviceTagline: DEFAULT_SEVICE_TAGLINE,
    headerToolsLinks: toolsLinks,
    searchBar: true,
  },
};

export const WithTranslate: Story = {
  args: {
    serviceTitle: DEFAULT_SERVICE_TITLE,
    serviceTagline: DEFAULT_SEVICE_TAGLINE,
    headerToolsLinks: toolsLinks,
    translate: {
      languages: [
        { label: 'Français', value: 'fr' },
        { label: 'English', value: 'en' },
      ],
      // currentLangCode: 'fr',
    },
  },
};

/** With Logo */
export const WithLogo: Story = {
  args: {
    operatorImagePath: 'img/placeholder.16x9.png',
  },
};

/** Vertical Logo */
export const VerticalLogo: Story = {
  args: {
    operatorImagePath: 'img/placeholder.9x16.png',
    operatorImageVertical: true,
  },
};

/** Badge Beta */
export const BadgeBeta: Story = {
  args: {
    serviceTitle: DEFAULT_SERVICE_TITLE,
    serviceTagline: DEFAULT_SEVICE_TAGLINE,
    headerToolsLinks: toolsLinks,
    searchBar: true,
    beta: true,
  },
};

/** Full */
export const Full: Story = {
  args: {
    serviceTitle: DEFAULT_SERVICE_TITLE,
    logoTooltipMessage: DEFAULT_LOGO_TOOLTIP,
    serviceTagline: DEFAULT_SEVICE_TAGLINE,
    operatorImagePath: 'img/placeholder.16x9.png',
    headerToolsLinks: toolsLinks,
    searchBar: true,
    menu: [
      { label: 'Accès direct', route: '#', active: true },
      { label: 'Accès direct 2', route: '#', target: '_blank' },
      { label: 'Accès direct 3', route: '#' },
      {
        label: 'Accès routerLink',
        routerLink: '/',
        routerLinkActive: 'class-active',
      },
    ],
  },
};

/** Déroulant */
export const DropdownMenu: Story = {
  args: {
    serviceTitle: DEFAULT_SERVICE_TITLE,
    logoTooltipMessage: DEFAULT_LOGO_TOOLTIP,
    serviceTagline: DEFAULT_SEVICE_TAGLINE,
    operatorImagePath: 'img/placeholder.16x9.png',
    searchBar: false,
    menu: menuDeroulantHeader,
  },
};

/** Méga menu */
export const MegaMenu: Story = {
  args: {
    serviceTitle: DEFAULT_SERVICE_TITLE,
    logoTooltipMessage: DEFAULT_LOGO_TOOLTIP,
    serviceTagline: DEFAULT_SEVICE_TAGLINE,
    operatorImagePath: 'img/placeholder.16x9.png',
    headerToolsLinks: toolsLinks,
    searchBar: true,
    menu: megaMenuHeader,
  },
};

/** Display */
export const Display: Story = {
  args: {
    display: true,
    pictoPath: 'artwork/pictograms/environment/',
  },
  decorators: [
    componentWrapperDecorator((story) => `<div class="sb-title">Lien d'accès aux paramètres d'affichage</div>${story}`),
  ],
  render: (args) => ({
    props: args,
    template: `<dsfr-header [display]="display" [pictoPath]="pictoPath"></dsfr-header>`,
  }),
};

/** Display ArtworkDirPath */
export const DisplayArtworkDirPath: Story = {
  name: 'Display (artworkDirPath)',
  args: {
    display: true,
    artworkDirPath: 'artwork',
  },
  decorators: [
    componentWrapperDecorator(
      (story) => `<div class="sb-title">Lien d'accès aux paramètres d'affichage (custom artworkDirPath)</div>${story}`,
    ),
  ],
};

/**Custom Display */
export const CustomDisplay: Story = {
  args: {
    headerToolsLinks: toolsLinksDisplay,
    display: false,
  },
  decorators: [
    componentWrapperDecorator(
      (story) => `<div class="sb-title">Custom Display</div>${story} <dsfr-display></dsfr-display>`,
    ),
  ],
};
