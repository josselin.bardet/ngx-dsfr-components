import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrModalModule } from '../../modal';
import { DemoHeaderFocusComponent } from './demo-header-focus.component';

const meta: Meta = {
  title: 'COMPONENTS/Header',
  component: DemoHeaderFocusComponent,
  parameters: {
    docs: {
      inline: false,
    },
  },
  decorators: [moduleMetadata({ imports: [DsfrModalModule] })],
};
export default meta;
type Story = StoryObj<DemoHeaderFocusComponent>;

export const Accessibility: Story = {
  decorators: dsfrDecorator('Accessibilité', 'Tabuler pour accéder aux liens ouvrant chacun une modale.'),
  args: {
    serviceTitle: 'Nom du service',
    serviceTagline: "Description de l'organisation",
    headerToolsLinks: [
      {
        label: 'Un bouton',
        icon: 'fr-icon-account-line',
        ariaControls: 'accountModal',
        mode: 'button',
      },
      {
        label: 'Un lien',
        icon: 'fr-icon-logout-box-r-line',
        route: 'logout',
        ariaControls: 'logoutModal',
        ariaLabel: 'Se déconnecter de Scolarité services',
      },
    ],
  },
};
