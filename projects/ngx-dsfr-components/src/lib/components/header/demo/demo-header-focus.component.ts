import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { DsfrLink } from '../../../shared';
import { DsfrModalAction, DsfrModalModule } from '../../modal';
import { DsfrHeaderMenuItem } from '../header.model';
import { DsfrHeaderModule } from '../header.module';

@Component({
  selector: 'demo-header-focus',
  templateUrl: './demo-header-focus.component.html',
  standalone: true,
  imports: [CommonModule, DsfrHeaderModule, DsfrModalModule],
})
export class DemoHeaderFocusComponent implements OnInit {
  @Input() headerToolsLinks: DsfrLink[];
  @Input() serviceTitle: string;
  @Input() serviceTagline: string;

  /** @internal */ modalActions: DsfrModalAction[] = [
    { label: 'Valider', callback: () => {} },
    { label: 'Annuler', callback: () => {}, variant: 'secondary' },
  ];
  menuDeroulantHeader: DsfrHeaderMenuItem[] = [
    { label: 'Accès rapide' },
    {
      label: 'Menu déroulant',
      subItems: [{ label: 'Accès routerLink', routerLink: '/' }, { label: 'Accès direct 3' }],
    },
    { label: 'Accès rapide actif', link: '#', active: true },
    { label: 'Menu déroulant 2', link: '#', subItems: [{ label: 'Accès routerLink' }, { label: 'Accès direct 3' }] },
    {
      label: 'Menu déroulant 3',
      link: '#',
      subItems: [{ label: 'Accès routerLink', target: '_self' }, { label: 'Accès direct 3' }],
    },
  ];

  /** @internal */
  onLinkSelect(link: DsfrLink) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.headerToolsLinks = [
        ...this.headerToolsLinks,
        {
          label: 'Un 2ème bouton',
          icon: 'fr-icon-account-line',
          ariaControls: 'accountModal',
          mode: 'button',
        },
      ];
    }, 1000);
    setTimeout(() => {
      this.headerToolsLinks = [
        ...this.headerToolsLinks,
        {
          label: 'Un 2ème bouton',
          icon: 'fr-icon-account-line',
          ariaControls: 'accountModal',
          mode: 'button',
        },
      ];
    }, 2000);

    setTimeout(() => {
      this.headerToolsLinks = [
        {
          label: 'Un 2ème bouton',
          icon: 'fr-icon-account-line',
          ariaControls: 'accountModal',
          mode: 'button',
        },
      ];
    }, 2000);
  }
}
