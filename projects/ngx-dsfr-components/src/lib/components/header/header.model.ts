import { DsfrLink } from '../../shared';
import { DsfrLang } from '../translate';
import { DsfrMegaMenu } from './mega-menu/mega-menu.model';

// -- MenuItem ---------------------------------------------------------------------------------------------------------

/**
 * Entrée du menu dans le header. Peut contenir un sous-menu (déroulant), un méga-menu ou être un lien direct.
 */
export interface DsfrHeaderMenuItem extends DsfrLink {
  /* Est étendu (dans le cas ou il contient un menu déroulant ou méga-menu) */
  expanded?: boolean;
  /** Méga-menu (optionnel) */
  megaMenu?: DsfrMegaMenu;
  /** Sous-menu affiché en tant que ménu déroulant (optionnel) */
  subItems?: DsfrHeaderMenuItem[];
}

// -- Translate ---------------------------------------------------------------------------------------------------------

/**
 * Permet de configurer le sélecteur de langues à afficher dans le header.
 */
export interface DsfrHeaderTranslate {
  /**
   * Liste des langues proposées à l'utilisateur.
   * @see DsfrLang
   *
   */
  languages: DsfrLang[];

  /**
   * Code de la langue courante.
   * @see DsfrTranslateComponent
   * @deprecated @since 1.7 Il est conseillé de plus imposer une langue à ce niveau, la langue du site défini par l'utilisateur est rerouver soit dans le localStorage soit à défaut dans la balise html du site.
   */
  currentLangCode?: string;
}
