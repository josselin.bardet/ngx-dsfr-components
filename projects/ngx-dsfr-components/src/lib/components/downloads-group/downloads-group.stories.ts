import { Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrDownload, DsfrDownloadModule } from '../download';
import descriptionMd from '../download/download.doc.md';
import { DsfrDownloadsGroupComponent } from './downloads-group.component';
import { argHeadingLevel } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Downloads group',
  component: DsfrDownloadsGroupComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrDownloadModule],
    }),
  ],
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
  argTypes: {
    headingLevel: argHeadingLevel,
  },
};
export default meta;

const Template: StoryFn<DsfrDownloadsGroupComponent> = (args) => ({
  props: args,
});

const listDownload: DsfrDownload[] = [
  {
    fileName: '1er fichier',
    link: '/img/champollion.jpg',
    mimeType: 'image/jpeg',
    sizeBytes: 1000000,
  },
  {
    fileName: '2e fichier',
    link: '/files/lorem-ipsum.pdf',
    mimeType: 'application/pdf',
    sizeBytes: 100000,
    langCode: 'en',
  },
];

/** Default */
export const Default = Template.bind({});
Default.args = {
  heading: 'Titre facultatif',
  downloads: listDownload,
};
