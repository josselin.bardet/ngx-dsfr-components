import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrDownloadModule } from '../download';
import { DsfrDownloadsGroupComponent } from './downloads-group.component';

@NgModule({
  declarations: [DsfrDownloadsGroupComponent],
  exports: [DsfrDownloadsGroupComponent],
  imports: [CommonModule, DsfrDownloadModule],
})
export class DsfrDownloadsGroupModule {}
