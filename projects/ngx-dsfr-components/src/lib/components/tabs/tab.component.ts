import { Component, HostBinding, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { newUniqueId } from '../../shared';
import { BUTTON_TAB_ID_PREFIX, EduTabHeader } from './tabs.model';

export const DSFR_TAB_SELECTORS = 'dsfr-tab-panel, dsfr-tab';

@Component({
  selector: DSFR_TAB_SELECTORS,
  templateUrl: './tab.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrTabComponent implements OnInit, Required<EduTabHeader> {
  /** Identifiant de l'onglet, obligatoire, généré si non fourni. */
  @Input() tabId: string;

  /** Titre de l'onglet, obligatoire. */
  @Input() label: string;

  /** Nom de l'icône, l'icône est à gauche du libellé, optionnel. */
  @Input() icon: string;

  /** @internal*/
  buttonId: string;

  @HostBinding('id') get id() {
    return this.tabId;
  }

  @HostBinding('attr.role') get attrRole() {
    return 'tabpanel';
  }

  @HostBinding('attr.aria-labelledby') get attrLabelledBy() {
    return this.buttonId;
  }

  @HostBinding('attr.tabindex') get attrTabindex() {
    return '0';
  }

  @HostBinding('attr.data-tabid') get attrTabId() {
    return this.tabId;
  }

  @HostBinding('attr.data-label') get attrLabel() {
    return this.label;
  }

  @HostBinding('attr.data-iconClass') get attrIconClass() {
    return this.icon || null;
  }

  @HostBinding('class') get class() {
    return 'fr-tabs__panel';
  }

  /** @internal */
  ngOnInit() {
    this.tabId ??= newUniqueId();
    this.buttonId = BUTTON_TAB_ID_PREFIX + this.tabId;
  }
}
