import {
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  Optional,
  Output,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { newUniqueId } from '../../shared';
import { DsfrTabComponent } from './tab.component';
import { BUTTON_TAB_ID_PREFIX, DsfrTabRoute, EduTabHeader } from './tabs.model';

@Component({
  selector: 'dsfr-tabs',
  templateUrl: './tabs.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrTabsComponent {
  /**
   * Attribut aria-label de la liste d'onglets.
   */
  @Input() tabsAriaLabel = "Système d'onglets";

  /**
   * Onglet sélectionné (commence à 0).
   * Ne peut pas être utilisé quand on utilise l'input `routes`.
   */
  @Input() selectedTabIndex = 0;

  /**
   * Permet de positionner la largeur du viewport à 100% en mobile.
   */
  @Input() fullViewport = false;

  /**
   * Définition des routes si l'on souhaite utiliser `<router-outlet>`.
   *
   * @since 1.9.0
   */
  @Input() routes!: DsfrTabRoute[];

  /**
   * Émis lors de la sélection d'un onglet. La valeur émise correspond au `tabId` de l'onglet sélectionné.
   */
  @Output() tabSelect = new EventEmitter<string>();

  /** @internal */
  @ContentChildren(DsfrTabComponent) tabComponents!: QueryList<DsfrTabComponent>;

  /** @internal */
  routerTabId: string;

  constructor(
    @Optional() private router: Router,
    @Optional() private activatedRoute: ActivatedRoute,
    private elRef: ElementRef,
  ) {
    this.routerTabId = newUniqueId();
  }

  get headers(): EduTabHeader[] {
    return this.routes || this.tabComponents?.toArray();
  }

  private get tabsCount(): number {
    return this.routes ? this.routes.length : this.tabComponents ? this.tabComponents.length : 0;
  }

  /** @internal */
  getHeaderValue(header: EduTabHeader, key: string): any {
    return header[<never>key];
  }

  asRoute(header: EduTabHeader): DsfrTabRoute {
    return <DsfrTabRoute>header;
  }

  /** @internal */
  getIconClasses(tabHeader: EduTabHeader): string {
    return !tabHeader.icon ? '' : `${tabHeader.icon} fr-tabs__tab--icon-left`;
  }

  /**
   * [mode routes] Retourne l'identifiant unique du bouton de contrôle de l'onglet passé en paramètre.
   *
   * @internal
   */
  getButtonTabId(header: EduTabHeader) {
    return BUTTON_TAB_ID_PREFIX + header.tabId;
  }

  /**
   * [mode routes] Retourne l'identifiant unique du bouton de contrôle de l'onglet courant.
   *
   * @internal
   */
  getCurrentButtonTabId() {
    return this.selectedTabIndex >= 0 ? BUTTON_TAB_ID_PREFIX + this.routes[this.selectedTabIndex].tabId : null;
  }

  /** @internal */
  onSelect(event: MouseEvent, index: number) {
    event.preventDefault();
    event.stopPropagation();
    this.selectedTabIndex = index;
    this.tabSelect.emit(this.headers[this.selectedTabIndex].tabId);
  }

  /** @internal */
  onRouterLinkActive(isActive: boolean, tabIndex: number) {
    if (isActive) {
      this.selectedTabIndex = tabIndex;
    }
  }

  /**
   * Gestion de la navigation au clavier en mode routes.
   *
   *  @internal
   */
  onButtonTabKeydown(event: KeyboardEvent, index: number): void {
    // just in case
    if (this.selectedTabIndex !== index) return;

    let newIndex = -1;

    switch (event.code) {
      case 'ArrowLeft':
        newIndex = this.isFirstTab(index) ? this.tabsCount - 1 : index - 1;
        break;

      case 'ArrowRight':
        newIndex = this.isLastTab(index) ? 0 : index + 1;
        break;

      case 'Home':
        newIndex = 0;
        break;

      case 'End':
        newIndex = this.tabsCount - 1;
        break;

      default:
        break;
    }

    if (newIndex > -1) {
      this.performKeyAction(newIndex);
    }
  }

  private isFirstTab(index: number): boolean {
    return index === 0;
  }

  private isLastTab(index: number): boolean {
    return index === this.tabsCount - 1;
  }

  private performKeyAction(newIndex: number) {
    const route = this.routes[newIndex];
    const extras: NavigationExtras = route.routerLinkExtras || { relativeTo: this.activatedRoute };
    this.router.navigate([route.path], extras);
    this.elRef.nativeElement.querySelector('#' + this.getButtonTabId(route))?.focus();
  }
}
