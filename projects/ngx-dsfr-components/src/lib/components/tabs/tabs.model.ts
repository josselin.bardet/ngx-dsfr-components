import { NavigationExtras } from '@angular/router';

export const BUTTON_TAB_ID_PREFIX = 'buttontab-';

/**
 * Description d'un onglet du composant.
 */
export interface EduTabHeader {
  /**
   * Label de l'onglet, obligatoire
   */
  label: string;

  /**
   * Lien composant vers onglet. Permet de sélectionner un onglet lorsqu'on sélectionne le path correspondant.
   */
  tabId: string;

  /**
   * Icône optionnelle de l'onglet.
   */
  icon?: string;
}

/**
 * Description d'un onglet du composant.
 */
export interface DsfrTabRoute extends EduTabHeader {
  /**
   * Lien onglet vers composant. Path invoqué lorsque l'on clique sur un onglet.
   */
  path: string;

  /** Options additionnelles de navigation pour le routerLink (queryParams, state etc.). */
  routerLinkExtras?: NavigationExtras;
}
