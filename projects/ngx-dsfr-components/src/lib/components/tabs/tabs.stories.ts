import { dsfrDecorator } from '.storybook/storybook-utils';
import { RouterOutlet } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrAccordionModule } from '../accordion';
import { DsfrTabComponent } from './tab.component';
import { DsfrTabsComponent } from './tabs.component';

const meta: Meta = {
  title: 'COMPONENTS/Tabs',
  component: DsfrTabsComponent,
  decorators: [
    moduleMetadata({
      declarations: [DsfrTabComponent],
      imports: [DsfrAccordionModule, RouterTestingModule, RouterOutlet],
    }),
  ],
  argTypes: {
    tabSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrTabsComponent>;

const icon = 'fr-icon-checkbox-circle-line';
const image = 'img/placeholder.16x9.png';
const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing, <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">link test</a> incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.`;

function tab(tabPanelId: string, label: string, icon: string, title: string, image: string = '') {
  return `<dsfr-tab tabId="${tabPanelId}" label="${label}" icon="${icon}">
  <h4 class="fr-h4">${title}</h4>
  <img *ngIf="image" class="fr-responsive-img" [src]="image" data-fr-js-ratio="true" alt="" />
  <p>${loremIpsum}</p>
  <ul>
    <li>list item</li>
    <li>list item</li>
  </ul>
</dsfr-tab>`;
}

const tab1 = tab('tab1', 'Label Tab 1', icon, 'Contenu 1', image);
const tab2 = tab('tab2', 'Label Tab 2', icon, 'Contenu 2', image);
const tab3 = tab('tab3', 'Label Tab 3', icon, 'Contenu 3', image);
const tab4 = tab('tab4', 'Label Tab 4', icon, 'Contenu 4', image);
const tab1_nested = tab('tab1', 'Label Tab 1', '', 'Contenu 1');
const tab2_nested = tab('tab2', 'Label Tab 2', '', 'Contenu 2');
const tab3_nested = tab('tab3', 'Label Tab 3', '', 'Contenu 3');
const tab4_nested = tab('tab4', 'Label Tab 4', '', 'Contenu 4');

const default_template = `<dsfr-tabs [selectedTabIndex]="selectedTabIndex" [tabsAriaLabel]="tabsAriaLabel" [fullViewport]="fullViewport">
  ${tab1}
  ${tab2}
  ${tab3}
  ${tab4}
</dsfr-tabs>
`;

const embedded_template = `
<dsfr-tabs [selectedTabIndex]="selectedTabIndex" [tabsAriaLabel]="tabsAriaLabel" [fullViewport]="fullViewport">
  <dsfr-tab label="Main Tab 1">
    <dsfr-tabs>
      ${tab1_nested}
      ${tab2_nested}
    </dsfr-tabs>
  </dsfr-tab>
  <dsfr-tab label="Main Tab 2">
    <dsfr-tabs>
      ${tab3_nested}
      ${tab4_nested}
    </dsfr-tabs>
  </dsfr-tab>
</dsfr-tabs>
`;

const accordion_template = `
<dsfr-tabs>
  <dsfr-tab label="Label Tab 1">
    <dsfr-accordion heading="Intitulé accordéon 1">
      <p>${loremIpsum}</p>
    </dsfr-accordion>
  </dsfr-tab>
  <dsfr-tab label="Label Tab 2">
    <dsfr-accordion heading="Intitulé accordéon 2">
      <p>${loremIpsum}</p>
    </dsfr-accordion>
  </dsfr-tab>
</dsfr-tabs>
`;

export const Default: Story = {
  decorators: dsfrDecorator('Onglets 4 éléments'),
  args: {
    selectedTabIndex: 0,
    fullViewport: false,
    tabsAriaLabel: "Système d'onglets",
  },
  render: (args) => ({
    props: args,
    template: default_template,
  }),
};

export const Embedded: Story = {
  decorators: dsfrDecorator('Onglets dans onglets'),
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: embedded_template,
  }),
};

export const Accordion: Story = {
  decorators: dsfrDecorator('Accordéon dans onglets'),
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: accordion_template,
  }),
};

export const Mobile: Story = {
  decorators: dsfrDecorator('Onglets 4 éléments'),
  args: {
    ...Default.args,
    fullViewport: true,
    selectedTabIndex: 1,
  },
  render: (args) => ({
    props: args,
    template: default_template,
  }),
};
