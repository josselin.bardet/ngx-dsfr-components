import { titleDecorator } from '.storybook/storybook-utils';
import { EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { jsonPath2Value, LocalStorage, MESSAGES_EN, MESSAGES_FR } from '../../shared';
import { DISPLAY_MODAL_ID, DsfrDisplayComponent } from './display.component';

const meta: Meta = {
  title: 'COMPONENTS/Display',
  component: DsfrDisplayComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  parameters: {
    docs: {
      story: {
        inline: false,
        height: 600,
      },
    },
  },
  argTypes: {
    displayChange: { control: EventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrDisplayComponent>;

const codeLang = LocalStorage.get('lang');
const label = jsonPath2Value(codeLang === 'en' ? MESSAGES_EN : MESSAGES_FR, 'display.heading');

const buttonDecorator = componentWrapperDecorator(
  (story) => `
    <button class="fr-link fr-fi-theme-fill fr-link--icon-left" 
            aria-controls="${DISPLAY_MODAL_ID}" 
            data-fr-opened="false">
      ${label}
    </button>
    ${story}
  `,
);

/** Default */
export const Default: Story = {
  decorators: [buttonDecorator, titleDecorator("Lien d'accès au paramètrage")],
  args: {},
  render: (args: DsfrDisplayComponent) => ({
    props: args,
    template: `<dsfr-display></dsfr-display>`,
  }),
};

/** ArtworkDirPath */
export const ArtworkDirPath: Story = {
  name: 'Custom artworkDirPath',
  decorators: [buttonDecorator, titleDecorator('Surcharge de la propriété <code>artworkDirPath</code>')],
  args: {
    ...Default.args,
    pictoPath: 'artwork',
  },
  render: (args: DsfrDisplayComponent) => ({
    props: args,
    template: `<dsfr-display [artworkDirPath]="artworkDirPath"></dsfr-display>`,
  }),
  parameters: Default.parameters,
};

/** PictoPath */
export const PictoPath: Story = {
  name: 'Legacy pictoPath',
  decorators: [buttonDecorator, titleDecorator('Utilisation de la propriété <code>pictoPath</code> (dépréciée)')],
  args: {
    ...Default.args,
    pictoPath: 'artwork/pictograms/environment',
  },
  render: (args: DsfrDisplayComponent) => ({
    props: args,
    template: `<dsfr-display [pictoPath]="pictoPath"></dsfr-display>`,
  }),
  parameters: Default.parameters,
};
