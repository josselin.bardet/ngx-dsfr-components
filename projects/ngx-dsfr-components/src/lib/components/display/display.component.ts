import { Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { I18nService, LocalStorage, StorageEnum, isOnBrowser } from '../../shared';
import { DSFR_CONFIG_TOKEN } from '../../shared/config/config-token';
import { DsfrConfig } from '../../shared/config/config.model';

export const DISPLAY_MODAL_ID = 'theme-modal-id';

type DisplayTheme = 'light' | 'dark' | 'system';

@Component({
  selector: 'dsfr-display',
  templateUrl: './display.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrDisplayComponent implements OnInit {
  /**
   * l'identifiant de la modale portant les paramètres d'affichage.
   */
  @Input() displayId = DISPLAY_MODAL_ID;

  /**
   * Chemin vers le répertoire exposant les pictogrammes illustratifs DSFR.
   */
  @Input() artworkDirPath: string;

  /**
   * Événement émis lorsque le thème change avec la valeur du nouveau thème : `light`, `dark` ou `system`.
   */
  @Output() displayChange = new EventEmitter<string>();

  /**
   * Thème courant du composant.
   *
   * @internal
   */
  currentTheme: DisplayTheme;

  /** @internal */
  readonly themes = {
    light: {
      id: 'theme-light',
      value: 'light',
      label: this.i18n.t('display.light.label'),
      img: 'sun.svg',
      link: '/pictograms/environment/sun.svg',
      deprecatedLink: '/sun.svg',
    },
    dark: {
      id: 'theme-dark',
      value: 'dark',
      label: this.i18n.t('display.dark.label'),
      img: 'moon.svg',
      link: '/pictograms/environment/moon.svg',
      deprecatedLink: '/moon.svg',
    },
    system: {
      id: 'theme-system',
      value: 'system',
      label: this.i18n.t('display.system.label'),
      hint: this.i18n.t('display.system.hint'),
      img: 'system.svg',
      link: '/pictograms/system/system.svg',
      deprecatedLink: '/system.svg',
    },
  };

  /** @internal */ readonly closeI18n = this.i18n.t('commons.close');
  /** @internal */ name = 'radios-theme';
  /** @internal */ themeValues = Object.values(this.themes);
  /** @internal */ _svgRootPath: string;

  private _useDeprecatedLink = false;

  /** @internal */
  constructor(
    @Inject(DSFR_CONFIG_TOKEN) private config: DsfrConfig,
    public i18n: I18nService,
    private elementRef: ElementRef,
  ) {}

  get pictoPath(): string {
    return this.artworkDirPath;
  }

  /**
   * Chemin des pictogrammes (du composant display) renseigné par le développeur.
   *
   * Note: ce chemin doit permettre de récupérer directement les fichiers SVG suivants : moon.svg, sun.svg, system.svg
   *
   * @deprecated Use `artworkDirPath` instead.
   */
  @Input() set pictoPath(path: string) {
    this.artworkDirPath = path;
    this._useDeprecatedLink = true;
  }

  ngOnInit(): void {
    this.initDisplayTheme();
    if (this.artworkDirPath === undefined) {
      this.artworkDirPath = this.config.artworkDirPath;
    }
  }

  /** @internal */
  onChange() {
    this.setHtmlSchemeAttribute(this.currentTheme);
    this.displayChange.emit(this.currentTheme);
  }

  /** @internal */
  buildSvgPath(theme: any, sprite: string) {
    return this.artworkDirPath + (this._useDeprecatedLink ? theme.deprecatedLink : theme.link) + sprite;
  }

  private initDisplayTheme(): void {
    if (!isOnBrowser()) return;

    // Si l’utilisateur effectue une modification, son choix est conservé (dans le local storage) pour les visites ultérieures.
    let savedTheme = <DisplayTheme>LocalStorage.get(StorageEnum.SCHEME);
    if (savedTheme) {
      this.setHtmlSchemeAttribute(savedTheme);
    }

    // Sinon on prend le theme de la balise HTML
    else {
      const nativeElt = this.elementRef?.nativeElement;
      const html = nativeElt?.ownerDocument?.documentElement;
      savedTheme = html?.getAttribute('data-fr-scheme');
    }

    this.currentTheme = savedTheme || 'system';
  }

  private setHtmlSchemeAttribute(value: DisplayTheme) {
    const nativeElt = this.elementRef?.nativeElement;
    const html = nativeElt?.ownerDocument?.documentElement;
    html?.setAttribute('data-fr-scheme', value);
  }
}
