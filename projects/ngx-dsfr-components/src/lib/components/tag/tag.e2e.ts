import { expect, test } from '@playwright/test';

test('tag.default', async ({ page }) => {
  await page.goto('?path=/story/components-tag--default');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const paragraph = await frame.locator('p.fr-tag');
  await expect(paragraph).toBeEnabled();
});

/* selectable */
test('tag.selectable', async ({ page }) => {
  await page.goto('?path=/story/components-tag--selectable');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const button = await frame.locator('button.fr-tag');
  await expect(button).toBeEnabled();
  await expect(button).toHaveAttribute('aria-pressed', 'false');
  await expect(button).toHaveAttribute('aria-label', 'Retirer le filtre Label tag');
});

test('tag.selected', async ({ page }) => {
  await page.goto('?path=/story/components-tag--selected');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const button = await frame.locator('button.fr-tag');
  await expect(button).toBeEnabled();
  await expect(button).toHaveAttribute('aria-pressed', 'true');
});

test('tag.clickable-with-path', async ({ page }) => {
  await page.goto('?path=/story/components-tag--clickable');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const anchor = await frame.locator('a.fr-tag');
  await expect(anchor).toBeEnabled();
});

/* custom */
test('tag.custom', async ({ page }) => {
  await page.goto('?path=/story/components-tag--clickable-custom');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const anchor = await frame.locator('a.fr-tag');
  await expect(anchor).toBeEnabled();
  await expect(anchor).toHaveClass('fr-tag fr-tag--green-emeraude');
});

/* deletable */
test('tag.deletable', async ({ page }) => {
  await page.goto('?path=/story/components-tag--deletable');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const button = await frame.locator('button.fr-tag');
  await expect(button).toBeEnabled();
  await expect(button).toHaveClass('fr-tag fr-tag--dismiss');
});

test('tag.with-icon', async ({ page }) => {
  await page.goto('?path=/story/components-tag--icon-on-left');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const paragraph = await frame.locator('p.fr-tag');
  await expect(paragraph).toBeEnabled();
  await expect(paragraph).toHaveClass('fr-tag fr-tag--icon-left fr-icon-arrow-left-line');
});

test('tag.small', async ({ page }) => {
  await page.goto('?path=/story/components-tag--small');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const paragraph = await frame.locator('p.fr-tag');
  await expect(paragraph).toBeEnabled();
  await expect(paragraph).toHaveClass('fr-tag fr-tag--sm');
});
