import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrLinkModule } from '../link';
import { DsfrTagComponent } from './tag.component';

@NgModule({
  declarations: [DsfrTagComponent],
  exports: [DsfrTagComponent],
  imports: [CommonModule, DsfrLinkModule],
})
export class DsfrTagModule {}
