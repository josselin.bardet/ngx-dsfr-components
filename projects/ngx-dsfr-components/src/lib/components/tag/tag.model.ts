import { DsfrNavigation } from '../../shared';

/** @since 1.4.0 Événement sur sélection d'un tag (si l'id est renseigné). */
export interface DsfrTagEvent {
  id: string;
  label: string;
  link: string;
  route: string;
  selected: boolean;
}

/** Description d'un élément tag */
export interface DsfrTag extends Omit<DsfrNavigation, 'linkTarget'> {
  /** @since 1.3.2 id du tag, optionnel */
  id?: string;

  /** Permet de personnaliser la couleur du tag. **/
  customClass?: string;

  /**
   * Permet d'avoir un tag cliquable disabled.
   * @since 1.3
   */
  disabled?: boolean;

  /** Icône  */
  icon?: string;

  /** Libelle du tag. */
  label: string;

  /** Lien du tag à renseigner si le mode est 'clickable'. */
  link?: string;

  /** Mode spécifique : default / selectable / clickable / deletable */
  mode?: DsfrTagMode;

  /** Path angular. */
  route?: string;

  /** État selected du tag. */
  selected?: boolean;

  /** Taille du tag (small ou médium). */
  small?: boolean;

  /** Tooltip du tag */
  //TODO: à déprécier au profit d'un input tooltipMessage (pour des raisons de consistence)
  title?: string;
}

/** Modes du tag sous forme de constantes. */
export namespace DsfrTagModeConst {
  /** Par défaut, le tag ne peut pas changer d'état ni accepter aucun lien. */
  export const DEFAULT = 'default';

  /** Un tag cliquable permet de débrancher sur un lien iou un path Angular. */
  export const CLICKABLE = 'clickable';

  /** Un tag 'selectable' change le tag d'état. */
  export const SELECTABLE = 'selectable';

  export const DELETABLE = 'deletable';
}
type T = typeof DsfrTagModeConst;

/** Modes du tag */
export type DsfrTagMode = T[keyof T];
