import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { DsfrLinkTarget, I18nService } from '../../shared';
import { DsfrTag, DsfrTagEvent, DsfrTagMode, DsfrTagModeConst } from './tag.model';

@Component({
  selector: 'dsfr-tag',
  templateUrl: './tag.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrTagComponent implements Omit<DsfrTag, 'title'>, OnChanges {
  /** id du tag, optionnel */
  @Input() tagId: string;

  /**
   * Permet de personnaliser la couleur du tag. Il faut donner la classe exacte (ex : `fr-tag--green-emeraude`) que vous trouverez
   * [ici](https://gouvfr.atlassian.net/wiki/spaces/DB/pages/310706305/Tag#Personnalisation).
   */
  @Input() customClass: string;

  /**
   * Permet d'avoir un tag cliquable disabled.
   * @since 1.3
   */
  @Input() disabled: boolean;

  /** Classe de l'icône (cf. DSFR TAG). */
  @Input() icon: string | undefined; // undefined car peut provenir d'un DsfrTag.icon dans le cas d'un groupe

  /** Libelle du tag. */
  @Input() label: string;

  /** Lien href externe, exclusif avec route et routerLink. */
  @Input() link: string;

  /** Target du lien. Target par défaut de l'application si la propriété est non renseignée. */
  @Input() linkTarget: DsfrLinkTarget;

  /** Path interne. Exclusif avec link et routerLink */
  @Input() route: string;

  /** Path angular géré en tant que directive routerLink. Exclusif avec link et route. */
  @Input() routerLink: string | string[];

  /** RouterLink : classe utilisée pour la directive routerLink active. */
  @Input() routerLinkActive: string | string[] | undefined;

  /** RouterLink : valeurs additionnelles de navigation pour le routerLink (queryParams, state etc.) */
  @Input() routerLinkExtras: NavigationExtras;

  /** État d'un tag 'selectable'. */
  // 1.3 Non automatiquement modifié lorsque l'état du bouton 'aria-pressed' change, 'aria-pressed' est géré
  // le JavaScript du DSFR, d'où la gestion de l'événement '(click)' pour synchroniser 'selected' et 'aria-pressed'.
  @Input() selected = false;

  /** Taille du tag (small ou médium). */
  @Input() small = false;

  /** Donne l'attribut title pour les liens. */
  @Input() tooltipMessage: string;

  /**
   * Événement émis suite au click sur un tag, le contenu de l'événement est soit le lien, la route ou à défaut le label du tag.
   * @since 1.4.0, si l'id du tag est renseigné, l'événement émet un objet de type DsfrTagEvent.
   */
  @Output() tagSelect = new EventEmitter<string | DsfrTagEvent>();

  /** @internal */
  ariaPressed: boolean;

  /** Mode spécifique : default / selectable / clickable / deletable (les modes exclusifs entre eux). */
  private _mode: DsfrTagMode;

  private _id: string;

  /** @internal */
  constructor(public i18n: I18nService) {}

  get mode(): DsfrTagMode {
    if (!this._mode) {
      if (this.selected) this._mode = DsfrTagModeConst.SELECTABLE;
      else if (this.link || this.route || this.routerLink) this._mode = DsfrTagModeConst.CLICKABLE;
    }
    return this._mode;
  }

  /** Mode spécifique : default / selectable / clickable / deletable (les modes exclusifs entre eux). */
  @Input() set mode(value: DsfrTagMode) {
    this._mode = value;
  }

  /**
   * @deprecated (@since 1.5) utiliser tagId.
   * Attention en cas d'utilisation de cet attribut, il doit être utilisé en tant que propriété et non en attribut,
   * ex. [id]="'monid'"
   */
  @Input() set id(value: string) {
    if (value) {
      this._id = value;
      this.tagId ??= this._id;
    }
  }

  ngOnChanges({ selected }: SimpleChanges) {
    // On ne se préoccupe pas trop du mode qui à l'initialisation est 'default' (comme ça chacun porte ses responsabilités)
    if (selected) {
      // On synchronise l'attribut 'aria-pressed' avec la nouvelle valeur de selected
      this.ariaPressed = this.selected || false; // || false au cas où on reçoit undefined ou null
      // On n'émet pas d'événement
    }
  }

  /**
   * Valeur pour le 'href' dans le template.
   * @internal
   */
  getHrefValue(): string {
    return this.route || this.link;
  }

  /**
   * Tag `selectable` : événement `tagSelect()` avec le contenu de du lien ou à défaut son label.
   * @internal
   */
  onSelectable() {
    this.selected = !this.selected; // @since 1.3.2
    this.emitTagSelect(this.getHrefValue() || this.label);
  }

  /**
   * Événement (click) sur un lien.
   * @internal
   */
  onClickable(event: Event) {
    if (this.route) {
      event.preventDefault();
      this.emitTagSelect(this.route);
    }
  }

  /**
   * Tag `selectable` : événement `tagSelect()` avec le contenu de `link` ou à défaut son `label`.
   * @internal
   */
  onDeletable() {
    this.emitTagSelect(this.getHrefValue() || this.label);
  }

  /** @internal */
  getSelectableClasses(): string[] {
    return this.getDefaultClasses();
  }

  /** @internal */
  getClickableClasses(): string {
    const classes = this.getDefaultClasses();
    if (this.customClass) classes.push(this.customClass);
    return classes.join(' ');
  }

  /** @internal */
  getNotClickableClasses(): string[] {
    return this.getDefaultClasses();
  }

  /** @since 1.4.0 Emission de DsfrTagEvent */
  private emitTagSelect(value: string) {
    if (!this.tagId) this.tagSelect.emit(value);
    else {
      this.tagSelect.emit({
        id: this.tagId,
        label: this.label,
        link: this.link,
        route: this.route,
        selected: this.selected,
      });
    }
  }

  private getDefaultClasses(): string[] {
    const classes: string[] = ['fr-tag'];

    if (this.small) classes.push('fr-tag--sm');
    if (this.icon) {
      classes.push('fr-tag--icon-left');
      classes.push(this.icon);
    }
    // Un tag non cliquable ne peut pas avoir de custom class

    return classes;
  }
}
