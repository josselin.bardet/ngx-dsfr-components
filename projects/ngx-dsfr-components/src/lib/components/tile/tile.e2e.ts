import { expect, test } from '@playwright/test';

test('tag.default', async ({ page }) => {
  await page.goto('?path=/story/components-tile--default');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const div = await frame.locator('div.fr-tile');
  await expect(div).toBeEnabled();
});

test('tag.horizontal', async ({ page }) => {
  await page.goto('?path=/story/components-tile--horizontal');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const div = await frame.locator('div.fr-tile');
  await expect(div).toBeEnabled();
  await expect(div).toHaveClass('fr-tile fr-enlarge-link fr-tile--horizontal');
});
