import {
  argHeadingLevel,
  bgDecorator,
  gridDecoratorMD,
  gridDecoratorSM,
  titleDecorator,
} from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrFileSizeUnitConst, DsfrLinkTargetConst, LinkDownloadComponent, PictogramModule } from '../../shared';
import { DsfrBadge } from '../badge';
import { DsfrBadgesGroupModule } from '../badges-group';
import { DsfrPanelBackgroundConst, DsfrPanelBorderConst } from '../card';
import { DEFAULT_HEADING_LEVEL } from '../card/base-panel.component';
import { DsfrMimeTypeConst } from '../download';
import { DsfrLinkModule } from '../link';
import { DsfrTag } from '../tag';
import { DsfrTagsGroupModule } from '../tags-group';
import { DsfrTileComponent } from './tile.component';

const meta: Meta = {
  title: 'COMPONENTS/Tile',
  component: DsfrTileComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrBadgesGroupModule, DsfrTagsGroupModule, DsfrLinkModule, PictogramModule, LinkDownloadComponent],
    }),
  ],
  argTypes: {
    customBackground: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBackgroundConst) },
    customBorder: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBorderConst) },
    downloadSizeUnit: { control: { type: 'inline-radio' }, options: Object.values(DsfrFileSizeUnitConst) },
    headingLevel: argHeadingLevel,
    size: { control: { type: 'inline-radio' }, options: ['SM', 'MD'] },
    linkTarget: { control: { type: 'inline-radio' }, options: Object.values(DsfrLinkTargetConst) },
    useGreyBackground: { table: { disable: true } },
    tileSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrTileComponent>;

const bgAliceblue = bgDecorator('aliceblue');
const artworkCityHall = 'artwork/pictograms/buildings/city-hall.svg';
const heading = 'Intitulé de la tuile';
const description = 'Lorem ipsum dolor sit amet, consectetur adipiscing, incididu';
const detail = 'Détail (optionnel)';
const route = '/maroute';
const badges: DsfrBadge[] = [{ label: 'Label Badge', customClass: 'fr-badge--purple-glycine' }];
const tags: DsfrTag[] = [{ label: 'label tag' }];

// Toutes les propriétés ayant une valeur par défaut ou obligatoires
const DefaultValues: Story = {
  args: {
    // Valeurs par défaut
    artworkDirPath: 'artwork',
    customBackground: 'default',
    customBorder: 'default',
    disabled: false,
    download: false,
    downloadDirect: true,
    downloadAssessFile: false,
    enlargeLink: true,
    headingLevel: DEFAULT_HEADING_LEVEL,
    horizontal: false,
    size: 'MD',
    // Obligatoires
    heading: heading,
  },
};

// -- Tailles ----------------------------------------------------------------------------------------------------------

export const Default: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile verticale MD, taille par défaut')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    description: description,
    detail: detail,
    route: route,
  },
};

export const Small: Story = {
  decorators: [gridDecoratorSM, titleDecorator('Tuile taille SM')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    description: description,
    detail: detail,
    route: route,
    size: 'SM',
  },
};

// -- Sans image -------------------------------------------------------------------------------------------------------

export const NoImage: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile sans image cas max')],
  args: {
    ...DefaultValues.args,
    description: description,
    detail: detail,
    route: route,
    badges: badges,
  },
};

export const TitleOnly: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile sans image, minimum')],
  args: {
    ...DefaultValues.args,
    route: route,
  },
};

export const Description: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile titre et description')],
  args: {
    ...DefaultValues.args,
    description: description,
    route: route,
  },
};

export const Detail: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile titre et détails')],
  args: {
    ...DefaultValues.args,
    detail: detail,
    route: route,
  },
};

// -- Contenu ----------------------------------------------------------------------------------------------------------

export const Badge: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile verticale avec badge dans le contenu')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    description: description,
    route: route,
    badges: badges,
  },
};

export const Tag: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile verticale avec tag dans le contenu')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    description: description,
    route: route,
    tags: tags,
  },
};

// -- Variantes --------------------------------------------------------------------------------------------------------

export const Grey: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Tuile verticale variation accentuée en contrast grey')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    description: description,
    route: route,
    badges: badges,
    customBackground: 'grey',
  },
};

export const Borderless: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Tuile verticale variation sans bordure')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    description: description,
    route: route,
    badges: badges,
    customBorder: 'no-border',
  },
};

export const Transparent: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Tuile verticale variation sans fond')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    description: description,
    route: route,
    badges: badges,
    customBackground: 'transparent',
  },
};

export const Shadow: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Tuile verticale variation ombre portée')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    description: description,
    route: route,
    badges: badges,
    customBorder: 'shadow',
  },
};

// -- Sans lien --------------------------------------------------------------------------------------------------------

export const NoLink: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile sans lien')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
  },
};

// -- Sans lien étendu -------------------------------------------------------------------------------------------------

export const Link: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile verticale sans lien étendu à la tuile')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    enlargeLink: false,
    link: '#',
  },
};

export const ExternalLink: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile verticale sans lien étendu à la tuile, nouvelle fenêtre')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    enlargeLink: false,
    link: '#',
    linkTarget: '_blank',
  },
};

// -- Désactivée -------------------------------------------------------------------------------------------------------

export const Disabled: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile verticale sans lien étendu à la tuile, nouvelle fenêtre')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    disabled: true,
    link: '#',
  },
};

// -- Icône ------------------------------------------------------------------------------------------------------------

export const NoIcon: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile sans icône')],
  args: {
    ...DefaultValues.args,
    //artworkFilePath: artworkCityHall, fixme: rajouter quand DSFR aura corrigé sa classe no-icon
    route: route,
    noIcon: true,
  },
};

export const Icon: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile avec icône lien externe')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    route: route,
  },
};

// -- Tuile horizontale ------------------------------------------------------------------------------------------------

export const Horizontal: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile horizontale avec détails et tag')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    detail: detail,
    route: route,
    tags: tags,
    horizontal: true,
  },
};

export const HorizontalSM: Story = {
  decorators: [gridDecoratorSM, titleDecorator('Tuile SM horizontale avec détails et badge')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    detail: detail,
    route: route,
    badges: badges,
    horizontal: true,
    size: 'SM',
  },
};

export const BreakpointMD: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile horizontale puis vertical à partir du breakpoint md')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    detail: detail,
    route: route,
    horizontal: true,
    rotateOn: 'MD',
  },
};

// -- Download ---------------------------------------------------------------------------------------------------------

const artworkDownload = 'artwork/pictograms/document/document-download.svg';
const downloadHeading = 'Télécharger le document XX';
const downloadDescription = 'Description (optionnelle)';
const downloadPath = 'files/lorem-ipsum.pdf';

export const Download: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile de téléchargement')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkDownload,
    heading: downloadHeading,
    description: downloadDescription,
    link: downloadPath,
    download: true,
    downloadAssessFile: false,
    downloadSizeBytes: 76 * 1024 + 100,
    downloadMimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  },
};

export const DownloadAuto: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile de téléchargement avec détail renseigné automatiquement')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkDownload,
    heading: downloadHeading,
    description: downloadDescription,
    link: downloadPath,
    download: true,
    downloadAssessFile: true,
  },
};

export const DownloadDocLang: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Tuile de téléchargement avec fichier en langue étrangère')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkDownload,
    heading: downloadHeading,
    description: downloadDescription,
    link: downloadPath,
    download: true,
    downloadAssessFile: true,
    downloadLangCode: 'la',
  },
};

// -- Spécifique -------------------------------------------------------------------------------------------------------

export const Slots: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Utilisation des slots du composant pour le titre et la description')],
  args: {
    ...DefaultValues.args,
    artworkFilePath: artworkCityHall,
    description: description,
    detail: detail,
    route: route,
  },
  render: (args) => ({
    props: args,
    template: slotTemplate,
  }),
};

//  [description] + [heading] dans les slots
const slotTemplate = `
<dsfr-tile
  [artworkDirPath] = "artworkDirPath"
  [artworkFilePath] = "artworkFilePath"
  [badges] = "badges"
  [customBackground] = "customBackground"
  [customBorder] = "customBorder"
  [ariaLabel] = "dataAriaLabel"
  [detail] = "detail"
  [disabled] = "disabled"
  [download] = "download"
  [downloadAssessFile] = "downloadAssessFile"
  [downloadLangCode] = "downloadLangCode"
  [downloadMimeType] = "downloadMimeType"
  [downloadSizeBytes] = "downloadSizeBytes"
  [enlargeLink] = "enlargeLink"
  [headingLevel] = "headingLevel"
  [horizontal] = "horizontal"
  [link] = "link"
  [linkTarget] = "linkTarget"
  [route] = "route"
  [routerLink] = "routerLink"
  [routerLinkExtras] = "routerLinkExtras"
  [size] = "size"
  [tags] = "tags"
  >
    <ng-container heading>${Slots.args?.heading}</ng-container>
    <ng-container desc>
      <div><em>${Slots.args?.description}</em></div>
    </ng-container>
</dsfr-tile>
`;
