import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrTileComponent } from '../tile.component';
import { DsfrTileModule } from '../tile.module';
import { DemoTileGridComponent } from './demo-tile-grid.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Tile',
  component: DemoTileGridComponent,
  subcomponents: [DsfrTileComponent],
  decorators: [moduleMetadata({ imports: [DsfrTileModule] })],
  argTypes: {
    size: { control: { type: 'inline-radio' }, options: ['SM', 'MD'] },
  },
};
export default meta;
type Story = StoryObj<DemoTileGridComponent>;

// -- Grille de tuile --------------------------------------------------------------------------------------------------

export const Grid: Story = {
  decorators: dsfrDecorator('Grilles de tuiles'),
  args: {
    size: 'MD',
    horizontal: false,
  },
};
