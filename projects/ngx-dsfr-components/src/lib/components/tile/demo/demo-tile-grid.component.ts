import { CommonModule } from '@angular/common';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrBadge } from '../../badge';
import { DsfrTag } from '../../tag';
import { DsfrTileModule } from '../tile.module';

@Component({
  selector: 'demo-tile-grid',
  templateUrl: './demo-tile-grid.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, DsfrTileModule],
})
export class DemoTileGridComponent {
  @Input() horizontal = false;
  @Input() size: 'SM' | 'MD' | undefined = 'MD';

  readonly artworkFilePath = 'artwork/pictograms/buildings/city-hall.svg';
  readonly heading = 'Intitulé de la tuile';
  readonly description = 'Lorem ipsum dolor sit amet, consectetur adipiscing, incididu';
  readonly detail = 'Détail (optionnel)';
  readonly route = '/maroute';
  readonly badges: DsfrBadge[] = [{ label: 'Label Badge', customClass: 'fr-badge--purple-glycine' }];
  readonly tags: DsfrTag[] = [{ label: 'label tag' }];

  get gridClass(): string {
    return this.calcGridClass();
  }

  private calcGridClass(): string {
    let gridClass: string;
    if (!this.horizontal)
      gridClass =
        this.size !== 'SM' ? 'fr-col-12 fr-col-md-4 fr-col-lg-3' : 'fr-col-12 fr-col-sm-6 fr-col-md-3 fr-col-lg-2';
    else
      gridClass =
        this.size !== 'SM' ? 'fr-col-12 fr-col-md-6 fr-col-lg-4' : 'fr-col-12 fr-col-sm-6 fr-col-md-4 fr-col-lg-3';
    return gridClass;
  }
}
