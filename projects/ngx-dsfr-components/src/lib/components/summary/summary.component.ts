import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrHeadingLevel, newUniqueId } from '../../shared';
import { DsfrSummary, DsfrSummaryLink } from './summary.model';

@Component({
  selector: 'dsfr-summary',
  templateUrl: './summary.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrSummaryComponent {
  /** Le titre du composant. */
  @Input() heading: string;

  /** Le niveau de titre devant être utilisé, une balise `p` sera utilisée si la propriété n'est pas renseignée. */
  @Input() headingLevel: DsfrHeadingLevel;

  /** Les entrées du sommaire (required) */
  @Input() entries: DsfrSummary = [];

  /** Événement sur lien avec la valeur de l'ancre. */
  @Output() entrySelect = new EventEmitter<string>();

  /** @internal */ headingId = newUniqueId();

  /**
   * Emission de la valeur de l'ancre (fragment ou link) au clic sur un lien
   * @internal
   */
  /** @internal */
  onLinkSelect(entry: DsfrSummaryLink): void {
    // on propage l'output, pas besoin de gérer ici le preventDefault, c'est géré en amont
    if (entry.route) {
      this.entrySelect.emit(entry.route);
    }
  }
}
