import { RouterTestingModule } from '@angular/router/testing';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { HeadingModule } from '../../shared';
import { DsfrBackToTopModule } from '../backtotop';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrSummaryComponent } from './summary.component';

const meta: Meta = {
  title: 'COMPONENTS/Summary',
  component: DsfrSummaryComponent,
  decorators: [
    moduleMetadata({ imports: [HeadingModule, DsfrBackToTopModule, ItemLinkComponent, RouterTestingModule] }),
  ],
  parameters: { actions: false },
  argTypes: {
    headingLevel: { control: { type: 'inline-radio' }, options: ['P', 'H2', 'H3', 'H4', 'H5', 'H6'] },
    entrySelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrSummaryComponent>;
const loremIpsum =
  'Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et.';
const content = `
<p></p>
<div id="anchor-1"><h5>Contenu 1</h5><p>${loremIpsum}</p></div>
<div id="anchor-1.1"><h6>Contenu 1.1</h6><p>${loremIpsum}</p></div>
<div id="anchor-1.2"><h6>Contenu 1.2</h6><p>${loremIpsum}</p></div>
<div id="anchor-2"><h5>Contenu 2</h5><p>${loremIpsum}</p></div>
<div id="anchor-2.1"><h6>Contenu 2.1</h6><p>${loremIpsum}</p></div>
<dsfr-backtotop></dsfr-backtotop>
`;

export const Default: Story = {
  decorators: [componentWrapperDecorator((story) => `${story}${content}`)],
  args: {
    heading: 'Sommaire',
    entries: [
      {
        label: 'Titre du lien 1',
        link: '#anchor-1',
        subEntries: [
          { label: 'Titre du lien 1.1', link: '#anchor-1.1' },
          { label: 'Titre du lien 1.2', link: '#anchor-1.2' },
        ],
      },
      {
        label: 'Titre du lien 2',
        link: '#anchor-2',
        subEntries: [{ label: 'Titre du lien 2.1', link: '#anchor-2.1' }],
      },
    ],
  },
};

export const RouterLink: Story = {
  args: {
    ...Default.args,
    entries: [
      {
        label: 'Router link 1',
        routerLink: '/path-1',
      },
    ],
  },
};
