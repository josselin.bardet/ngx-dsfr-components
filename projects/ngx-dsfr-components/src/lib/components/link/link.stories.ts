import { dsfrDecorator } from '.storybook/storybook-utils';
import { RouterTestingModule } from '@angular/router/testing';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrLinkTargetConst, DsfrPositionConst, DsfrSizeConst } from '../../shared';
import { DsfrTooltipDirective } from '../tooltip';
import { DsfrLinkComponent } from './link.component';

const meta: Meta = {
  title: 'COMPONENTS/Link',
  component: DsfrLinkComponent,
  decorators: [moduleMetadata({ imports: [RouterTestingModule, DsfrTooltipDirective] })],
  parameters: {},
  argTypes: {
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) },
    iconPosition: { control: { type: 'inline-radio' }, options: Object.values(DsfrPositionConst) },
    targetLink: { control: { type: 'select' }, options: [''].concat(Object.values(DsfrLinkTargetConst)) },
    linkSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrLinkComponent>;

const defaultValues: Story = {
  args: {
    disabled: false,
    iconPosition: 'right',
    size: 'MD',
  },
};

const label = 'Label lien';

export const Default: Story = {
  decorators: dsfrDecorator('Lien seul'),
  args: {
    ...defaultValues.args,
    label: label,
    link: '#',
    tooltipMessage: 'Lien dans le même onglet',
  },
};

export const IconOnLeft: Story = {
  decorators: dsfrDecorator('Lien avec icône à gauche'),
  args: {
    ...defaultValues.args,
    label: label,
    link: '#',
    icon: 'fr-icon-arrow-left-line',
    iconPosition: 'left',
  },
  name: 'Icon on left',
};

export const IconOnRight: Story = {
  decorators: dsfrDecorator('Lien avec icône à droite'),
  args: {
    ...defaultValues.args,
    label: label,
    link: '#',
    icon: 'fr-icon-arrow-right-line',
  },
  name: 'Icon on right',
};

export const Small: Story = {
  decorators: dsfrDecorator('Lien seul SM'),
  args: {
    ...defaultValues.args,
    label: label,
    link: '#',
    size: 'SM',
  },
};

export const Large: Story = {
  decorators: dsfrDecorator('Lien seul LG'),
  args: {
    ...defaultValues.args,
    label: label,
    link: '#',
    size: 'LG',
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorator('Lien seul désactivé'),
  args: {
    ...defaultValues.args,
    label: label,
    routerLink: '/path',
    disabled: true,
  },
};

export const External: Story = {
  decorators: dsfrDecorator('Lien externe'),
  args: {
    ...defaultValues.args,
    label: 'Lien externe',
    link: 'https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/liens',
    linkTarget: '_blank',
  },
};

export const Tooltip: Story = {
  decorators: dsfrDecorator('Tooltip', 'Depuis DSFR 1.10'),
  args: {
    ...defaultValues.args,
    label: label,
    route: 'route',
    tooltipMessage: 'Lien avec route',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-link [route]="route" [tooltip]="tooltipMessage" [label]="label"></dsfr-link>`,
  }),
};

export const RouterLink: Story = {
  decorators: dsfrDecorator('Lien avec routerLink'),
  args: {
    ...defaultValues.args,
    label: label,
    tooltipMessage: 'Lien avec routerLink',
    routerLink: '/test',
    routerLinkActive: 'active',
    routerLinkExtras: {
      queryParams: { page: 1, id: 15 },
      queryParamsHandling: 'preserve',
    },
  },
};

export const Route: Story = {
  decorators: dsfrDecorator('Lien avec route'),
  args: {
    ...defaultValues.args,
    label: label,
    link: 'route',
    tooltipMessage: 'Lien avec route',
  },
};
