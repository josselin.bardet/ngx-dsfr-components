import { CommonModule } from '@angular/common';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrLink, DsfrPosition, DsfrPositionConst } from '../../shared';
import { DsfrLinkComponent } from './link.component';

@Component({
  selector: 'edu-item-link',
  templateUrl: './link.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, RouterModule],
})
export class ItemLinkComponent extends DsfrLinkComponent {
  // DsfrPositionConst.RIGHT pour être, malheureusement, compatible avec l'existant. undefined dans l'idéal.
  @Input() defaultIconPosition: DsfrPosition | undefined = DsfrPositionConst.RIGHT;

  /**@internal */
  private _item: DsfrLink | undefined;

  /**@internal */
  get item(): DsfrLink | undefined {
    return this._item;
  }

  /** @since 1.7 Optionnel, utilisation avec un model DsfrLink (exclusif avec les autres propriétés) */
  @Input() set item(value: DsfrLink | undefined) {
    this._item = value;
    if (this._item) {
      this.ariaControls = this._item.ariaControls;
      this.ariaCurrent = this._item.active ? 'page' : undefined;
      this.ariaLabel = this._item.ariaLabel;
      this.routerLink = this._item.routerLink;
      this.route = this._item.route;
      this.routerLinkActive = this._item.routerLinkActive;
      this.routerLinkActiveOptions = this._item?.routerLinkActiveOptions;
      this.routerLinkExtras = this._item.routerLinkExtras;
      this.customClass = this.customClass || this._item.customClass;
      this.disabled = this._item.disabled || false;
      this.mode = this._item.mode;
      this.icon = this._item.icon;
      this.iconPosition = this._item.iconPosition || this.defaultIconPosition;
      this.label = this._item.label;
      this.link = this._item.link;
      this.linkId = this._item.linkId;
      this.linkTarget = this._item.linkTarget || this._item.target;
      this.tooltipMessage = this._item.tooltipMessage;
    }
  }
}
