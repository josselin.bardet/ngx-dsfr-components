import { CommonModule } from '@angular/common';
import { Component, Directive, ElementRef, Input, ViewContainerRef } from '@angular/core';
import { ComponentRef } from 'react';
import { newUniqueId } from '../../shared';

@Component({
  selector: 'edu-tooltip',
  template: `<span class="fr-tooltip fr-placement" [id]="tooltipId" role="tooltip" [innerHTML]="message"></span>`,
  standalone: true,
  imports: [CommonModule],
})
class TooltipComponent {
  message: string;
  tooltipId = newUniqueId();
}

@Directive({
  selector: '[tooltip]',
  standalone: true,
})
export class DsfrTooltipDirective {
  private tooltipComponent: TooltipComponent;

  constructor(private elementRef: ElementRef, private viewContainer: ViewContainerRef) {
    // Composant
    const tooltipRef = this.viewContainer.createComponent<TooltipComponent>(TooltipComponent);
    // @ts-ignore
    this.tooltipComponent = (<ComponentRef<TooltipComponent>>tooltipRef).instance;

    // Tooltip id
    const nativeElt = this.elementRef.nativeElement;
    nativeElt.setAttribute('aria-describedby', this.tooltipComponent.tooltipId);

    // Message textuel
    const message = nativeElt.getAttribute('tooltip');
    if (message) this.tooltip = message;
  }

  @Input() set tooltip(value: string) {
    this.tooltipComponent.message = value;
  }
}
