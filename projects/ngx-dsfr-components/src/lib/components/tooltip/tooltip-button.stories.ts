import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrTooltipButtonComponent } from './tooltip-button.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Tooltip Button',
  component: DsfrTooltipButtonComponent,
  decorators: [moduleMetadata({ imports: [] })],
};
export default meta;
type Story = StoryObj<DsfrTooltipButtonComponent>;

const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.`;

export const Default: Story = {
  decorators: dsfrDecorator('Ouverture du tooltip au clic'),
  args: {
    tooltip: loremIpsum,
  },
};
