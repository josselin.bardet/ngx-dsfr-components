import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrTooltipDirective } from './tooltip.directive';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Tooltip',
  decorators: [moduleMetadata({ imports: [DsfrTooltipDirective] })],
};
export default meta;
type Story = StoryObj<DsfrTooltipDirective>;

const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.`;

export const Default: Story = {
  decorators: dsfrDecorator('Ouverture du tooltip au survol'),
  args: {
    tooltip: loremIpsum,
  },
  render: (args) => ({
    props: args,
    template: `<a class="fr-link" href="#" tooltip="Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.">Exemple</a>`,
  }),
};

export const Property: Story = {
  decorators: dsfrDecorator('Texte passé par une propriété'),
  args: {
    tooltip: loremIpsum,
  },
  render: (args) => ({
    props: args,
    template: `<a class="fr-link" href="#" [tooltip]="tooltip">Exemple</a>`,
  }),
};
