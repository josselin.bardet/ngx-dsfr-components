import { KeyValue, KeyValuePipe } from '@angular/common';

/**
 * Modèle de présentation d'une colonne.
 */
export interface DsfrTableColumn {
  /**
   * La clé permettant la mise en correspondance avec les champs des lignes de données.
   */
  key: string;

  /**
   * Le libellé d'entête de la colonne.
   */
  heading: string;
}

/**
 * Modèle de présentation du composant Table.
 */
export interface DsfrDataTable {
  /**
   * @deprecated Depuis 1.9.0, utiliser la propriété `columns` à la place.
   */
  titles?: {};

  /**
   * En-têtes des colonnes [{key: key1, title: title1}, {key: key2, title: title2}, ...]
   */
  columns?: DsfrTableColumn[];

  /**
   * Lignes de données correspondant aux collonnes  [{key1: data1-1, key2: data1-2,…}, ...]
   */
  rows: any[];
}

/**
 * Modèle de présentation INTERNE du composant table.
 *
 * @internal
 */
export class EduTableModel {
  columns: DsfrTableColumn[];
  rows: any[];

  constructor(data: DsfrDataTable, keyValuePipe: KeyValuePipe) {
    this.rows = data.rows;
    if (data.columns) {
      this.columns = data.columns;
    } else {
      this.columns = keyValuePipe.transform(new Map(Object.entries(data.titles ?? {}))).map(
        (t: KeyValue<any, any>) =>
          <DsfrTableColumn>{
            key: t.key,
            heading: t.value,
          },
      );
    }
  }
}
