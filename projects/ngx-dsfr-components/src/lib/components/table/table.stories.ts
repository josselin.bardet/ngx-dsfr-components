import { DSFR_TABLE } from '.storybook/dsfr-themes';
import { KeyValuePipe } from '@angular/common';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrTableComponent } from './table.component';

const meta: Meta = {
  title: 'COMPONENTS/Table',
  component: DsfrTableComponent,
  parameters: {},
  decorators: [moduleMetadata({ providers: [KeyValuePipe] })],
  argTypes: {
    customClass: { control: { type: 'select' }, options: Object.values(DSFR_TABLE) },
  },
};
export default meta;

const Template: StoryFn<DsfrTableComponent> = (args) => ({
  props: args,
});

const myTitles = {
  c1: 'Titre 1',
  c2: 'Titre 2',
  c3: 'Titre 3',
  c4: 'Titre 4',
  c5: 'Titre 5',
  c6: 'Titre 6',
};
const myColumns = [
  { key: 'c1', heading: 'Titre 1' },
  { key: 'c2', heading: 'Titre 2' },
  { key: 'c3', heading: 'Titre 3' },
  { key: 'c4', heading: 'Titre 4' },
  { key: 'c5', heading: 'Titre 5' },
  { key: 'c6', heading: 'Titre 6' },
];
/** @deprecated */
const myTable = {
  titles: myTitles,
  rows: [
    {
      c1: 'Row 1 - Col 1',
      c5: 'Row 1 - Col 5',
      c2: 'Row 1 - Col 2',
      c4: 'Row 1 - Col 4',
      c3: 'Row 1 - Col 3',
      c6: 'Row 1 - Col 6',
    },
    {
      c6: 'Row 2 - Col 6',
      c1: 'Row 2 - Col 1 IS LONGER',
      c3: 'Row 2 - Col 3',
      c4: 'Row 2 - Col 4',
      c5: '',
      c2: 'Row 2 - Col 2',
    },
  ],
};
const myTable2 = {
  columns: myColumns,
  rows: [
    {
      c1: 'Row 1 - Col 1',
      c5: 'Row 1 - Col 5',
      c2: 'Row 1 - Col 2',
      c4: 'Row 1 - Col 4',
      c3: 'Row 1 - Col 3',
      c6: 'Row 1 - Col 6',
    },
    {
      c6: 'Row 2 - Col 6',
      c1: 'Row 2 - Col 1 IS LONGER',
      c3: 'Row 2 - Col 3',
      c4: 'Row 2 - Col 4',
      c5: '',
      c2: 'Row 2 - Col 2',
    },
  ],
};
const myColumnsOrder = [
  {
    key: 'b',
    heading: 'Colonne b',
  },
  {
    key: 'a',
    heading: 'Colonne a',
  },
];
const myTableOrder = [
  {
    a: 'Valeur a',
    b: 'Valeur b',
  },
];

/** Default */
export const Default = Template.bind({});
Default.args = {
  data: myTable2,
  caption: 'Tableau par défaut',
};

/** Bordered */
export const Bordered = Template.bind({});
Bordered.args = {
  data: myTable,
  caption: 'Tableau avec bordures',
  bordered: true,
};

/** No Scroll */
export const NoScroll = Template.bind({});
NoScroll.args = {
  data: myTable,
  caption: 'Tableau No Scroll',
  noScroll: true,
};
NoScroll.decorators = [
  componentWrapperDecorator((story) => `<div class="demo-phone-container"><div>${story}</div></div>`),
];

/** Fixed Layout */
export const FixedLayout = Template.bind({});
FixedLayout.args = {
  data: myTable,
  caption: 'Tableau avec un fixedLayout',
  fixedLayout: true,
};

/** Caption Bottom  */
export const CaptionBottom = Template.bind({});
CaptionBottom.args = {
  data: myTable,
  caption: 'Caption en bas',
  captionBottom: true,
};

/** No Caption */
export const NoCaption = Template.bind({});
NoCaption.args = {
  data: myTable,
  noCaption: true,
};

/** No Data */
export const NoData = Template.bind({});
NoData.args = {
  data: { titles: myTitles, rows: [] },
  caption: 'Table sans données',
};

/** Custom color */
export const CustomColor = Template.bind({});
CustomColor.args = {
  data: myTable,
  caption: 'Tableau avec une couleur personnalisée',
  customClass: DSFR_TABLE.GREEN_EMERAUDE,
};

/** Colonne ordonnées */
export const CustomColumnsOrder = Template.bind({});
CustomColumnsOrder.args = {
  data: { columns: myColumnsOrder, rows: myTableOrder },
  caption: "Conservation de l'ordre d'insertion des colonnes",
};
