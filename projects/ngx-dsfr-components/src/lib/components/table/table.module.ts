import { CommonModule, KeyValuePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrTableComponent } from './table.component';

@NgModule({
  declarations: [DsfrTableComponent],
  exports: [DsfrTableComponent],
  imports: [CommonModule],
  providers: [KeyValuePipe],
})
export class DsfrTableModule {}
