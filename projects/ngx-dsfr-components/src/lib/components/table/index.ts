export { DsfrDataTable, DsfrTableColumn } from './data-table.model';
export { DsfrTableComponent } from './table.component';
export { DsfrTableModule } from './table.module';
