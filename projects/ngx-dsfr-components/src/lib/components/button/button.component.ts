import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrPosition, DsfrPositionConst, DsfrSize, DsfrSizeConst } from '../../shared';
import { DsfrButton, DsfrButtonType, DsfrButtonVariant, DsfrButtonVariantConst } from './button.model';

@Component({
  selector: 'dsfr-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrButtonComponent implements DsfrButton {
  /** Libellé du bouton, obligatoire. */
  @Input() label: string;

  /** Type du button, `submit` par défaut. */
  @Input() type: DsfrButtonType;

  /**
   * Message du `tooltip` (attribut `title` du bouton) ou ajout d'un `span` en `sr-only` si pas de label.
   */
  @Input() tooltipMessage: string;

  /** Style du bouton, `primary` par défaut. */
  @Input() variant: DsfrButtonVariant = DsfrButtonVariantConst.PRIMARY;

  /** Taille du bouton, `MD` par défaut  */
  @Input() size: DsfrSize = DsfrSizeConst.MD;

  /** Nom de l'icône. */
  @Input() icon: string | undefined;

  /** Position de l'icône, à gauche par défaut. */
  @Input() iconPosition: DsfrPosition;

  /** Permet de désactiver le bouton d'action, `false` par défaut. */
  @Input() disabled = false;

  /**
   *  @deprecated (since 1.1.0)
   *  Permet de passer le libellé du bouton en majuscules, 'false' par défaut.
   */
  /** @internal */
  @Input() uppercase = false;

  /**
   * @deprecated (since 1.1.0)
   * Permet d'activer un visuel dénotant un "chargement" (busy), 'false' par défaut.
   * */
  /** @internal */
  @Input() loader = false;

  /** 👓 Spécifie le libellé qui sera retranscrit par les narrateurs d'écran. */
  @Input() ariaLabel: string;

  /** Permet d'inverser le contraste du marqueur de focus, `false` par défaut. */
  @Input() invertedOutlineContrast = false;

  /** Permet d'identifier le button. */
  @Input() id: string;

  /**
   * 👓 `ariaControls` est utilisé pour la manipulation d'une modale par exemple.
   * (prise en charge du retour de focus à la fermeture de la modale).
   */
  // ariaControls ne fait pas partie de l'interface ?
  @Input() ariaControls: string;

  /** Style personnalisé `@since 1.3.0` */
  @Input() customClass: string;

  /** @internal */
  getClasses(): string[] {
    const classes: string[] = ['fr-btn'];

    if (this.customClass) classes.push(this.customClass);

    if (this.variant === DsfrButtonVariantConst.SECONDARY) classes.push('fr-btn--secondary');
    else if (this.variant === DsfrButtonVariantConst.TERTIARY) classes.push('fr-btn--tertiary');
    else if (this.variant === DsfrButtonVariantConst.TERTIARY_NO_OUTLINE) classes.push('fr-btn--tertiary-no-outline');

    if (this.icon) {
      classes.push(this.icon);
      if (this.label) {
        if (this.iconPosition === DsfrPositionConst.RIGHT) classes.push('fr-btn--icon-right');
        else classes.push('fr-btn--icon-left');
      }
    }

    if (this.size === DsfrSizeConst.SM) classes.push('fr-btn--sm');
    else if (this.size === DsfrSizeConst.LG) classes.push('fr-btn--lg');

    // 'déprécié'
    if (this.uppercase) classes.push('uppercase');
    if (this.invertedOutlineContrast) classes.push('inverted-outline-contrast');

    return classes;
  }
}
