import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrSizeConst } from '../../shared';
import { DsfrTooltipDirective } from '../tooltip';
import { DsfrButtonComponent } from './button.component';
import { DsfrButtonTypeConst } from './button.model';
import { argButtonVariant, argIcon, dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Button',
  component: DsfrButtonComponent,
  decorators: [moduleMetadata({ imports: [DsfrTooltipDirective] })],
  argTypes: {
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) },
    variant: argButtonVariant,
    iconPosition: {
      control: { type: 'inline-radio' },
      options: ['left', 'right'],
      if: { arg: 'icon' }, // https://storybook.js.org/docs/angular/essentials/controls#conditional-controls
    },
    type: { control: { type: 'inline-radio' }, options: Object.values(DsfrButtonTypeConst) },
    icon: argIcon,
  },
};
export default meta;
type Story = StoryObj<DsfrButtonComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Bouton simple'),
  args: {
    label: 'Label bouton',
    type: 'submit',
    variant: 'primary',
    size: 'MD',
    disabled: false,
    tooltipMessage: '',
    icon: undefined,
    iconPosition: undefined,
    ariaLabel: '',
    invertedOutlineContrast: false,
    id: '',
    ariaControls: '',
    customClass: '',
  },
};

export const Small: Story = {
  decorators: dsfrDecorator('Bouton simple SM'),
  args: {
    ...Default.args,
    label: 'Bouton simple SM',
    size: 'SM',
  },
};

export const Large: Story = {
  decorators: dsfrDecorator('Bouton simple LG'),
  args: {
    ...Default.args,
    label: 'Bouton simple LG',
    size: 'LG',
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorator('Bouton désactivé'),
  args: {
    ...Default.args,
    disabled: true,
  },
};

export const IconOnLeft: Story = {
  decorators: dsfrDecorator('Bouton avec icône à gauche'),
  args: {
    ...Default.args,
    icon: 'fr-icon-checkbox-circle-line',
  },
};

export const IconOnRight: Story = {
  decorators: dsfrDecorator('Bouton avec icône à droite'),
  args: {
    ...IconOnLeft.args,
    iconPosition: 'right',
  },
};

export const IconOnly: Story = {
  decorators: dsfrDecorator('Bouton avec icône seule'),
  args: {
    ...Default.args,
    label: '',
    icon: 'fr-icon-checkbox-circle-line',
  },
};

export const Custom: Story = {
  decorators: dsfrDecorator('Bouton avec style personnalisé'),
  args: {
    ...Default.args,
    customClass: 'fr-btn--display',
  },
};

export const Secondary: Story = {
  decorators: dsfrDecorator('Bouton secondaire'),
  args: {
    ...Default.args,
    variant: 'secondary',
  },
};

export const Tertiary: Story = {
  decorators: dsfrDecorator('Bouton tertiaire'),
  args: {
    ...Default.args,
    variant: 'tertiary',
  },
};

export const NoOutline: Story = {
  decorators: dsfrDecorator('Bouton tertiaire sans contour'),
  args: {
    ...Default.args,
    variant: 'tertiary-no-outline',
  },
};

export const Tooltip: Story = {
  decorators: dsfrDecorator('Bouton avec tooltip', 'DSFR 1.10'),
  args: {
    ...Default.args,
    tooltipMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-button 
[tooltip]="tooltipMessage" 
[label]="label"
[type]="type"
[variant]="variant"
[size]="size"
[disabled]="disabled"
[icon]="icon"
[iconPosition]="iconPosition"
[ariaLabel]="ariaLabel"
[invertedOutlineContrast]="invertedOutlineContrast"
[id]="id"
[ariaControls]="ariaControls"
[customClass]="customClass"
></dsfr-button>`,
  }),
  parameters: { docs: { story: { inline: false, height: 160 } } },
};
