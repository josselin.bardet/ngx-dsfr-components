import { DsfrPosition, DsfrSize } from '../../shared';

/** Interface traduite en bouton par certains composants de la librairie. */
export interface DsfrButton {
  /** Libellé du bouton. */
  label: string;

  /** Type du button,'submit' par défaut. */
  type?: DsfrButtonType;

  /** Message du tooltip (attribut title) du bouton. */
  tooltipMessage?: string;

  /** Style du bouton, 'primary' par défaut. */
  variant?: DsfrButtonVariant;

  /** Taille du bouton, 'MD' par défaut  */
  size?: DsfrSize;

  /** Nom de l'icône. */
  icon?: string;

  /** Position de l'icône définie, 'left' par défaut. */
  iconPosition?: DsfrPosition;

  /** Permet de désactiver le bouton d'action, 'false' par défaut. */
  disabled?: boolean;

  /** Permet de passer le libellé du bouton en majuscules, 'false' par défaut. */
  uppercase?: boolean;

  /** Permet d'activer un visuel dénotant un "chargement" (busy), 'false' par défaut. */
  loader?: boolean;

  /** [accessibilité] Spécifie le libellé qui sera retranscrit par les narrateurs d'écran. */
  ariaLabel?: string;

  /** Permet d'inverser le contraste du marqueur de focus, 'false' par défaut. */
  invertedOutlineContrast?: boolean;

  /**Permet d'identifier le button. */
  id?: string;
}

/**
 * Les types du bouton sous forme énumérée.
 */
export namespace DsfrButtonTypeConst {
  export const SUBMIT = 'submit';
  export const RESET = 'reset';
  export const BUTTON = 'button';
}

/**
 * Le type d'un bouton au sens HTML.
 * @see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button
 */
type Type = typeof DsfrButtonTypeConst;
export type DsfrButtonType = Type[keyof Type];

/**
 * Les constantes dénotant les variantes du bouton.
 */
export namespace DsfrButtonVariantConst {
  export const PRIMARY = 'primary';
  export const SECONDARY = 'secondary';
  export const TERTIARY = 'tertiary';
  export const TERTIARY_NO_OUTLINE = 'tertiary-no-outline';
}
/**
 * Les variantes du bouton exportées en tant que type.
 */
type Variant = typeof DsfrButtonVariantConst;
export type DsfrButtonVariant = Variant[keyof Variant];
