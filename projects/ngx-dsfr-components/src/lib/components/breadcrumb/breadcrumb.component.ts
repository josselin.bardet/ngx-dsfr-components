import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrLink, I18nService } from '../../shared';

@Component({
  selector: 'dsfr-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrBreadcrumbComponent {
  /** Tableau d'item du fil d'Ariane. */
  @Input() items: DsfrLink[] = [];

  /** Attribut aria-label de la balise `nav` du fil d'Ariane. */
  @Input() ariaLabel = this.i18n.t('breadcrumb.ariaLabel');

  /** Confirmation du clic sur le bouton. */
  @Output() linkSelect = new EventEmitter<DsfrLink>();

  constructor(/** @internal */ public i18n: I18nService) {}

  /** @internal */
  onItem(item: DsfrLink): void {
    this.linkSelect.emit(item);
  }
}
