import { Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrLink } from '../../shared';
import { ItemLinkComponent } from '../link/item-link.component';
import descriptionMd from './breadcrumb.doc.md';
import { DsfrBreadcrumbComponent } from './index';

const meta: Meta = {
  title: 'COMPONENTS/Breadcrumb',
  component: DsfrBreadcrumbComponent,
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
  decorators: [
    moduleMetadata({
      imports: [ItemLinkComponent],
    }),
  ],
  argTypes: {
    linkSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;

const Template: StoryFn<DsfrBreadcrumbComponent> = (args) => ({
  props: args,
});

const breadcrumbUrl = '?path=/story/components-breadcrumb--default';

const data: DsfrLink[] = [
  { label: 'Accueil', link: '/', icon: 'fr-icon-home-4-line', ariaLabel: 'home' },
  { label: 'item 2', link: breadcrumbUrl },
  { label: 'item 3', link: breadcrumbUrl },
  { label: 'item 4 (no href)' },
  { label: 'Page actuelle', link: breadcrumbUrl, ariaLabel: 'current page' },
];

/** Default */
export const Default = Template.bind({});
Default.args = {
  items: data,
};
