import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrBreadcrumbComponent } from './breadcrumb.component';

@NgModule({
  declarations: [DsfrBreadcrumbComponent],
  exports: [DsfrBreadcrumbComponent],
  imports: [CommonModule, ItemLinkComponent],
})
export class DsfrBreadcrumbModule {}
