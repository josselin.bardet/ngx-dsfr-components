Le fil d’Ariane (ou _breadcrumb_ en anglais pour _miettes de pain_) est un système de navigation secondaire qui permet à l’utilisateur de se situer sur le site qu’il
consulte.

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/fil-d-ariane)

- _Module_ : `DsfrBreadcrumbModule`
- _Composant_ : `DsfrBreadcrumbComponent`
- _Tag_ : `dsfr-breadcrumb`
