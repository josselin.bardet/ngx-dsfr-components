import { AfterViewInit, Component, ElementRef, ViewEncapsulation } from '@angular/core';
import { DomUtils } from '../../shared';

@Component({
  selector: 'dsfr-accordions-group',
  templateUrl: './accordions-group.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrAccordionsGroupComponent implements AfterViewInit {
  constructor(private _elementRef: ElementRef) {}

  ngAfterViewInit() {
    DomUtils.surroundChildWithli(this._elementRef, 'dsfr-accordion');
  }
}
