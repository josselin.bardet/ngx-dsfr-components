import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrAccordionModule } from '../accordion';
import { DsfrAccordionsGroupComponent } from './accordions-group.component';

const meta: Meta = {
  title: 'COMPONENTS/Accordions group',
  component: DsfrAccordionsGroupComponent,
  decorators: [moduleMetadata({ imports: [DsfrAccordionModule] })],
  argTypes: {},
  parameters: {
    controls: { hideNoControlsWarning: true },
  },
};
export default meta;
type Story = StoryObj<DsfrAccordionsGroupComponent>;

const data = `
  <div>
    <h4 class="fr-h4">Contenu </h4>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing, <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">link test</a> incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.</p>
    <ul>
        <li>list item</li>
        <li>list item</li>
        <li>list item</li>
    </ul>
  </div>`;

export const Default: Story = {
  decorators: dsfrDecorator("Groupe d'accordéons"),
  args: {},
  parameters: { controls: { hideNoControlsWarning: true } }, // ne fonctionne pas cf. https://github.com/storybookjs/storybook/issues/24422
  render: (args) => ({
    props: args,
    template: `
<dsfr-accordions-group>
  <dsfr-accordion heading="Intitulé accordéon">${data}</dsfr-accordion>
  <dsfr-accordion heading="Intitulé accordéon">Données de tests ...</dsfr-accordion>
  <dsfr-accordion heading="Intitulé accordéon">Données de tests ...</dsfr-accordion>
</dsfr-accordions-group>`,
  }),
};
