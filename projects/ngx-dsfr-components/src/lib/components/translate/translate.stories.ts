import { Meta, StoryObj } from '@storybook/angular';
import { DsfrTranslateComponent } from './translate.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Translate',
  component: DsfrTranslateComponent,
  argTypes: {
    currentLangCode: { control: { type: 'inline-radio' }, options: ['fr', 'en'] },
    langChange: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrTranslateComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Sélecteur de langue'),
  args: {
    languages: [
      { value: 'fr', label: 'FR - Français' },
      { value: 'en', label: 'EN - English' },
    ],
    outline: true,
  },
};

export const NoOutline: Story = {
  decorators: dsfrDecorator('Sélecteur de langue sans bordure'),
  args: {
    ...Default.args,
    outline: false,
  },
};

export const MultipleColumns: Story = {
  decorators: dsfrDecorator('Sélecteur de langue sur plusieurs colonnes'),
  args: {
    ...Default.args,
    languages: [
      { value: 'fr', label: 'FR - Français' },
      { value: 'en', label: 'EN - English' },
      { value: 'es', label: 'ES - Español' },
      { value: 'de', label: 'DE - Deutsch' },
      { value: 'tr', label: 'TR - Türkçe' },
      { value: 'ro', label: 'RO - Română' },
      { value: 'el', label: 'EL - Ἑλληνική' },
      { value: 'zh', label: 'ZH - 國語' },
      { value: 'uk', label: 'UK - Українська' },
    ],
  },
};

export const Link: Story = {
  decorators: dsfrDecorator('Sélecteur de langue avec liens href'),
  args: {
    ...Default.args,
    languages: [
      { value: 'fr', label: 'FR - Français', link: '?path=/story/components-translate--link&args=currentCode:fr' },
      { value: 'en', label: 'EN - English', link: '?path=/story/components-translate--link&args=currentCode:en' },
      { value: 'it', label: 'IT - Italian', link: '?path=/story/components-translate--link&args=currentCode:it' },
    ],
  },
};
