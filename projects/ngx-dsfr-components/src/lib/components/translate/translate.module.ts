import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrTranslateComponent } from './translate.component';

@NgModule({
  declarations: [DsfrTranslateComponent],
  exports: [DsfrTranslateComponent],
  imports: [CommonModule, FormsModule],
})
export class DsfrTranslateModule {}
