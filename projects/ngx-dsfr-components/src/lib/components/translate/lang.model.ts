import { DsfrOption } from '../../shared';

/**
 * Interface correspondant à une langue
 * `label`: De la forme 'FR - Français', 'EN - English', etc.
 * `value`: [ISO 639-1](https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1), code sur 2 caractères en minuscules
 */
export interface DsfrLang extends Required<DsfrOption> {
  /** Contrairement à un simple select, la sélection d'une langue peut directement invoquer une page désignée par href. */
  link?: string;
}
