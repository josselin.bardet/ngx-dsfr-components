import { ElementRef } from '@angular/core';
import { Meta, StoryFn } from '@storybook/angular';
import { DsfrNoticeComponent } from './notice.component';
import descriptionMd from './notice.doc.md';

const meta: Meta = {
  title: 'COMPONENTS/Notice',
  component: DsfrNoticeComponent,
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
  argTypes: {
    noticeClose: { control: { type: 'EventEmitter' } },
  },
};
export default meta;

const Template: StoryFn<DsfrNoticeComponent> = (args: DsfrNoticeComponent) => ({
  props: args,
});

export const Closable2 = Template.bind({});
Closable2.args = {
  message: `Titre du bandeau`,
  closable: true,
};

/** Default */
export const Default: StoryFn<DsfrNoticeComponent> = (args) => ({
  props: args,
  template: `
    <div class="sb-title">Bandeau d'information simple (via ng-content)</div>
    <dsfr-notice [closable]="closable" [message]="message">Titre du bandeau d'information</dsfr-notice>`,
});
// Default.args = {
//   message: `Titre du bandeau d'information`,
// };

/** Using property */
export const UsingProp: StoryFn<DsfrNoticeComponent> = (args) => ({
  props: args,
  template: `
    <div class="sb-title">Bandeau d'information simple (via property)</div>
    <dsfr-notice [closable]="closable" [message]="message"></dsfr-notice>`,
});
UsingProp.args = {
  message: `Titre du bandeau d'information`,
};
UsingProp.storyName = 'Simple (via prop)';

/** With close button */
export const Closable: StoryFn<DsfrNoticeComponent> = (args) => ({
  props: args,
  template: `
    <div class="sb-title">Bandeau d'information avec bouton fermer</div>
    <dsfr-notice [closable]="closable" [message]="message">Titre du bandeau</dsfr-notice>`,
});
Closable.args = {
  closable: true,
};

/** With link (slot mode) */
export const WithLinkUsingSlot: StoryFn<DsfrNoticeComponent> = (args) => ({
  props: args,
  template: `
    <div class="sb-title">Bandeau d'information simple (via slot)</div>
    <dsfr-notice [closable]="closable" [message]="message">Titre du bandeau, <a href='#' rel='noopener' target='_blank'>lien au fil du texte</a></dsfr-notice>`,
});
WithLinkUsingSlot.storyName = 'With link (via slot)';

/** With link (property mode) */
export const WithLinkUsingProp: StoryFn<DsfrNoticeComponent> = (args) => ({
  props: args,
  template: `
    <div class="sb-title">Bandeau d'information simple (via property)</div>
    <dsfr-notice [closable]="closable" [message]="message"></dsfr-notice>`,
});
WithLinkUsingProp.args = {
  message: `Titre du bandeau, <a href='#' rel='noopener' target='_blank'>lien au fil du texte</a>`,
};
WithLinkUsingProp.storyName = 'With link (via prop)';

/** User Controlled */
export const CloseControlled: StoryFn<DsfrNoticeComponent> = (args) => ({
  props: { ...args, myCloseHandler: myCloseHandler },
  template: `
    <div class="sb-title">Fermeture contrôlée par le développeur</div>
    <dsfr-notice [closable]="closable" [message]="message" (noticeClose)="myCloseHandler($event)">
      Titre du bandeau
    </dsfr-notice>`,
});
CloseControlled.args = {
  closable: 'controlled',
};

function myCloseHandler(comp: ElementRef) {
  const parent = comp.nativeElement.parentNode;
  parent.removeChild(comp.nativeElement);
  const p = document.createElement('p');
  p.innerHTML = '<i>Close action has been controlled by userdev</i>';
  parent.appendChild(p);
}
