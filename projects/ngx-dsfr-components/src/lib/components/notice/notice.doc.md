Le bandeau d’information importante permet aux utilisateurs de voir ou d’accéder à une information importante et
temporaire.

Il est affiché sur l’ensemble des pages en desktop et en mobile. Il affiche une information importante et urgente
(un usage trop fréquent risque de faire “disparaitre” ce bandeau).

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/bandeau-d-information-importante)

- _Module_ : `DsfrNoticeModule`
- _Composant_ : `DsfrNoticeComponent`
- _Tag_ : `dsfr-notice`

---

### ☝️ Points d'attention sur l'usage du wrapper Angular

- Le message peut être renseigné en utilisant soit le slot `ng-content` soit l'input `message`
- Notez que c'est l'input `message` qui prend la priorité sur le `ng-content`
- Si vous souhaitez gérer vous-même ce qui se passe lorsque l'utilisateur clique sur le bouton de fermeture du bandeau
  alors vous devez positionner la propriété `closable` avec pour valeur `controlled`, puis enregistrez un handler sur
  l'output `noticeClose` qui vous transmet une référence sur l'instance `DsfrNoticeComponent`
