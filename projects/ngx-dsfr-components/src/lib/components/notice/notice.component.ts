import { Component, ElementRef, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { I18nService } from '../../shared';

@Component({
  selector: 'dsfr-notice',
  templateUrl: './notice.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrNoticeComponent {
  /**
   * Le message qui sera affiché dans le bandeau.
   * Renseigner le message via l'input est prioritaire sur le slot.
   */
  @Input() message: string;

  /**
   * Permet d'afficher le bouton servant à fermer le bandeau d'information.<br>
   * La valeur 'controlled' permet d'afficher le bouton de fermeture mais c'est vous qui devez contrôler l'action de
   * fermeture du bandeau.
   */
  @Input() closable: boolean | 'controlled' = false;

  /**
   * Signale la fermeture manuelle du bandeau d'information.
   */
  @Output() readonly noticeClose = new EventEmitter<ElementRef>();

  constructor(/** @internal */ public i18n: I18nService, private hostElement: ElementRef) {}

  /** @internal */
  onClose() {
    if (this.closable && this.closable !== 'controlled') {
      this.hostElement.nativeElement.parentNode?.removeChild(this.hostElement.nativeElement);
    }
    this.noticeClose.emit(this.hostElement);
  }
}
