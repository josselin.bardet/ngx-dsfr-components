export namespace DsfrShareNameConst {
  export const FACEBOOK = 'facebook';
  export const LINKEDIN = 'linkedin';
  export const MAIL = 'mail';
  export const MASTODON = 'mastodon';
  export const TWITTER = 'twitter';
}

type T = typeof DsfrShareNameConst;
export type DsfrShareName = T[keyof T];
