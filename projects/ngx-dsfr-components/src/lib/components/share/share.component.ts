import {
  AfterViewInit,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  QueryList,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { DsfrLinkTarget, I18nService } from '../../shared';
import { DsfrShareLinkComponent } from './share-link/share-link.component';
import { DsfrShareNameConst } from './share.model';

@Component({
  selector: 'dsfr-share',
  templateUrl: './share.component.html',
  styleUrls: ['./router-link.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrShareComponent implements OnChanges, AfterViewInit {
  /** @internal */
  @ContentChildren(DsfrShareLinkComponent) shareLinks!: QueryList<DsfrShareLinkComponent>;

  /** Permet de fournir le lien affichant le gestionnaire de consentement (exclusif avec la route). */
  @Input() consentManagerLink: string;

  /** Target du lien. Target par défaut de l'application si la propriété est non renseignée. */
  @Input() consentManagerLinkTarget?: DsfrLinkTarget | undefined;

  /** Permet de fournir la route affichant le gestionnaire de consentement (prioritaire sur le lien). */
  @Input() consentManagerRoute: string;

  /** Permet d'indiquer si l'usage des cookies a été autorisé par l'utilisateur final. */
  @Input() hasCookiePermissions = true;

  /** Si route, événement avec la route en paramètre. */
  @Output() readonly consentManagerSelect = new EventEmitter<string>();

  /** @internal Message de copie dans le presse-papier. */
  message: string;

  /** @internal Texte du message de permission des cookies. */
  cookiesText: any;

  /** @internal */
  constructor(public i18n: I18nService) {
    this.cookiesText = i18n.t('share.cookiesText');
  }

  /** @internal */
  ngAfterViewInit() {
    this.setDisabled(!this.hasCookiePermissions);
  }

  /** @internal */
  ngOnChanges(changes: SimpleChanges) {
    if (changes['hasCookiePermissions']) this.setDisabled(!this.hasCookiePermissions);
  }

  /** @internal */
  onConsentLinkSelect() {
    if (this.consentManagerRoute) {
      this.consentManagerSelect.emit(this.consentManagerRoute);
    }
  }

  /** @internal */
  onCopy(event: Event): void {
    event.preventDefault();
    navigator.clipboard.writeText(window.location.href).then(() => {
      this.message = 'Le lien a été copié dans le presse-papier.';
      setTimeout((): string => (this.message = ''), 3000);
    });
  }

  private setDisabled(disabled: boolean) {
    this.shareLinks?.forEach((shareLink) => {
      if (shareLink.name != DsfrShareNameConst.MAIL) shareLink.disabled = disabled;
    });
  }
}
