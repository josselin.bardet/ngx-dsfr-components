import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrLinkModule } from '../link';
import { DsfrShareLinkComponent } from './share-link/share-link.component';
import { DsfrShareComponent } from './share.component';

@NgModule({
  declarations: [DsfrShareComponent, DsfrShareLinkComponent],
  exports: [DsfrShareComponent, DsfrShareLinkComponent],
  imports: [CommonModule, RouterModule, DsfrLinkModule],
})
export class DsfrShareModule {}
