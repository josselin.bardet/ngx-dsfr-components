La citation permet de citer un texte dans une page éditoriale. La citation peut provenir d'un extrait d’un discours oral formulé par une tierce personne ou d’un texte écrit.

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/citation)

- _Module_ : `DsfrQuoteModule`
- _Composant_ : `DsfrQuoteComponent`
- _Tag_ : `dsfr-quote`
