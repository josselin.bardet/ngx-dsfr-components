import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrTextSize } from '../../shared';

export interface DsfrQuoteDetail {
  text: string;
  isTitle?: boolean;
  sourceUrl?: string;
}

@Component({
  selector: 'dsfr-quote',
  templateUrl: './quote.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrQuoteComponent {
  /**
   * Texte de la citation.
   */
  @Input() text = '';

  /**
   * Taille du texte de la citation. XL par défaut si non précisé.
   */
  @Input() textSize: DsfrTextSize;

  /**
   * URL pour afficher une image d'illustration.
   */
  @Input() imagePath = '';

  /**
   * Texte alternatif à utiliser uniquement si l'image à une information à passer.
   */
  @Input() imageAlt = '';

  /**
   * Auteur de la citation.
   */
  @Input() author = '';

  /**
   * URL de la source de la citation.
   */
  @Input() sourceUrl = '';

  /**
   * Tableau des détails de la citation, sous la forme : detail=[{text:string, isTitle?:boolean, sourceURL?:string}]
   *
   * Se réferrer à la doc DSFR : https://gouvfr.atlassian.net/wiki/spaces/DB/pages/771358744/Citation+-+Quote
   */
  @Input() details: DsfrQuoteDetail[] = [];

  /**
   * Permet de personnaliser la couleur du tag. Il faut donner la class exact (ex : fr-quote--green-emeraude) que vous
   * trouverez ici : https://gouvfr.atlassian.net/wiki/spaces/DB/pages/771358744/Citation+-+Quote#Personnalisation .
   */
  @Input() customClass = '';

  constructor() {}
}
