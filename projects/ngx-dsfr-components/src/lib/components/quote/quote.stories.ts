import { Meta, StoryFn, componentWrapperDecorator } from '@storybook/angular';
import { DsfrTextSizeConst } from '../../shared';
import { DsfrQuoteComponent } from './index';
import descriptionMd from './quote.doc.md';

const meta: Meta = {
  title: 'COMPONENTS/Quote',
  component: DsfrQuoteComponent,
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
  argTypes: {
    textSize: {
      control: { type: 'inline-radio' },
      options: Object.values(DsfrTextSizeConst),
    },
  },
};
export default meta;

const Template: StoryFn<DsfrQuoteComponent> = (args) => ({
  props: args,
});

/** Default */
export const Default = Template.bind({});
Default.args = {
  text: `« Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae 
  sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis 
  volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut. »`,
  sourceUrl: 'https://en.wikipedia.org/wiki/Lorem_ipsum',
  author: 'Cicero',
  details: [
    { text: 'Titre sans source', isTitle: true },
    { text: 'Titre avec source', isTitle: true, sourceUrl: 'https://en.wikipedia.org/wiki/Lorem_ipsum' },
    { text: 'Détail sans source' },
    { text: 'Détail avec source', sourceUrl: 'https://en.wikipedia.org/wiki/Lorem_ipsum' },
  ],
};
Default.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Citation par défaut (texte taille xl)</div>${story}`),
];

export const TextSizeLg = Default.bind({});
TextSizeLg.args = {
  ...Default.args,
  textSize: DsfrTextSizeConst.LG,
};
TextSizeLg.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Citation en taille lg</div>${story}`),
];

export const Illustration = Default.bind({});
Illustration.args = {
  ...Default.args,
  imagePath: 'img/placeholder.1x1.png',
  imageAlt: 'texte alternatif',
};
Illustration.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Citation avec illustration</div>${story}`),
];

export const CustomClass = Default.bind({});
CustomClass.args = {
  ...Default.args,
  customClass: 'fr-quote--green-emeraude',
};
delete CustomClass.args.imagePath;
delete CustomClass.args.imageAlt;
CustomClass.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Citation accentuée</div>${story}`),
];
