import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrQuoteComponent } from './quote.component';

@NgModule({
  declarations: [DsfrQuoteComponent],
  exports: [DsfrQuoteComponent],
  imports: [CommonModule],
})
export class DsfrQuoteModule {}
