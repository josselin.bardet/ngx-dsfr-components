import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { I18nService } from '../../shared';

@Component({
  selector: 'dsfr-franceconnect',
  templateUrl: './franceconnect.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule],
})
export class DsfrFranceConnectComponent {
  /** Utilisation de FranceConnect+ (qui est plus sécurisé). */
  @Input() secure: boolean;

  /** Demande de connexion par FranceConnect */
  @Output() franceConnectSelect = new EventEmitter<boolean>();

  /** @internal */
  constructor(public i18n: I18nService) {}

  /** @internal */
  onConnect() {
    this.franceConnectSelect.emit(this.secure);
  }
}
