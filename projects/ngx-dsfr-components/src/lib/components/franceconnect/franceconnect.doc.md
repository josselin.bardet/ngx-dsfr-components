Le bouton FranceConnect permet à une administration (ou un fournisseur de logiciel agissant pour le compte d’une
administration) de proposer une connexion ou une création de compte simplifiée.

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/bouton-franceconnect)

- _Module_ : `DsfrFranceConnectComponent`
- _Composant_ : `DsfrFranceConnectComponent`
- _Tag_ : `dsfr-franceconnect`

Fonctionnalités :

- un événement `select` est émis lorsque l'utilisateur clique sur le logo avec, en paramètre, la valeur de la
  propriété `secure`.
