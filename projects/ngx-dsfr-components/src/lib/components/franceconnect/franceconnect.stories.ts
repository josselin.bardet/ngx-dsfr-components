import { Meta, StoryFn } from '@storybook/angular';
import descriptionMd from './franceconnect.doc.md';
import { DsfrFranceConnectComponent } from './index';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/France Connect',
  component: DsfrFranceConnectComponent,
  parameters: { docs: { description: { component: descriptionMd } } },
  argTypes: {
    franceConnectSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;

const Template: StoryFn<DsfrFranceConnectComponent> = (args) => ({
  props: args,
});

/** Default */
export const Default = Template.bind({});
Default.decorators = dsfrDecorator('Bouton FranceConnect');
Default.args = {};

/** Default */
export const Plus = Template.bind({});
Plus.decorators = dsfrDecorator('Bouton FranceConnect+');
Plus.args = {
  secure: true,
};
