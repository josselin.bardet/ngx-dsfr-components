export namespace DsfrAlertSizeConst {
  export const SM = 'SM';
  export const MD = 'MD';
}

export type DsfrAlertSize = typeof DsfrAlertSizeConst[keyof typeof DsfrAlertSizeConst];
