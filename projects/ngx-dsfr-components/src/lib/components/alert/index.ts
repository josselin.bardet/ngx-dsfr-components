export { DsfrAlertComponent } from './alert.component';
export { DsfrAlertModule } from './alert.module';
export { DsfrAlertSize } from './alert.size';
