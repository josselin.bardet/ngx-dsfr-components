import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HeadingModule } from '../../shared';
import { DsfrAlertComponent } from './alert.component';

@NgModule({
  declarations: [DsfrAlertComponent],
  exports: [DsfrAlertComponent],
  imports: [CommonModule, HeadingModule],
})
export class DsfrAlertModule {}
