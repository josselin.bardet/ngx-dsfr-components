import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrHeadingLevel, DsfrSeverity, DsfrSeverityConst, DsfrSizeConst, I18nService } from '../../shared';
import { DsfrAlertSize, DsfrAlertSizeConst } from './alert.size';

@Component({
  selector: 'dsfr-alert',
  templateUrl: './alert.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrAlertComponent {
  /** Le titre de l'alerte. */
  @Input() heading: string;

  /** Le niveau de titre devant être utilisé, une balise <p> sera utilisée si la propriété n'est pas renseignée. */
  @Input() headingLevel: DsfrHeadingLevel;

  /** Le corps du message de l'alerte. */
  @Input() message: string;

  /** Le niveau d'alerte. 'info' par défaut. */
  @Input() severity: DsfrSeverity;

  /** La valeur à utiliser pour le rôle ARIA. 'alert' par défaut. */
  @Input() ariaRoleValue: 'alert' | 'status' = 'alert';

  /** La taille de l'alerte. 'MD' par défaut. */
  @Input() size: DsfrAlertSize;

  /** Le libellé associé au bouton de fermeture de l'alerte. */
  @Input() closeControlLabel: string;

  /** Doit être vrai si l'alerte apparait dynamiquement en cours de navigation. (ajout de role=alert pour l'accessibilité),
   *  false par défaut */
  // https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/alerte
  // Pour les développeurs
  // Nous avons retiré l’attribut role="alert" des exemples de code pour les alertes présentes au chargement de la page.
  // En effet, l'élément avec un role="alert" sont les premières choses lues par les technologies d'assistance.
  // De ce fait, le role="alert" doit être réservé aux alertes ajoutées (/injectées) dynamiquement au cours de la navigation.
  // Ex : alertes de notification suite à une action utilisateur ou mise à jour d’un statut.
  @Input() hasAriaRole = false;

  /** L'alerte peut être masquée */
  @Input() closeable = false;

  constructor(private i18n: I18nService) {
    this.closeControlLabel = this.i18n.t('alert.close');
    this.severity = DsfrSeverityConst.INFO;
    this.size = DsfrAlertSizeConst.MD;
  }

  /** @deprecated @since 1.7.0 use 'closeable' instead (avec un 'e') */
  get closable(): boolean {
    return this.closeable;
  }

  /** @deprecated (@since 1.7.0) utiliser `closeable` à la place (avec un 'e') */
  @Input() set closable(value: boolean) {
    this.closeable = value;
  }

  /** @internal */
  getClasses(): string[] {
    const classes = ['fr-alert'];
    if (this.severity === DsfrSeverityConst.ERROR) classes.push('fr-alert--error');
    if (this.severity === DsfrSeverityConst.SUCCESS) classes.push('fr-alert--success');
    if (this.severity === DsfrSeverityConst.INFO) classes.push('fr-alert--info');
    if (this.severity === DsfrSeverityConst.WARNING) classes.push('fr-alert--warning');
    if (this.size === DsfrSizeConst.SM) classes.push('fr-alert--sm');
    return classes;
  }

  /** @internal */
  isSmall(): boolean {
    return this.size == DsfrSizeConst.SM;
  }

  /**
   * Suppression de l'alerte au clic sur le bouton de fermeture (script DSFR)
   * @param event Event
   */
  /** @internal */
  onClose(event: any): void {
    const alert: Node = event.target.parentNode;
    if (alert) {
      alert.parentNode?.removeChild(alert);
    }
  }
}
