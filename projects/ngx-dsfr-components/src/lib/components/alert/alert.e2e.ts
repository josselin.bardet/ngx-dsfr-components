import { expect, test } from '@playwright/test';

test('alert.initial-display', async ({ page }) => {
  await page.goto('?path=/story/components-alert--initial-display');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const div = await frame.locator('div.fr-alert');
  await expect(div).toBeDefined();
});
