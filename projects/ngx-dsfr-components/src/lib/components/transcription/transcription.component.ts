import { CommonModule } from '@angular/common';
import { AfterContentInit, Component, Input, ViewEncapsulation } from '@angular/core';
import { I18nService, newUniqueId } from '../../shared';

@Component({
  selector: 'dsfr-transcription',
  templateUrl: './transcription.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule],
})
export class DsfrTranscriptionComponent implements AfterContentInit {
  /**
   * Contenu de la transcription, prioritaire sur le slot <ng-content /> sans sélecteur.
   * Le slot permet d'avoir un contenu plus riche.
   */
  @Input() content: string;

  /** Titre du média de niveau h1. */
  @Input() heading: string;

  /** @internal */ collapseId: string;
  /** @internal */ dialogId: string;
  /** @internal */ titleId: string;
  /** @internal */ roleDialog = false;

  constructor(public i18n: I18nService) {}

  ngAfterContentInit(): void {
    this.collapseId = newUniqueId();
    this.dialogId = newUniqueId();
    this.titleId = newUniqueId();
  }

  /** @internal */
  enlargeScript() {
    this.roleDialog = !this.roleDialog;
  }
}
