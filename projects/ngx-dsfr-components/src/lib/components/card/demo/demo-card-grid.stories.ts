import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrCardModule } from '../card.module';
import { DemoCardGridComponent } from './demo-card-grid.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Card',
  component: DemoCardGridComponent,
  decorators: [moduleMetadata({ imports: [DsfrCardModule, DsfrButtonsGroupModule, DsfrButtonModule] })],
  argTypes: {
    size: { control: { type: 'inline-radio' }, options: ['SM', 'MD'] },
  },
};
export default meta;
type Story = StoryObj<DemoCardGridComponent>;

// -- Grille de cartes -------------------------------------------------------------------------------------------------

export const Grid: Story = {
  decorators: dsfrDecorator('Grilles de cartes'),
  args: {
    size: 'MD',
    horizontal: false,
  },
};
