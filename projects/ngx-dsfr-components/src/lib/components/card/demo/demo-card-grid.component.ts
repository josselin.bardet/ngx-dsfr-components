import { CommonModule } from '@angular/common';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrBadge } from '../../badge';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrTag } from '../../tag';
import { DsfrCardModule } from '../card.module';

@Component({
  selector: 'demo-card-grid',
  templateUrl: './demo-card-grid.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, DsfrCardModule, DsfrButtonsGroupModule, DsfrButtonModule],
})
export class DemoCardGridComponent {
  @Input() horizontal = false;
  @Input() size: 'SM' | 'MD' = 'MD';

  readonly heading = 'Intitulé de la carte (sur lequel se trouve le lien)';
  readonly image16x9 = 'img/placeholder.16x9.png';
  readonly route = '/maroute';
  readonly loremIpsum = `Lorem ipsum dolor sit amet, consectetur, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et`;
  readonly badges: DsfrBadge[] = [
    { label: 'Label Badge', customClass: 'fr-badge--purple-glycine' },
    { label: 'Label Badge', customClass: 'fr-badge--purple-glycine' },
  ];
  readonly tags: DsfrTag[] = [{ label: 'label tag' }, { label: 'label tag' }];
  readonly detail = 'detail (optionnel)';
  readonly detailIcon = 'fr-icon-warning-fill';

  get gridClass(): string {
    return this.calcGridClass();
  }

  private calcGridClass(): string {
    let gridClass: string;
    if (!this.horizontal)
      gridClass =
        this.size !== 'SM' ? 'fr-col-12 fr-col-md-6 fr-col-lg-4' : 'fr-col-12 fr-col-sm-6 fr-col-md-4 fr-col-lg-3';
    else
      gridClass =
        this.size !== 'SM' ? 'fr-col-12 fr-col-md-8 fr-col-lg-6' : 'fr-col-12 fr-col-sm-6 fr-col-md-4 fr-col-lg-5';
    return gridClass;
  }
}
