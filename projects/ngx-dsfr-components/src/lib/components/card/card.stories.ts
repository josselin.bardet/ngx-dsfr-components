import {
  argHeadingLevel,
  bgDecorator,
  gridDecoratorLG,
  gridDecoratorMD,
  gridDecoratorSM,
  titleDecorator,
} from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrFileSizeUnitConst, DsfrLinkTargetConst, DsfrSizeConst, SvgIconModule } from '../../shared';
import { DsfrBadge } from '../badge';
import { DsfrBadgesGroupModule } from '../badges-group';
import { DsfrButtonModule } from '../button';
import { DsfrButtonsGroupModule } from '../buttons-group';
import { DsfrLinkModule } from '../link';
import { DsfrTag } from '../tag';
import { DsfrTagsGroupModule } from '../tags-group';
import { DEFAULT_HEADING_LEVEL } from './base-panel.component';
import { DsfrPanelBackgroundConst, DsfrPanelBorderConst } from './base-panel.model';
import { DsfrCardComponent } from './card.component';
import { DsfrImageFitConst, DsfrImageRatioConst, DsfrImageTypeConst } from './card.model';

const meta: Meta = {
  title: 'COMPONENTS/Card',
  component: DsfrCardComponent,
  decorators: [
    moduleMetadata({
      imports: [
        DsfrBadgesGroupModule,
        DsfrTagsGroupModule,
        SvgIconModule,
        DsfrLinkModule,
        DsfrButtonsGroupModule,
        DsfrButtonModule,
      ],
    }),
  ],
  parameters: {
    actions: false,
  },
  argTypes: {
    customBackground: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBackgroundConst) },
    customBorder: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBorderConst) },
    downloadAssessFile: { control: { type: 'inline-radio' }, options: [false, true, 'bytes'] },
    downloadLangCode: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadMimeType: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadSizeBytes: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadSizeUnit: { control: { type: 'inline-radio' }, options: Object.values(DsfrFileSizeUnitConst) },
    headingLevel: argHeadingLevel,
    imageFit: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageFitConst) },
    imageRatio: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageRatioConst) },
    imageType: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageTypeConst) },
    linkTarget: { control: { type: 'inline-radio' }, options: Object.values(DsfrLinkTargetConst) },
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) },
    cardSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrCardComponent>;

const heading = 'Intitulé de la carte (sur lequel se trouve le lien)';
const image16x9 = 'img/placeholder.16x9.png';
const route = '/maroute';
const loremIpsum = `Lorem ipsum dolor sit amet, consectetur, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et`;
const bgAliceblue = bgDecorator('aliceblue');
const badgeMedia: DsfrBadge[] = [{ label: 'Label Badge' }];
const badges: DsfrBadge[] = [
  { label: 'Label Badge', customClass: 'fr-badge--purple-glycine' },
  { label: 'Label Badge', customClass: 'fr-badge--purple-glycine' },
];
const tags: DsfrTag[] = [{ label: 'label tag' }, { label: 'label tag' }];
const detail = 'detail (optionnel)';
const detailIcon = 'fr-icon-warning-fill';

// Toutes les propriétés ayant une valeur par défaut et propriétés obligatoires
const DefaultValues: Story = {
  args: {
    // Valeurs par défaut
    badgesOnMedia: false,
    customBackground: 'default',
    customBorder: 'default',
    disabled: false,
    download: false,
    downloadDirect: true,
    downloadAssessFile: true,
    enlargeLink: false, // Bizarre != de la tuile
    hasFooter: false,
    headingLevel: DEFAULT_HEADING_LEVEL,
    horizontal: false,
    imageType: 'img',
    size: 'MD',
    // Obligatoires
    heading: heading,
  },
};

// -- Tailles ----------------------------------------------------------------------------------------------------------

export const Default: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte verticale MD, taille par défaut')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
  },
};

export const Small: Story = {
  decorators: [gridDecoratorSM, titleDecorator('Carte taille SM')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    size: 'SM',
  },
};

export const Large: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Carte taille LG')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    size: 'LG',
  },
};

// -- Variantes --------------------------------------------------------------------------------------------------------

export const Grey: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Carte verticale variation accentuée en contrast grey')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    customBackground: 'grey',
  },
};

export const Borderless: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Carte verticale variation sans bordure')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    customBorder: 'no-border',
  },
};

export const Transparent: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Carte verticale variation sans fond')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    customBackground: 'transparent',
  },
};

export const Shadow: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Carte verticale variation ombre portée')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    customBorder: 'shadow',
  },
};

// -- Sans image -------------------------------------------------------------------------------------------------------

export const NoImage: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Carte sans image')],
  args: {
    ...DefaultValues.args,
    route: route,
    enlargeLink: true,
  },
};

// -- Image et ratio ---------------------------------------------------------------------------------------------------

export const ImageRatio: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator("Carte verticale avec image au ratio d'aspect 32x9")],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    imageRatio: '16:9/2',
  },
};

export const ImageSVG: Story = {
  decorators: [gridDecoratorMD, bgAliceblue, titleDecorator('Carte avec image svg')],
  args: {
    ...DefaultValues.args,
    imagePath: 'icon/sprites.svg#userLine',
    imageType: 'svg',
    route: route,
    enlargeLink: true,
    imageRatio: '16:9/2',
  },
};

// -- En-tête ----------------------------------------------------------------------------------------------------------

export const Header: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte verticale avec taxonomie dans le header')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    badges: badgeMedia,
    badgesOnMedia: true,
  },
};

// -- Contenu ----------------------------------------------------------------------------------------------------------

export const Badge: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte verticale avec badges dans le contenu')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    description: loremIpsum,
    badges: badges,
  },
};

export const Tag: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte verticale avec tags dans le contenu')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    description: loremIpsum,
    tags: tags,
  },
};

export const Detail: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte verticale avec détail')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
  },
};

export const DetailButtom: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte verticale avec détails en haut et en bas')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
    detailBottom: detail,
    detailBottomIcon: detailIcon,
  },
};

export const DetailsBadges: Story = {
  name: 'Details & Badges',
  decorators: [gridDecoratorSM, titleDecorator('Carte verticale SM avec détails et badges')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
    detailBottom: detail,
    detailBottomIcon: detailIcon,
    size: 'SM',
    badges: badges,
  },
};

export const DetailsTags: Story = {
  name: 'Details & Tags',
  decorators: [gridDecoratorLG, titleDecorator('Carte verticale LG avec détails et tags')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
    detailBottom: detail,
    detailBottomIcon: detailIcon,
    size: 'LG',
    tags: tags,
  },
};

// -- Sans lien --------------------------------------------------------------------------------------------------------

export const NoLink: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte sans lien')],
  args: {
    ...DefaultValues.args,
    heading: 'Intitulé de la carte',
    imagePath: image16x9,
  },
};

// -- Sans lien étendu -------------------------------------------------------------------------------------------------

export const Link: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte verticale sans lien étendu à la carte')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    link: '#',
  },
};

export const ExternalLink: Story = {
  decorators: [
    gridDecoratorMD,
    titleDecorator('Carte verticale sans lien étendu et ouverture dans une nouvelle fenêtre'),
  ],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    link: '#',
    linkTarget: '_blank',
  },
};

// -- Désactivée -------------------------------------------------------------------------------------------------------

export const Disabled: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte avec lien désactivé (a sans href)')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    description: loremIpsum,
    route: route,
    enlargeLink: true,
    disabled: true,
  },
};

// -- Zone d'action -------------------------------------------------------------------------------------------------------

const baseTemplate = `
<dsfr-card
  [badges] = "badges"
  [badgesOnMedia] = "badgesOnMedia"
  [customBackground] = "customBackground"
  [customBorder] = "customBorder"
  [description] = "description"
  [detail] = "detail"
  [detailBottom] = "detailBottom"
  [detailBottomIcon] = "detailBottomIcon"
  [detailIcon] = "detailIcon"
  [download] = "download"
  [downloadAssessFile] = "downloadAssessFile"
  [downloadLangCode]="downloadLangCode"
  [downloadMimeType] = "downloadMimeType"
  [downloadSizeBytes] = "downloadSizeBytes"
  [disabled] = "disabled"
  [enlargeLink] = "enlargeLink"
  [hasFooter] = "hasFooter"
  [heading] = "heading"
  [headingLevel] = "headingLevel"
  [horizontal] = "horizontal"
  [imageAlt] = "imageAlt"
  [imageFit] = "imageFit"
  [imagePath] = "imagePath"
  [imageRatio] = "imageRatio"
  [imageType] = "imageType"
  [link] = "link"
  [linkTarget] = "linkTarget"
  [route] = "route"
  [routerLink] = "routerLink"
  [size] = "size" 
  [tags] = "tags"
>FOOTER</dsfr-card>
  `;

export const ActionButtons: Story = {
  decorators: [gridDecoratorMD, titleDecorator("Carte verticale avec zone d'action boutons")],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    link: '#',
    hasFooter: true,
  },
  render: (args) => ({
    props: args,
    template: baseTemplate.replace(
      'FOOTER',
      `
      <dsfr-buttons-group footer inline="always">
          <dsfr-button label="Label" variant="secondary"></dsfr-button>
          <dsfr-button label="Label"></dsfr-button>
      </dsfr-buttons-group>
`,
    ),
  }),
};

export const ActionLinks: Story = {
  decorators: [gridDecoratorMD, titleDecorator("Carte verticale avec zone d'action liens")],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    link: '#',
    hasFooter: true,
  },
  render: (args) => ({
    props: args,
    template: baseTemplate.replace(
      'FOOTER',
      `
      <ul footer class="fr-links-group fr-links-group--inline">
          <li><a class="fr-link fr-icon-arrow-right-line fr-link--icon-right" id="link-1686" href="#">label</a></li>
          <li><a class="fr-link fr-icon-external-link-line fr-link--icon-right" id="link-1687" href="/iframe.html?viewMode=story&id=components-card--action-links" target="_blank">label</a></li>
      </ul>
`,
    ),
  }),
};

// -- Carte horizontale ------------------------------------------------------------------------------------------------

export const Horizontal: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Carte horizontale avec détails et tags')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    route: route,
    enlargeLink: true,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
    detailBottom: detail,
    detailBottomIcon: detailIcon,
    tags: tags,
    horizontal: true,
  },
};
