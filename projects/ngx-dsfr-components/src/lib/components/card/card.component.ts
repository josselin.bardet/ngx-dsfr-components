import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrSizeConst, I18nService, LangService } from '../../shared';
import { BasePanelComponent } from './base-panel.component';
import { DsfrPanelBackgroundConst, DsfrPanelBorderConst } from './base-panel.model';
import { DsfrImageFit, DsfrImageRatio, DsfrImageType } from './card.model';

@Component({
  selector: 'dsfr-card',
  templateUrl: './card.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrCardComponent extends BasePanelComponent {
  /** Si true, les badges seront affichés sur la zone media. */
  @Input() badgesOnMedia: boolean;

  /**
   * Le détail, optionnel. Supporte le format HTML (slot avec sélecteur "detail")
   */
  @Input() detail: string;

  /** Icône de la zone de détail, optionnel. */
  @Input() detailIcon: string;

  /** Icône devant le détail situé au bas de la carte, optionnel. */
  @Input() detailBottomIcon: string;

  /**
   * Zone d'actions, composée de bouton ou de liens (jusqu'à 4 éléments), optionnelle (mais incompatible avec la
   * deuxième zone de détail).
   * Usage : cette propriété doit être activée pour afficher le contenu fourni par l'intermédiaire du slot 'actions'
   */
  @Input() hasFooter = false;

  /** Texte alternatif d'une image à utiliser uniquement si l'image à une information à passer. */
  @Input() imageAlt: string;

  /**
   * Selon la valeur utilisée pour 'imageFit', l'élément peut être rogné, mis à l'échelle ou étiré, afin de remplir
   * la boîte qui le contient.
   */
  @Input() imageFit: DsfrImageFit;

  /** Url de l'image de l'entête, optionnel. */
  @Input() imagePath: string;

  /** Ratio de l'image. */
  @Input() imageRatio: DsfrImageRatio;

  /**
   * Type d'illustration
   * - `img` : pour l'utilisation d'une balise '<img>'.
   * - `svg` : pour l'utilisation de '<edu-svg-icon>'.
   * Si c'est le cas, `@ImagePath` devient le path du fichier `sprite.svg` et la concaténation de l'id du svg à afficher.
   */
  @Input() imageType: DsfrImageType = 'img';

  /** Signale quand la carte est sélectionnée. */
  @Output() readonly cardSelect: EventEmitter<string> = new EventEmitter();

  /**@internal */
  constructor(langService: LangService, public i18n: I18nService) {
    super(langService);
    this.enlargeLink = false;
  }

  /** @internal */
  getClasses(): {} {
    return {
      'fr-card': true,
      'fr-card--horizontal': this.horizontal,
      'fr-card--download': this.download,
      'fr-enlarge-link': this.enlargeLink && !this.hasFooter,
      'fr-card--no-arrow': this.hasFooter,
      'fr-card--no-border': this.customBorder === DsfrPanelBorderConst.NO_BORDER,
      'fr-card--shadow': this.customBorder === DsfrPanelBorderConst.SHADOW,
      'fr-card--grey': this.customBackground === DsfrPanelBackgroundConst.GREY,
      'fr-card--no-background': this.customBackground === DsfrPanelBackgroundConst.TRANSPARENT,
      'fr-card--sm': this.size === DsfrSizeConst.SM,
      'fr-card--lg': this.size === DsfrSizeConst.LG,
    };
  }

  /** @internal */
  onLinkSelect(): void {
    // on propage l'output, pas besoin de gérer ici le preventDefault, c'est géré en amont
    if (this.route) {
      this.cardSelect.emit(this.route);
    }
  }
}
