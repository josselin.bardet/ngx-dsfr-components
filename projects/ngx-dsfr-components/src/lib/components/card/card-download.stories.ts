import { argHeadingLevel, gridDecoratorMD, titleDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import {
  DsfrFileSizeUnitConst,
  DsfrLinkTargetConst,
  DsfrSizeConst,
  LinkDownloadComponent,
  SvgIconModule,
} from '../../shared';
import { DsfrBadgesGroupModule } from '../badges-group';
import { DsfrButtonModule } from '../button';
import { DsfrButtonsGroupModule } from '../buttons-group';
import { DsfrMimeTypeConst } from '../download';
import { DsfrLinkModule } from '../link';
import { DsfrTagsGroupModule } from '../tags-group';
import { DEFAULT_HEADING_LEVEL } from './base-panel.component';
import { DsfrPanelBackgroundConst, DsfrPanelBorderConst } from './base-panel.model';
import { DsfrCardComponent } from './card.component';
import { DsfrImageFitConst, DsfrImageRatioConst, DsfrImageTypeConst } from './card.model';

const meta: Meta = {
  title: 'COMPONENTS/Card/Download',
  component: DsfrCardComponent,
  decorators: [
    moduleMetadata({
      imports: [
        DsfrBadgesGroupModule,
        DsfrTagsGroupModule,
        SvgIconModule,
        DsfrLinkModule,
        DsfrButtonsGroupModule,
        DsfrButtonModule,
        LinkDownloadComponent,
      ],
    }),
  ],
  parameters: {
    actions: false,
  },
  argTypes: {
    customBackground: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBackgroundConst) },
    customBorder: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBorderConst) },
    downloadAssessFile: { control: { type: 'inline-radio' }, options: [false, true, 'bytes'] },
    downloadLangCode: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadMimeType: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadSizeBytes: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadSizeUnit: { control: { type: 'inline-radio' }, options: Object.values(DsfrFileSizeUnitConst) },
    headingLevel: argHeadingLevel,
    imageFit: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageFitConst) },
    imageRatio: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageRatioConst) },
    imageType: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageTypeConst) },
    linkTarget: { control: { type: 'inline-radio' }, options: Object.values(DsfrLinkTargetConst) },
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) },
  },
};
export default meta;

type Story = StoryObj<DsfrCardComponent>;

const heading = 'Intitulé de la carte (sur lequel se trouve le lien)';
const image16x9 = 'img/placeholder.16x9.png';

// Toutes les propriétés ayant une valeur par défaut et propriétés obligatoires
const DefaultValues: Story = {
  args: {
    // Valeurs par défaut
    badgesOnMedia: false,
    customBackground: 'default',
    customBorder: 'default',
    disabled: false,
    download: false,
    downloadDirect: true,
    downloadAssessFile: true,
    enlargeLink: false, // Bizarre != de la tuile
    hasFooter: false,
    headingLevel: DEFAULT_HEADING_LEVEL,
    horizontal: false,
    imageType: 'img',
    size: 'MD',
    // Obligatoires
    heading: heading,
  },
};

// -- Download ---------------------------------------------------------------------------------------------------------

const downloadHeading = 'Télécharger le document XX';
const downloadDescription = 'Description (optionnelle)';
const downloadDetail = 'Détail optionnel';
const downloadPath = 'files/lorem-ipsum.pdf';

export const DownloadWithLink: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte de téléchargement avec lien')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    heading: downloadHeading,
    description: downloadDescription,
    link: downloadPath,
    download: true,
    enlargeLink: false,
    downloadAssessFile: false,
    downloadSizeBytes: 76 * 1024 + 100,
    downloadMimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  },
};

export const DownloadWithRoute: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte de téléchargement avec route (action programmatique)')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    heading: downloadHeading,
    description: downloadDescription,
    route: downloadPath,
    download: true,
    downloadAssessFile: false,
    downloadSizeBytes: 76 * 1024 + 100,
    downloadMimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  },
};

export const DownloadAuto: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte de téléchargement avec détail renseigné automatiquement')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    heading: downloadHeading,
    description: downloadDescription,
    detail: downloadDetail,
    link: downloadPath,
    downloadDirect: false,
    download: true,
    downloadAssessFile: true,
  },
};

export const DownloadDocLang: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Carte de téléchargement avec fichier en langue étrangère')],
  args: {
    ...DefaultValues.args,
    imagePath: image16x9,
    heading: downloadHeading,
    description: downloadDescription,
    detail: downloadDetail,
    link: downloadPath,
    download: true,
    downloadDirect: 'newFileName',
    downloadAssessFile: true,
    downloadLangCode: 'la',
  },
};
