// -- Background -------------------------------------------------------------------------------------------------------

import { DsfrPanelBackground, DsfrPanelBackgroundConst, DsfrPanelBorder } from './base-panel.model';

/** @deprecated @since 1.5.0 use DsfrPanelBackgroundConst instead */
export namespace DsfrCardBackgroundConst {
  export const DEFAULT = 'default';
  export const GREY = 'grey';
  export const TRANSPARENT = 'transparent';
}

/** @deprecated @since 1.5.0 use DsfrPanelBackground instead */
export type DsfrCardBackground = DsfrPanelBackground;

// -- Border -----------------------------------------------------------------------------------------------------------

/** @deprecated @since 1.5.0 use DsfrPanelBorderConst instead */
export namespace DsfrCardBorderConst {
  export const DEFAULT = 'default';
  export const NO_BORDER = 'no-border';
  export const SHADOW = 'shadow';
}

/** @deprecated @since 1.5.0 use DsfrPanelBorder instead */
export type DsfrCardBorder = DsfrPanelBorder;

// -- Image Fit --------------------------------------------------------------------------------------------------------

export namespace DsfrImageFitConst {
  export const FILL = 'fill';
  export const CONTAIN = 'contain';
  export const COVER = 'cover';
  export const NONE = 'none';
  export const SCALE_DOWN = 'scale-down';
}
type TypeImageFit = typeof DsfrImageFitConst;

export type DsfrImageFit = TypeImageFit[keyof TypeImageFit];

// -- Image Ratio ------------------------------------------------------------------------------------------------------

export namespace DsfrImageRatioConst {
  export const RATIO_32_9 = '16:9/2';
  export const RATIO_16_9 = '16:9';
  export const RATIO_3_2 = '3:2';
  export const RATIO_4_3 = '4:3';
  export const RATIO_1_1 = '1:1';
  export const RATIO_3_4 = '3:4';
  export const RATIO_2_3 = '2:3';
}
type TypeRatio = typeof DsfrImageRatioConst;

export type DsfrImageRatio = TypeRatio[keyof TypeRatio];

// -- Image Type ------------------------------------------------------------------------------------------------------

export namespace DsfrImageTypeConst {
  export const IMG = 'img';
  export const SVG = 'svg';
}

type K = typeof DsfrImageTypeConst;

export type DsfrImageType = K[keyof K];
