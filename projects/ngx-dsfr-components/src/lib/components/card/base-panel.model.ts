// -- Background -------------------------------------------------------------------------------------------------------

export namespace DsfrPanelBackgroundConst {
  export const DEFAULT = 'default';
  export const GREY = 'grey';
  export const TRANSPARENT = 'transparent';
}
type TypeBackground = typeof DsfrPanelBackgroundConst;

export type DsfrPanelBackground = TypeBackground[keyof TypeBackground];

// -- Border -----------------------------------------------------------------------------------------------------------

export namespace DsfrPanelBorderConst {
  export const DEFAULT = 'default';
  export const NO_BORDER = 'no-border';
  export const SHADOW = 'shadow';
}
type TypeBorder = typeof DsfrPanelBorderConst;

export type DsfrPanelBorder = TypeBorder[keyof TypeBorder];
