import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrLogoComponent } from './logo.component';

@NgModule({
  declarations: [DsfrLogoComponent],
  imports: [CommonModule],
})
export class DsfrLogoModule {}
