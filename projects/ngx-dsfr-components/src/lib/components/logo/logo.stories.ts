import { componentWrapperDecorator, Meta, StoryFn } from '@storybook/angular';
import { DsfrSizeConst } from '../../shared';
import descriptionMd from './logo.doc.md';
import { DsfrLogoComponent } from './logo.component';

const meta: Meta = {
  title: 'COMPONENTS/Logo',
  component: DsfrLogoComponent,
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
  argTypes: {
    size: { control: 'inline-radio', options: Object.values(DsfrSizeConst) },
  },
};
export default meta;

const Template: StoryFn<DsfrLogoComponent> = (args) => ({
  props: args,
});

/** Logo MD (par défaut) */
export const Default = Template.bind({});
Default.args = {
  label: 'Intitulé<br/>officiel',
};

/** Example logo long */
export const Long = Template.bind({});
Long.args = {
  label: 'Ministère<br/>de la transition<br/>écologique<br/>et solidaire',
};

/** Slot */
export const Slot: StoryFn<DsfrLogoComponent> = (args) => ({
  props: args,
  template: `
  <dsfr-logo [size]="size">
      Ministère
      <br>de la transition
      <br>écologique
      <br>et solidaire
  </dsfr-logo>
  `,
});
