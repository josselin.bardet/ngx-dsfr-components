import {
  AfterViewInit,
  Component,
  ContentChildren,
  ElementRef,
  Input,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { DomUtils, DsfrPosition, DsfrPositionConst, DsfrSize, DsfrSizeConst } from '../../shared';
import { DsfrButtonComponent } from '../button';
import { DsfrAlign, DsfrAlignConst, DsfrInline, DsfrInlineConst } from './buttons-group.model';

@Component({
  selector: 'dsfr-buttons-group',
  templateUrl: './buttons-group.component.html',
  styleUrls: ['./buttons-group.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrButtonsGroupComponent implements AfterViewInit {
  /** @internal */
  @ContentChildren(DsfrButtonComponent) buttons!: QueryList<DsfrButtonComponent>;

  /**
   * Permet de basculer la disposition des boutons en mode horizontal.
   * En mode vertical, mode par défaut, les boutons prennent 100% de la largeur du conteneur.
   */
  @Input() inline: DsfrInline = DsfrInlineConst.NEVER;

  /**
   * Placement des boutons en mode `inline` :
   * - `default` : aligne les boutons sur la gauche.
   * - `right` : aligne les boutons sur la droite
   * - `between` : les boutons ont les mêmes espacements ente eux.
   * - `center` : aligne les boutons au centre du conteneur.
   * - `reverse` : les boutons se lisent de droite à gauche (implique nécessairement un placement à droite).
   *
   * 📌 n'a aucun effet en mode vertical.
   */
  @Input() alignment: DsfrAlign;

  /**
   * Permet de mettre automatiquement tous les boutons d’un groupe à la même largeur.
   * Cela permet une uniformité des boutons en mode horizontal.
   * (en mode vertical, les boutons prennent 100% de la largeur du conteneur)
   * Cette option peut être combinée avec la propriété d'alignement.
   */
  @Input() equisized = false;

  /** Taille des boutons du groupe. */
  @Input() size: DsfrSize = DsfrSizeConst.MD;

  private _iconPosition: DsfrPosition;

  constructor(private _elementRef: ElementRef) {}

  get iconPosition(): DsfrPosition {
    return this._iconPosition;
  }

  /**
   * Positionnement (gauche ou droite) au niveau du groupe des icônes dans les boutons qui
   * contiennent à la fois une icône et un label.
   */
  @Input() set iconPosition(value: DsfrPosition) {
    this._iconPosition = value;
    this.updateButtonsIconPosition();
  }

  /**
   * Cette méthode calcule les styles du composant en évitant les magic strings.
   * @internal
   */
  getClasses(): string[] {
    let classes: string[] = [];

    // Inline class
    switch (this.inline) {
      case 'always':
        classes.push('fr-btns-group--inline');
        break;
      case 'LG':
        classes.push('fr-btns-group--inline-lg');
        break;
      case 'MD':
        classes.push('fr-btns-group--inline-md');
        break;
      case 'SM':
        classes.push('fr-btns-group--inline-sm');
        break;

      default:
        break;
    }

    // Alignment class
    switch (this.alignment) {
      case DsfrAlignConst.CENTER:
        classes.push('fr-btns-group--center');
        break;
      case DsfrAlignConst.BETWEEN:
        classes.push('fr-btns-group--between');
        break;
      case DsfrAlignConst.RIGHT:
        classes.push('fr-btns-group--right');
        break;
      case DsfrAlignConst.REVERSE:
        classes.push('fr-btns-group--inline-reverse', 'fr-btns-group--right');
        break;

      default:
        break;
    }

    // Size class
    if (this.size === DsfrSizeConst.SM) {
      classes.push('fr-btns-group--sm');
    } else if (this.size === DsfrSizeConst.LG) {
      classes.push('fr-btns-group--lg');
    }

    // Equisized class
    if (this.equisized) {
      classes.push('fr-btns-group--equisized');
    }

    // IconPosition class
    if (this._iconPosition) {
      classes.push(
        this._iconPosition === DsfrPositionConst.LEFT ? 'fr-btns-group--icon-left' : 'fr-btns-group--icon-right',
      );
    }

    return classes;
  }

  ngAfterViewInit() {
    // Position des icônes de boutons
    this.updateButtonsIconPosition();

    // Ajout de <li> autour des boutons
    DomUtils.surroundChildWithli(this._elementRef, 'dsfr-button');
  }

  /** On force la position des icônes des boutons. */
  private updateButtonsIconPosition(): void {
    this.buttons?.forEach((button) => (button.iconPosition = this._iconPosition));
  }
}
