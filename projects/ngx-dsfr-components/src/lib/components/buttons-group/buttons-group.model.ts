// -- Alignement -------------------------------------------------------------------------------------------------------

/**
 * Constantes des modes d'alignement.
 */
export namespace DsfrAlignConst {
  /** Aligne les boutons sur la gauche. */
  export const DEFAULT = 'default';

  /** Aligne les boutons sur la droite  */
  export const RIGHT = 'right';

  /* Les boutons ont les mêmes espacements ente eux.  */
  export const BETWEEN = 'between';

  /** Aligne les boutons au centre du conteneur. */
  export const CENTER = 'center';

  /** Combine un alignement à droite avec une inversion du sens des boutons de droite à gauche. */
  export const REVERSE = 'reverse';
}

/**
 * Type des modes d'alignement.
 */
export type DsfrAlign = typeof DsfrAlignConst[keyof typeof DsfrAlignConst];

// -- Inline -----------------------------------------------------------------------------------------------------------

import { DsfrSizeConst } from '../../shared';

export namespace DsfrInlineConst {
  /**
   * Mode vertical (par défaut).
   */
  export const NEVER = 'never';
  /**
   * Toujours en mode horizontal.
   */
  export const ALWAYS = 'always';
  /**
   * Mode horizontal à partir du point de rupture SM (576px).
   */
  export const FROM_SM = DsfrSizeConst.SM;
  /**
   * Mode horizontal à partir du point de rupture MD 576px.
   */
  export const FROM_MD = DsfrSizeConst.MD;
  /**
   * Mode horizontal à partir du point de rupture LG ().
   */
  export const FROM_LG = DsfrSizeConst.LG;
}

type T = typeof DsfrInlineConst;
export type DsfrInline = T[keyof T];
