import { argPosition, argSize, dsfrDecorator, gridDecoratorLG, titleDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrButtonModule } from '../button';
import { DsfrAlignConst, DsfrButtonsGroupComponent, DsfrInlineConst } from './index';

const meta: Meta = {
  title: 'COMPONENTS/Buttons group',
  component: DsfrButtonsGroupComponent,
  decorators: [moduleMetadata({ imports: [DsfrButtonModule] })],
  argTypes: {
    alignment: {
      control: { type: 'inline-radio' },
      options: Object.values(DsfrAlignConst),
      if: { arg: 'inline', neq: 'never' },
    },
    iconPosition: argPosition,
    inline: { control: { type: 'inline-radio' }, options: Object.values(DsfrInlineConst) },
    size: argSize,
  },
};
export default meta;
type Story = StoryObj<DsfrButtonsGroupComponent>;

const template = (content: string) => {
  return `<dsfr-buttons-group
  [inline]="inline"
  [alignment]="alignment"
  [equisized]="equisized"
  [iconPosition]="iconPosition"
  [size]="size">
    ${content}
  </dsfr-buttons-group>`;
};

const defaultContent = `
  <dsfr-button label="Label bouton" variant="secondary"></dsfr-button>
  <dsfr-button label="Label bouton" variant="secondary"></dsfr-button>
  <dsfr-button label="Label bouton" variant="secondary"></dsfr-button>`;
const defaultTemplate = template(defaultContent);

const iconsContent = `
  <dsfr-button label="Label bouton" icon="fr-icon-checkbox-circle-line" iconPosition="right"></dsfr-button>
  <dsfr-button label="Label bouton" icon="fr-icon-checkbox-circle-line" variant="secondary"></dsfr-button>
  <dsfr-button label="Label bouton" icon="fr-icon-checkbox-circle-line" variant="secondary"></dsfr-button>`;
const iconsTemplate = template(iconsContent);

const iconsOnlyContent = `
  <dsfr-button icon="fr-icon-checkbox-circle-line"></dsfr-button>
  <dsfr-button icon="fr-icon-checkbox-circle-line" variant="secondary"></dsfr-button>
  <dsfr-button icon="fr-icon-checkbox-circle-line" variant="secondary"></dsfr-button>`;
const iconsOnlyTemplate = template(iconsOnlyContent);

const inlineContent = `
  <dsfr-button label="Label bouton" (click)="onClick()" ></dsfr-button>
  <dsfr-button [disabled]="true" (click)="onClick()" label="Label bouton"></dsfr-button>
  <dsfr-button label="Label bouton" variant="secondary"></dsfr-button>`;
const inlineTemplate = template(inlineContent);

const equisizedContent = `
  <dsfr-button label="Label bouton"></dsfr-button>
  <dsfr-button label="Label bouton long" variant="secondary"></dsfr-button>
  <dsfr-button label="Label bouton plus long" variant="secondary"></dsfr-button>`;
const equisizedTemplate = template(equisizedContent);

const alignContent = `
  <dsfr-button label="Label bouton 1"></dsfr-button>
  <dsfr-button label="Label bouton 2" variant="secondary"></dsfr-button>
  <dsfr-button label="Label bouton 3" variant="secondary"></dsfr-button>`;
const alignTemplate = template(alignContent);
const alignRender = (args: DsfrButtonsGroupComponent) => ({ props: args, template: alignTemplate });

export const Default: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Groupe de boutons')],
  args: {
    alignment: undefined,
    equisized: false,
    inline: 'never',
    iconPosition: undefined,
    size: 'MD',
  },
  render: (args) => ({
    props: args,
    template: defaultTemplate,
  }),
};

export const Small: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Groupe de boutons SM')],
  args: {
    ...Default.args,
    size: 'SM',
  },
  render: Default.render,
};

export const Large: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Groupe de boutons LG')],
  args: {
    ...Default.args,
    size: 'LG',
  },
  render: Default.render,
};

export const Icons: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Groupe de boutons avec icônes à gauche')],
  args: {
    ...Default.args,
    iconPosition: 'left',
  },
  render: (args) => ({
    props: args,
    template: iconsTemplate,
  }),
};

export const IconsOnly: Story = {
  decorators: dsfrDecorator("Groupe de boutons d'icônes seules"),
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: iconsOnlyTemplate,
  }),
};

export const Inline: Story = {
  decorators: dsfrDecorator('Groupe de boutons inline', 'Sur mobile, les boutons sont toujours horizontaux'),
  args: {
    ...Default.args,
    inline: 'always',
    size: 'SM',
  },
  render: (args) => ({
    props: {
      ...args,
      onClick: function () {
        alert('click');
      },
    },
    template: inlineTemplate,
  }),
};

export const Breakpoint: Story = {
  decorators: dsfrDecorator(
    'Groupe de boutons inline à partir du breakpoint MD (>= 768 px)',
    'Sur mobile, les boutons sont verticaux',
  ),
  args: {
    ...Inline.args,
    inline: 'MD',
  },
  render: Inline.render,
};

export const Equisized: Story = {
  decorators: dsfrDecorator(
    'Groupe de boutons inline de même taille (equisized)',
    '(incompatible avec les différents inline breakpoint SM, MD et LG)',
  ),
  args: {
    ...Inline.args,
    equisized: true,
  },
  render: (args) => ({
    props: args,
    template: equisizedTemplate,
  }),
};

export const AlignBetween: Story = {
  decorators: dsfrDecorator('Groupe de boutons inline avec un espacement identique'),
  args: {
    ...Inline.args,
    alignment: 'between',
  },
  render: alignRender,
};

export const AlignCenter: Story = {
  decorators: dsfrDecorator('Groupe de boutons inline alignés au centre'),
  args: {
    ...Inline.args,
    alignment: 'center',
  },
  render: alignRender,
};

export const AlignRight: Story = {
  decorators: dsfrDecorator('Groupe de boutons inline alignés a droite'),
  args: {
    ...Inline.args,
    alignment: 'right',
  },
  render: alignRender,
};

export const AlignReverse: Story = {
  decorators: dsfrDecorator(
    'Groupe de boutons inline inversés',
    'Les boutons sont placés de droite à gauche dans le container',
  ),
  args: {
    ...Inline.args,
    alignment: 'reverse',
  },
  render: alignRender,
};
