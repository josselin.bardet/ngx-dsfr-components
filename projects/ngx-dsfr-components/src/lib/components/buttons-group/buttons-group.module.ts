import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrButtonsGroupComponent } from './buttons-group.component';

@NgModule({
  declarations: [DsfrButtonsGroupComponent],
  exports: [DsfrButtonsGroupComponent],
  imports: [CommonModule],
})
export class DsfrButtonsGroupModule {}
