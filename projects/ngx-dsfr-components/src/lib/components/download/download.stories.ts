import { componentWrapperDecorator, Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrFileSizeUnitConst } from '../../shared';
import { DsfrTranslateModule } from '../translate';
import { DsfrDownloadVariantConst } from './download-variant';
import { DsfrDownloadComponent } from './download.component';
import descriptionMd from './download.doc.md';
import { DsfrMimeTypeConst } from './dsfr-mime.type';

// TODO Revoir les stories à la mode MDX
const meta: Meta = {
  title: 'COMPONENTS/Download',
  component: DsfrDownloadComponent,
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
  decorators: [
    moduleMetadata({
      imports: [DsfrTranslateModule],
    }),
  ],
  argTypes: {
    mimeType: { control: { type: 'select' }, options: Object.values(DsfrMimeTypeConst) },
    sizeUnit: { control: { type: 'inline-radio' }, options: Object.values(DsfrFileSizeUnitConst) },
    variant: { control: { type: 'inline-radio' }, options: Object.values(DsfrDownloadVariantConst) },
  },
};
export default meta;
const Template: StoryFn<DsfrDownloadComponent> = (args) => ({
  props: args,
});

const componentLang = `<dsfr-translate [languages]="[{ value: 'fr', label: 'FR - Français' },{ value: 'en', label: 'EN - English' }]" currentCode="en"></dsfr-translate>`;
//const decoratorLang = componentWrapperDecorator((story) => `${componentLang}${story}`);
const decoratorLang = componentWrapperDecorator((story) => `${story}`);

/** Default */
export const Default = Template.bind({});
Default.args = {
  link: 'files/lorem-ipsum.pdf',
  fileName: 'lorem-ipsum',
  mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  sizeBytes: 76 * 1024,
};
Default.decorators = [decoratorLang];

/** DocEN */
export const DocEN = Template.bind({});
DocEN.args = {
  link: 'files/lorem-ipsum.pdf',
  fileName: 'lorem-ipsum',
  mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  sizeBytes: 76 * 1024,
  langCode: 'en',
};
DocEN.decorators = [decoratorLang];

/** No File Size */
export const NoFileSize = Template.bind({});
NoFileSize.args = {
  link: '#',
  fileName: 'lorem-ipsum',
  mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
};

/** Direct */
export const Direct = Template.bind({});
Direct.args = {
  link: 'files/lorem-ipsum.pdf',
  fileName: 'lorem-ipsum',
  mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  sizeBytes: 76 * 1024,
  directDownload: true,
};

/** NewFileName */
export const NewFileName = Template.bind({});
NewFileName.args = {
  link: 'files/lorem-ipsum.pdf',
  fileName: 'lorem-ipsum',
  mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  sizeBytes: 76 * 1024,
  directDownload: 'Nouveau nom',
};

/** Bloc */
export const Bloc = Template.bind({});
Bloc.args = {
  link: 'files/lorem-ipsum.pdf',
  fileName: 'lorem-ipsum',
  mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  sizeBytes: 76 * 1024,
  variant: DsfrDownloadVariantConst.BLOCK,
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla est purus, ultrices in porttitor\n' +
    'in, accumsan non quam. Nam consectetur porttitor rhoncus. Curabitur eu est et leo feugiat\n' +
    'auctor vel quis lorem.',
  langCode: 'la', // attr.data-fr-assess-file
};

/** BlocButton */
export const BlocButton = Template.bind({});
BlocButton.args = {
  fileName: 'lorem-ipsum',
  mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  sizeBytes: 76 * 1024 + 1224,
  variant: DsfrDownloadVariantConst.BLOCK,
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla est purus, ultrices in porttitor\n' +
    'in, accumsan non quam. Nam consectetur porttitor rhoncus. Curabitur eu est et leo feugiat\n' +
    'auctor vel quis lorem.',
  langCode: 'la', // attr.data-fr-assess-file
};
