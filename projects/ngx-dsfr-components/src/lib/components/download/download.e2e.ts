import { expect, test } from '@playwright/test';

/** Default */
test('download.default', async ({ page }) => {
  await page.goto('?path=/story/components-download--default');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const span = await frame.locator('span.fr-download__detail');
  await expect(span).toBeEnabled();
  await expect(span).toContainText('PDF - 77,12 ko');
});

/** DocEN */
test('download.doc-en', async ({ page }) => {
  await page.goto('?path=/story/components-download--doc-en');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const span = await frame.locator('span.fr-download__detail');
  await expect(span).toBeEnabled();
  await expect(span).toContainText('PDF - 77,12 ko - Anglais');
});

/** NoFileSize */
test('download.NoFileSize', async ({ page }) => {
  await page.goto('?path=/story/components-download--no-file-size');
  const frame = page.frameLocator('#storybook-preview-iframe');

  // Les métadatas sont systématiques
  const anchor = await frame.locator('.fr-download__link');
  await expect(anchor).toBeEnabled();
  await expect(anchor).not.toHaveAttribute('data-fr-assess-file');
});

/** Direct */
test('download.direct', async ({ page }) => {
  await page.goto('?path=/story/components-download--direct');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const anchor = await frame.locator('a');
  await expect(anchor).toBeEnabled();
  await expect(anchor).toHaveAttribute('download', '');
});

/** NewFileName */
test('download.newFileName', async ({ page }) => {
  await page.goto('?path=/story/components-download--new-file-name');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const anchor = await frame.locator('a');
  await expect(anchor).toBeEnabled();
  await expect(anchor).toHaveAttribute('download', 'Nouveau nom');
});

/** BlocButton */
test('download.bloc', async ({ page }) => {
  await page.goto('?path=/story/components-download--bloc');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const div = await frame.locator('div:has(.fr-download--card)');
  await expect(div).toBeEnabled();
});

/** BlocButton */
test('download.BlocButton', async ({ page }) => {
  await page.goto('?path=/story/components-download--bloc-button');
  const frame = page.frameLocator('#storybook-preview-iframe');

  // const button = await frame.locator('button:has(.fr-download__link)');
  // await expect(button).toBeEnabled();
});
