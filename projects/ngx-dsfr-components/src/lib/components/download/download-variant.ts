/**
 * Constantes correspondant aux différentes variantes du composant download.
 */
export namespace DsfrDownloadVariantConst {
  export const LINK = 'link';
  export const BLOCK = 'block';
}

/**
 * Les variantes du composant download exportées en tant que type.
 */
export type DsfrDownloadVariant = typeof DsfrDownloadVariantConst[keyof typeof DsfrDownloadVariantConst];
