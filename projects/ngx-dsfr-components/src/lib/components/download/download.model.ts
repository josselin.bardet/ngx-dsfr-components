export interface DsfrDownload {
  /**
   * Obligatoire si le document n'est pas du même langage que la page courante.
   * Attribut hreflang, ex: hreflang="en".
   */
  langCode?: string;
  /**
   * Equivalent à l'attribut html natif 'download'.
   * Si == 'true', télécharge directement le fichier sans l'ouvrir, 'false' par défaut.
   * Peut prendre le nom du fichier à télécharger si on souhaite renommer ce fichier.
   */
  directDownload?: boolean | string;

  /**
   * Lien de téléchargement du fichier.
   * Si cette propriété n'est pas renseignée, un bouton remplace l'ancre et l'événement (click) du bouton doit être intercepté.
   */
  link: string;

  /** Format du fichier - obligatoire. Cf. DsfrMimeType */
  mimeType: string;

  /** Nom du ficher - obligatoire. */
  fileName: string;

  /** Poids du fichier en octets. Obligatoire dans le Dsfr mais peut être renseigné ultérieurement. */
  sizeBytes?: number;
}
