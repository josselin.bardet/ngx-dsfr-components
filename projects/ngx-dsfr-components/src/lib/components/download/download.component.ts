import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrFileSizeUnit, I18nService, LangService, downloadDetail } from '../../shared';
import { DsfrDownloadVariant, DsfrDownloadVariantConst } from './download-variant';
import { DsfrDownload } from './download.model';

@Component({
  selector: 'dsfr-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrDownloadComponent implements DsfrDownload {
  /**
   * Obligatoire si le document n'est pas du même langage que la page courante.
   * Ex : langCode="en".
   */
  @Input() langCode: string;

  /**
   * Equivalent à l'attribut html natif 'download'.
   * Si == 'true', télécharge directement le fichier sans l'ouvrir, 'false' par défaut.
   * Peut prendre le nom du fichier à télécharger si on souhaite renommer ce fichier.
   */
  @Input() directDownload: boolean | string = false;

  /**
   * Lien de téléchargement du fichier.
   * Si cette propriété n'est pas renseignée, un bouton remplace l'ancre et l'événement (click) du bouton doit être intercepté.
   */
  @Input() link: string;

  /** Format du fichier - obligatoire. Cf. DsfrMimeType */
  @Input() mimeType: string;

  /** Nom du ficher - obligatoire. */
  @Input() fileName: string;

  /** Poids du fichier en octets. Obligatoire dans le Dsfr mais peut être renseigné ultérieurement. */
  @Input() sizeBytes: number | undefined;

  /** Permet de basculer la présentation en card. */
  @Input() variant: DsfrDownloadVariant = DsfrDownloadVariantConst.LINK;

  /** Description du fichier (uniquement en mode bloc). */
  @Input() description: string;

  /**
   * Indique que les métadonnées du fichier seront positionnées automatiquement par le script DSFR. Les propriétés
   * fileMimeType, fileSizeBytes, hreflang sont alors ignorées. Si la propriété est positionnée à false alors ce sont
   * les valeurs des propriétés fileMimeType, fileSizeBytes, hreflang qui seront utlisées.
   */
  @Input() assessFile = true;

  /** cf. accesseurs. */
  private _sizeUnit: DsfrFileSizeUnit;

  constructor(private langService: LangService, /** @internal */ public i18n: I18nService) {}

  get currentLang(): string {
    return this.langService.lang;
  }

  /**
   * Retourne l'affichage des détails dans cas précis, sinon retourne '' et laisse calculer l'affichage par le script DSFR.
   */
  get detail(): string {
    return downloadDetail(this.mimeType, this.sizeBytes, this.sizeUnit);
  }

  get sizeUnit(): DsfrFileSizeUnit {
    return this._sizeUnit ? this._sizeUnit : this.langService.lang === 'fr' ? 'octets' : 'bytes';
  }

  /**
   * Permet d'afficher la taille soit en bytes (KB, MB, ...) soit en octets (Ko, Mo, ...). Par défaut, l'unité est
   * en octets lorsque la langue courante est 'fr', 'bytes' dans les autres cas.
   */
  @Input() set sizeUnit(value: DsfrFileSizeUnit) {
    this._sizeUnit = value;
  }

  /**
   * Retourne vrai si on affiche un bouton à la place d'une ancre.
   * @internal
   */
  hasButtonMarkup(): boolean {
    return !this.link;
  }

  /**
   * Si === 'true', télécharge directement le fichier sans l'ouvrir, 'false' par défaut.
   * @internal
   */
  isDirectDownload(): boolean {
    return this.directDownload !== false;
  }

  /** @internal */
  getNewFileName(): string {
    return typeof this.directDownload === 'string' ? this.directDownload : '';
  }

  /** @internal */
  isBlockMode() {
    return this.variant === DsfrDownloadVariantConst.BLOCK;
  }
}
