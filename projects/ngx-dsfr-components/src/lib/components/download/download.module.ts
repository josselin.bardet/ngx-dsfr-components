import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrDownloadComponent } from './download.component';

@NgModule({
  declarations: [DsfrDownloadComponent],
  exports: [DsfrDownloadComponent],
  imports: [CommonModule],
})
export class DsfrDownloadModule {}
