Le menu latéral permet aux utilisateurs de naviguer entre les différentes pages d’une rubrique ou d’un même thème.
Le menu latéral est un système de navigation vertical présentant une liste de liens placée à côté du contenu (à gauche
ou à droite de la page) et donnant accès jusqu'à **3 niveaux** d’arborescence.

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/menu-lateral)

- _Module_ : `DsfrSidemenuModule`
- _Composant_ : `DsfrSidemenuComponent`
- _Tag_ : `dsfr-sidemenu`

Un menu est décrit par le type `DsfrMenu` et chaque item par `DsfrMenuItem`.

```typescript
/** Menu */
export interface DsfrMenu {
  /** Titre du menu (optionnel) */
  title?: string;
  /** Entrées du menu */
  items: DsfrMenuItem[];
}

/** Entrée de menu */
export interface DsfrMenuItem extends DsfrLink {
  /** Indique le titre de rubrique */
  title?: string;
  /** Indique l'identifiant de la zone de menu contrôlée par cette entête de menu */
  controlId?: string;
  /* Entrées du menu (liens directs ou sous-menus si plusieurs niveaux) */
  subItems?: DsfrMenuItem[];
}
```
