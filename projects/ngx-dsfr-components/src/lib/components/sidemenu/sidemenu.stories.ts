import { RouterTestingModule } from '@angular/router/testing';
import { Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrPositionConst } from '../../shared';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrMenu } from './menu.model';
import { DsfrSidemenuComponent } from './sidemenu.component';
import descriptionMd from './sidemenu.doc.md';

const meta: Meta = {
  title: 'COMPONENTS/SideMenu',
  component: DsfrSidemenuComponent,
  decorators: [
    moduleMetadata({
      imports: [RouterTestingModule, ItemLinkComponent],
    }),
  ],
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
  argTypes: {
    itemSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;

const data: DsfrMenu = {
  title: 'Titre du menu',
  items: [
    {
      label: 'Niveau 1 actif avec sous-menus niv 2 et 3 actifs',
      link: '#',
      active: true,

      subItems: [
        { label: 'Niveau 2 + routerLink', routerLink: '/', active: false },
        {
          label: 'Niveau 2 actif + icône',
          link: '#',
          icon: 'fr-icon-error-line',
          active: true,
          subItems: [
            { label: 'Niveau 3 actif externe', link: '#', active: true, target: '_blank' },
            { label: 'Niveau 3', link: '#' },
          ],
        },
      ],
    },
    {
      label: 'Autre niveau 1 externe',
      target: '_blank',
    },
    {
      label: 'Niveau 1 avec 2 sous-niveaux',
      subItems: [
        { label: 'Niveau 2-A', link: '#' },
        {
          label: 'Niveau 2-B avec titre de rubrique',
          title: 'Titre de rubrique niveau 3',
          subItems: [
            {
              label: 'Niveau 3-A',
              link: '#',
              subItems: [{ label: 'Niveau 4' }],
            },
            { label: 'Niveau 3-B', link: '#' },
            { label: 'Niveau 3-C', link: '#' },
          ],
        },
      ],
    },
  ],
};

const Template: StoryFn<DsfrSidemenuComponent> = (args) => ({
  props: args,
});

/** Default */
export const Default = Template.bind({});
Default.args = {
  menu: data,
};

/** Full Height On Right */
export const StickyFullHeightRight = Template.bind({});
StickyFullHeightRight.args = {
  position: DsfrPositionConst.RIGHT,
  stickyFullHeight: true,
  menu: data,
};

/** With Icons */
export const WithIcons = Template.bind({});
WithIcons.args = {
  menu: {
    items: [
      {
        label: 'Icône Remix',
        routerLink: '/',
        icon: 'fr-icon-checkbox-circle-line',
        active: true,
        subItems: [
          { label: 'Sub item', link: '#' },
          { label: 'Sub item with route', route: 'foo' },
        ],
      },
      {
        label: "En essayant avec une icône FontAwesome cela ne fonctionne pas du coup, c'est normal",
        link: '#',
        icon: 'ri-arrow-right-fill',
      },
    ],
  },
};

/** Active */
export const Active = Template.bind({});
Active.args = {
  menu: {
    items: [
      {
        label: 'Menu 1',
        route: 'menu1',
        subItems: [
          { label: 'Sub item 1-1', route: 'subitem1-1' },
          { label: 'Sub item 1-2', route: 'subitem1-2' },
        ],
      },
      {
        label: 'Menu 2',
        route: 'menu2',
        active: true,
        subItems: [
          { label: 'Sub item 2-1', route: 'subitem2-1' },
          { label: 'Sub item 2-2', route: 'subitem2-2', active: true },
        ],
      },
    ],
  },
};
