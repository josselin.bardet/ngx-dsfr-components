import {
  AfterContentInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { DsfrPosition, DsfrPositionConst, newUniqueId } from '../../shared';
import { DsfrMenu, DsfrMenuItem } from './menu.model';

@Component({
  selector: 'dsfr-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrSidemenuComponent implements OnChanges, AfterContentInit {
  /** Attribut aria-label du menu latéral.  */
  @Input() ariaLabel = 'Menu latéral';

  /** Id du menu, généré par défaut si non affecté. */
  @Input() controlId: string;

  /** Modèle de donnée du menu. */
  //FIXME: RPA les items de menu fournis par le client peuvent muter sur la prop active, il serait préférable de
  // dissocier le modèle du client (immutable) des données de présentation.
  @Input() menu: DsfrMenu;

  /** Affiche le menu latéral en version fixe  */
  @Input() sticky = false;

  /** Affiche le menu latéral en version fixe sur 100% de la hauteur de la page. */
  @Input() stickyFullHeight = false;

  /** Le menu latéral est affiché à gauche par défaut mais il est possible de l'afficher à droite. */
  @Input() position: DsfrPosition = DsfrPositionConst.LEFT;

  /** Activer l'item sélectionné */
  @Input() autoActive = true;

  /** Evénement émis suite à la sélection d'un item avec lien `a` ou `routerLink`. */
  @Output() itemSelect = new EventEmitter<DsfrMenuItem>();

  /** Pointeur sur le dernier item sélctionné (permet de le désélectionner lors d'une nouvelle sélection). */
  //FIXME: RPA cette approche ne fonctionne pas : il peut y avoir plusieurs items sélectionnés dans le menu hiérachique
  // du coup un algo qui désactiverait tout puis activerait l'item sélectionné ainsi que ses ascendants serait plus
  // simple et fournirait le comporetement attendu avec la la propriété autoActive
  private _prevItemSelected: DsfrMenuItem | undefined;

  ngOnChanges({ menu }: SimpleChanges): void {
    if (menu) {
      this._prevItemSelected = this.findItemActive(this.menu.items);
      // Ajout des controlIds
      this.checkControlId(this.menu?.items);
    }
  }

  ngAfterContentInit() {
    if (!this.controlId) this.controlId = newUniqueId();
    this.checkControlId(this.menu?.items);
  }

  /** @internal */
  onSelectItem(item: DsfrMenuItem): void {
    if (this.autoActive) {
      this.selectItem(item);
    }

    this.itemSelect.emit(item);
  }

  private findItemActive(items: DsfrMenuItem[]): DsfrMenuItem | undefined {
    if (!items) return undefined;

    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      let active = item.active ? item : undefined;
      if (!active && item.subItems) active = this.findItemActive(item.subItems);
      if (active) return active;
    }
    return undefined;
  }

  private selectItem(item: DsfrMenuItem): void {
    if (this._prevItemSelected) {
      this._prevItemSelected.active = false;
    }
    item.active = true;
    this._prevItemSelected = item;
  }

  private checkControlId(menuItems: DsfrMenuItem[]): void {
    if (!menuItems) return;

    menuItems.forEach((menuItem) => {
      if (menuItem.subItems) {
        if (!menuItem.controlId) menuItem.controlId = newUniqueId();
        this.checkControlId(menuItem.subItems);
      }
    });
  }
}
