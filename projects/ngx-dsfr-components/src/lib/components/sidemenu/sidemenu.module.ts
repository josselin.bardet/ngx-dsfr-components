import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrSidemenuComponent } from './sidemenu.component';

@NgModule({
  declarations: [DsfrSidemenuComponent],
  exports: [DsfrSidemenuComponent],
  imports: [CommonModule, RouterModule, ItemLinkComponent],
})
export class DsfrSidemenuModule {}
