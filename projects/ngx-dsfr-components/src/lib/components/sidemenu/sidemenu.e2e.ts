import { expect, test } from '@playwright/test';

/** Default */
test('sidemenu.default', async ({ page }) => {
  await page.goto('?path=/story/components-sidemenu--default');
  const frame = page.frameLocator('#storybook-preview-iframe');

  // fr-sidemenu
  const nav = await frame.locator('nav.fr-sidemenu');
  await expect(nav).toBeEnabled();
  await expect(nav).toHaveAttribute('role', 'navigation');

  // fr-sidemenu__inner
  const menuInner = nav.locator('div.fr-sidemenu__inner');
  await expect(menuInner).toBeEnabled();

  // fr-collapse
  const menuCollapse = menuInner.locator('div.fr-collapse');
  await expect(menuInner).toBeEnabled();

  // fr-sidemenu__title
  const menuTitle = nav.getByText('Titre du menu');
  await expect(menuTitle).toBeEnabled();

  // item
  const item = menuCollapse.getByText('Niveau 2 actif + icône');
  await expect(item).toBeEnabled();
});
