import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrSizeConst } from '../../shared';
import { DsfrBadgeSeverity, DsfrBadgeSize } from './badges.model';

@Component({
  selector: 'dsfr-badge',
  templateUrl: './badge.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrBadgeComponent {
  /** Label du badge, obligatoire. */
  @Input() label: string;

  /**
   * L'icône de sévérité n'est pas affiché si `true` (`false` par défaut).
   * Attention seuls les badges dits "système" peuvent avoir une icône, les badges "custom" ne le peuvent pas.
   */
  @Input() noIcon = false;

  /** Taille du badge, optionnel, `MD` par défaut. */
  @Input() size: DsfrBadgeSize = 'MD';

  /** Classe personnalisée pour la couleur du badge.  */
  @Input() customClass: string;

  /** @internal */
  private _severity: DsfrBadgeSeverity | undefined = undefined;

  get severity(): DsfrBadgeSeverity | undefined {
    return this._severity;
  }

  /** Niveau d'alerte, optionnel. */
  @Input() set severity(value: DsfrBadgeSeverity | undefined) {
    this._severity = !value ? undefined : <DsfrBadgeSeverity>value?.toLowerCase();
  }

  /** @internal */
  getClasses() {
    return {
      // Severity
      'fr-badge--new': this._severity === 'new',
      'fr-badge--success': this._severity === 'success',
      'fr-badge--info': this._severity == 'info',
      'fr-badge--warning': this._severity == 'warning',
      'fr-badge--error': this._severity === 'error',
      // small
      'fr-badge--sm': this.size === DsfrSizeConst.SM && this.label,
      // no-icon (system badges)
      'fr-badge--no-icon': this.noIcon,
    };
  }
}
