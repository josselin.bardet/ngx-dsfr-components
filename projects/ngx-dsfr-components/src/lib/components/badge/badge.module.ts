import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { DsfrBadgeComponent } from './badge.component';

@NgModule({
  imports: [CommonModule],
  declarations: [DsfrBadgeComponent],
  exports: [DsfrBadgeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DsfrBadgeModule {}
