import { Meta, StoryObj } from '@storybook/angular';
import { DsfrBadgeComponent, DsfrBadgeSeverityConst } from './index';
import { DsfrBadgeClasses } from '.storybook/dsfr-themes';
import { dsfrDecorator, gridDecoratorMD, titleDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Badge',
  component: DsfrBadgeComponent,
  argTypes: {
    customClass: { control: { type: 'select' }, options: [''].concat(Object.values(DsfrBadgeClasses)) },
    severity: { control: { type: 'inline-radio' }, options: Object.values(DsfrBadgeSeverityConst) },
    size: { control: { type: 'inline-radio' } },
  },
};
export default meta;
type Story = StoryObj<DsfrBadgeComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Badge seul'),
  args: {
    label: 'Label badge',
    customClass: '',
    noIcon: false,
    severity: undefined,
    size: 'MD',
  },
};

export const LongBadge: Story = {
  decorators: [gridDecoratorMD, titleDecorator('Badge sur une seule ligne - ellipsis')],
  args: {
    ...Default.args,
    label:
      'Label badge très long, coupé par des points de suspensions lorsque le label dépasse la largeur du conteneur',
  },
};

export const Success: Story = {
  decorators: dsfrDecorator('Badge - succès'),
  args: {
    ...Default.args,
    severity: 'success',
  },
};

export const NoIcon: Story = {
  decorators: dsfrDecorator('Badge - succès sans icône'),
  args: {
    ...Success.args,
    noIcon: true,
  },
};

export const Error: Story = {
  decorators: dsfrDecorator('Badge - erreur'),
  args: {
    ...Default.args,
    severity: 'error',
  },
};

export const Info: Story = {
  decorators: dsfrDecorator('Badge - information'),
  args: {
    ...Default.args,
    severity: 'info',
  },
};

export const Warning: Story = {
  decorators: dsfrDecorator('Badge - avertissement'),
  args: {
    ...Default.args,
    severity: 'warning',
  },
};

export const New: Story = {
  decorators: dsfrDecorator('Badge - nouveauté'),
  args: {
    ...Default.args,
    severity: 'new',
  },
};

export const Custom: Story = {
  decorators: dsfrDecorator('Badge - personnalisé green-menthe'),
  args: {
    ...Default.args,
    customClass: 'fr-badge--green-menthe',
  },
};

export const Small: Story = {
  decorators: dsfrDecorator('Badge taille SM'),
  args: {
    ...Default.args,
    size: 'SM',
  },
};
