import { EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormEmailModule } from '../../forms';
import { DsfrHeadingLevelConst } from '../../shared';
import { DsfrFollowTypeConst } from './follow.model';
import { DsfrFollowComponent, DsfrFollowLinkComponent } from './index';
import { argHeadingLevel, dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Follow',
  component: DsfrFollowComponent,
  decorators: [
    moduleMetadata({
      declarations: [DsfrFollowLinkComponent],
      imports: [FormsModule, DsfrFormEmailModule],
    }),
  ],
  parameters: { actions: false },
  argTypes: {
    type: { control: { type: 'inline-radio' }, options: Object.values(DsfrFollowTypeConst) },
    headingLevel: argHeadingLevel,
    subscribe: { control: { type: EventEmitter } },
  },
};
export default meta;
type Story = StoryObj<DsfrFollowComponent>;

const TemplateWithNetworks = `<dsfr-follow [type]="type" [headingLevel]="headingLevel" [description]="description" [emailError]="emailError" [registered]="registered">
    <dsfr-follow-link name="facebook" link="https://fr-fr.facebook.com/"></dsfr-follow-link>
    <dsfr-follow-link name="twitter" link="https://twitter.com/"></dsfr-follow-link>
    <dsfr-follow-link name="instagram" link="https://www.instagram.com/"></dsfr-follow-link>
    <dsfr-follow-link name="linkedin" link="https://www.linkedin.com/"></dsfr-follow-link>
    <dsfr-follow-link name="youtube" link="https://www.youtube.com/"></dsfr-follow-link>
</dsfr-follow>`;
const description =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas varius tortor nibh, sit amet tempor nibh finibus et.';

export const Default: Story = {
  decorators: dsfrDecorator('Réseaux sociaux seuls'),
  args: {
    description: '',
    email: '',
    emailError: '',
    headingLevel: DsfrHeadingLevelConst.H2,
    registered: false,
    type: DsfrFollowTypeConst.NETWORKS,
  },
  render: (args) => ({
    props: args,
    template: TemplateWithNetworks,
  }),
};

export const Newsletter: Story = {
  decorators: dsfrDecorator("Lettre d'info seule"),
  args: {
    ...Default.args,
    type: DsfrFollowTypeConst.NEWSLETTER,
    description: description,
  },
};

export const NewsletterMail: Story = {
  decorators: dsfrDecorator("Lettre d'info seule avec formulaire"),
  args: {
    ...Default.args,
    type: DsfrFollowTypeConst.NEWSLETTER_MAIL,
    description: description,
  },
};

export const NewsletterNetworks: Story = {
  decorators: dsfrDecorator("Réseaux sociaux et Lettre d'info mise en avant"),
  args: {
    ...Default.args,
    type: DsfrFollowTypeConst.NEWSLETTER_NETWORKS,
    description: description,
  },
  render: (args) => ({
    props: args,
    template: TemplateWithNetworks,
  }),
};

export const NewsletterNetworksMail: Story = {
  decorators: dsfrDecorator("Réseaux sociaux et Lettre d'info avec formulaire"),
  args: {
    ...Default.args,
    type: DsfrFollowTypeConst.NEWSLETTER_NETWORKS_MAIL,
    description: description,
  },
  render: (args) => ({
    props: args,
    template: TemplateWithNetworks,
  }),
};

export const NewsletterNetworksMailSuccess: Story = {
  decorators: dsfrDecorator("Réseaux sociaux et Lettre d'info avec formulaire - succès"),
  args: {
    ...Default.args,
    type: DsfrFollowTypeConst.NEWSLETTER_NETWORKS_MAIL,
    description: description,
    registered: true,
  },
  render: (args) => ({
    props: args,
    template: TemplateWithNetworks,
  }),
};

export const NewsletterNetworksMailError: Story = {
  decorators: dsfrDecorator("Réseaux sociaux et Lettre d'info avec formulaire - erreur"),
  args: {
    ...Default.args,
    type: DsfrFollowTypeConst.NEWSLETTER_NETWORKS_MAIL,
    description: description,
    emailError: "Message d'erreur personnalisé",
  },
  render: (args) => ({
    props: args,
    template: TemplateWithNetworks,
  }),
};
