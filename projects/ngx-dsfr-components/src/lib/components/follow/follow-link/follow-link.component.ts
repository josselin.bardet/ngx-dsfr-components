import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrLinkTarget, I18nService } from '../../../shared';
import { DsfrFollowName } from '../follow.model';

@Component({
  selector: 'dsfr-follow-link',
  templateUrl: './follow-link.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrFollowLinkComponent {
  /** Lien obligatoire. */
  @Input() link: string;

  /** La propriété 'name' est obligatoire et doit être sélectionnée dans une liste énumérée. */
  @Input() name: DsfrFollowName;

  /** Target optionnelle, target html par défaut si non renseigné. */
  @Input() target?: DsfrLinkTarget;

  constructor(public i18n: I18nService) {}
}
