export namespace DsfrFollowNameConst {
  export const DAILYMOTION = 'dailymotion';
  export const FACEBOOK = 'facebook';
  export const GITHUB = 'github';
  export const INSTAGRAM = 'instagram';
  export const LINKEDIN = 'linkedin';
  export const MASTODON = 'mastodon';
  export const SNAPCHAT = 'snapchat';
  export const TELEGRAM = 'telegram';
  export const TIKTOK = 'tiktok';
  export const TWITCH = 'twitch';
  export const TWITTER = 'twitter';
  export const VIMEO = 'vimeo';
  export const YOUTUBE = 'youtube';
}

type Names = typeof DsfrFollowNameConst;
export type DsfrFollowName = Names[keyof Names];

export namespace DsfrFollowTypeConst {
  export const NETWORKS = 'networks';
  export const NEWSLETTER = 'newsletter';
  export const NEWSLETTER_MAIL = 'newsletter-mail';
  export const NEWSLETTER_NETWORKS = 'newsletter-networks';
  export const NEWSLETTER_NETWORKS_MAIL = 'newsletter-networks-mail';
}

type Types = typeof DsfrFollowTypeConst;
export type DsfrFollowType = Types[keyof Types];
