import { Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { DsfrFormEmailComponent } from '../../forms';
import { DsfrHeadingLevel, DsfrHeadingLevelConst, I18nService } from '../../shared';
import { DsfrFollowType, DsfrFollowTypeConst } from './follow.model';

@Component({
  selector: 'dsfr-follow',
  templateUrl: './follow.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrFollowComponent {
  /** @internal */
  @ViewChild(DsfrFormEmailComponent) emailComponent: DsfrFormEmailComponent;

  /**
   * Niveau du titre, h2 par défaut
   */
  @Input() headingLevel: DsfrHeadingLevel = DsfrHeadingLevelConst.H2;

  /** Description (pour certains types seulement) optionnelle. */
  @Input() description = '';

  /** Email par défaut. */
  @Input() email = '';

  /** Message d'erreur concernant l'email. */
  @Input() emailError = '';

  /** Indique que l'inscription a bien été prise en compte. */
  @Input() registered = false;

  /** Type du composant, réseaux sociaux seuls par défaut  */
  @Input() type: DsfrFollowType = DsfrFollowTypeConst.NETWORKS;

  /** Abonnement si l'adresse est valide. */
  @Output() subscribe = new EventEmitter<string>();

  /* Tous les types possibles du composant. */
  /** @internal */ followType = DsfrFollowTypeConst;

  /* Les entêtes. */
  /** @internal */ readonly headingNetworks = this.i18n.t('follow.networks.heading');
  /** @internal */ readonly headingNewsletter = this.i18n.t('follow.newsletter.heading');

  /* Les données du champ email. */
  /** @internal*/ readonly inputPlaceholder = this.i18n.t('follow.newsletter.input.placeholder');
  /** @internal*/ readonly inputHint = this.i18n.t('follow.newsletter.input.hint');
  /** @internal */ readonly buttonLabel = this.i18n.t('follow.newsletter.button.label');
  /** @internal */ readonly buttonTitle = this.i18n.t('follow.newsletter.button.title');

  /** @internal */
  constructor(public i18n: I18nService) {
    this.type = DsfrFollowTypeConst.NETWORKS;
  }

  /** @internal */
  onSubscribe(email: string) {
    if (this.emailComponent?.isValid()) {
      this.subscribe.emit(email);
    }
  }
}
