import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormEmailModule } from '../../forms';
import { DsfrFollowLinkComponent } from './follow-link/follow-link.component';
import { DsfrFollowComponent } from './follow.component';

@NgModule({
  declarations: [DsfrFollowComponent, DsfrFollowLinkComponent],
  exports: [DsfrFollowComponent, DsfrFollowLinkComponent],
  imports: [CommonModule, FormsModule, DsfrFormEmailModule],
})
export class DsfrFollowModule {}
