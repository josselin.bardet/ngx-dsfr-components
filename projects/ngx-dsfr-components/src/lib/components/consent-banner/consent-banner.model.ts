/**
 * Description d'une finalité.
 */
export interface DsfrSubFinality {
  /** Le nom de la finalité. */
  name: string;
  /**true: accept, false: refuse, undefined: pas de selection. */
  accept?: boolean;
  /* La finalité est exemptée de consentement.  */
  exempt?: boolean;
  /** Désactivation de la possibilité d'accepter ou refuser la finalité.  */
  disabled?: boolean;
}

/**
 * Description d'une finalité parente.
 */
export interface DsfrFinality extends DsfrSubFinality {
  /* Description. */
  description?: string;
  /* Sous-finalités. */
  subFinalities?: DsfrSubFinality[];
}
