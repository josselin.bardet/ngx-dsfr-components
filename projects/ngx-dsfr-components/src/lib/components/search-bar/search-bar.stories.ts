import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrSearchBarComponent } from './search-bar.component';
import descriptionMd from './search-bar.doc.md';

const meta: Meta = {
  title: 'COMPONENTS/SearchBar',
  component: DsfrSearchBarComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  parameters: { docs: { description: { component: descriptionMd } } },
  argTypes: {
    searchChange: { control: { type: 'EventEmitter' } },
    searchSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;

const Template: StoryFn<DsfrSearchBarComponent> = (args) => ({
  props: args,
});

/** Default */
export const Default = Template.bind({});
Default.args = {};

/** Default */
export const Large = Template.bind({});
Large.args = {
  large: true,
};
