La barre de recherche est un système de navigation qui permet à l'utilisateur d’accéder rapidement à un contenu en lançant une recherche sur un mot clé ou une expression.

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/barre-de-recherche/)

- _Module_ : `DsfrSearchModule`
- _Composant_ : `DsfrSearchComponent`
- _Tag_ : `dsfr-search`
