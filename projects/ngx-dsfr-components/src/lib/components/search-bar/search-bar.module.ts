import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrSearchBarComponent } from './search-bar.component';

@NgModule({
  declarations: [DsfrSearchBarComponent],
  exports: [DsfrSearchBarComponent],
  imports: [CommonModule, FormsModule],
})
export class DsfrSearchBarModule {}
