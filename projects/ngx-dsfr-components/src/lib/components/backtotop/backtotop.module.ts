import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrBackToTopComponent } from './backtotop.component';

@NgModule({
  declarations: [DsfrBackToTopComponent],
  exports: [DsfrBackToTopComponent],
  imports: [CommonModule],
})
export class DsfrBackToTopModule {}
