import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrBackToTopComponent } from './backtotop.component';

const meta: Meta = {
  title: 'COMPONENTS/Back to Top',
  component: DsfrBackToTopComponent,
  decorators: [moduleMetadata({ imports: [] })],
};
export default meta;
type Story = StoryObj<DsfrBackToTopComponent>;

export const Default: Story = {
  args: {},
};
