import { Component, ViewEncapsulation } from '@angular/core';
import { I18nService } from '../../shared';

@Component({
  selector: 'dsfr-backtotop',
  templateUrl: './backtotop.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrBackToTopComponent {
  constructor(/** @internal */ public i18n: I18nService) {}
}
