import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrAccordionModule } from '../accordion.module';
import { DemoAccordionComponent } from './demo-accordion.component';

const meta: Meta = {
  title: 'COMPONENTS/Accordion',
  component: DemoAccordionComponent,
  decorators: [moduleMetadata({ imports: [DsfrAccordionModule] })],
  parameters: {
    docs: {
      story: {
        inline: false,
      },
    },
  },
};
export default meta;
type Story = StoryObj<DemoAccordionComponent>;

export const ExpandViewChild: Story = {
  name: 'Expand with @ViewChild',
};
