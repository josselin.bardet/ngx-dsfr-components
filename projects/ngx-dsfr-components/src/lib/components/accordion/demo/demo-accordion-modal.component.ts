import { CommonModule } from '@angular/common';
import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { DsfrButtonModule } from '../../button';
import { DsfrModalModule } from '../../modal';
import { DsfrAccordionComponent } from '../accordion.component';
import { DsfrAccordionModule } from '../accordion.module';

@Component({
  selector: 'demo-accordion-modal',
  template: `
    <div class="sb-title">Accordéon utilisé dans une modale</div>
    <div class="sb-message">L'état <code>expanded</code> de l'accordéon est forcé à la ré-ouverture de la modale</div>
    <dsfr-button
      label="Ouvrir Ma modale"
      [variant]="'secondary'"
      ariaControls="ariaControlTest"
      [type]="'button'"></dsfr-button>
    <dsfr-modal dialogId="ariaControlTest" titleModal="Ma modale" (disclose)="onModalDisclose()">
      <dsfr-accordion [expanded]="true" #myAcc>
        <ng-container heading>Titre de l'accordéon</ng-container>
        <ng-container content>Contenu de la modale</ng-container>
      </dsfr-accordion>
    </dsfr-modal>
  `,
  styles: ['.sb-title button { margin-left: 30px; }'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, DsfrModalModule, DsfrAccordionModule, DsfrButtonModule],
})
export class DemoAccordionModalComponent {
  @ViewChild('myAcc') myAcc: DsfrAccordionComponent;

  onModalDisclose() {
    this.myAcc.expanded = true;
  }
}
