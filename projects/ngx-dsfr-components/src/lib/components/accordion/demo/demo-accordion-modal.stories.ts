import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrAccordionModule } from '../accordion.module';
import { DemoAccordionModalComponent } from './demo-accordion-modal.component';

const meta: Meta = {
  title: 'COMPONENTS/Accordion',
  component: DemoAccordionModalComponent,
  decorators: [moduleMetadata({ imports: [DsfrAccordionModule] })],
  parameters: {
    docs: {
      story: {
        inline: false,
      },
    },
  },
};
export default meta;
type Story = StoryObj<DemoAccordionModalComponent>;

export const ModalWithAccordion: Story = {};
