import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { DsfrHeadingLevel, DsfrHeadingLevelConst, newUniqueId } from '../../shared';

@Component({
  selector: 'dsfr-accordion',
  templateUrl: './accordion.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrAccordionComponent implements OnInit {
  /**
   * Contenu de l'accordéon, accepte du html, prioritaire sur le slot par défaut.
   */
  @Input() content: string;

  /**
   * Indique si l'accordéon doit être ouvert initialement.
   */
  @Input() expanded: boolean = false;

  /**
   * Le titre de l'accordéon est du texte simple.
   */
  @Input() heading: string;

  /**
   * Le niveau du titre dans la structure, ne change pas l'apparence, `<h3>` par défaut.
   */
  @Input() headingLevel: DsfrHeadingLevel = DsfrHeadingLevelConst.H3;

  /**
   * Index qui identifie l'accordéon sur la page. Identifiant unique généré par défaut.
   *
   * @deprecated (@since 1.8.0) jamais utilisé
   */
  @Input() index: string;

  /**
   * Attribut d'accessibilité pour l'entête de l'accordéon.
   */
  @Input() headingAriaLabel: string;

  /**
   * Événement présentant le nouvel état de l'accordéon.
   */
  @Output() expandedChange = new EventEmitter<boolean>();

  /** @internal */
  accordionId: string;

  constructor(private _elementRef: ElementRef) {}

  ngOnInit() {
    this.accordionId = newUniqueId();
  }

  /** @internal */
  onExpandButtonAttrChange(mutation: MutationRecord) {
    if (mutation.attributeName === 'aria-expanded') {
      const elem: HTMLElement = <HTMLElement>mutation.target;
      if (String(this.expanded) !== elem.ariaExpanded) {
        this.expanded = elem.ariaExpanded === 'true';
        this.expandedChange.emit(this.expanded);
      }
    }
  }
}
