import { expect, test } from '@playwright/test';

test('accordion.default', async ({ page }) => {
  await page.goto('?path=/story/components-accordion--default');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const heading = await frame.getByRole('button');
  await expect(heading).toHaveAttribute('aria-expanded', 'false');

  // action click
  await heading.first().click();
  await expect(heading).toHaveAttribute('aria-expanded', 'true');
});
