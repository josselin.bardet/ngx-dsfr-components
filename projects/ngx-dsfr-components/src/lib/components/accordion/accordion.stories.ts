import { argHeadingLevel, dsfrDecorator } from '.storybook/storybook-utils';
import { EventEmitter } from '@angular/core';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { HeadingModule, WatchAttrDirective } from '../../shared';
import { DsfrAccordionComponent } from './accordion.component';

const meta: Meta = {
  title: 'COMPONENTS/Accordion',
  component: DsfrAccordionComponent,
  decorators: [moduleMetadata({ imports: [HeadingModule, WatchAttrDirective] })],
  argTypes: {
    headingLevel: argHeadingLevel,
    expandedChange: { control: EventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrAccordionComponent>;

const heading = 'Intitulé accordéon';
const data = `
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing, <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">link test</a> incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing, <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">link test</a> incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.</p>
    <ul>
        <li>list item</li>
        <li>list item</li>
        <li>list item</li>
    </ul>
`;
const template = `
  <dsfr-accordion    
      [content]="content"
      [expanded]="expanded"
      [heading]="heading"
      [headingLevel]="headingLevel"
      [headingAriaLabel]="headingAriaLabel">
    ${data}
  </dsfr-accordion>`;
const templateNested1 = `
  <dsfr-accordion [content]="content"
      [expanded]="expanded"
      [heading]="heading"
      [headingLevel]="headingLevel"
      [headingAriaLabel]="headingAriaLabel">
    <ng-container content>
        <dsfr-accordion heading="Accordéon imbriqué" content="Contenu de l'accordéon imbriqué"></dsfr-accordion>
    </ng-container>
  </dsfr-accordion>`;

export const Default: Story = {
  decorators: dsfrDecorator('Accordéon simple'),
  args: {
    content: '',
    expanded: false,
    heading: heading,
    headingLevel: 'H3',
    headingAriaLabel: '',
  },
  render: (args: DsfrAccordionComponent) => ({
    props: args,
    template: template,
  }),
};

export const Nested: Story = {
  decorators: dsfrDecorator('Accordéon dans un accordéon'),
  args: {
    ...Default.args,
  },
  render: (args: DsfrAccordionComponent) => ({
    props: args,
    template: templateNested1,
  }),
};

export const TextContent: Story = {
  decorators: dsfrDecorator('Contenu sans balise HTML'),
  args: {
    ...Default.args,
    content: "Contenu de l'accordéon",
  },
  render: (args: DsfrAccordionComponent) => ({
    props: args,
    template: template,
  }),
};
