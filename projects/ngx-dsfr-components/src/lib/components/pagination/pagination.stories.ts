import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { EduPageLinkComponent } from './page-link.component';
import { DsfrPaginationComponent } from './pagination.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Pagination',
  component: DsfrPaginationComponent,
  decorators: [moduleMetadata({ declarations: [EduPageLinkComponent] })],
  argTypes: {
    // https://storybook.js.org/docs/angular/essentials/controls#conditional-controls
    currentPage: { if: { arg: 'previousOnly', truthy: false } },
    pageCount: { if: { arg: 'previousOnly', truthy: false } },
    pageSelect: { control: { type: 'EventEmitter' } },
    pageSelectEvent: { control: { type: 'EventEmitter' } },
    backSelect: { control: { type: 'EventEmitter' } },
    backEvent: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrPaginationComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Page précédente'),
  args: {
    previousOnly: true,
    currentPage: 1,
    pageCount: 132,
  },
};

export const FirstPage: Story = {
  decorators: dsfrDecorator('Première page'),
  args: {
    previousOnly: false,
    currentPage: 1,
    pageCount: 132,
  },
};

export const MiddlePage: Story = {
  decorators: dsfrDecorator('Page intermédiaire'),
  args: {
    previousOnly: false,
    currentPage: 128,
    pageCount: 132,
  },
};

export const LastPage: Story = {
  decorators: dsfrDecorator('Dernière page'),
  args: {
    previousOnly: false,
    currentPage: 132,
    pageCount: 132,
  },
};
