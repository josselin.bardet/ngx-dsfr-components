import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'edu-page-link',
  templateUrl: './page-link.component.html',
  styleUrls: ['./page-link.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EduPageLinkComponent {
  @Input() active = false;
  @Input() customClass: string;
  @Input() disabled = false;
  @Input() label: string;
  @Input() labelCustomClass: string;
  @Input() num: number;
  @Output() pageSelectEvent = new EventEmitter<number>();
  private _tooltipMessage: string;

  get tooltipMessage(): string {
    return this._tooltipMessage ? this._tooltipMessage : this.label;
  }

  @Input() set tooltipMessage(value: string) {
    this._tooltipMessage = value;
  }

  onPage(event: Event, pageNum: number) {
    event.preventDefault();
    this.pageSelectEvent.emit(pageNum);
  }
}
