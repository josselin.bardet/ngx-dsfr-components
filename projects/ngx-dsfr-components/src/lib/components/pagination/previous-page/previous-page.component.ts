import { Component, EventEmitter, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'dsfr-previous-page',
  templateUrl: './previous-page.component.html',
  encapsulation: ViewEncapsulation.None,
})
/** @deprecated @since 1.6 utiliser `dsfr-pagination` à la place. */
export class DsfrPreviousPageComponent {
  /* Evènement page précédente. L'événement initial n'est pas propagé. */
  @Output() backEvent = new EventEmitter();

  onBack() {
    this.backEvent.emit();
  }
}
