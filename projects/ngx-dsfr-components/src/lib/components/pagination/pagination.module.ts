import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EduPageLinkComponent } from './page-link.component';
import { DsfrPaginationComponent } from './pagination.component';
import { DsfrPreviousPageComponent } from './previous-page/previous-page.component';

@NgModule({
  declarations: [DsfrPaginationComponent, EduPageLinkComponent, DsfrPreviousPageComponent],
  exports: [DsfrPaginationComponent],
  imports: [CommonModule, RouterModule],
})
export class DsfrPaginationModule {}
