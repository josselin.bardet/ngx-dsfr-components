export type Interval = [number, number];

/**
 * si n <= 5   : [[1,n]],                 n=4 -> (1 2 3 4)
 *                                        n=5 -> (1 2 3 4 5)
 * si p <= 2   : [[1,3], [n, n]],    p=2, n=6 -> (1 2 3 ... 6)
 * si p >= n-3 : [min(p-1, n-2), n], p=3, n=6 -> (... 2 3 4 5 6)
 *                                   p=4, n=6 -> (... 3 4 5 6)
 *                                   p=5, n=6 -> (... 4 5 6)
 *                                   p=5, n=6 -> (... 4 5 6)
 * sinon [[p-1, p+1], [n, n]],       p=3, n=7 -> (... 2 3 4 ... 7)
 * @param p n° de la page courante, démarre à 1
 * @param n nombre de pages
 */
export function calcPages(p: number, n: number): Interval[] {
  // n <= 5
  if (n <= 5) return [[1, n]];

  // p <= 2
  if (p <= 2)
    return [
      [1, 3],
      [n, n], // on affiche toujours la dernière page
    ];

  // p >= n-3
  if (p >= n - 3) return [[Math.min(p - 1, n - 2), n]];

  // sinon
  return [
    [p - 1, p + 1],
    [n, n], // on affiche toujours la dernière page
  ];
}
