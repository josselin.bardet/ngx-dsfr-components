import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { I18nService } from '../../shared';
import { Interval, calcPages } from './pagination-calc';

@Component({
  selector: 'dsfr-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrPaginationComponent implements OnChanges {
  /* Nombre de pages du composant. */
  @Input() pageCount = 0;

  /* N° de la page active, démarre à : 1 */
  @Input() currentPage = 1;

  /** @since 1.6 Affiche uniquement l'icône d'accès à la page précédement si vrai. */
  @Input() previousOnly = false;

  /* Evènement de sélection d'une page, passe le n° de la page en paramètre. L'événement initial n'est pas propagé. */
  @Output() pageSelect = new EventEmitter<number>();

  /** @deprecated @since 1.6 utiliser `pageSelect` à la place. */
  @Output() pageSelectEvent = new EventEmitter<number>();

  /* Evènement page précédente. L'événement initial n'est pas propagé. */
  @Output() backSelect = new EventEmitter<void>();

  /** @deprecated @since 1.6 utiliser `backSelect` à la place. */
  @Output() backEvent = new EventEmitter<void>();

  /** @internal */ intervals: Interval[] = [];

  /** @internal */
  constructor(public i18n: I18nService) {}

  get prevNum(): number {
    return Math.max(1, this.currentPage - 1);
  }

  get nextNum(): number {
    return Math.min(this.currentPage + 1, this.pageCount);
  }

  ngOnChanges(changes: SimpleChanges) {
    //FIXME: RPA je me demande si la maj de this.intervals ne devrait pas être conditionnée à la détection de
    // changement sur les inputs currentPage / pageCount
    if (this.currentPage && this.pageCount) this.intervals = calcPages(this.currentPage, this.pageCount);
  }

  /** @internal */
  onBack() {
    this.backSelect.emit();
    this.backEvent.emit();
  }

  /** @internal */
  onPage(pageNum: number) {
    this.pageSelect.emit(pageNum);
    this.pageSelectEvent.emit(pageNum);
  }

  /** @internal */
  previous(): number {
    if (!this.isFirstPage()) this.onPage(this.currentPage - 1);
    return this.currentPage;
  }

  next(): number {
    if (!this.isLastPage()) this.onPage(this.currentPage + 1);
    return this.currentPage;
  }

  /** @internal */
  isFirstPage(): boolean {
    return this.pageCount === 0 || this.currentPage == 1;
  }

  /** @internal */
  isLastPage(): boolean {
    return this.pageCount === 0 || this.currentPage == this.pageCount;
  }

  /** @internal */
  pagesFrom(interval: Interval): number[] {
    const firstPage = interval[0];
    const lastPage = interval[1];
    const count = lastPage - firstPage + 1;
    return Array(count)
      .fill(0)
      .map((e, i) => i + firstPage);
  }
}
