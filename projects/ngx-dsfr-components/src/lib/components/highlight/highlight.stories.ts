import { DSFR_HIGHLIGHT } from '.storybook/dsfr-themes';
import { componentWrapperDecorator, Meta, StoryFn } from '@storybook/angular';
import { DsfrSizeConst } from '../../shared';
import { DsfrHighlightComponent } from './highlight.component';
import descriptionMd from './highlight.doc.md';

const meta: Meta = {
  title: 'COMPONENTS/Highlight',
  component: DsfrHighlightComponent,
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
  argTypes: {
    text: { if: { arg: 'text', truthy: true } },
    textSize: {
      control: { type: 'inline-radio' },
      options: Object.values(DsfrSizeConst),
    },
    customClass: { control: { type: 'select' }, options: [''].concat(Object.values(DSFR_HIGHLIGHT)) },
  },
};
export default meta;

const Template: StoryFn<DsfrHighlightComponent> = (args) => ({
  props: args,
});
Template.args = {
  textSize: DsfrSizeConst.MD,
};

/** Default */
export const Default = Template.bind({});
Default.args = {
  ...Template.args,
  text: 'Les parents d’enfants de 11 à 14 ans n’ont aucune démarche à accomplir : les CAF versent automatiquement l’ARS aux familles déjà allocataires qui remplissent les conditions.',
};
Default.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Contenu transmis via input 'text'</div>${story}`),
];

/** UsingSlot */
export const UsingSlot: StoryFn<DsfrHighlightComponent> = (args) => ({
  props: args,
  template: `
  <dsfr-highlight customClass="customClass">
    Les parents d’enfants de 11 à 14 ans n’ont aucune démarche à accomplir : les CAF versent automatiquement l’ARS aux familles déjà allocataires qui remplissent les conditions.
  </dsfr-highlight>
  `,
});
UsingSlot.args = {
  ...Template.args,
};
UsingSlot.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Contenu transmis via slot par défaut</div>${story}`),
];
