La mise en exergue (_highlight_) permet à l’utilisateur de distinguer et repérer une information facilement.

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/mise-en-exergue)

- _Module_ : `DsfrHighlightModule`
- _Composant_ : `DsfrHighlightComponent`
- _Tag_ : `dsfr-highlight`

### ☝️ Points d'attention sur l'usage du wrapper Angular

- Il est possible d'injecter le texte soit via l'input `text` soit par le slot par défaut
- Si vous passez par le slot, inutile de wrapper votre texte dans une balise `<p>` (ce serait redondant)
