import { expect, test } from '@playwright/test';

/** Default */
test('download.default', async ({ page }) => {
  await page.goto('?path=/story/components-highlight--default');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const div = await frame.locator('div.fr-highlight');
  await expect(div).toBeEnabled();
});
