import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrHighlightComponent } from './highlight.component';

@NgModule({
  declarations: [DsfrHighlightComponent],
  exports: [DsfrHighlightComponent],
  imports: [CommonModule],
})
export class DsfrHighlightModule {}
