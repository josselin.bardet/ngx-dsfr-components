import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrSize, DsfrSizeConst } from '../../shared';

@Component({
  selector: 'dsfr-highlight',
  templateUrl: './highlight.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrHighlightComponent {
  /** Contenu du composant, html accepté. */
  @Input() text: string;

  /** Choix de la taille du texte, medium par défaut. */
  @Input() textSize: DsfrSize;

  /**
   * Permet de personnaliser la couleur.
   * Il faut donner le nom de class exact (ex : fr-highlight--green-emeraude) que vous trouverez via ce [lien]
   * (@link https://gouvfr.atlassian.net/wiki/spaces/DB/pages/223019199/Mise+en+exergue+-+Highlight#Couleurs-d%E2%80%99accent)
   */
  @Input() customClass: string;

  /** @internal */
  getClasses(): { [className: string]: boolean } {
    return { 'fr-text--sm': this.textSize === DsfrSizeConst.SM, 'fr-text--lg': this.textSize === DsfrSizeConst.LG };
  }
}
