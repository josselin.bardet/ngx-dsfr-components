import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrStepperComponent } from './stepper.component';

@NgModule({
  declarations: [DsfrStepperComponent],
  exports: [DsfrStepperComponent],
  imports: [CommonModule],
})
export class DsfrStepperModule {}
