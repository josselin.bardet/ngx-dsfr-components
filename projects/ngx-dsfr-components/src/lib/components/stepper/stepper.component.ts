import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'dsfr-stepper',
  templateUrl: './stepper.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrStepperComponent {
  /** Nombre total d'étapes. */
  @Input() totalSteps = 1;

  /** Index de l'étape courante. */
  @Input() currentStep = 1;

  /** Titre de l'étape courante. */
  @Input() currentStepTitle: string;

  /** Titre de l'étape suivante. */
  @Input() nextStepTitle: string;
}
