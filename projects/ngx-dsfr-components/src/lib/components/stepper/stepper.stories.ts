import { Meta, StoryFn } from '@storybook/angular';
import { DsfrStepperComponent } from './stepper.component';
import descriptionMd from './stepper.doc.md';

const meta: Meta = {
  title: 'COMPONENTS/Stepper',
  component: DsfrStepperComponent,
  parameters: { docs: { description: { component: descriptionMd } } },
};
export default meta;

const steps = ['Formation et enseignements', 'Informations responsable', 'Informations élève', 'Validation'];
const currentStep = 1;
const nextStepTitle = currentStep < steps.length ? steps[currentStep] : undefined;

/** Default */
export const Default: StoryFn<DsfrStepperComponent> = (args) => ({
  props: args,
});
Default.args = {
  totalSteps: steps.length,
  currentStep: currentStep,
  currentStepTitle: steps[currentStep - 1],
  nextStepTitle: nextStepTitle,
};
