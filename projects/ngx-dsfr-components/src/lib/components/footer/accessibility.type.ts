export type DsfrAccessibility = 'none' | 'partially' | 'fully-compliant';
