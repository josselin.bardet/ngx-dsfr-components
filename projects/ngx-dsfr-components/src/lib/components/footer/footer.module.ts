import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrDisplayModule } from '../display';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrFooterComponent } from './footer.component';

@NgModule({
  declarations: [DsfrFooterComponent],
  exports: [DsfrFooterComponent],
  imports: [CommonModule, RouterModule, ItemLinkComponent, DsfrDisplayModule],
})
export class DsfrFooterModule {}
