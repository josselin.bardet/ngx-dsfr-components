import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { DsfrLink, DsfrLinkTarget, I18nService } from '../../shared';
import { DSFR_CONFIG_TOKEN } from '../../shared/config/config-token';
import { DsfrConfig } from '../../shared/config/config.model';
import { DISPLAY_MODAL_ID } from '../display';
import { DsfrAccessibility } from './accessibility.type';
import { DsfrFooterReboundLinks } from './footer-rebound-links.model';
import { DsfrLogo } from './logo.model';
import { DsfrPartner } from './partner.model';

@Component({
  selector: 'dsfr-footer',
  templateUrl: './footer.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrFooterComponent implements OnInit {
  /** Liens institutionnels */
  /** @internal */
  static readonly DEF_INSTITUTIONAL_LINKS: DsfrLink[] = [
    { label: 'legifrance.gouv.fr', link: 'https://legifrance.gouv.fr' },
    { label: 'gouvernement.fr', link: 'https://gouvernement.fr' },
    { label: 'service-public.fr', link: 'https://service-public.fr' },
    { label: 'data.gouv.fr', link: 'https://data.gouv.fr' },
  ];

  /** Liens réglementaires obligatoires */
  /** @internal */
  static readonly DEF_MANDATORY_LINKS: DsfrLink[] = [
    {
      label: 'Accessibilité : ',
      link: 'https://www.gouvernement.fr/accessibilite',
      linkTarget: '_blank',
    },
    { label: 'Mentions légales', link: '#' },
    {
      label: 'Données personnelles',
      link: 'https://www.gouvernement.fr/donnees-personnelles-et-cookies',
      linkTarget: '_blank',
    },
    {
      label: 'Gestion des cookies',
      link: 'https://www.gouvernement.fr/donnees-personnelles-et-cookies',
      linkTarget: '_blank',
    },
  ];

  /**
   * Affichage du lien 'Paramètre d'affichage' pour gérer les modes clair/sombre.
   */
  @Input() display: boolean;

  /**
   * Chemin vers le répertoire exposant les pictogrammes illustratifs DSFR.
   */
  @Input() artworkDirPath: string;

  /** Bloc marque de la marianne */
  @Input() logo: DsfrLogo = {
    label: 'République<br/>Française',
    link: '',
    tooltipMessage:
      "Accueil - [À MODIFIER - texte alternatif de l’image : nom de l'opérateur ou du site serviciel] - République Française",
    imagePath: '',
    imageAlt: '',
  };

  /** Texte de présentation (html possible). */
  @Input() presentation: string;

  /** Tableau des liens institutionnels - obligatoires. Initialisé par défaut. */
  @Input() institutionalLinks = DsfrFooterComponent.DEF_INSTITUTIONAL_LINKS;

  /**
   * Liens d'obligations légales. Cette liste doit être définie en fonction du site, toutefois les liens & contenus suivants
   * sont obligatoires : “accessibilité : non/partiellement/totalement conforme”, mentions légales, données personnelles et gestion des cookies. - obligatoire.
   */
  @Input() mandatoryLinks = DsfrFooterComponent.DEF_MANDATORY_LINKS;

  /** Logo partenaire principal. */
  @Input() partnerMain: DsfrPartner;

  /** Listes des logos partenaires. */
  @Input() partnersSub: DsfrPartner[];

  /** Liste de liens de rebond catégorisé. */
  @Input() reboundLinks: DsfrFooterReboundLinks[];

  /** Mention de licence. */
  @Input() license = 'Emplacement de la mention de licence - OBLIGATOIRE.';

  /**Indique le lien mandatory séléctionné. */
  @Output() mandatoryLinkSelect = new EventEmitter<string>();

  /**
   * @internal
   */
  _useDeprecatedPictoPath = false;

  /** @internal */
  constructor(@Inject(DSFR_CONFIG_TOKEN) private config: DsfrConfig, public i18n: I18nService) {}

  get pictoPath(): string {
    return this.artworkDirPath;
  }

  get displayModalId() {
    return DISPLAY_MODAL_ID;
  }

  /**
   * Chemin des pictogrammes (du composant display) renseigné par le développeur.
   *
   * Note: ce chemin doit permettre de récupérer directement les fichiers SVG suivants : moon.svg, sun.svg, system.svg
   *
   * @deprecated Use `artworkDirPath` instead.
   */
  @Input() set pictoPath(path: string) {
    this.artworkDirPath = path;
    this._useDeprecatedPictoPath = true;
  }

  /** Mention légale de la conformité d'accessibilité (none / partially / fully-compliant) */
  @Input() set accessibility(accessibility: DsfrAccessibility) {
    let labelAccessibility: string = 'Accessibilité : non conforme';
    switch (accessibility) {
      case 'fully-compliant':
        labelAccessibility = 'Accessibilité : totalement conforme';
        break;
      case 'partially':
        labelAccessibility = 'Accessibilité : partiellement conforme';
        break;
    }

    DsfrFooterComponent.DEF_MANDATORY_LINKS[0].label = labelAccessibility;
  }

  ngOnInit(): void {
    if (this.artworkDirPath === undefined) {
      this.artworkDirPath = this.config.artworkDirPath;
    }
  }

  /** @internal */
  getLinkTarget(item: DsfrLink): DsfrLinkTarget | undefined {
    return item.target ?? item.linkTarget;
  }

  /** @internal */
  onMandatoryLinkSelect(link: string) {
    this.mandatoryLinkSelect.emit(link);
  }
}
