/**
 * Représentation du bloc marque
 */
export interface DsfrLogo {
  /**
   * Label associé au bloc marque (Marianne). Respectez la structure avec les <br>.
   * (Ministère, gouvernement, république française). Par défaut  'République<br/>Française',
   */
  label?: string;
  /** Url du lien 'retour accueil' du logo de la Marianne.  */
  //TODO: implémenter la totalité de l'interface DsfrNavigation
  link: string;
  /** Title du logo */
  tooltipMessage: string;
  /** Path pour src d'image d'illustration. */
  imagePath?: string;
  /** L’alternative de l’image (attribut alt) doit impérativement être renseignée et reprendre le texte visible dans l’image **/
  imageAlt?: string;
}
