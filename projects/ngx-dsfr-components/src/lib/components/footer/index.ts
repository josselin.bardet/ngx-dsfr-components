export { DsfrAccessibility } from './accessibility.type';
export { DsfrFooterComponent } from './footer.component';
export { DsfrFooterModule } from './footer.module';
export { DsfrPartner } from './partner.model';
export { DsfrFooterReboundLinks } from './footer-rebound-links.model';
