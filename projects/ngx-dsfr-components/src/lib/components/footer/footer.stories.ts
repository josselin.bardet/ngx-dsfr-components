import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrDisplayModule } from '../display';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrFooterReboundLinks } from './footer-rebound-links.model';
import { DsfrFooterComponent } from './footer.component';
import { DsfrPartner } from './partner.model';

const meta: Meta = {
  title: 'COMPONENTS/Footer',
  component: DsfrFooterComponent,
  decorators: [
    moduleMetadata({
      imports: [FormsModule, RouterTestingModule, ItemLinkComponent, DsfrDisplayModule],
    }),
  ],
  parameters: {
    actions: { argTypesRegex: '^on.*' },
  },
  argTypes: {
    accessibility: { control: { type: 'inline-radio' } },
    mandatoryLinkSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;

const Template: StoryFn<DsfrFooterComponent> = (args) => ({
  props: args,
});

const loremIpsum =
  '<strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <em>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</em>.';
const loremIpsumShort = 'Lorem ipsum dolor [...] fugiat nulla pariatur.';

const partnerMainTest: DsfrPartner = {
  imagePath: 'img/placeholder.9x16.png',
  imageAlt: 'logo operator',
  link: '#',
};

const partnersSubTest: Array<DsfrPartner> = [
  {
    imagePath: 'img/placeholder.9x16.png',
    imageAlt: 'logo operator',
    link: '/',
  },
  {
    imagePath: 'img/placeholder.9x16.png',
    imageAlt: 'logo operator',
    link: '#',
    customHeight: '2rem',
  },
];

const reboundLinkTest: Array<DsfrFooterReboundLinks> = [
  {
    title: 'Catégorie 1',
    links: [
      { label: 'Lien de navigation', link: '#' },
      { label: 'Lien de navigation', link: '#' },
    ],
  },
  {
    title: 'Exemples',
    links: [
      { label: 'legifrance.gouv.fr', link: 'https://legifrance.gouv.fr' },
      { label: 'gouvernement.fr', link: 'https://gouvernement.fr' },
      { label: 'service-public.fr', link: 'https://service-public.fr' },
    ],
  },
  {
    title: 'Catégorie 3',
    links: [{ label: 'Lien de navigation', link: '#' }],
  },
];

/** Default */
export const Default = Template.bind({});
Default.args = {
  presentation: loremIpsum,
  institutionalLinks: DsfrFooterComponent.DEF_INSTITUTIONAL_LINKS,
  mandatoryLinks: DsfrFooterComponent.DEF_MANDATORY_LINKS,
  partnersSub: [],
  reboundLinks: [],
};

/** With Logo */
export const WithLogo = Template.bind({});
WithLogo.args = {
  presentation: loremIpsum,
  logo: {
    imagePath: 'img/placeholder.9x16.png',
    label: 'République<br>Française',
    link: '',
    tooltipMessage: '',
  },
  institutionalLinks: DsfrFooterComponent.DEF_INSTITUTIONAL_LINKS,
  mandatoryLinks: DsfrFooterComponent.DEF_MANDATORY_LINKS,
  partnersSub: [],
  reboundLinks: [],
};

/** Full */
export const Full = Template.bind({});
Full.args = {
  presentation: loremIpsum,
  logo: {
    imagePath: 'img/placeholder.9x16.png',
    label: 'République<br>Française',
    link: '',
    tooltipMessage: '',
  },
  reboundLinks: reboundLinkTest,
  license: `ici c\'est une mention de la licence <a href=\"#\">et là un lien</a>`,
  institutionalLinks: DsfrFooterComponent.DEF_INSTITUTIONAL_LINKS,
  mandatoryLinks: DsfrFooterComponent.DEF_MANDATORY_LINKS,
  partnersSub: [],
  accessibility: 'none',
};

/** With Partners */
export const WithPartners = Template.bind({});
WithPartners.args = {
  presentation: loremIpsum,
  logo: {
    imagePath: 'img/placeholder.9x16.png',
    label: 'République<br>Française',
    link: '',
    tooltipMessage: '',
  },
  institutionalLinks: DsfrFooterComponent.DEF_INSTITUTIONAL_LINKS,
  mandatoryLinks: DsfrFooterComponent.DEF_MANDATORY_LINKS,
  partnerMain: partnerMainTest,
  partnersSub: partnersSubTest,
  reboundLinks: [],
  accessibility: 'partially',
};

/** Display */
export const Display: StoryFn<DsfrFooterComponent> = (args) => ({
  props: args,
});
Display.args = {
  display: true,
};
Display.decorators = [
  componentWrapperDecorator(
    (story) => `<div class="sb-title">Avec lien d'accès aux paramètres d'affichage</div>${story}`,
  ),
];
Display.parameters = {
  docs: {
    story: {
      inline: false,
      height: 600,
    },
  },
};

/** DisplayArtworkDirPath */
export const DisplayArtworkDirPath: StoryFn<DsfrFooterComponent> = (args) => ({
  props: args,
});
DisplayArtworkDirPath.storyName = 'Display (artworkDirPath)';
DisplayArtworkDirPath.args = {
  display: true,
  artworkDirPath: 'artwork',
};
DisplayArtworkDirPath.decorators = [
  componentWrapperDecorator(
    (story) =>
      `<div class="sb-title">Avec lien d'accès aux paramètres d'affichage et <code>artworkDirPath</code> custom</div>${story}`,
  ),
];
