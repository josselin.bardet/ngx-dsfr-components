import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormCheckboxModule, DsfrFormInputModule } from '../index';
import { DsfrFormFieldsetElementDirective } from './form-fieldset-element.directive';
import { DsfrFormFieldsetComponent } from './form-fieldset.component';

const meta: Meta = {
  title: 'FORMS/Fieldset',
  component: DsfrFormFieldsetComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrFormInputModule, DsfrFormCheckboxModule],
      declarations: [DsfrFormFieldsetElementDirective],
    }),
  ],
};
export default meta;
type Story = StoryObj<DsfrFormFieldsetComponent>;
const createFieldsetWith = (htmlContent: string): string => {
  return `
<dsfr-form-fieldset
    [id]="id"
    [legend]="legend"
    [legendRegular]="legendRegular"
    [legendSrOnly]="legendSrOnly"
    [hint]="hint"
    [inline]="inline"
    [disabled]="disabled"
    [error]="error"
    [valid]="valid"
  >
  ${htmlContent}
</dsfr-form-fieldset>`;
};

const legend = 'Légende pour l’ensemble des éléments ';
const hint = 'Texte de description additionnel';

const inputContent = `
<dsfr-form-input *fieldsetElement label="Libellé champ de saisie"></dsfr-form-input>
<dsfr-form-input *fieldsetElement label="Libellé champ de saisie"></dsfr-form-input>
`;

const checkboxContent = `
<dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher "></dsfr-form-checkbox>
<dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher "></dsfr-form-checkbox>
<dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher "></dsfr-form-checkbox>
`;

const customContent = `
<dsfr-form-checkbox *fieldsetElement="'maClasse'" label="Libellé case à cocher "></dsfr-form-checkbox>
<dsfr-form-checkbox *fieldsetElement="'maClasse'" label="Libellé case à cocher "></dsfr-form-checkbox>
<dsfr-form-checkbox *fieldsetElement="'maClasse'" label="Libellé case à cocher "></dsfr-form-checkbox>
`;

export const Default: Story = {
  args: {
    legend: legend,
    hint: '',
    inline: false,
    disabled: false,
    legendRegular: false,
    legendSrOnly: false,
    error: undefined,
    valid: '',
    id: '',
  },
  render: (args) => ({
    props: args,
    template: createFieldsetWith(inputContent),
  }),
};

export const Checkboxes: Story = {
  args: {
    ...Default.args,
    hint: hint,
  },
  render: (args) => ({
    props: args,
    template: createFieldsetWith(checkboxContent),
  }),
};

export const InLine: Story = {
  args: {
    ...Default.args,
    hint: hint,
    inline: true,
  },
  render: (args) => ({
    props: args,
    template: createFieldsetWith(checkboxContent),
  }),
};

export const Error: Story = {
  args: {
    ...Default.args,
    hint: hint,
    error: 'Texte d’erreur obligatoire',
  },
  render: (args) => ({
    props: args,
    template: createFieldsetWith(checkboxContent),
  }),
};

export const Valid: Story = {
  args: {
    ...Default.args,
    hint: hint,
    valid: 'Texte de validation',
  },
  render: (args) => ({
    props: args,
    template: createFieldsetWith(checkboxContent),
  }),
};

export const Disabled: Story = {
  args: {
    ...Default.args,
    hint: hint,
    disabled: true,
  },
  render: (args) => ({
    props: args,
    template: createFieldsetWith(checkboxContent),
  }),
};

export const SrOnly: Story = {
  name: 'Legend sr-only',
  args: {
    ...Default.args,
    legendSrOnly: true,
  },
  render: (args) => ({
    props: args,
    template: createFieldsetWith(checkboxContent),
  }),
};

export const Custom: Story = {
  args: {
    ...Default.args,
    hint: hint,
  },
  render: (args) => ({
    props: args,
    template: createFieldsetWith(customContent),
  }),
};
