import { Component, Input, OnInit } from '@angular/core';
import { newUniqueId } from '../../shared';

@Component({ template: '' })
export abstract class BaseFieldsetComponent implements OnInit {
  /**
   * L'identifiant du fieldset - Optionnel (sera généré si non fourni).
   */
  @Input() id: string | undefined;

  /**
   * La légende du fieldset. Peut être indiquée via cet input ou via un slot [legend].
   */
  @Input() legend: string;

  /**
   * Permet de rétablir une graisse standard sur la légende.
   */
  @Input() legendRegular = false;

  /**
   * Permet de masquer la légende en la préservant pour les lecteurs d'écran.
   */
  @Input() legendSrOnly = false;

  /**
   * Texte de description additionnel.
   */
  @Input() hint: string;

  /**
   * Permet de désactiver l'ensemble des champs du fieldset.
   */
  @Input() disabled = false;

  /**
   * Le(s) message(s) d'erreur à afficher le cas échéant.
   */
  @Input() error: string | string[] | undefined;

  /**
   * Le message de validation à afficher le cas échéant.
   */
  @Input() valid: string;

  /** @internal */ legendId: string;
  /** @internal */ messagesId: string;

  /**
   * Retourne les erreurs sous forme de tableau même si on a juste un string
   * @return type compatible avec la propriété error, c.-à-d. undefined si non renseigné
   * @internal
   */
  get errors(): string[] | undefined {
    return this.error ? (Array.isArray(this.error) ? this.error : [this.error]) : undefined;
  }

  ngOnInit() {
    this.id ??= newUniqueId();
    this.legendId = this.id + '-legend';
    this.messagesId = this.id + '-messages';
  }

  // @since 1.8 sinon même en erreur avec la valeur []
  /** @internal */
  hasErrors(): boolean {
    const errors = this.errors;
    return errors !== undefined && errors.length > 0;
  }

  /** @internal */
  isLegendRegular(): boolean {
    return this.legendRegular || this.hasErrors() || !!this.valid;
  }
}
