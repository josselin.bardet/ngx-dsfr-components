import { Directive, Input, TemplateRef } from '@angular/core';

/**
 * Directive structurelle permettant au composant parent d'identifier les composants devant être encapsulés au sein
 * d'une structure HTML complémentaire.
 */
@Directive({
  selector: '[fieldsetElement]',
})
export class DsfrFormFieldsetElementDirective {
  /**
   * Permet d'indiquer des classes CSS additionnelles à ajouter à l'élément de fielset.
   */
  @Input() fieldsetElement: string;

  /**
   * On injecte le templateRef de manière à pouvoir le rendre accessible au contexte parent.
   * Le composant parent pourra référencer ce templateRef au sein d'une directive ngTemplateOutlet
   * de façon à projeter le contenu à l'endroit adéquat.
   */
  constructor(public templateRef: TemplateRef<unknown>) {}
}
