import { ReactiveFormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';

import { DsfrFormTelComponent } from '../form-tel.component';
import { DemoTelReactiveFormComponent } from './demo-tel-reactive-form.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Phone',
  component: DemoTelReactiveFormComponent,
  decorators: [moduleMetadata({ imports: [ReactiveFormsModule, DsfrFormTelComponent] })],
};
export default meta;
type Story = StoryObj<DemoTelReactiveFormComponent>;

export const FormControl: Story = {
  decorators: dsfrDecorator('Exemple FormControl'),
  args: {
    label: 'Label champs de numéro de téléphone',
  },
};
