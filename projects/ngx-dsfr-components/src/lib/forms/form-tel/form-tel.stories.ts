import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../components';
import { DsfrFormTelComponent } from './form-tel.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Phone',
  component: DsfrFormTelComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrButtonModule] })],
  argTypes: {
    buttonSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormTelComponent>;

/** Default */
export const Default: Story = {
  decorators: dsfrDecorator("Demande d'un numéro de téléphone"),
  args: {},
};

/** Error */
export const Error: Story = {
  decorators: dsfrDecorator("Demande d'un numéro de téléphone - Erreur"),
  args: {
    error: 'Le format de numéro de téléphone saisie n’est pas valide. Le format attendu est : (+33) 2 43 55 55 55',
  },
};

/** NgModel */
export const WithNgModel: Story = {
  decorators: dsfrDecorator("Demande d'un numéro de téléphone - NgModel"),
  args: { value: '', label: 'Numéro de téléphone' },
  render: (args) => ({
    props: { ...args },
    template: `
    <dsfr-form-tel [label]="label" [(ngModel)]="value"></dsfr-form-tel>
    <br> <span>ngModel value : {{value}}</span>
    `,
  }),
};
