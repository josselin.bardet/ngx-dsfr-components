export { DsfrFormPasswordComponent } from './form-password.component';
export { DsfrFormPasswordValidationRule } from './form-password.model';
export { DsfrFormPasswordModule } from './form-password.module';
