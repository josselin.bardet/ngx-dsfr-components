import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';
import { DsfrFormPasswordModule } from '../form-password.module';
import { DemoPasswordCreationComponent } from './demo-password-creation.component';

@Component({
  selector: 'demo-password-reactive-form',
  templateUrl: './demo-password-reactive-form.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrFormPasswordModule],
})
export class DemoPasswordReactiveFormComponent extends DemoPasswordCreationComponent {
  /** @internal */ formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    super();
    this.formGroup = this.fb.group({
      password: ['', this.passwordValidator()],
    });
  }

  /** @internal */
  // Si on gère un validator
  passwordValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return this.validate(control?.value) ? null : { format: 'incorrect' };
    };
  }
}
