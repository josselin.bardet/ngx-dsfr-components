import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DemoPasswordCreationComponent } from './demo-password-creation.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Password',
  component: DemoPasswordCreationComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
};
export default meta;
type Story = StoryObj<DemoPasswordCreationComponent>;

export const NgModel: Story = {
  decorators: dsfrDecorator("Creation d'un mot de passe"),
  args: {
    autocomplete: 'new-password',
    label: 'Créer un mot de passe',
    validationRules: [
      { message: '12 caractères minimum' },
      { message: '1 caractère spécial minimum (-_&)' },
      { message: '1 chiffre minimum' },
    ],
  },
};
