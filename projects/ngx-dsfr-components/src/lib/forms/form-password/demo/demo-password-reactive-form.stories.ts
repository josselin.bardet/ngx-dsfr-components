import { ReactiveFormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DemoPasswordReactiveFormComponent } from './demo-password-reactive-form.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Password',
  component: DemoPasswordReactiveFormComponent,
  decorators: [moduleMetadata({ imports: [ReactiveFormsModule] })],
};
export default meta;
type Story = StoryObj<DemoPasswordReactiveFormComponent>;

export const ReactiveForm: Story = {
  decorators: dsfrDecorator('Reactive Form'),
  args: {
    autocomplete: 'new-password',
    label: 'Créer un mot de passe',
    validationRules: [
      { message: '12 caractères minimum' },
      { message: '1 caractère spécial minimum (-_&)' },
      { message: '1 chiffre minimum' },
    ],
  },
};
