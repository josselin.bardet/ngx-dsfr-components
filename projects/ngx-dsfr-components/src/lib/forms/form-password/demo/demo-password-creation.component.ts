import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormPasswordValidationRule } from '../form-password.model';
import { DsfrFormPasswordModule } from '../form-password.module';

@Component({
  selector: 'demo-password-creation',
  templateUrl: './demo-password-creation.component.html',
  standalone: true,
  imports: [CommonModule, FormsModule, DsfrFormPasswordModule],
})
export class DemoPasswordCreationComponent implements OnInit {
  @Input() label: string;
  @Input() autocomplete: string;
  @Input() validationRules: DsfrFormPasswordValidationRule[] = [];

  /** @internal */ password: string;

  /** @internal */
  ngOnInit(): void {
    this.validate(this.password);
  }

  /** @internal */
  onNgModelChange() {
    this.validate(this.password);
  }

  /** @internal */
  protected validate(password: string): boolean {
    if (this.validationRules?.length < 3) return false;
    this.validationRules[0].onError = !password || password.length < 12;
    this.validationRules[1].onError = !password || password.search(/[-_&]/) === -1;
    this.validationRules[2].onError = !password || password.search(/\d/) === -1;
    return this.validationRules.find((r) => r.onError === true) === undefined;
  }
}
