import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrLinkModule } from '../../components';
import { DsfrFormPasswordComponent } from './form-password.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Password',
  component: DsfrFormPasswordComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrLinkModule] })],
  argTypes: {
    recoveryRouteSelect: { control: { type: 'EventEmitter' } },
    value: { control: { type: 'text' } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormPasswordComponent>;

const DefaultValues: Story = {
  args: {
    // DefaultValueAccessorComponent
    value: '',
    disabled: false,
    // DefaultControlComponent
    ariaControls: '',
    hint: '',
    label: '',
    name: '',
    // Password
    autocomplete: 'current-password',
    recoveryLink: '',
    recoveryRoute: '',
    recoveryRouterLink: '',
    recoveryRouterLinkActive: '',
    validationRules: [],
  },
};
const label = 'Mot de passe';
const hint = 'Texte de description additionnel';

export const Default: Story = {
  decorators: dsfrDecorator('Mot de passe de connexion'),
  args: {
    ...DefaultValues.args,
    label: label,
    recoveryRouterLink: '/recovery',
  },
};

export const Hint: Story = {
  decorators: dsfrDecorator('Mot de passe avec description'),
  args: {
    ...DefaultValues.args,
    label: label,
    hint: hint,
  },
};
