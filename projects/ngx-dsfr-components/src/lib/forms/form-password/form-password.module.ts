import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrLinkModule } from '../../components';
import { DsfrFormPasswordComponent } from './form-password.component';

@NgModule({
  declarations: [DsfrFormPasswordComponent],
  exports: [DsfrFormPasswordComponent],
  imports: [CommonModule, FormsModule, DsfrLinkModule],
})
export class DsfrFormPasswordModule {}
