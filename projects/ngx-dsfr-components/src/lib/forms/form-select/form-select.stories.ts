import { dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormSelectComponent } from './form-select.component';
import { DsfrSelectOption } from './form-select.model';

const meta: Meta = {
  title: 'FORMS/Select',
  component: DsfrFormSelectComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  parameters: { actions: false },
  argTypes: {
    value: { control: { type: 'text' } },
    selectChange: { control: { type: 'EventEmitter' } },
  },
};
export default meta;

type Story = StoryObj<DsfrFormSelectComponent>;

const label = 'Label pour liste déroulante';
const options: DsfrSelectOption[] = [
  { label: 'Option 1', value: 1 },
  { label: 'Option 2', value: 2 },
  { label: 'Option 3', value: 3 },
  { label: 'Option 4', value: 4 },
];

/** Default */
export const Default: Story = {
  decorators: dsfrDecorator('Liste déroulante par défaut'),
  args: {
    label: label,
    options: options,
  },
};

/** Disabled */
export const Disabled: Story = {
  decorators: dsfrDecorator('Liste déroulante désactivée'),
  args: { label: label, options: options, disabled: true },
};

/** OptionDisabled */
export const OptionDisabled: Story = {
  decorators: dsfrDecorator('Liste déroulante avec une option désactivée'),
  argTypes: {
    value: { control: { type: 'number' } },
  },
  args: {
    label: label,
    options: [
      { label: 'Option 1', value: 1, disabled: true },
      { label: 'Option 2', value: 2 },
      { label: 'Option 3', value: 3 },
      { label: 'Option 4', value: 4 },
    ],
  },
};

/** Hint */
export const Hint: Story = {
  decorators: dsfrDecorator('Liste déroulante avec texte de description'),
  args: {
    label: label,
    options: options,
    hint: 'Texte de description additionnel',
  },
};

/** Valid */
export const Valid: Story = {
  decorators: dsfrDecorator('Liste déroulante valide'),
  args: {
    label: label,
    options: options,
    valid: 'Texte de validation',
  },
};

/** Error */
export const Error: Story = {
  decorators: dsfrDecorator('Liste déroulante erreur'),
  args: {
    label: label,
    options: options,
    error: 'Texte d’erreur obligatoire',
  },
};

/** Placeholder */
export const Placeholder: Story = {
  decorators: dsfrDecorator('Liste déroulante avec placeholder'),
  args: {
    label: label,
    options: options,
    placeholder: 'Choisir une option ...',
  },
};
Placeholder.decorators = dsfrDecorator('Liste déroulante avec placeholder');

/** InitialValue */
export const InitialValue: Story = {
  decorators: dsfrDecorator('Liste déroulante avec valeur initiale'),
  args: {
    label: label,
    options: options,
    value: 2,
  },
};

/** Group */
// cf. https://developer.mozilla.org/en-US/docs/Web/HTML/Element/optgroup
export const Group: Story = {
  decorators: dsfrDecorator('Liste déroulante avec groupes'),
  args: {
    label: label,
    options: [
      {
        label: 'Theropods',
        options: [
          { label: 'Tyrannosaurus', value: 'T-T' },
          { label: 'Velociraptor', value: 'T-V' },
          { label: 'Deinonychus', value: 'T-D' },
        ],
      },
      {
        label: 'Sauropods',
        options: [
          { label: 'Deinonychus', value: 'S-T' },
          { label: 'Saltasaurus', value: 'S-S' },
          { label: 'Apatosaurus', value: 'S-A' },
        ],
      },
    ],
    value: 'T-D',
  },
};

/** Slot Label */
export const SlotLabel: Story = {
  decorators: dsfrDecorator('Liste déroulante avec label dans un slot'),
  args: { options: options },
  render: (args) => ({
    props: args,
    template: `
    <dsfr-form-select [error]="error" [valid]="valid" [placeholder]="placeholder" [label]="label" [options]="options">
        <span label>Label (dans un span) pour liste déroulante</span>
    </dsfr-form-select>`,
  }),
};

export const WithNgModel: Story = {
  name: 'NgModel',
  decorators: dsfrDecorator('Liste déroulante avec NgModel'),
  args: { options: options, value: 1 },
  render: (args) => ({
    props: { ...args },
    template: `
         <dsfr-form-select [options]="options" [placeholder]="placeholder" [(ngModel)]="value" [label]="label"></dsfr-form-select>
         <p>Sélection : {{ value}}</p>
       `,
  }),
};

const optionsNoSelection: DsfrSelectOption[] = [
  { label: 'Choisir une catégorie', value: '' },
  { label: 'Catégorie 1', value: '1' },
  { label: 'Catégorie 2', value: '2' },
  { label: 'Catégorie 3', value: '3' },
];

export const NoSelectionOption: Story = {
  name: 'Deselectable',
  decorators: dsfrDecorator('Liste déroulante dé-sélectionnable'),
  args: { options: optionsNoSelection },
  render: (args) => ({
    props: { ...args },
    template: `
         <dsfr-form-select [options]="options" 
                           [placeholder]="'Choisir une catégorie'" 
                           [(ngModel)]="value" 
                           [label]="label"></dsfr-form-select>
         <p>ngModel value : <code>{{ value | json }}</code></p>
       `,
  }),
};
