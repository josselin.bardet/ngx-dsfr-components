import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DsfrFormSelectComponent } from './form-select.component';

@NgModule({
  declarations: [DsfrFormSelectComponent],
  exports: [DsfrFormSelectComponent],
  imports: [CommonModule, FormsModule],
})
export class DsfrFormSelectModule {}
