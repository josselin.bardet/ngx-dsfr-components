import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../../components';
import { DsfrSelectOption } from '../form-select.model';
import { DsfrFormSelectModule } from '../form-select.module';

@Component({
  selector: 'demo-select',
  templateUrl: './demo-form-select.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, FormsModule, DsfrFormSelectModule, DsfrButtonModule],
})
export class DemoFormSelectComponent implements OnInit {
  @Input() showNg: boolean = false;
  formGroup: FormGroup;
  monModel = '1';

  readonly options: DsfrSelectOption[] = [
    { label: 'Option 1', value: '1' },
    { label: 'Option 2', value: '2' },
    { label: 'Option 3', value: '3' },
    { label: 'Option 4', value: '4' },
  ];

  readonly optionsObjects: DsfrSelectOption[] = [
    { label: 'Option 1', value: { id: '1', label: 'test' } },
    { label: 'Option 2', value: { id: '2', label: 'tteest' } },
    { label: 'Option 3', value: { id: '3', label: 'test' } },
    { label: 'Option 4', value: { id: '4', label: 'test' } },
  ];

  optionsNumbers: DsfrSelectOption[];

  listOptions: any[] = [];

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      simple: [],
      withObjects: [],
      withNumbers: [0],
    });

    for (let i = 0; i < 500; i++) {
      this.optionsObjects.push({ label: 'Option ' + i, value: { id: i, label: 'test' } });
    }
    this.formGroup.controls.simple.setValue('2');
    this.formGroup.controls.withObjects.setValue({ id: 2 });
    setTimeout(() => {
      this.optionsNumbers = [
        { label: 'Option 0', value: 0 },
        { label: 'Option 1', value: 1 },
        { label: 'Option 2', value: 2 },
        { label: 'Option 3', value: 3 },
      ];
    }, 500);
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
