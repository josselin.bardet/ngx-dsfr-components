import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../../components';
import { DsfrFormCheckboxModule } from '../form-checkbox.module';

@Component({
  selector: 'demo-checkbox',
  templateUrl: './demo-checkbox.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrFormCheckboxModule, DsfrButtonModule],
})
export class DemoCheckboxComponent {
  @Input() label: string;

  /** @internal */
  formGroup: FormGroup;

  private _disabled = false;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      notifications: [false],
    });
  }

  get disabled(): boolean {
    return this._disabled;
  }

  @Input() set disabled(value: boolean) {
    this._disabled = value;
    const control = this.formGroup.controls['notifications'];
    if (this._disabled) control?.disable();
    else control?.enable();
  }

  /** @internal */
  onSubmit() {
    alert(`${this.label} : ${this.getControlValue('notifications')}`);
  }

  private getFormControl(name: string): AbstractControl<any> {
    return this.formGroup.controls[name];
  }

  private getControlValue(name: string) {
    return this.getFormControl(name)?.value;
  }
}
