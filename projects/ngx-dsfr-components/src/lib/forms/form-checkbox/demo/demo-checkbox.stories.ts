import { dsfrDecorator } from '.storybook/storybook-utils';
import { ReactiveFormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../../components';
import { DsfrFormCheckboxComponent } from '../form-checkbox.component';
import { DsfrFormCheckboxModule } from '../form-checkbox.module';
import { DemoCheckboxComponent } from './demo-checkbox.component';

const meta: Meta = {
  title: 'FORMS/Checkbox',
  component: DemoCheckboxComponent,
  decorators: [moduleMetadata({ imports: [ReactiveFormsModule, DsfrFormCheckboxModule, DsfrButtonModule] })],
  parameters: {
    docs: {
      story: {
        inline: false,
        height: 140,
      },
    },
  },
};
export default meta;
type Story = StoryObj<DsfrFormCheckboxComponent>;

export const ReactiveForm: Story = {
  decorators: dsfrDecorator('Case à cocher dans un formulaire réactif'),
  args: {
    disabled: false,
    label: 'Recevoir les notifications',
  },
};
