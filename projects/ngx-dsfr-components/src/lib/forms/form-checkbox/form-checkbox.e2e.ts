import { expect, FrameLocator, Locator, test } from '@playwright/test';

test('checkbox.default', async ({ page }) => {
  await page.goto('?path=/story/forms-checkbox--default');
  const frameLocator: FrameLocator = page.frameLocator('#storybook-preview-iframe');

  const inputLocator: Locator = await frameLocator.locator('input[type=checkbox]');

  expect(inputLocator).toBeDefined();

  const checkedAttr = await inputLocator.getAttribute('checked');
  expect(checkedAttr).toBeNull();

  const idAttr = await inputLocator.getAttribute('id');
  expect(idAttr.length).toBeDefined();
});

test('checkbox.click', async ({ page }) => {
  await page.goto('?path=/story/forms-checkbox--default');
  // Switch the context to the iframe by its name or CSS selector
  const iframeElement = await page.$('#storybook-preview-iframe');
  const frame = await iframeElement.contentFrame();
  // Perform actions within the iframe
  const label = await frame.$('label');
  const input = await frame.$('input[type=checkbox]');
  let checkedProp = await input.getProperty('checked');
  let checkedValue = await checkedProp.jsonValue();
  await expect(checkedValue).toBe(false);
  expect(await input.isChecked()).toBeFalsy();
  /*
  L'instruction ci-desous part en timeout sans doute à cause au markup/style DSFR, en 1.8.5 le label couvre l'input
  Cel semble différent en 1.9 à tester
  */
  // await input.check();
  await label.click();
  checkedProp = await input.getProperty('checked');
  checkedValue = await checkedProp.jsonValue();
  expect(await input.isChecked()).toBeTruthy();
  await expect(checkedValue).toBe(true);
});

test('checkbox.checked', async ({ page }) => {
  await page.goto('?path=/story/forms-checkbox--checked');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const input = await frame.locator('input[type=checkbox]');
  await expect(input).toBeDefined();

  const attribute = await input.getAttribute('checked');
  expect(attribute).toBeDefined();
});
