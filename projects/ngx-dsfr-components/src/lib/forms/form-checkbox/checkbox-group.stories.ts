import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormFieldsetComponent, DsfrFormFieldsetModule } from '../fieldset';
import { DsfrFormCheckboxModule } from '../form-checkbox';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta<DsfrFormFieldsetComponent> = {
  title: 'FORMS/Checkbox Group',
  component: DsfrFormFieldsetComponent,
  decorators: [
    moduleMetadata({
      imports: [FormsModule, DsfrFormFieldsetModule, DsfrFormCheckboxModule],
    }),
  ],
  parameters: { actions: false },
};
export default meta;
type Story = StoryObj<DsfrFormFieldsetComponent>;

const legend = 'Légende pour l’ensemble des éléments';
const template = `
<dsfr-fieldset
  [id]="inputId"
  [disabled]="disabled"
  [error]="error"
  [hint]="hint"
  [inline]="inline"
  [legend]="legend"
  [legendRegular]="legendRegular"
  [legendSrOnly]="legendSrOnly"
  [valid]="valid"
  [legend]="legend"
  [error]="error"
  [valid]="valid">
  <dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher"></dsfr-form-checkbox>
  <dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher" [value]="true"></dsfr-form-checkbox>
  <dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher"></dsfr-form-checkbox>
</dsfr-fieldset>
`;
const template_small = `
<dsfr-fieldset
  [id]="inputId"
  [disabled]="disabled"
  [error]="error"
  [hint]="hint"
  [inline]="inline"
  [legend]="legend"
  [legendRegular]="legendRegular"
  [legendSrOnly]="legendSrOnly"
  [valid]="valid"
  [legend]="legend"
  [error]="error"
  [valid]="valid">
  <dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher" [small]="true"></dsfr-form-checkbox>
  <dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher" [small]="true" [value]="true"></dsfr-form-checkbox>
  <dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher" [small]="true"></dsfr-form-checkbox>
</dsfr-fieldset>
`;
const template_hint = `
<dsfr-fieldset
  [id]="inputId"
  [disabled]="disabled"
  [error]="error"
  [hint]="hint"
  [inline]="inline"
  [legend]="legend"
  [legendRegular]="legendRegular"
  [legendSrOnly]="legendSrOnly"
  [valid]="valid"
  [legend]="legend"
  [error]="error"
  [valid]="valid">
  <dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher" hint="Texte de description additionnel"></dsfr-form-checkbox>
  <dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher" hint="Texte de description additionnel" [value]="true"></dsfr-form-checkbox>
  <dsfr-form-checkbox *fieldsetElement label="Libellé case à cocher" hint="Texte de description additionnel"></dsfr-form-checkbox>
</dsfr-fieldset>
`;

/** Default */
export const Default: Story = {
  decorators: dsfrDecorator('Ensemble de cases à cocher'),
  args: {
    legend: legend,
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

/** Small */
export const Small: Story = {
  decorators: dsfrDecorator('Ensemble de cases à cocher, petite taille'),
  args: {
    legend: legend,
  },
  render: (args) => ({
    props: args,
    template: template_small,
  }),
};

/** Disabled */
export const Disabled: Story = {
  decorators: dsfrDecorator('Ensemble de cases à cocher désactivées'),
  args: {
    legend: legend,
    disabled: true,
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

/** Inline */
export const Inline: Story = {
  decorators: dsfrDecorator('Ensemble de cases à cocher en ligne'),
  args: {
    legend: legend,
    inline: true,
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

/** Hint */
export const Hint: Story = {
  decorators: dsfrDecorator('Ensemble de cases à cocher avec texte d‘aide'),
  args: {
    legend: legend,
  },
  render: (args) => ({
    props: args,
    template: template_hint,
  }),
};

/** Error */
export const Error: Story = {
  decorators: dsfrDecorator('Ensemble de cases à cocher avec erreur'),
  args: {
    legend: legend,
    error: 'Texte d’erreur obligatoire',
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

/** Valid */
export const Valid: Story = {
  decorators: dsfrDecorator('Ensemble valide de cases à cocher'),
  args: {
    legend: legend,
    valid: 'Texte de validation',
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};
