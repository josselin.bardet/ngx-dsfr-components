import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormCheckboxComponent } from './form-checkbox.component';

@NgModule({
  declarations: [DsfrFormCheckboxComponent],
  exports: [DsfrFormCheckboxComponent],
  imports: [CommonModule, FormsModule],
})
export class DsfrFormCheckboxModule {}
