import { Component, ElementRef, forwardRef, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DefaultControlComponent } from '../../shared';

@Component({
  selector: 'dsfr-form-checkbox',
  templateUrl: './form-checkbox.component.html',
  styleUrls: ['./form-checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrFormCheckboxComponent),
      multi: true,
    },
  ],
})
/*
 * - fix 1.6 : La div messages est toujours présente même dans un groupe (cf. DSFR 1.9)
 */
export class DsfrFormCheckboxComponent extends DefaultControlComponent<boolean> implements OnInit {
  /**
   * Message d'erreur, quand il est présent les couleurs du contrôle changent.
   */
  @Input() error: string;

  /**
   * Message de validation, quand il est présent les couleurs du contrôle changent.
   */
  @Input() valid: string;

  /**
   * Donne une taille de 16px au lieu de 24px.
   */
  @Input() small = false;

  /** @internal */ defaultChecked: boolean; // Initialisé à la première valeur de 'value'. Permet de positionner l'attribut 'checked'
  /** @internal */ messagesGroupId: string;

  constructor(private elementRef: ElementRef) {
    super();
    this.value = false;
  }

  /** @since 1.5 */
  get checked(): boolean {
    return this.value ?? false;
  }

  /** @since 1.5 */
  get indeterminate(): boolean {
    const nativeElt = this.elementRef.nativeElement;
    const checkbox = nativeElt.querySelector('input');
    return checkbox.indeterminate;
  }

  /** @since 1.4 */
  @Input() set indeterminate(value: boolean) {
    const nativeElt = this.elementRef.nativeElement;
    const checkbox = nativeElt.querySelector('input');
    if (value !== checkbox.indeterminate) {
      checkbox.indeterminate = value;
      checkbox.classList.toggle('indeterminate');
    }
  }

  /** @internal */
  ngOnInit() {
    super.ngOnInit();
    this.defaultChecked = super.value ?? false;
    this.messagesGroupId = `${this.inputId}-messages`;
  }
}
