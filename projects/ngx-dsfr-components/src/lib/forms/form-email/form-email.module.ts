import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../components';
import { DsfrFormEmailComponent } from './form-email.component';

@NgModule({
  declarations: [DsfrFormEmailComponent],
  exports: [DsfrFormEmailComponent],
  imports: [CommonModule, FormsModule, DsfrButtonModule],
})
export class DsfrFormEmailModule {}
