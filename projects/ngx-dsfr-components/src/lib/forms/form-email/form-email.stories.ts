import { EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../components';
import { DsfrFormEmailComponent } from './form-email.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Email',
  component: DsfrFormEmailComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrButtonModule] })],
  argTypes: {
    buttonSelect: { control: { type: EventEmitter } },
    inputWrapMode: { control: { type: 'inline-radio' } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormEmailComponent>;

const defaultInput: Story = {
  args: {
    // Value Accessor
    disabled: false,
    value: undefined,
    // DefaultControl
    ariaControls: '',
    inputId: '',
    hint: undefined,
    label: '',
    name: '',
    // FormInput
    ariaAutocomplete: '',
    ariaExpanded: false,
    ariaLabel: '',
    autocomplete: '',
    autoCorrect: true,
    role: '',
    error: '',
    valid: '',
    required: false,
    placeHolder: '',
    min: '',
    max: '',
    minLength: undefined,
    maxLength: undefined,
    spellCheck: true,
    icon: undefined,
    rows: undefined,
    pattern: '',
    customClass: '',
    inputWrapMode: 'addon',
    buttonAriaLabel: '',
    buttonDisabled: false,
    buttonIcon: undefined,
    buttonLabel: '',
    buttonTooltipMessage: '',
    buttonType: undefined,
    buttonVariant: 'primary',
  },
};

export const Default: Story = {
  decorators: dsfrDecorator("Demande d'une adresse électronique"),
  args: {
    ...defaultInput.args,
  },
};

export const Error: Story = {
  decorators: dsfrDecorator("Demande d'une adresse électronique - Erreur"),
  args: {
    ...Default.args,
    error: 'Le format de l’adresse électronique saisie n’est pas valide. Le format attendu est : nom@exemple.org',
  },
};

export const Hint: Story = {
  decorators: dsfrDecorator('Demande et indication de traitement d’une adresse électronique'),
  args: {
    ...Default.args,
    hint: 'Cette adresse est utilisée uniquement pour la connexion au service. Format attendu : nom@domaine.fr',
  },
};

export const NgModel: Story = {
  name: 'NgModel',
  decorators: dsfrDecorator('Utilisation de NgModel'),
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-form-email
    [disabled]= "disabled"
    [ariaControls]= "ariaControls"
    [inputId]= "inputId"
    [hint]= "hint"
    [label]= "label"
    [name]= "name"
    [ariaAutocomplete]= "ariaAutocomplete"
    [ariaExpanded]= "ariaExpanded"
    [ariaLabel]= "ariaLabel"
    [autocomplete]= "autocomplete"
    [autoCorrect]="autoCorrect"
    [role]= "role"
    [error]= "error"
    [valid]= "valid"
    [required]= "required"
    [placeHolder]= "placeHolder"
    [min]= "min"
    [max]= "max"
    [minLength]= "minLength"
    [maxLength]= "maxLength"
    [spellCheck]="spellCheck"
    [icon]= "icon"
    [rows]= "rows"
    [pattern]= "pattern"
    [customClass]= "customClass"
    [inputWrapMode]="inputWrapMode"
    [buttonAriaLabel]= "buttonAriaLabel"
    [buttonDisabled]= "buttonDisabled"
    [buttonIcon]= "buttonIcon"
    [buttonLabel]= "buttonLabel"
    [buttonTooltipMessage]= "buttonTooltipMessage"
    [buttonType]= "buttonType"
    [buttonVariant]="buttonVariant"
    [(ngModel)]="value" ></dsfr-form-email>
    
<div class="sb-smaller">Valeur du contrôle : {{ value }}</div>`,
  }),
};
