import { Component, forwardRef, HostListener, OnInit, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { I18nService, isEmailValid } from '../../shared';
import { DsfrFormInputComponent, DsfrInputTypeConst } from '../form-input';

@Component({
  selector: 'dsfr-form-email',
  templateUrl: '../form-input/form-input.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrFormEmailComponent),
      multi: true,
    },
  ],
})
/*
 * Saisie d'une adresse email, si la valeur est validée en sortie de champ, elle est passée en minuscules.
 */
export class DsfrFormEmailComponent extends DsfrFormInputComponent implements OnInit {
  /** @internal */ readonly errorMessage = this.i18n.t('email.error');

  private _isValid: boolean;

  /** @internal */
  constructor(private i18n: I18nService) {
    super();
  }

  /**
   * Au fil de la saisie, on ne vérifie le mail que s'il y avait déjà une erreur
   */
  /** @internal */
  @HostListener('input')
  onValueChange() {
    if (this.error) this.validate();
  }

  /**
   * Vérification quand on sort du champ
   */
  /** @internal */
  @HostListener('change')
  onFocusOut() {
    if (this.validate()) this.value = this.value?.toLowerCase();
  }

  /**
   * Vrai si l'email est validé.
   */
  /** @internal */
  isValid(): boolean {
    this._isValid ??= this.validate();
    return this._isValid;
  }

  /** @internal*/
  ngOnInit() {
    super.ngOnInit();
    this.type = DsfrInputTypeConst.EMAIL;
    this.spellCheck = false;
    this.label ??= this.i18n.t('email.label');
    this.hint ??= this.i18n.t('email.hint');
    this.autocomplete = 'email'; // @since 1.6
    this.autoCorrect = false; // @since 1.6
  }

  /**
   * Méthode validation, positionne la propriété error s'il y a lieu.
   */
  private validate(): boolean {
    // Validators.email ne marche pas très bien dans la version 14, en effet 'nom@d' est validé
    // TODO Vérifier le validator d'email d'Angular 15
    this._isValid = isEmailValid(this.value, this.pattern);
    this.error = this._isValid ? '' : this.errorMessage;
    return this._isValid;
  }
}
