import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DsfrFormEmailModule } from '../form-email.module';

@Component({
  selector: 'demo-email-reactive',
  templateUrl: './demo-email-reactive.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrFormEmailModule],
})
export class DemoEmailReactiveComponent {
  /** @internal */ formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      email: '',
    });
  }

  /** @internal */
  getControlValue() {
    return this.formGroup.controls['email']?.value;
  }
}
