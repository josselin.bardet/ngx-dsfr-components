import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrUploadComponent } from './upload.component';
import { argEventEmitter, controlDisable, dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Upload',
  component: DsfrUploadComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  argTypes: {
    id: controlDisable,
    fileSelect: argEventEmitter,
  },
};
export default meta;
type Story = StoryObj<DsfrUploadComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Bouton upload 1 seul fichier'),
  args: {
    label: '', // label par défaut
    hint: "Texte d'aide qui précise les contraintes au niveau du ou des fichiers attendus",
    error: '',
    multiple: false,
    accept: '',
    disabled: false,
  },
};

export const FileFormat: Story = {
  decorators: dsfrDecorator('Bouton upload 1 seul fichier'),
  args: {
    ...Default.args,
    hint: 'Formats supportés : jpeg, jpg, png, pdf',
    accept: '.jpeg, .jpg, .png, .pdf',
  },
};

export const Error: Story = {
  decorators: dsfrDecorator('Bouton upload avec erreur'),
  args: {
    ...Default.args,
    error: 'Format de fichier non supporté',
  },
};

export const Multiple: Story = {
  decorators: dsfrDecorator('Bouton upload fichiers multiples'),
  args: {
    ...Default.args,
    hint: 'Sélectionnez un ou plusieurs fichers',
    multiple: true,
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorator('Bouton upload désactivé'),
  args: {
    ...Default.args,
    disabled: true,
  },
};

export const NgModel: Story = {
  name: 'NgModel',
  decorators: dsfrDecorator('Bouton upload avec ngModel'),
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-form-upload
  [label]="label"
  [hint]="hint"
  [error]="error"
  [multiple]="multiple"
  [accept]="accept"
  [disabled]="disabled"
  [(ngModel)]="value"></dsfr-form-upload>
<div class="sb-smaller">Valeur du modèle : {{ value }}</div>`,
  }),
};
