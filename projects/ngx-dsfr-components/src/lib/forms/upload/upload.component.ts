import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DefaultControlComponent, I18nService, newUniqueId } from '../../shared';

@Component({
  selector: 'dsfr-form-upload, dsfr-upload',
  templateUrl: './upload.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrUploadComponent),
      multi: true,
    },
  ],
})
export class DsfrUploadComponent extends DefaultControlComponent<FileList> implements OnInit, OnChanges {
  /** Texte d'erreur. */
  @Input() error: string;

  /** Permet l'ajout de plusieurs fichiers à la fois. */
  @Input() multiple = false;

  /** Spécifie un filtre pour les types de fichiers que l'utilisateur peut sélectionner (`@since 1.9.0`). */
  @Input() accept: string;

  /** Evénement émis à la sélection d'un ou plusieurs fichiers. */
  @Output()
  fileSelect = new EventEmitter<FileList>();

  /** @internal */ messagesGroupId: string;

  constructor(private i18n: I18nService) {
    super();
  }

  /** @deprecated (@since 1.4.0) utiliser hint à la place. */
  get description(): string | undefined {
    return this.hint;
  }

  /**
   * @deprecated (@since 1.4.0) utiliser hint à la place.
   * Description pour l'upload (précise les contraintes au niveau du ou des fichiers attendus : format, poids attendus, nombre de fichiers possible…).
   */
  /** @internal */
  @Input() set description(value: string | undefined) {
    this.hint = value;
  }

  /** @internal */
  ngOnInit() {
    super.ngOnInit();
    this.name ??= 'file-upload'; // 'file-upload' est la valeur statique jusqu'en 1.3
    this.messagesGroupId = newUniqueId();
  }

  /** @internal */
  ngOnChanges(changes: SimpleChanges) {
    if ([changes['label']]) this.label ||= this.i18n.t('upload.label'); // default
    if ([changes['hint']]) this.hint ||= this.i18n.t('upload.hint'); // default
  }

  /** @internal */
  onFileChange(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.value = target.files ?? undefined;
    if (this.value) this.fileSelect.emit(this.value);
  }

  /** @internal */
  hasMessages(): boolean {
    return !!this.error;
  }
}
