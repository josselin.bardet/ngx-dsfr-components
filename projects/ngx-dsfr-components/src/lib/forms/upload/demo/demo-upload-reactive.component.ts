import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DsfrUploadModule } from '../upload.module';

@Component({
  selector: 'demo-upload-reactive',
  templateUrl: './demo-upload-reactive.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrUploadModule],
})
export class DemoUploadReactiveComponent {
  @Input() label: string;
  @Input() hint: string | undefined;
  @Input() error: string;
  @Input() multiple: boolean;
  @Input() accept: string;
  @Input() disabled: boolean;

  /** @internal */ formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      files: '',
    });
  }

  /** @internal */
  getControlValue() {
    return this.formGroup.controls['files']?.value;
  }

  /** @internal */
  getControlType(): string {
    const value = this.getControlValue();
    return typeof value;
  }
}
