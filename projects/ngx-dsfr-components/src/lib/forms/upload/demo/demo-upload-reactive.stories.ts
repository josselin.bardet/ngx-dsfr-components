import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrUploadModule } from '../upload.module';
import { DemoUploadReactiveComponent } from './demo-upload-reactive.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Upload',
  component: DemoUploadReactiveComponent,
  decorators: [moduleMetadata({ imports: [DsfrUploadModule] })],
};
export default meta;
type Story = StoryObj<DemoUploadReactiveComponent>;

export const ReactiveForm: Story = {
  decorators: dsfrDecorator('Bouton upload dans un ReactiveForm'),
  args: {
    label: '', // label par défaut
    hint: "Texte d'aide qui précise les contraintes au niveau du ou des fichiers attendus",
    error: '',
    multiple: false,
    accept: '',
    disabled: false,
  },
};
