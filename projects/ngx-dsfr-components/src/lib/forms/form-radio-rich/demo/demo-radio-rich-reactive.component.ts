import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DemoRadioReactiveComponent } from '../../form-radio/demo/demo-radio-reactive.component';
import { DsfrFormRadioRichModule } from '../form-radio-rich.module';

@Component({
  selector: 'demo-radio-rich-reactive',
  templateUrl: './demo-radio-rich-reactive.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrFormRadioRichModule],
})
export class DemoRadioRichReactiveComponent extends DemoRadioReactiveComponent {}
