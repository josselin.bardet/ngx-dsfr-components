/** Boutons radios avec illustrations */
import { DsfrRadio } from '../form-radio';

export interface DsfrRadioRich extends DsfrRadio {
  /** Chemin de l'image. */
  imagePath?: string;
  /** Texte alternatif pour l'image. */
  imageAlt?: string;
  /** Chemin relatif à artworkDirPath dénotant le fichier d'illustration à utiliser. */
  artworkFilePath?: string;
}
