import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { PictogramModule } from '../../shared';
import { DsfrFormFieldsetModule } from '../fieldset';
import { DsfrFormRadioRichComponent } from './form-radio-rich.component';
import { DsfrRadioRich } from './form-radio-rich.model';

const meta: Meta = {
  title: 'FORMS/Radio Rich',
  component: DsfrFormRadioRichComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrFormFieldsetModule, PictogramModule] })],
  argTypes: {
    value: { control: { type: 'text' } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormRadioRichComponent>;

const legend = 'Légende pour l’ensemble des champs';
const hint = 'Texte de description additionnel';
const error = 'Texte d’erreur obligatoire';
const valid = 'Texte de validation';
const labelRadio = 'Libellé bouton radio ';
const artworkFilePath = 'artwork/pictograms/buildings/city-hall.svg';
const options: DsfrRadioRich[] = [
  { label: labelRadio + 1, value: '1', artworkFilePath: artworkFilePath },
  { label: labelRadio + 2, value: '2', artworkFilePath: artworkFilePath },
  { label: labelRadio + 3, value: '3', imagePath: 'img/placeholder.1x1.png', imageAlt: 'illustration' },
];
const textOptions: DsfrRadioRich[] = [
  { label: labelRadio + 1, value: '1' },
  { label: labelRadio + 2, value: '2' },
  { label: labelRadio + 3, value: '3' },
];
const hintOptions: DsfrRadioRich[] = [
  { label: labelRadio + 1, value: '1', hint: hint, artworkFilePath: artworkFilePath },
  { label: labelRadio + 2, value: '2', hint: hint, artworkFilePath: artworkFilePath },
  { label: labelRadio + 3, value: '3', hint: hint, imagePath: 'img/placeholder.1x1.png', imageAlt: 'illustration' },
];

const defaultRadioArgs = {
  // DefaultValueAccessorComponent default values
  disabled: false,
  value: undefined,
  //DefaultControlComponent default values
  ariaControls: '',
  hint: '',
  inputId: '',
  label: '',
  // DefaultRadioComponent
  inline: false,
  error: '',
  valid: '',
  required: false,
  legendRegular: false,
};

export const Default: Story = {
  args: {
    ...defaultRadioArgs,
    legend: legend,
    name: 'radioRichDefault',
    options: options,
  },
};

export const InLine: Story = {
  args: {
    ...Default.args,
    name: 'radioRichInline',
    inline: true,
  },
};

export const TextOnly: Story = {
  args: {
    ...Default.args,
    name: 'radioRichTextOnly',
    options: textOptions,
  },
};

export const Hint: Story = {
  args: {
    ...Default.args,
    name: 'radioRichHint',
    options: hintOptions,
    hint: hint,
  },
};

export const Error: Story = {
  args: {
    ...Default.args,
    name: 'radioRichError',
    error: error,
  },
};

export const Valid: Story = {
  args: {
    ...Default.args,
    name: 'radioRichValid',
    valid: valid,
  },
};

export const Disabled: Story = {
  args: {
    ...Default.args,
    name: 'radioRichDisabled',
    disabled: true,
  },
};

let value: any;
export const NgModel: Story = {
  name: 'NgModel',
  args: {
    ...Default.args,
    name: 'radioRichNgModel',
  },
  render: (args) => ({
    props: { ...args, value },
    template: `
  <dsfr-form-radio-rich 
    [disabled]="disabled"
    [value]="value"
    [ariaControls]="ariaControls"
    [hint]="hint"
    [inputId]="inputId"
    [label]="label"
    [inline]="inline"
    [error]="error"
    [valid]="valid"
    [legend]="legend"
    [legendRegular]="legendRegular"
    [name]="name"
    [options]="options"
    [required]="required"
    [(ngModel)]="value">
  </dsfr-form-radio-rich>

  <div class="sb-smaller">Valeur du modèle : {{ value }}</div>
  `,
  }),
};
