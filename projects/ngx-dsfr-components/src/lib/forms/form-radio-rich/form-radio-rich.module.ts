import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PictogramModule } from '../../shared';
import { DsfrFormFieldsetModule } from '../fieldset/form-fieldset.module';
import { DsfrFormRadioRichComponent } from './form-radio-rich.component';

@NgModule({
  declarations: [DsfrFormRadioRichComponent],
  exports: [DsfrFormRadioRichComponent],
  imports: [CommonModule, FormsModule, DsfrFormFieldsetModule, PictogramModule],
})
export class DsfrFormRadioRichModule {}
