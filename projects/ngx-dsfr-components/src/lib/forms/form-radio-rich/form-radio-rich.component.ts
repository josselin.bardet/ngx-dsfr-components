import { Component, forwardRef, Input, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DefaultRadioComponent } from '../default-radio.component';
import { DsfrRadioRich } from './form-radio-rich.model';

@Component({
  selector: 'dsfr-form-radio-rich',
  templateUrl: './form-radio-rich.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrFormRadioRichComponent),
      multi: true,
    },
  ],
})
export class DsfrFormRadioRichComponent extends DefaultRadioComponent {
  /**
   * Le modèle de présentation permettant de transmettre la liste des radios.
   */
  @Input() options: DsfrRadioRich[];

  /**
   * @deprecated (@since 1.1.0) utiliser la propriété options à la place.
   */
  @Input() set radioRich(options: DsfrRadioRich[]) {
    this.options = options;
  }
}
