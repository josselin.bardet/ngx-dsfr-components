import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormFieldsetModule } from '../fieldset';
import { DsfrFormRadioComponent } from './form-radio.component';

@NgModule({
  declarations: [DsfrFormRadioComponent],
  exports: [DsfrFormRadioComponent],
  imports: [CommonModule, FormsModule, DsfrFormFieldsetModule],
})
export class DsfrFormRadioModule {}
