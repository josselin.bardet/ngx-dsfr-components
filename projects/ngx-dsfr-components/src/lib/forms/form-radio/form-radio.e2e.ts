import { expect, test } from '@playwright/test';

/** Default */
test('radio.default', async ({ page }) => {
  await page.goto('?path=/story/forms-radio--default');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const input = await frame.locator('input[type="radio"]');
  // await expect(input).toBeEnabled();
});

/** Inline */
test('radio.inline', async ({ page }) => {
  await page.goto('?path=/story/forms-radio--in-line');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const fieldsetElt = await frame.locator('div.fr-fieldset__element').first();
  await expect(fieldsetElt).toBeEnabled();
  await expect(fieldsetElt).toHaveClass('fr-fieldset__element fr-fieldset__element--inline');
});
