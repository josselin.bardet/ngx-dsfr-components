import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormFieldsetModule } from '../fieldset';
import { DsfrFormRadioComponent } from './form-radio.component';
import { DsfrRadio } from './form-radio.model';

const meta: Meta = {
  title: 'FORMS/Radio',
  component: DsfrFormRadioComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrFormFieldsetModule] })],
  argTypes: {
    value: { control: { type: 'text' } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormRadioComponent>;

const legend = 'Légende pour l’ensemble des champs';
const hint = 'Texte de description additionnel';
const error = 'Texte d’erreur obligatoire';
const valid = 'Texte de validation';
const labelRadio = 'Libellé bouton radio ';
const options: DsfrRadio[] = [
  { label: labelRadio + 1, value: '1' },
  { label: labelRadio + 2, value: '2' },
  { label: labelRadio + 3, value: '3' },
];
const specificHintOptions: DsfrRadio[] = [
  { label: labelRadio + 1, value: '1', hint: hint },
  { label: labelRadio + 2, value: '2', hint: hint },
  { label: labelRadio + 3, value: '3', hint: hint },
];

const defaultRadioArgs = {
  // DefaultValueAccessorComponent default values
  disabled: false,
  value: undefined,
  //DefaultControlComponent default values
  ariaControls: '',
  hint: '',
  inputId: '',
  label: '',
  // DefaultRadioComponent
  inline: false,
  error: '',
  valid: '',
  required: false,
  legendRegular: false,
};

export const Default: Story = {
  args: {
    ...defaultRadioArgs,
    name: 'defaultRadios',
    legend: legend,
    options: options,
    small: false,
  },
};

export const NgModel: Story = {
  name: 'NgModel',
  args: {
    ...Default.args,
    name: 'ngmodelRadios',
    small: true,
    value: '2',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-form-radio [(ngModel)]="value" [legend]="legend" [name]="name" [options]="options"></dsfr-form-radio>
<div class="sb-smaller">Valeur du modèle : {{ value }}</div>`,
  }),
};

export const Small: Story = {
  args: {
    ...Default.args,
    name: 'smallRadios',
    small: true,
  },
};

export const Disabled: Story = {
  args: {
    ...Default.args,
    name: 'disabledRadios',
    disabled: true,
  },
};

export const InLine: Story = {
  args: {
    ...Default.args,
    name: 'inlineRadios',
    inline: true,
  },
};

export const Hint: Story = {
  args: {
    ...Default.args,
    name: 'hintRadios',
    hint: hint,
  },
};

export const Error: Story = {
  args: {
    ...Default.args,
    name: 'errorRadios',
    error: error,
  },
};

export const Valid: Story = {
  args: {
    ...Default.args,
    name: 'validRadios',
    valid: valid,
  },
};

export const SpecificHint: Story = {
  args: {
    ...Default.args,
    name: 'specificHintRadios',
    options: specificHintOptions,
  },
};
