import { Component, forwardRef, Input, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DefaultRadioComponent } from '../default-radio.component';
import { DsfrRadio } from './form-radio.model';

@Component({
  selector: 'dsfr-form-radio',
  templateUrl: './form-radio.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrFormRadioComponent),
      multi: true,
    },
  ],
})
export class DsfrFormRadioComponent extends DefaultRadioComponent {
  /**
   * Le modèle de présentation permettant de transmettre la liste des radios.
   */
  @Input() options: DsfrRadio[];

  /**
   * Réduit la taille du radio à 16px (au lieu de 24px).
   */
  @Input() small = false;

  /**
   * @deprecated (@since 1.1.0), utiliser la propriété options à la place.
   */
  @Input() set radios(options: DsfrRadio[]) {
    this.options = options;
  }
}
