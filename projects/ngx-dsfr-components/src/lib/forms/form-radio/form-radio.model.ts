/** Boutons radios simples */
// TODO faire hériter de DsfrOption
export interface DsfrRadio {
  /** Valeur */
  value: any;
  /** Label  */
  label: string;
  /** Texte d'aide optionnel */
  hint?: string;
}
