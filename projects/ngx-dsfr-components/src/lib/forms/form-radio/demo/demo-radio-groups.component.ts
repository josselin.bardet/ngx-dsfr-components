import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrRadio } from '../form-radio.model';
import { DsfrFormRadioModule } from '../form-radio.module';

@Component({
  selector: 'demo-radio-groups',
  template: `<dsfr-form-radio [name]="name1" inline="true" [options]="options1" [(ngModel)]="value1"></dsfr-form-radio>
    <div class="sb-smaller">Valeur du modèle : {{ value1 }}</div>
    <dsfr-form-radio [name]="name2" inline="true" [options]="options2" [(ngModel)]="value2"></dsfr-form-radio>
    <div class="sb-smaller">Valeur du modèle : {{ value2 }}</div>`,
  standalone: true,
  imports: [CommonModule, DsfrFormRadioModule, FormsModule],
})
export class DemoRadioGroupsComponent {
  @Input() name1: string;
  @Input() options1: DsfrRadio[];
  @Input() name2: string;
  @Input() options2: DsfrRadio[];
  /** @internal */ value1: string;
  /** @internal */ value2: string;
}
