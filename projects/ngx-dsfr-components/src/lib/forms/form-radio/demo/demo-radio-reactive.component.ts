import { CommonModule } from '@angular/common';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DsfrRadio } from '../form-radio.model';
import { DsfrFormRadioModule } from '../form-radio.module';

@Component({
  selector: 'demo-radio-reactive',
  templateUrl: './demo-radio-reactive.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrFormRadioModule],
})
export class DemoRadioReactiveComponent implements OnChanges {
  @Input() legend: string;
  @Input() options: DsfrRadio[];
  @Input() value: any;
  @Input() disabled = false;

  /** @internal */ formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      radio: { value: undefined, disabled: this.disabled },
    });
  }

  ngOnChanges({ value, disabled }: SimpleChanges) {
    if (value && value.currentValue !== value.previousValue) {
      this.formGroup.controls['radio']?.setValue(this.value);
    }
    if (disabled) {
      if (disabled.currentValue) {
        this.formGroup.controls['radio'].disable();
      } else {
        this.formGroup.controls['radio'].enable();
      }
    }
  }

  /** @internal */
  getControlValue() {
    return this.formGroup.controls['radio']?.value;
  }

  /** @internal */
  getControlType(): string {
    const value = this.getControlValue();
    return typeof value;
  }

  /** @internal */
  getControlDisabled(): boolean {
    return this.formGroup.controls['radio']?.disabled;
  }
}
