import { expect, test } from '@playwright/test';

test('toggle.default', async ({ page }) => {
  await page.goto('?path=/story/forms-toggle--default');
  const frame = page.frameLocator('#storybook-preview-iframe');

  const input = await frame.locator('input.fr-toggle__input');
  await expect(input).toBeDefined();
  const id = await input.getAttribute('id');
  expect(id).toBeDefined();
});
