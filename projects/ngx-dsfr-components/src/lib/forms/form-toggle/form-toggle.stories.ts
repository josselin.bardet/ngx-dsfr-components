import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormToggleComponent } from './form-toggle.component';
import { argPosition, dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Toggle',
  component: DsfrFormToggleComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  argTypes: {
    labelPosition: argPosition,
    error: { control: { type: 'boolean' } },
    valid: { control: { type: 'boolean' } },
    value: { control: { type: 'boolean' } },
    id: { table: { disable: true } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormToggleComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Toggle simple'),
  args: {
    label: 'Label action interrupteur',
    labelPosition: 'right',
    value: false,
    hint: '',
    disabled: false,
    error: false,
    valid: false,
    showSeparator: false,
    checkedHintLabel: '',
    uncheckedHintLabel: '',
    showCheckedHint: false, // Devrait être la valeur par défaut alors qu'actuellement = true
    ariaControls: '',
    name: '',
  },
};

export const Hint: Story = {
  decorators: dsfrDecorator('Toggle + texte d’aide'),
  args: {
    ...Default.args,
    hint: 'Texte d’aide pour clarifier l’action',
    showSeparator: false,
  },
};

export const State: Story = {
  decorators: dsfrDecorator('Toggle + état'),
  args: {
    ...Hint.args,
    showCheckedHint: true,
  },
};

export const Checked: Story = {
  decorators: dsfrDecorator('Toggle pré-coché'),
  args: {
    ...State.args,
    value: true,
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorator('Toggle disabled'),
  args: {
    ...Checked.args,
    disabled: true,
  },
};

export const Separator: Story = {
  decorators: dsfrDecorator('Toggle + séparateur'),
  args: {
    ...Checked.args,
    showSeparator: true,
  },
};

export const Error: Story = {
  decorators: dsfrDecorator('Toggle en erreur'),
  args: {
    ...Checked.args,
    error: true,
  },
};

export const Valid: Story = {
  decorators: dsfrDecorator('Toggle valide'),
  args: {
    ...Checked.args,
    valid: true,
  },
};

export const NgModel: Story = {
  name: 'NgModel',
  decorators: dsfrDecorator('Utilisation de ngModel'),
  args: {
    ...Checked.args,
  },
  render: (args) => ({
    props: args,
    template: `
    <dsfr-form-toggle [(ngModel)]="value" [label]="label"></dsfr-form-toggle>
    <div class="sb-smaller">Valeur du modèle : {{ value }}</div>
  `,
  }),
};
