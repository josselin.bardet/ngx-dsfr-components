import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormToggleComponent } from './form-toggle.component';

@NgModule({
  declarations: [DsfrFormToggleComponent],
  exports: [DsfrFormToggleComponent],
  imports: [CommonModule, FormsModule],
})
export class DsfrFormToggleModule {}
