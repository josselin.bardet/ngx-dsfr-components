import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { DsfrFormToggleModule } from '../form-toggle.module';

@Component({
  selector: 'demo-toggle-reactive',
  templateUrl: './demo-toggle-reactive.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrFormToggleModule],
})
export class DemoToggleReactiveComponent {
  /** @internal */ formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      toggle: false,
    });
  }

  /** @internal */
  getControlValue() {
    return this.formGroup.controls['toggle']?.value;
  }

  getControlType(): string {
    const value = this.getControlValue();
    return typeof value;
  }
}
