import { ReactiveFormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormToggleModule } from '../form-toggle.module';
import { DemoToggleReactiveComponent } from './demo-toggle-reactive.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Toggle',
  component: DemoToggleReactiveComponent,
  decorators: [moduleMetadata({ imports: [ReactiveFormsModule, DsfrFormToggleModule] })],
};
export default meta;
type Story = StoryObj<DemoToggleReactiveComponent>;

export const ReactiveForm: Story = {
  decorators: dsfrDecorator('Reactive Form'),
  args: {},
};
