import { Component, forwardRef, Input, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DefaultControlComponent, DsfrPosition, DsfrPositionConst, I18nService } from '../../shared';

@Component({
  selector: 'dsfr-form-toggle',
  templateUrl: './form-toggle.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrFormToggleComponent),
      multi: true,
    },
  ],
})
export class DsfrFormToggleComponent extends DefaultControlComponent<boolean> {
  /** Position du libellé, à droite par défaut. */
  @Input() labelPosition: DsfrPosition = DsfrPositionConst.RIGHT;

  /**
   * Permet d'afficher le libellé, court, décrivant l'état de l’interrupteur (activé / désactivé), placé en dessous du
   * switch. Il est affiché par défaut, il est conseillé de le mettre afin de faciliter la compréhension de
   * l’utilisateur - Optionnel.
   */
  @Input() showCheckedHint = true;

  /**
   * Propriété permettant de surcharger le libellé, court, dénotant l'état checked du toggle, sans avoir à passer
   * par le fichier d'internationalisation.
   */
  @Input() checkedHintLabel: string;

  /**
   * Propriété permettant de surcharger le libellé court dénotant l'état checked du toggle, sans avoir à passer
   * par le fichier d'internationalisation.
   */
  @Input() uncheckedHintLabel: string;

  /**
   * Affiche un séparateur horizontal sous le composant.
   * Ne devrait être utilisé que dans le cadre d'un toggles-group.
   */
  @Input() showSeparator: boolean;

  /**
   * Symbolise l'erreur du toggle
   */
  @Input() error: boolean;

  /**
   * Symbolise le succès du toggle
   */
  @Input() valid: boolean;

  constructor(private i18n: I18nService) {
    super();
  }

  get hintId() {
    return this.inputId + '-hint-text';
  }

  /**
   * Permet d'initialiser de forcer la valeur initiale à une valeur booléenne
   * @internal
   */
  writeValue(value: boolean | undefined) {
    super.writeValue(value ?? false);
  }

  /**
   * @ignore
   */
  getDataLabelChecked(): string {
    return this.showCheckedHint ? this.checkedHintLabel || this.i18n.t('toggle.dataLabelChecked') : '';
  }

  /**
   * @ignore
   */
  getDataLabelUnchecked(): string {
    return this.showCheckedHint ? this.uncheckedHintLabel || this.i18n.t('toggle.dataLabelUnchecked') : '';
  }
}
