import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../components';
import { DsfrFormInputComponent } from './form-input.component';

@NgModule({
  declarations: [DsfrFormInputComponent],
  exports: [DsfrFormInputComponent],
  imports: [CommonModule, FormsModule, DsfrButtonModule],
})
export class DsfrFormInputModule {}
