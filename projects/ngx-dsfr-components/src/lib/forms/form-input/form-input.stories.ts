import { argBoolean, argButtonVariant, dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../components';
import { DsfrInputTypeConst } from './form-input.model';
import { DsfrFormInputComponent } from './index';

const meta: Meta = {
  title: 'FORMS/Input',
  component: DsfrFormInputComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrButtonModule] })],
  argTypes: {
    disabled: argBoolean,
    inputWrapMode: { control: { type: 'inline-radio' } },
    required: argBoolean,
    type: { control: { type: 'select' }, options: [''].concat(Object.values(DsfrInputTypeConst)) },
    value: { control: { type: 'string' } },
    buttonVariant: argButtonVariant,
    buttonSelect: { control: { type: 'EventEmitter' } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormInputComponent>;

const label = 'Libellé champ de saisie';
export const Default: Story = {
  decorators: dsfrDecorator("Input de type 'text'"),
  args: {
    label: label,
    ariaExpanded: false,
    autoCorrect: true,
    disabled: false,
    required: false,
    inputWrapMode: 'addon',
    buttonDisabled: false,
    buttonVariant: 'primary',
    type: DsfrInputTypeConst.TEXT,
  },
};

export const Number: Story = {
  decorators: dsfrDecorator("Input de type 'number'"),
  args: {
    ...Default.args,
    type: 'number',
    pattern: '[0-9]*',
  },
};

export const Search: Story = {
  decorators: dsfrDecorator("Input de type 'search'"),
  args: {
    ...Default.args,
    type: 'search',
  },
};

export const Time: Story = {
  decorators: dsfrDecorator("Input de type 'time'"),
  args: {
    ...Default.args,
    type: 'time',
  },
};

export const DateTimeLocal: Story = {
  decorators: dsfrDecorator("Input de type 'datetime-local'"),
  args: {
    ...Default.args,
    type: 'datetime-local',
  },
};

export const TextArea: Story = {
  decorators: dsfrDecorator("Input de type 'textarea'"),
  args: {
    ...Default.args,
    type: 'textarea',
  },
};

export const Password: Story = {
  decorators: dsfrDecorator(
    "Input de type 'password'",
    "Préférez l'utilisation du composant <a href='/?path=/story/forms-password--default'>Mot de passe</a>",
  ),
  args: {
    ...Default.args,
    type: 'password',
  },
};

export const Combo: Story = {
  decorators: dsfrDecorator('Combo champ + bouton'),
  args: {
    ...Default.args,
    buttonLabel: 'Envoyer',
    buttonIcon: 'fr-icon-arrow-down-s-line',
  },
};

export const Placeholder: Story = {
  decorators: dsfrDecorator('Champ avec placeholder'),
  args: {
    ...Default.args,
    placeHolder: 'placeholder',
  },
};

export const InitialValue: Story = {
  decorators: dsfrDecorator('Champ avec valeur initiale'),
  args: {
    ...Default.args,
    value: 'value',
  },
};

export const CustomIcon: Story = {
  decorators: dsfrDecorator('Champ avec icône personalisée'),
  args: {
    ...Default.args,
    icon: 'ri-alert-line',
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorator('Champ désactivé'),
  args: {
    ...Default.args,
    disabled: true,
  },
};

export const Hint: Story = {
  decorators: dsfrDecorator('Champ avec texte additionnel'),
  args: {
    ...Default.args,
    hint: 'Texte de description additionnel',
  },
};

export const Valid: Story = {
  decorators: dsfrDecorator('Champ valide avec texte de succès'),
  args: {
    ...Default.args,
    valid: 'Texte de validation',
  },
};

export const Error: Story = {
  decorators: dsfrDecorator("Champ en erreur avec texte d'erreur"),
  args: {
    ...Default.args,
    error: 'Texte d’erreur obligatoire',
  },
};
