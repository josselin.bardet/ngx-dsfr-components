import { Component, EventEmitter, forwardRef, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DsfrButtonType, DsfrButtonVariant } from '../../components';
import { DefaultControlComponent, isStringEmptyOrNull } from '../../shared';
import { DsfrInputMode, DsfrInputType, DsfrInputTypeConst } from './form-input.model';

const INPUT_TYPE_2_MODE = {
  'date': '',
  'datetime-local': '',
  'email': 'email',
  'number': 'numeric',
  'password': '',
  'search': 'search',
  'tel': 'tel',
  'text': '',
  'textarea': '',
  'time': '',
};

@Component({
  selector: 'dsfr-form-input',
  templateUrl: './form-input.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [{ provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DsfrFormInputComponent), multi: true }],
})
export class DsfrFormInputComponent extends DefaultControlComponent<string> implements OnInit {
  // Accessibilité -----------------------------------------------------------------------------------------------------
  /** Spécifie le type de l'auto-complete (email, name, etc.). */
  @Input() autocomplete: string | undefined;
  /** @since 1.4 */
  @Input() ariaAutocomplete: string;
  /** @since 1.4 */
  @Input() ariaExpanded = false;
  /** L'input ariaLabel est utilisé pour définir une légende non visible associée à un élément HTML dont le sens est
   * transmis uniquement par le visuel. */
  @Input() ariaLabel: string;
  /** @since 1.4 */
  @Input() role: string;
  // -------------------------------------------------------------------------------------------------------------------

  /** @since 1.6 [Safari uniquement](https://developer.mozilla.org/fr/docs/Web/HTML/Element/input#autocorrect)*/
  @Input() autoCorrect = true;

  /**
   * Message d'erreur, quand il est présent les couleurs du contrôle changent.
   */
  @Input() error: string;

  /**
   * Message de validation, quand il est présent les couleurs du contrôle changent.
   */
  @Input() valid: string;

  /**
   * Indique si le champ est obligatoire ou non, faux par défaut.
   */
  @Input() required = false;

  /**
   * Placeholder de l'input.
   */
  @Input() placeHolder: string;

  /**
   * Attribut min de l'input.
   */
  @Input() min: string;

  /**
   * Attribut max de l'input.
   */
  @Input() max: string;

  /**
   * Attribut minLength de l'input.
   */
  @Input() minLength: number;

  /**
   * Attribut maxLength de l'input.
   */
  @Input() maxLength: number;

  /** Désactive la correction orthographique sur les champs relatifs au nom et aux prénoms. */
  @Input() spellCheck = true;

  /**
   * Ajoute un icon à droite dans le champ de saisie.
   */
  @Input() icon: string;

  /**
   * Type 'textarea' : nombre de lignes.
   */
  @Input() rows: number;

  /**
   * Pattern de l'input.
   */
  @Input() pattern: string;

  /**
   * Ajout d'un style spécifique, permet par exemple de la limiter la largeur d'un input.
   */
  @Input() customClass: string;

  /**
   * Change la mise en page d'un input accompagné d'un bouton.
   */
  // TODO est-ce qu ça ne devrait pas tout simplement s'appeler wrapMode puisqu'on est dans un input ?
  @Input() inputWrapMode: 'addon' | 'action' = 'addon';

  /** [accessibilité] Spécifie le libellé qui sera retranscrit par les narrateurs d'écran. */
  @Input() buttonAriaLabel: string;

  /**
   * Permet de désactiver le bouton d'action, 'false' par défaut.
   */
  @Input() buttonDisabled = false;

  /**
   * Crée un combo champ + bouton si buttonIcon est renseigné
   */
  @Input() buttonIcon: string | undefined; // undefined car peut être créé à partir de DsfrButton (composant name par exemple)

  /**
   * Crée un combo champ + bouton si buttonLabel est renseigné
   */
  @Input() buttonLabel: string;

  /**
   * Tooltip message sur le bouton s'il y a lieu.
   */
  @Input() buttonTooltipMessage: string;

  /**
   * Type du button,'submit' par défaut.
   */
  @Input() buttonType: DsfrButtonType;

  /** Style du bouton, 'primary' par défaut. */
  @Input() buttonVariant: DsfrButtonVariant = 'primary';

  /**
   * Emission de l'événement si le type du bouton est != de `submit`.
   */
  @Output() buttonSelect = new EventEmitter<Event>();

  /**
   * @deprecated (since 1.2) utiliser `customClass` à la place.
   * Largeur de l'input, `'100%'` par défaut, `'10rem'` par défaut pour un type date.
   */
  @Input() width: string;

  /** L'input mode est initialisé par défaut en fonction du type de l'input, 'decimal', 'url' ne sont pas traité pour l'instant. */
  /** @internal */ inputMode: DsfrInputMode;
  /** @internal */ messagesGroupId: string;

  private _type: DsfrInputType = DsfrInputTypeConst.TEXT;

  get type(): DsfrInputType {
    return this._type;
  }

  /** @deprecated since 1.2 use `type` instead. */
  get textarea(): boolean {
    return this.isTextArea();
  }

  /**
   * Type de l'input, 'text' par défaut.
   */
  @Input() set type(value: DsfrInputType) {
    this._type = value;
    this.inputMode = <DsfrInputMode>INPUT_TYPE_2_MODE[this._type] || undefined;
    if (this._type === DsfrInputTypeConst.NUMBER) {
      this.pattern = '[0-9]*';
    }
  }

  /** @deprecated (@since 1.2) utiliser `type` à la place. */
  @Input() set textarea(value: boolean) {
    this.type = value ? DsfrInputTypeConst.TEXTAREA : DsfrInputTypeConst.TEXT;
  }

  /** @internal */
  ngOnInit() {
    super.ngOnInit();
    this.messagesGroupId = `${this.inputId}-messages`;
  }

  /** @internal */
  isTextArea() {
    return this._type === DsfrInputTypeConst.TEXTAREA;
  }

  /** @internal */
  isNumber() {
    return this._type === DsfrInputTypeConst.NUMBER;
  }

  /** @internal */
  hasButton(): boolean {
    return !isStringEmptyOrNull(this.buttonLabel) || this.buttonIcon !== undefined;
  }

  /** @internal */
  hasInputWrap(): boolean {
    return this.hasButton() || !isStringEmptyOrNull(this.icon);
  }

  /** @internal */
  getWrapClasses(): string[] {
    let classes = ['fr-input-wrap'];
    if (this.hasButton() && this.inputWrapMode === 'addon') classes.push('fr-input-wrap--addon');
    if (this.hasButton() && this.inputWrapMode === 'action') classes.push('fr-input-wrap--action');
    if (this.icon) classes.push(this.icon);

    return classes;
  }

  /** @internal */
  onButtonClick(event: Event) {
    if (this.buttonType != 'submit') this.buttonSelect.emit(event);
  }

  /** @internal */
  hasMessages(): boolean {
    return !!(this.error || this.valid);
  }
}
