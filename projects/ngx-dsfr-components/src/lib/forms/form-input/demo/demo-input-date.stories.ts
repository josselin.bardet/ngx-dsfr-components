import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../../components';
import { DsfrFormInputModule } from '../form-input.module';
import { DemoInputDateComponent } from './demo-input-date.component';
import { gridDecoratorSM, titleDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Input',
  component: DemoInputDateComponent,
  decorators: [moduleMetadata({ imports: [DsfrFormInputModule, FormsModule, DsfrButtonModule] })],
};
export default meta;
type Story = StoryObj<DemoInputDateComponent>;

export const Date: Story = {
  decorators: [
    gridDecoratorSM,
    titleDecorator("Input de type 'date'", "Préférez l'utilisation du modèle de bloc de Date unique"),
  ],
  args: {},
};
