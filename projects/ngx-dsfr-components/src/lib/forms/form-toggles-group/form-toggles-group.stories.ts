import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormToggleModule } from '../form-toggle';
import { DsfrFormTogglesGroupComponent } from './form-toggles-group.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'FORMS/Toggles Group',
  component: DsfrFormTogglesGroupComponent,
  decorators: [moduleMetadata({ imports: [DsfrFormToggleModule] })],
};
export default meta;
type Story = StoryObj<DsfrFormTogglesGroupComponent>;

const hint = "Texte d’aide pour clarifier l'action";

const template = (content: string) => {
  return `<dsfr-form-toggles-group 
   [id]="id"
   [legend]="legend"
   [legendRegular]="legendRegular"
   [legendSrOnly]="legendSrOnly"
   [hint]="hint"
   [disabled]="disabled"
   [error]="error"
   [valid]="valid"
   [showSeparators]="showSeparators">
    ${content}
</dsfr-form-toggles-group>`;
};

const defaultContent = `<dsfr-form-toggle label="Label action interrupteur"></dsfr-form-toggle>
  <dsfr-form-toggle label="Label action interrupteur"></dsfr-form-toggle>
  <dsfr-form-toggle label="Label action interrupteur"></dsfr-form-toggle>`;

const hintContent = `<dsfr-form-toggle label="Label action interrupteur" hint="${hint}"></dsfr-form-toggle>
  <dsfr-form-toggle label="Label action interrupteur" hint="${hint}"></dsfr-form-toggle>
  <dsfr-form-toggle label="Label action interrupteur" hint="${hint}"></dsfr-form-toggle>`;

export const Default: Story = {
  decorators: dsfrDecorator('Groupe de toggles'),
  args: {
    legend: 'Légende pour l’ensemble des éléments ',
    hint: '',
    disabled: false,
    legendRegular: false,
    legendSrOnly: false,
    error: '',
    valid: '',
    showSeparators: false,
  },
  render: (args) => ({
    props: args,
    template: template(defaultContent),
  }),
};

export const Separator: Story = {
  decorators: dsfrDecorator('Toggles avec séparateur'),
  args: {
    ...Default.args,
    showSeparators: true,
  },
  render: (args) => ({
    props: args,
    template: template(defaultContent),
  }),
};

export const Hint: Story = {
  decorators: dsfrDecorator("Toggles avec texte d'aide"),
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: template(hintContent),
  }),
};

export const Disabled: Story = {
  decorators: dsfrDecorator("Toggles avec texte d'aide"),
  args: {
    ...Hint.args,
    disabled: true,
  },
  render: (args) => ({
    props: args,
    template: template(hintContent),
  }),
};

export const Error: Story = {
  decorators: dsfrDecorator("Toggles avec texte d'aide"),
  args: {
    ...Hint.args,
    error: 'Texte d’erreur obligatoire',
  },
  render: (args) => ({
    props: args,
    template: template(hintContent),
  }),
};

export const Valid: Story = {
  decorators: dsfrDecorator("Toggles avec texte d'aide"),
  args: {
    ...Hint.args,
    valid: 'Texte de validation',
  },
  render: (args) => ({
    props: args,
    template: template(hintContent),
  }),
};
