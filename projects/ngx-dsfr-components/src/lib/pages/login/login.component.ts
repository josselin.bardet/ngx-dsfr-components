import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import {
  DsfrAlertModule,
  DsfrButtonModule,
  DsfrButtonsGroupModule,
  DsfrFranceConnectComponent,
} from '../../components';
import {
  DsfrFormCheckboxModule,
  DsfrFormEmailComponent,
  DsfrFormEmailModule,
  DsfrFormFieldsetModule,
  DsfrFormPasswordModule,
} from '../../forms';
import { I18nService, LocalStorage, StorageEnum, isOnBrowser } from '../../shared';
import { DsfrLogin } from './login.model';

@Component({
  selector: 'dsfr-login',
  templateUrl: './login.component.html',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DsfrFranceConnectComponent,
    DsfrFormEmailModule,
    DsfrFormPasswordModule,
    DsfrFormFieldsetModule,
    DsfrFormCheckboxModule,
    DsfrButtonModule,
    DsfrButtonsGroupModule,
    DsfrAlertModule,
  ],
})
export class DsfrLoginComponent implements OnInit {
  /** @internal */ @ViewChild(DsfrFormEmailComponent) emailComponent: DsfrFormEmailComponent;

  /**
   * Indique une erreur de connexion. Message de l'erreur.
   */
  @Input() error: string;

  /** Url de récupération du mot de passe (optionnel) */
  @Input() recoveryLink: string;

  /** Path interne. Exclusif avec link et routerLink */
  @Input() recoveryRoute: string;

  /** Path angular géré en tant que directive routerLink. */
  @Input() recoveryRouterLink: string | string[];

  /** RouterLink : classe utilisée pour la directive routerLink active. */
  @Input() recoveryRouterLinkActive: string | string[];

  /** RouterLink : valeurs additionnelles de navigation pour le routerLink (queryParams, state, etc.) */
  @Input() recoveryRouterLinkExtras: NavigationExtras;

  /** Se souvenir de l'utilisateur. */
  @Input() rememberMe = false;

  /** Utilisation de FranceConnect+ (qui est plus sécurisé). */
  @Input() secure: false;

  /** Nom du service. */
  @Input() service: string;

  /** Demande de connexion via FranceConnect. */
  @Output() franceConnectSelect: EventEmitter<void> = new EventEmitter<void>();

  /** Evénement lorsque le mot de passe change. */
  @Output() passwordChange: EventEmitter<string> = new EventEmitter<string>();

  /** Emission de `recoveryRoute` s'il y a lieu. */
  @Output() recoveryRouteSelect: EventEmitter<string> = new EventEmitter<string>();

  /** Demande de connexion avec en paramètre : '{login : string ; password : string}'. */
  @Output() signinSelect: EventEmitter<DsfrLogin> = new EventEmitter<DsfrLogin>();

  /** Demande de création de compte */
  @Output() signupSelect: EventEmitter<void> = new EventEmitter<void>();

  /** @internal */ password: string; // Password de l'utilisateur.
  /** @internal */ login: string; // Email de l'utilisateur.
  /** @internal */ loginFormGroup: FormGroup;

  /** @internal */
  constructor(public i18n: I18nService, private fb: FormBuilder) {
    this.loginFormGroup = this.fb.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
      rememberMe: false,
    });
  }

  /** @internal */
  ngOnInit(): void {
    if (isOnBrowser()) {
      this.rememberMe ??= Boolean(LocalStorage.get(StorageEnum.REMEMBER_ME));
      if (this.rememberMe) this.login ??= LocalStorage.get(StorageEnum.LOGIN) ?? '';
    }

    this.setControlValue('login', this.login);
    this.setControlValue('password', this.password);
    this.setControlValue('rememberMe', this.rememberMe);
  }

  /** @internal */
  onFranceConnect() {
    this.franceConnectSelect.emit();
  }

  /**
   * Inscription, on laisse le développeur faire ce qu'il veut dans ce cas.
   * @internal
   */
  onSignUp() {
    this.signupSelect.emit();
  }

  /** Connexion */
  /** @internal */
  onSignIn() {
    if (!this.isFormLoginValid()) return; // Ce qui ne peut pas arriver

    const login = this.getControlValue('login');
    const password = this.getControlValue('password');
    const rememberMe = this.getControlValue('rememberMe');

    // localStorage
    this.saveToLocalStorage(rememberMe, login);

    // Event
    this.signinSelect.emit({ login: login, password: password, rememberMe: rememberMe });
  }

  /** @internal */
  onPasswordChange(value: string) {
    this.passwordChange.emit(value);
  }

  /** @internal */
  onRecoveryRouteSelect(route: string) {
    this.recoveryRouteSelect.emit(route);
  }

  /** @internal */
  isFormLoginValid() {
    return this.loginFormGroup.valid && this.emailComponent.isValid();
  }

  private setControlValue(name: string, value: any): void {
    this.loginFormGroup.controls[name]?.setValue(value);
  }

  private getControlValue(name: string): any {
    return this.loginFormGroup.controls[name]?.value;
  }

  private saveToLocalStorage(rememberMe: boolean, login: string): void {
    if (this.rememberMe) {
      LocalStorage.set(StorageEnum.REMEMBER_ME, String(rememberMe));
      LocalStorage.set(StorageEnum.LOGIN, login);
    } else {
      LocalStorage.remove(StorageEnum.REMEMBER_ME);
      LocalStorage.remove(StorageEnum.LOGIN);
    }
  }
}
