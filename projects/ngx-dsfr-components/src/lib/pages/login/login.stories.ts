import { EventEmitter } from '@angular/core';
import { Meta, StoryObj } from '@storybook/angular';
import { DsfrLoginComponent } from './login.component';
import { DsfrLogin } from './login.model';

const meta: Meta = {
  title: 'PAGES/Login',
  component: DsfrLoginComponent,
  argTypes: {
    signupSelect: { control: { type: EventEmitter<void> } },
    signinSelect: { control: { type: EventEmitter<DsfrLogin> } },
    recoveryRouteSelect: { control: { type: EventEmitter<string> } },
    passwordChange: { control: { type: EventEmitter<string> } },
    franceConnectSelect: { control: { type: EventEmitter<void> } },
  },
};
export default meta;
type Story = StoryObj<DsfrLoginComponent>;

const service = '[Nom de service-site]';
const recoveryLink = 'https://my-recovery-link';

const DefaultValues: Story = {
  args: {
    // Valeurs par défaut
    rememberMe: false,
    secure: false,
  },
};

export const Default: Story = {
  args: {
    ...DefaultValues,
    service: service,
    recoveryLink: recoveryLink,
  },
};

/** Error */
export const Error: Story = {
  args: {
    ...DefaultValues,
    service: service,
    recoveryLink: recoveryLink,
    error: 'Identifiant/mot de passe incorrect',
  },
};
