import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrButtonModule } from '../../components/button';
import { DsfrButtonsGroupModule } from '../../components/buttons-group';
import { DsfrPageResponseComponent, DsfrResponseComponent } from './response.component';

@NgModule({
  declarations: [DsfrResponseComponent],
  exports: [DsfrResponseComponent],
  imports: [CommonModule, DsfrButtonModule, DsfrButtonsGroupModule],
})
export class DsfrResponseModule {}

/**
 * @deprecated use DsfrResponseModule instead
 */
@NgModule({
  declarations: [DsfrPageResponseComponent],
  exports: [DsfrPageResponseComponent],
  imports: [CommonModule, DsfrButtonModule, DsfrButtonsGroupModule],
})
export class DsfrPageResponseModule {}
