import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { DsfrHeadingLevel, I18nService } from '../../shared';
import { DSFR_CONFIG_TOKEN } from '../../shared/config/config-token';
import { DsfrConfig } from '../../shared/config/config.model';

@Component({
  selector: 'dsfr-response',
  templateUrl: './response.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrResponseComponent implements OnInit {
  /**
   * Titre de la page (par défaut : 'Erreur inattendue').
   */
  @Input() heading = this.i18n.t('pageResponse.heading');

  /** Le niveau de titre devant être utilisé. */
  @Input() headingLevel: DsfrHeadingLevel;

  /**
   * Type d'erreur (404, 500,...).
   */
  @Input() error = 500;

  /**
   * Description de la page (par défaut: 'Essayez de rafraichir la page ou bien ressayez plus tard.').
   */
  @Input() description = this.i18n.t('pageResponse.description');

  /**
   * Détail de la page (par défaut : 'Désolé, le service rencontre un problème, nous travaillons pour le résoudre le plus rapidement possible.').
   */
  @Input() detail = this.i18n.t('pageResponse.detail');

  /**
   * Conditionne l'affichage du bouton contactez-nous.
   */
  @Input() showContact = true;
  /**
   * Conditionne l'affichage du bouton page d'accueil'.
   */
  @Input() showBackToHome = false;

  /**
   * Chemin vers le répertoire exposant les pictogrammes illustratifs DSFR.
   */
  @Input() artworkDirPath: string;

  /**
   * Indique que le bouton contact est cliqué.
   */
  @Output() contactSelect: EventEmitter<string> = new EventEmitter();
  /**
   * Indique que le bouton page d'accueil est cliqué.
   */
  @Output() backToHomeSelect: EventEmitter<string> = new EventEmitter();

  /** @internal */ noFirstNameModel: boolean;

  /** @internal */
  constructor(@Inject(DSFR_CONFIG_TOKEN) private config: DsfrConfig, public i18n: I18nService) {}

  get pictoPath(): string {
    return this.artworkDirPath;
  }

  /**
   * Chemin des pictogrammes (du composant display) renseigné par le développeur.
   *
   * Note: ce chemin doit permettre de récupérer directement les fichiers SVG suivants : moon.svg, sun.svg, system.svg
   *
   * @deprecated Use `artworkDirPath` instead.
   */
  @Input() set pictoPath(path: string) {
    this.artworkDirPath = path;
  }

  ngOnInit(): void {
    if (this.artworkDirPath === undefined) {
      this.artworkDirPath = this.config.artworkDirPath;
    }
  }

  /** @internal */
  onContact(): void {
    this.contactSelect.emit();
  }

  /** @internal */
  onHome(): void {
    this.backToHomeSelect.emit();
  }
}

/**
 * @deprecated use DsfrResponseComponent instead
 */
@Component({
  selector: 'dsfr-page-response',
  templateUrl: './response.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrPageResponseComponent extends DsfrResponseComponent {}
