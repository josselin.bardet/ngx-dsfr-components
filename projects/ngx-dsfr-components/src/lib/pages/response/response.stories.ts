import { dsfrDecorator } from '.storybook/storybook-utils';
import { EventEmitter } from '@angular/core';
import { Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrButtonModule } from '../../components/button';
import { DsfrButtonsGroupModule } from '../../components/buttons-group';
import { DsfrResponseComponent } from './response.component';

const meta: Meta = {
  title: 'PAGES/Response',
  component: DsfrResponseComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrButtonModule, DsfrButtonsGroupModule],
    }),
  ],
  argTypes: {
    contactSelect: { control: EventEmitter },
    backToHomeSelect: { control: EventEmitter },
  },
};
export default meta;

const Template: StoryFn<DsfrResponseComponent> = (args) => ({
  props: args,
});

/** Default */
export const Default = Template.bind({});
Default.decorators = dsfrDecorator('Erreur 500 avec contact');
Default.args = {};

/** 502 Sans contact */
export const Error502WithoutContact = Template.bind({});
Error502WithoutContact.args = {
  error: 502,
  showContact: false,
};
Error502WithoutContact.decorators = dsfrDecorator('Erreur 502 sans contact');

/** 404Full */
export const Error404Full = Template.bind({});
Error404Full.args = {
  heading: 'Page non trouvée',
  error: 404,
  description: 'La page que vous cherchez est introuvable. Excusez-nous pour la gène occasionnée.',
  detail:
    "Si vous avez tapé l'adresse web dans le navigateur, vérifiez qu'elle est correcte. La page n’est peut-être plus disponible. Dans ce cas, pour continuer votre visite vous pouvez consulter notre page d’accueil. Sinon contactez-nous pour que l’on puisse vous rediriger vers la bonne information.",
  showBackToHome: true,
};
Error404Full.decorators = dsfrDecorator('Erreur 404 full');
