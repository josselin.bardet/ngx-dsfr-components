import { Component, Input } from '@angular/core';

@Component({
  selector: 'edu-svg-icon',
  template: `
    <svg [attr.aria-hidden]="ariaHidden" [attr.role]="role" [attr.alt]="alt">
      <use attr.xlink:href="{{ iconPath }}"></use>
    </svg>
  `,
})
export class SvgIconComponent {
  @Input() iconPath: string;
  @Input() role = 'img';
  @Input() ariaHidden: boolean = true;
  @Input() alt: string;
}
