import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrHeadingLevel } from '../../models';

@Component({
  selector: 'edu-heading',
  templateUrl: './heading.component.html',
  encapsulation: ViewEncapsulation.None,
})
/** Permet de définir le titre de composant d'un niveau de titre de `<h2>` à `<h6>`, avec un niveau par défaut de `<h2>` à `<h6>` ou `<p>`. */
export class HeadingComponent {
  /** Classe du titre. */
  @Input() customClass: string;

  /** Le titre de l'accordéon est du texte simple. */
  @Input() heading: string;

  /** id du titre @since 1.9 */
  @Input() headingId: string;

  /** Le niveau du titre dans la structure. */
  @Input() level: DsfrHeadingLevel | undefined;

  /** Niveau par défaut si 'level' n'est pas renseigné. */
  @Input() defaultLevel: DsfrHeadingLevel | 'P';

  /** @internal */
  getLevel(): DsfrHeadingLevel | 'P' {
    return this.level ?? this.defaultLevel;
  }
}
