import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'edu-pictogram',
  templateUrl: './pictogram.component.html',
  encapsulation: ViewEncapsulation.None,
})
/** Ce composant permet de gérer facilement les pictogrammes issus du DSFR. */
export class PictogramComponent {
  /**
   * Chemin vers le répertoire exposant les pictogrammes illustratifs DSFR.
   */
  @Input() artworkDirPath = 'artwork';

  /**
   * Chemin relatif à artworkDirPath dénotant le fichier d'illustration à utiliser.
   */
  @Input() artworkFilePath: string;

  /**
   * Détermine l'affichage du pictogramme associé aux téléchargements.
   */
  @Input() download = false;
}
