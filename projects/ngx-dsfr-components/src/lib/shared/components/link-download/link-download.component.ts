import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrLink, DsfrLinkTarget, DsfrPositionConst } from '../../models';

@Component({
  selector: 'edu-link-download',
  templateUrl: './link-download.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, RouterModule],
})
/** Ce composant permet de gérer facilement une ancre href ou un `routerLink` sans ajout de styles.
 * Utilisé par le `header`, `footer`, `breadcrumb` et `sidemenu`. */
export class LinkDownloadComponent {
  /** Classe personnalisée du composant, par exemple, `fr-btn`, `fr-nav__link`, ... */
  @Input() customClass: string;

  /**
   * Active la version download avec l'attribut download.
   * Permet l'utilisation de `downloadAssessFile` et  `langCode`
   */
  @Input() downloadDirect: boolean | string = false;

  /** Option de détail de téléchargement renseigné automatiquement.
   * Si la valeur est 'bytes', l'unité sera en Bytes
   */
  @Input() downloadAssessFile: boolean | 'bytes' = false;

  /**
   * Langue courante. hreflang ne sera indiquée que différent de la langue courante.
   */
  @Input() langCode = 'fr';

  /**
   * Propage la valeur de 'route' ou de 'link' (selon le cas) lors du click sur le lien.
   */
  @Output() linkSelect = new EventEmitter<string>();

  private _item: DsfrLink;

  get disabled(): boolean {
    return !(this.item.route || this.item.routerLink || this.item.link);
  }

  get item(): DsfrLink {
    return this._item;
  }

  /** Item représentant un lien `<a>` avec ou sans `routerLink`. */
  @Input() set item(item: DsfrLink) {
    if (item.label) {
      this._item = { ...item, label: item.label };
    } else {
      this._item = item;
    }
    if (this.item) this.item.customClass = this.getLinkClasses();
  }

  /**
   * Si === 'true', télécharge directement le fichier sans l'ouvrir, 'false' par défaut.
   * @internal
   */
  isDirectDownload(): boolean {
    return this.downloadDirect !== false;
  }

  /** @internal */
  getHref(): string | undefined {
    if (this.item.route) return this.item.route;
    if (this.item.link) return this.item.link;
    return undefined;
  }

  /** @internal */
  getNewFileName(): string {
    return typeof this.downloadDirect === 'string' ? this.downloadDirect : '';
  }

  onLinkSelect(event: Event) {
    if (this.item.route && !this.item.routerLink) {
      event.preventDefault();
      this.linkSelect.emit(this.item.route);
    } else if (this.item.link) {
      this.linkSelect.emit(this.item.link);
    }
  }

  /** @internal */
  getLinkTarget(item: DsfrLink): DsfrLinkTarget | undefined {
    return item.target ?? item.linkTarget;
  }

  /** @internal */
  private getLinkClasses(): string {
    const classes = [];
    classes.push(!this.customClass ? 'fr-link' : this.customClass);

    if (this._item.icon) {
      classes.push(this._item.icon);
      // Dans le header, les liens utilisent la classe fr-btn, pour être cohérent, on va utiliser fr-btn--icon-left et right
      const likeButton = this.customClass?.search('fr-btn') >= 0;
      const prefixPos = likeButton ? 'fr-btn--icon-' : 'fr-link--icon-';
      classes.push(this._item.iconPosition === DsfrPositionConst.RIGHT ? prefixPos + 'right' : prefixPos + 'left');
    }

    return classes.join(' ');
  }
}
