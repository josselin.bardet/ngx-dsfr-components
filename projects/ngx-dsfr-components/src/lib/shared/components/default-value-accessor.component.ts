import { Component, Input } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

/**
 * Base class for all ControlValueAccessor classes defined in Forms package. Contains common logic and utility functions.
 * Le type de `value` est : T | undefined, undefined par défaut. Par exemple si on manipule un input de type number, celui-ci
 * sera initialisé à undefined par défaut.
 */
// Même si on ne précise pas undefined, à l'exécution, c'est quand même le cas.
// Même si on indique un type number, à l'exécution, si l'utilisateur saisie "1a", value sera de type string
@Component({
  template: '',
})
export abstract class DefaultValueAccessorComponent<T> implements ControlValueAccessor {
  /**
   * Permet de désactiver le champ.
   */
  @Input() disabled = false;

  private _value: T | undefined;

  get value(): T | undefined {
    return this._value;
  }

  /**
   * La valeur gérée par le champ de formulaire.
   */
  @Input() set value(value: T | undefined) {
    this._value = value;
    this.fnOnChange(this.value);
    this.fnOnTouched();
  }

  /**
   * Writes a new value to the element.
   * This method is called by the forms API to write to the view when programmatic changes from model to view are requested.
   *
   * @internal
   */
  writeValue(value: T | undefined): void {
    this._value = value;
  }

  /**
   * Registers a callback function that is called when the control's value changes in the UI.
   * When the value changes in the UI, call the registered function to allow the forms API to update itself:
   * host: {
   *    '(change)': '_onChange($event.target.value)'
   * }
   *  @internal
   */
  registerOnChange(fn: (_: any) => void): void {
    this.fnOnChange = fn;
  }

  /**
   * Registers a callback function that is called by the forms API on initialization to update the form model on blur.
   * On blur (or equivalent), your class should call the registered function to allow the forms API to update itself:
   * host: {
   *    '(blur)': '_onTouched()'
   * }
   *  @internal
   */
  registerOnTouched(fn: () => void): void {
    this.fnOnTouched = fn;
  }

  /**
   * Function that is called by the forms API when the control status changes to or from 'DISABLED'.
   * Depending on the status, it enables or disables the appropriate DOM element.
   *
   * @internal
   */
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  /** @internal */
  fnOnChange = (_: T | undefined) => {};

  /** @internal */
  fnOnTouched = () => {};
}
