export const MESSAGES_FR = {
  commons: {
    accept: 'Accepter',
    close: 'Fermer',
    disabled: 'Désactivé',
    download: 'Télécharger',
    enlarge: 'Agrandir',
    refuse: 'Refuser',
    search: 'Rechercher',
  },

  alert: {
    close: 'Masquer le message',
  },
  backtotop: {
    topOfPage: 'Haut de page',
  },
  breadcrumb: {
    viewBreadcrumb: 'Voir le fil d’Ariane',
    ariaLabel: 'vous êtes ici :',
  },
  consent: {
    acceptAll: 'Tout accepter',
    allowAllCookies: 'Autoriser tous les cookies',
    refuseAll: 'Tout refuser',
    refuseAllCookies: 'Refuser tous les cookies',
    customize: 'Personnaliser',
    customizeCookies: 'Personnaliser les cookies',
    welcome: [
      'Bienvenue ! Nous utilisons des cookies pour améliorer votre expérience et les services disponibles sur ce site. Pour en savoir plus, visitez la page',
      'Données personnelles et cookies',
      '. Vous pouvez, à tout moment, avoir le contrôle sur les cookies que vous souhaitez activer.',
    ],

    manager: {
      title: 'Panneau de gestion des cookies',
      preferences: 'Préférences pour tous les services.',
      personalData: 'Données personnelles et cookies',
      confirmChoices: 'Confirmer mes choix',
      finalityExempt: 'Activé',
      showDetails: 'Voir plus de détails',
    },
  },
  display: {
    heading: 'Paramètres d’affichage',
    hint: 'Choisissez un thème pour personnaliser l’apparence du site.',
    light: { label: 'Thème clair' },
    dark: { label: 'Thème sombre' },
    system: { label: 'Système', hint: 'Utilise les paramètres système.' },
  },
  email: {
    label: 'Adresse électronique',
    hint: 'Format attendu : nom@domaine.fr',
    error: 'Le format de l’adresse électronique saisie n’est pas valide. Le format attendu est : nom@exemple.org',
  },
  follow: {
    networks: { heading: 'Suivez-nous<br/>sur les réseaux sociaux' },
    newsletter: {
      heading: 'Abonnez-vous à notre lettre d’information',
      button: { label: 'S’abonner', title: 'S’abonner à notre lettre d’information' },
      input: {
        placeholder: 'Votre adresse électronique (ex. : nom@domaine.fr)',
        hint: `En renseignant votre adresse électronique, vous acceptez de recevoir nos actualités par courriel. Vous pouvez vous désinscrire à tout moment à l’aide des liens de désinscription ou en nous contactant.`,
        error: 'Le format de l’adresse électronique saisie n’est pas valide. Le format attendu est : nom@exemple.org',
      },
      registration: { success: 'Votre inscription a bien été prise en compte.' },
    },
    newWindow: 'Nouvelle fenêtre',
  },
  footer: {
    partners: { title: 'Nos partenaires' },
  },
  franceConnect: {
    title: 'S’identifier avec',
    link: { label: 'Qu’est-ce que FranceConnect', tooltip: 'Qu’est-ce que FranceConnect ? - nouvelle fenêtre' },
  },
  header: {
    menu: { label: 'Menu' },
  },
  login: {
    heading: 'Connexion à ',
    franceConnect: {
      heading: 'Se connecter avec FranceConnnect',
    },
    or: 'ou',
    account: {
      heading: 'Se connecter avec son compte',
      email: { label: 'Identifiant' },
      srOnly: { label: 'identifiants' },
      rememberMe: { label: 'Se souvenir de moi' },
      button: { label: 'Se connecter' },
    },
    noAccount: { heading: 'Vous n’avez pas de compte ?', button: { label: 'Créer un compte' } },
  },
  modal: {
    ariaLabel: 'Fermer la fenêtre',
  },
  notice: {
    close: 'Masquer le message',
  },
  pagination: {
    firstPage: 'Première page',
    previousPage: 'Page précédente',
    nextPage: 'Page suivante',
    lastPage: 'Dernière page',
    role: 'navigation',
    ariaLabel: 'Pagination',
  },
  password: {
    aria: { label: 'Afficher le mot de passe' },
    forgotPassword: 'Mot de passe oublié ?',
    label: 'Mot de passe',
    message: 'Votre mot de passe doit contenir :',
    show: 'Afficher',
    dataFrValid: 'validé',
    dataFrError: 'en erreur',
  },
  response: {
    contactUs: 'Contactez-nous',
  },
  select: {
    placeholder: 'Sélectionnez une option',
  },
  share: {
    heading: 'Partager la page',
    cookiesText: ['Veuillez', 'autoriser le dépôt de cookies', 'pour partager sur les réseaux sociaux.'],
    //
    copy: { title: 'Presse-papier', label: 'Copier dans le presse-papier' },
    facebook: { title: 'Facebook', label: 'Partager sur Facebook' },
    linkedin: { title: 'LinkedIn', label: 'Partager sur LinkedIn' },
    mail: { title: 'Email', label: 'Partager par email' },
    mastodon: { title: 'Mastodon', label: 'Partager sur Mastodon' },
    twitter: { title: 'Twitter', label: 'Partager sur Twitter' },
  },
  table: {
    noData: 'Aucune donnée',
  },
  tag: {
    removeFilter: 'Retirer le filtre',
  },
  tel: {
    label: 'Numéro de téléphone',
    hint: 'Format attendu : (+33) 1 22 33 44 55',
    error: 'Le format de numéro de téléphone saisie n’est pas valide. Le format attendu est : (+33) 2 43 55 55 55',
  },
  toggle: {
    dataLabelChecked: 'Activé',
    dataLabelUnchecked: 'Désactivé',
  },
  tooltip: {
    button: { label: 'Information contextuelle' },
  },
  transcription: {
    button: 'Transcription',
    ariaLabel: 'Agrandir la transcription',
  },
  translate: {
    tooltip: 'Sélectionner une langue',
  },
  upload: {
    label: 'Ajouter un fichier',
    hint: "** Texte d'aide obligatoire qui précise les contraintes au niveau du ou des fichiers attendus **",
  },
  date: {
    day: { label: 'Jour', hint: 'Exemple : 14' },
    month: { label: 'Mois', hint: 'Exemple : 12' },
    year: { label: 'Année', hint: 'Exemple : 1984' },
    error: {
      required: 'Tous les champs sont requis.',
      invalid: {
        format: 'Les champs doivent être au format numérique.',
        day: 'Le jour est invalide.',
        month: 'Le mois est invalide.',
        date: 'La date est invalide.',
      },
    },
  },
  pageResponse: {
    heading: 'Erreur inattendue',
    description: 'Essayez de rafraichir la page ou bien ressayez plus tard.',
    detail: 'Désolé, le service rencontre un problème, nous travaillons pour le résoudre le plus rapidement possible.',
  },
};
