export const MESSAGES_EN = {
  commons: {
    accept: 'Accept',
    close: 'Close',
    disabled: 'Disabled',
    download: 'Download',
    enlarge: 'Enlarge',
    refuse: 'Refuse',
    search: 'Search',
  },

  alert: {
    close: 'Hide message',
  },
  backtotop: {
    topOfPage: 'Top of page',
  },
  breadcrumb: {
    viewBreadcrumb: 'View breadcrumb',
    ariaLabel: 'you are here :',
  },
  consent: {
    acceptAll: 'Accept All',
    allowAllCookies: 'Allow all cookies',
    refuseAll: 'Refuse all',
    refuseAllCookies: 'Refuse all cookies',
    customize: 'Customize',
    customizeCookies: 'Customize cookies',
    welcome: [
      'Welcome ! We use cookies to improve your experience and the services available on this site. To find out more, visit the ',
      'Personal data and cookies',
      ' page. You can, at any time, have control over the cookies you wish to activate.',
    ],

    manager: {
      title: 'Cookies management panel',
      preferences: 'Preferences for all services.',
      personalData: 'Personal data and cookies',
      confirmChoices: 'Confirm my choices',
      finalityExempt: 'Enabled',
      showDetails: 'Show more details',
    },
  },
  display: {
    heading: 'Display settings',
    hint: 'Choose a theme to customize the look of the site.',
    light: { label: 'Light theme' },
    dark: { label: 'Dark theme' },
    system: { label: 'System', hint: 'Use system settings.' },
  },
  email: {
    label: 'Email address',
    hint: 'Expected format: name@domain.fr',
    error: 'The format of the email address entered is invalid. The expected format is: name@example.org',
  },
  follow: {
    networks: { heading: 'Follow us<br/>on social networks' },
    newsletter: {
      heading: 'Subscribe to our newsletter',
      button: { label: 'Subscribe', title: 'Subscribe to our newsletter' },
      input: {
        placeholder: 'Your email address (e.g. name@domain.fr)',
        hint: `By entering your email address, you agree to receive our news by email. You can unsubscribe at any time using the unsubscribe links or by contacting us.`,
        error: 'The format of the email address entered is invalid. The expected format is: name@example.org',
      },
      registration: { success: 'Your registration has been taken into account.' },
    },
    newWindow: 'New window',
  },
  footer: {
    partners: { title: 'Ours partners' },
  },
  franceConnect: {
    title: 'Identify with',
    link: { label: 'What is FranceConnect', tooltip: 'What is FranceConnect? - new window' },
  },
  header: {
    menu: { label: 'Menu' },
  },
  login: {
    heading: 'Connecting to ',
    franceConnect: {
      heading: 'Connect with FranceConnect',
    },
    or: 'or',
    account: {
      heading: 'Log in with your account',
      email: { label: 'Identifier' },
      srOnly: { label: 'identifiers' },
      rememberMe: { label: 'Remember me' },
      button: { label: 'Log in' },
    },
    noAccount: { heading: 'You do not have an account ?', button: { label: 'Create an account' } },
  },
  modal: {
    ariaLabel: 'Close the window',
  },
  notice: {
    close: 'Hide message',
  },
  pagination: {
    firstPage: 'First page',
    previousPage: 'Previous page',
    nextPage: 'Next page',
    lastPage: 'Last page',
    role: 'navigation',
    ariaLabel: 'Paging',
  },
  password: {
    aria: { label: 'Show password' },
    forgotPassword: 'Forgot your password ?',
    label: 'Password',
    message: 'Your password must contain:',
    show: 'Show',
    dataFrValid: 'valid',
    dataFrError: 'in error',
  },
  response: {
    contactUs: 'Contact us',
  },
  select: {
    placeholder: 'Select an option',
  },
  share: {
    heading: 'Share page',
    cookiesText: 'Please $1allow cookies$2 to be shared on social networks.',
    //
    copy: { title: 'Clipboard', label: 'Copy to clipboard' },
    facebook: { title: 'Facebook', label: 'Share on Facebook' },
    linkedin: { title: 'LinkedIn', label: 'Share on LinkedIn' },
    mail: { title: 'Email', label: 'Share by email' },
    mastodon: { title: 'Mastodon', label: 'Share on Mastodon' },
    twitter: { title: 'Twitter', label: 'Share on Twitter' },
  },
  table: {
    noData: 'No data',
  },
  tag: {
    removeFilter: 'Remove the filter',
  },
  tel: {
    label: 'Phone number',
    hint: 'Expected format: (+33) 1 22 33 44 55',
    error: 'The phone number format entered is invalid. The expected format is: (+33) 2 43 55 55 55',
  },
  toggle: {
    dataLabelChecked: 'Checked',
    dataLabelUnchecked: 'Unchecked',
  },
  tooltip: {
    button: { label: 'Contextual information' },
  },
  transcription: {
    button: 'Transcription',
    ariaLabel: 'Enlarge transcript',
  },
  translate: {
    tooltip: 'Select a language',
  },
  upload: {
    label: 'Add a file',
    hint: '** Mandatory help text which specifies the constraints at the level of the expected file(s) **',
  },
  date: {
    day: { label: 'Day', hint: 'Example: 14' },
    month: { label: 'Month', hint: 'Example: 12' },
    year: { label: 'Year', hint: 'Example: 1984' },
    error: {
      required: 'All fields are required.',
      invalid: {
        format: 'Fields must be in numeric format.',
        day: 'The day is not valid.',
        month: 'The month is not valid.',
        date: 'The date is not valid.',
      },
    },
  },
  pageResponse: {
    heading: 'Unexpected error',
    description: 'Try refreshing the page or try again later.',
    detail: 'Sorry, there is a problem with the service, we are working to resolve it as soon as possible.',
  },
};
