import { convertMimeType2FileFormat } from '../../components/download/dsfr-mime.type';

const KILO_OCTETS = 1024;
const MEGA_OCTETS = KILO_OCTETS * 1024;
const GIGA_OCTETS = MEGA_OCTETS * 1024;

/**
 * Construit le libellé de détail dans un composant de téléchargement lorsque ce libellé n'est pas fourni par le Dsfr
 * @param mimeType Type mime du fichier
 * @param sizeBytes Nombre d'octets
 * @param sizeUnit Unité utilisée dans la restitution
 * @return par exemple 'PDF - 10 octets'
 */
export function downloadDetail(mimeType: string, sizeBytes: number | undefined, sizeUnit: DsfrFileSizeUnit): string {
  return !sizeBytes
    ? convertMimeType2FileFormat(mimeType) || ''
    : convertMimeType2FileFormat(mimeType) + ' - ' + fileSizeToString(sizeBytes, sizeUnit);
}

/**
 * Retourne la taille sous forme de string sous forme de 'nombre unité'
 * @param bytes Taille à transformer exprimée en octets
 * @param sizeUnit Si true, l'unité sera en bytes (KB, MB, ... ) sinon en octets (ko, mo, ...)
 */
function fileSizeToString(bytes: number, sizeUnit?: DsfrFileSizeUnit): string {
  if (!bytes) return '';

  const bytesUnit = sizeUnit === 'bytes';

  let fileSize: number;
  let fileSizeUnit: string;

  if (bytes < KILO_OCTETS) {
    fileSize = bytes;
    fileSizeUnit = bytesUnit ? FileSizeUnit.BYTES : FileSizeUnit.OCTETS;
  } else if (bytes < MEGA_OCTETS) {
    fileSize = bytes / 1024;
    fileSizeUnit = bytesUnit ? FileSizeUnit.KB : FileSizeUnit.KO;
  } else if (bytes < GIGA_OCTETS) {
    fileSize = bytes / 1048576;
    fileSizeUnit = bytesUnit ? FileSizeUnit.MB : FileSizeUnit.MO;
  } else {
    fileSize = bytes / 1073741824;
    fileSizeUnit = bytesUnit ? FileSizeUnit.GB : FileSizeUnit.GO;
  }

  fileSize = Math.round(fileSize * 100) / 100;

  // @since 1.8, les options régionales sont prise en compte pour les décimales
  //FIXME: ne devrait-ton pas transmettre la locale depuis l'extérieur de manière à pouvoir la transmettre depuis i18nService ?
  const localSize = fileSize.toLocaleString();
  return localSize + ' ' + fileSizeUnit;
}

// -- Constantes d'unités de taille de fichier -------------------------------------------------------------------------

// TODO Un peu bizarre ces noms ?
enum FileSizeUnit {
  OCTETS = 'octets',
  KO = 'ko',
  MO = 'Mo',
  GO = 'Go',
  BYTES = 'bytes',
  KB = 'KB',
  MB = 'MB',
  GB = 'GB',
}

export namespace DsfrFileSizeUnitConst {
  export const BYTES = 'bytes';
  export const OCTETS = 'octets';
}

export type DsfrFileSizeUnit = typeof DsfrFileSizeUnitConst[keyof typeof DsfrFileSizeUnitConst];
