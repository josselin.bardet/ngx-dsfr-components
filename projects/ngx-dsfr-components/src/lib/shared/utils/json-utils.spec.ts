import { describe, test } from '@jest/globals';
import { jsonPath2Value } from './json-utils';

const messages = {
  commons: {
    accept: 'Accepter',
    close: 'Fermer',
    sub: { disabled: 'Désactivé' },
  },
  labels: ['label0', 'label1', 'label2'],
};

describe('JSON', () => {
  test('path', () => {
    expect(jsonPath2Value(messages, 'commons.close')).toBe('Fermer');
    expect(jsonPath2Value(messages, 'commons.sub.disabled')).toBe('Désactivé');
    expect(jsonPath2Value(messages, 'labels[1]')).toBe('label1');
  });
});
