/**
 * La fonction permet de savoir si le code en cours s'exécute côté serveur ou côté browser.
 */
export function isOnBrowser() {
  return typeof window !== 'undefined';
}
