import { describe, test } from '@jest/globals';
import { DsfrFileSizeUnitConst, downloadDetail } from './file-utils';

describe('FileUtils', () => {
  test('sizeToString', () => {
    expect(downloadDetail('application/pdf', 10, DsfrFileSizeUnitConst.OCTETS)).toBe('PDF - 10 octets');
    expect(downloadDetail('application/pdf', 1024, 'octets')).toBe('PDF - 1 ko');
    // @since 1.8, les options régionales sont prise en compte pour les décimales
    //FIXME: le test ne passe pas s'il est exécuté sur un système non français
    expect(downloadDetail('application/pdf', 63365, 'octets')).toBe('PDF - 61,88 ko');
  });
});
