import { ElementRef } from '@angular/core';

export class DomUtils {
  private constructor() {}

  static surroundChildWithli(parent: ElementRef<any>, childTagName: string): void {
    const nativeElement = parent.nativeElement;
    const children = nativeElement.getElementsByTagName(childTagName);
    for (let i = 0; i < children.length; i++) {
      const child = children[i];
      const li = document.createElement('li');
      child.replaceWith(li);
      li.appendChild(child);
    }
  }
}
