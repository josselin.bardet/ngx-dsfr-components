import { isOnBrowser } from './ssr-utils';

/**
 * Les méthodes statiques de cette classe permettent de gérer le local storage uniquement si le code s'exécute côté browser.
 */
export abstract class LocalStorage {
  /**
   * The setItem() method of the Storage interface, when passed a key name and value, will add that key to the given
   * Storage object, or update that key's value if it already exists.
   */
  static set(key: string, value: string): void {
    if (isOnBrowser()) window.localStorage?.setItem(key, value);
  }

  /**
   * The getItem() method of the Storage interface, when passed a key name, will return that key's value, or null if
   * he key does not exist, in the given Storage object.
   * @return Signature identique à l'interface Storage.getItem(key: string): string | null
   */
  static get(key: string): string | null {
    return isOnBrowser() ? window.localStorage?.getItem(key) : null;
  }

  /**
   * The removeItem() method of the Storage interface, when passed a key name, will remove that key from the given
   * Storage object if it exists. The Storage interface of the Web Storage API  provides access to a
   * particular domain's session or local storage.
   */
  static remove(key: string): void {
    if (isOnBrowser()) window.localStorage?.removeItem(key);
  }
}
