/**
 * https://www.w3resource.com/JSON/JSONPath-with-JavaScript.php
 * @param obj : object|array This parameter represents the Object representing the JSON structure.
 * @param path : string This parameter represents JSONPath expression string.
 * @return string (on pourrait aussi retourner un objet JSON si le chemin est partiel)
 */
export function jsonPath2Value(obj: any, path: string): string {
  const keys = path.split('.');
  let value = obj; // value est soit un objet JSON, soit une valeur terminale
  keys.forEach((key) => {
    // key, ex: 'labels[20]'
    const matchArr = key.match(/([^\[]*)\[(\d*)\]/);
    const k = matchArr ? matchArr[1] : undefined; // clé sans l'indice, ex : 'labels'
    const i = matchArr ? matchArr[2] : undefined; // indice, ex : 20
    value = k && i ? value[k][i] : value[key];
  });
  return value;
}
