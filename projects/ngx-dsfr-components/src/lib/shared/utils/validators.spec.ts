import { describe, test } from '@jest/globals';
import { formatPhoneNumber, isEmailValid, isPhoneNumberValid } from './validator';

describe('Validators', () => {
  test('isEmailValid', () => {
    expect(isEmailValid(undefined)).toBe(false);
    expect(isEmailValid('')).toBe(false);
    expect(isEmailValid('nom')).toBe(false);
    expect(isEmailValid('nom@domain.c')).toBe(false);
    expect(isEmailValid('nom@domain.com')).toBe(true);
    expect(isEmailValid('NOM@domain.com')).toBe(true);
    expect(isEmailValid('nom@domain.comix')).toBe(false);
  });

  test('isPhoneNumberValid', () => {
    expect(isEmailValid(undefined)).toBe(false);
    expect(isPhoneNumberValid('')).toBe(false);
    expect(isPhoneNumberValid('(+33)')).toBe(false);
    expect(isPhoneNumberValid('(+33) 1 22 33 44')).toBe(false);
    expect(isPhoneNumberValid('+33 1 22 33 44 55')).toBe(true);
    expect(isPhoneNumberValid('+33 01 22 33 44 55')).toBe(true);
    expect(isPhoneNumberValid('+33 01 22 33 44 55 66')).toBe(false);
    expect(isPhoneNumberValid('(+33) 01 22 33 44 55')).toBe(true);
    expect(isPhoneNumberValid('01 22 33 44 5')).toBe(false);
    expect(isPhoneNumberValid('01 22 33 44 55')).toBe(true);
    expect(isPhoneNumberValid('11 22 33 44 55')).toBe(false);
    expect(isPhoneNumberValid('1 22 33 44 55')).toBe(true);
  });

  test('formatPhoneNumber', () => {
    expect(formatPhoneNumber(undefined)).toBe('');
    expect(formatPhoneNumber('')).toBe('');
    expect(formatPhoneNumber('(+33)')).toBe('(+33)');
    expect(formatPhoneNumber('(+33) 1 22 33 44')).toBe('(+33) 1 22 33 44');
    expect(formatPhoneNumber('+33 1 22 33 44 55')).toBe('(+33) 1 22 33 44 55');
    expect(formatPhoneNumber('+33 01 22 33 44 55')).toBe('(+33) 1 22 33 44 55');
    expect(formatPhoneNumber('+33 01 22 33 44 55 66')).toBe('+33 01 22 33 44 55 66');
    expect(formatPhoneNumber('(+33) 01 22 33 44 55')).toBe('(+33) 1 22 33 44 55');
    expect(formatPhoneNumber('01 22 33 44 55')).toBe('01 22 33 44 55');
    expect(formatPhoneNumber('11 22 33 44 55')).toBe('11 22 33 44 55');
    expect(formatPhoneNumber('1 22 33 44 55')).toBe('01 22 33 44 55');
    expect(formatPhoneNumber('122334455')).toBe('01 22 33 44 55');
  });
});
