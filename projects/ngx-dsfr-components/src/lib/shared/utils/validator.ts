const PATTERN_EMAIL = '^[\\w\\d._%+-]+@[\\w\\d.-]+\\.[\\w]{2,4}$'; // https://www.abstractapi.com/guides/angular-email-validation + majuscules
const REGEXP_PHONE_NUMBER = /^\(?(\+\d{2})?\)?\s?0?(\d(\s?\d{2}){4})$/;

export function isEmailValid(value: string | undefined, pattern?: string): boolean {
  if (!value) return false;

  const regexp = new RegExp(pattern ? pattern : PATTERN_EMAIL);
  return regexp.test(value);
}

export function isPhoneNumberValid(value: string | undefined, pattern?: string): boolean {
  if (!value) return false;

  const regexp = pattern ? new RegExp(pattern) : REGEXP_PHONE_NUMBER;
  const matchArr = regexp.exec(value);
  return matchArr ? true : false;
}

export function formatPhoneNumber(value: string | undefined, pattern?: string): string {
  if (!value) return '';

  const regexp = pattern ? new RegExp(pattern) : REGEXP_PHONE_NUMBER;
  const matchArr = regexp.exec(value);
  if (!matchArr) return value;

  let num = value;
  // On ne formate que dans le cas où on utilise le 'pattern' par défaut
  if (!pattern) {
    let country = matchArr[1];
    num = matchArr[2].replace(/ /g, '');
    num = num.replace(/(\d)(\d{2})(\d{2})(\d{2})(\d{2})/, '$1 $2 $3 $4 $5');
    num = country ? `(${country}) ${num}` : `0${num}`;
  }

  return num;
}
