/**
 * Les positions sous forme énumérées.
 */
export namespace DsfrPositionConst {
  export const LEFT = 'left';
  export const RIGHT = 'right';
}

/**
 * Les positions DSFR.
 */
export type DsfrPosition = typeof DsfrPositionConst[keyof typeof DsfrPositionConst];
