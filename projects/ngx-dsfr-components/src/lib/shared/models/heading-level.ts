/**
 * Constantes des niveaux de titre.
 */
export namespace DsfrHeadingLevelConst {
  /** @deprecated (@since 1.8) use undefined instead */
  export const NONE = 'none';
  export const H2 = 'H2';
  export const H3 = 'H3';
  export const H4 = 'H4';
  export const H5 = 'H5';
  export const H6 = 'H6';
}

/**
 * Niveaux de titre de H2 à H6 ou NONE (indique alors qu'il faut utiliser une balise <p>)
 */
export type DsfrHeadingLevel = typeof DsfrHeadingLevelConst[keyof typeof DsfrHeadingLevelConst];
