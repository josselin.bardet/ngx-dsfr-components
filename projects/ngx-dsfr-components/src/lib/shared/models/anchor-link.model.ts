/** Modèle de lien d'accès rapide pour accèder à des zones de la page */
export interface DsfrAnchorLink {
  /** Nom du lien */
  label: string;
  /** Lien href (exclusif avec fragment). */
  link?: string;
  /** Lien du fragment pour routerLink angular (exclusif avec link et prioritaire). */
  fragment?: string;
}
