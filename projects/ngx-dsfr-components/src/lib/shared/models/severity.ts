/**
 * Les constantes des niveaux de sévérité DSFR.
 */
export namespace DsfrSeverityConst {
  export const INFO = 'info';
  export const SUCCESS = 'success';
  export const WARNING = 'warning';
  export const ERROR = 'error';
}

/**
 * Les niveaux de sévérité DSFR exposées sous forme de type.
 */
export type DsfrSeverity = typeof DsfrSeverityConst[keyof typeof DsfrSeverityConst];
