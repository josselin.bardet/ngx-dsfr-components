/**
 * Les tailles sous forme énumérées.
 */
export namespace DsfrSizeConst {
  export const SM = 'SM';
  export const MD = 'MD';
  export const LG = 'LG';
}

/**
 * Les tailles standards DSFR : SM, MD, LG.
 */
export type DsfrSize = typeof DsfrSizeConst[keyof typeof DsfrSizeConst];
