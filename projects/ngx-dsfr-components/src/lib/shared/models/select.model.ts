/**Importe l'interface des options d'un select. */
import { DsfrSelectOption } from '../../forms';

export interface DsfrSelect {
  /**Label.*/
  label: string;
  /**Le modèle de présentation permettant de transmettre la liste des options. */
  options: DsfrSelectOption[];
  /**Attribut name de l'input.*/
  name: string;
  /**Attribut ariaLabel pour legende non visible.*/
  ariaLabel?: string;
  /**Message d'erreur de l'input.*/
  error?: string;
  /**Message de validation de l'input.*/
  valid?: string;
  /**Text additionel décrivant le champs.*/
  hint?: string;
  /**Indique que le champs est obligatoite.*/
  required?: boolean;
  /**Attribut 'id' du champ, généré automatiquement par défaut.*/
  id?: string;
  /**Permer de désactiver le champs.*/
  disabled?: boolean;
  /**La valeur gérée par le champ de formulaire.*/
  value: string;
  /**Permet de personnaliser la première option (non sélectionnable). Cette propriété est positionnée à une valeur par défaut (ex : Sélectionnez une option) internationalisée.*/
  placeHolder?: string;
}
