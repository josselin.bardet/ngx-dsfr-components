/**
 * Constantes qui correspondent aux valeurs possibles de l'attribut target d'un lien html.
 */
export namespace DsfrLinkTargetConst {
  export const BLANK = '_blank';
  export const SELF = '_self';
  export const PARENT = '_parent';
  export const TOP = '_top';
}

/** @deprecated since 1.4 use DsfrLinkTargetConst instead */
export namespace DsfrTargetLinkConst {
  export const BLANK = '_blank';
  export const SELF = '_self';
  export const PARENT = '_parent';
  export const TOP = '_top';
}

/**
 * Les constantes target exposées sous forme de type.
 */
export type DsfrLinkTarget = typeof DsfrLinkTargetConst[keyof typeof DsfrLinkTargetConst];

/** @deprecated since 1.4 use DsfrLinkTarget instead */
export type DsfrTargetLink = DsfrLinkTarget;
