import { DsfrLinkTarget } from './link-target.model';
import { DsfrNavigation } from './navigation.model';
import { DsfrPosition } from './position';

/**
 * Décrit un lien.
 */
export interface DsfrLink extends DsfrNavigation {
  /**
   * Positionne `aria-current` à la bonne valeur selon le contexte.
   * Utilisé dans les méga menus du header et le side menu.
   */
  active?: boolean;

  /**
   * La propriété ariaLabel définit une valeur de chaîne qui étiquette un élément interactif.
   *
   * @since 1.7
   */
  ariaLabel?: string;

  /**
   * Identifie l'élément (ou les éléments) contrôlé par cet élément.
   * Fix : Un lien possédant un ariaControls est destiné à ouvrir l'élément désigné par cette valeur.
   * (cela ne respecte pas les recommandations d'accessibilité qui préconise un bouton dans ce cas).
   * Dans ce cas, le lien possède un attribut `data-fr-opened="false"`
   *
   * @since 1.7
   */
  ariaControls?: string;

  /** Permet de positionner une icône sur le lien, à gauche par défaut. */
  icon?: string;

  /**
   * Permet de changer la position par défaut de l'icône.
   *
   * @since 1.7.0
   */
  iconPosition?: DsfrPosition;

  /** Label du lien. */
  label: string;

  /**
   * Pour ajouter des classes CSS custom.
   *
   * @since 1.7.0
   */
  customClass?: string;

  /**
   * Permet de désactiver le composant.
   *
   * @since 1.7.0
   */
  disabled?: boolean;

  /**
   * Target du lien. Target par défaut de l'application si la propriété est non renseignée.
   *
   * @deprecated since 1.8, use linkTarget instead
   */
  target?: DsfrLinkTarget;

  /**
   * Transforme un lien en bouton si `button`, `link` par défaut.
   *
   * @since 1.7.0
   */
  mode?: 'link' | 'button';

  /**
   * Positionne l'attribut id sur le lien.
   *
   * @since 1.7
   */
  linkId?: string;

  /**
   * Message du tooltip (attribut title).
   *
   * @since 1.7
   */
  tooltipMessage?: string;
}
