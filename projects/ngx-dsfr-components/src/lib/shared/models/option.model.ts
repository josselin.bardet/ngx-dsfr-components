/** Type correspondant à l'option d'un select */
export interface DsfrOption {
  /** Libellé de l'option */
  label: string;

  /** Valeur de l'option */
  value?: any;
}
