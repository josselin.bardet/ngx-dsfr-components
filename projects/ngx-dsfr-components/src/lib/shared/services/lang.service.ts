import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LocalStorage } from '../utils';
import { LoggerService } from './logger.service';

/**
 * Le service est chargé de maintenir le code langue de l'application et de signaler le changement de langue aux observateurs.
 */
@Injectable({ providedIn: 'root' })
export class LangService {
  private static readonly DEF_LANG = 'fr';

  /** Observable de la langue. */
  subject = new BehaviorSubject<string>(LangService.DEF_LANG);

  /** Par défaut, le code de la langue est stocké dans le localStorage.  */
  private hasLocalStorage = true;

  /** Nom par défaut du local storage. */
  private localStorageName = 'lang';

  constructor(private logger: LoggerService) {
    if (this.hasLocalStorage) {
      const lang = LocalStorage.get(this.localStorageName);
      if (lang) this.lang = <string>LocalStorage.get(this.localStorageName);
    }
  }

  get lang(): string {
    return this.subject.value;
  }

  set lang(code: string) {
    if (!code || code === this.lang) return;

    this.logger.info(`Changement de langue courante: '${code}'`);
    this.subject.next(code);

    if (this.hasLocalStorage) LocalStorage.set(this.localStorageName, code);
  }
}
