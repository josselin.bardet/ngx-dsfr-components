import { Injectable } from '@angular/core';
import { jsonPath2Value } from '../utils';
import { LangService } from './lang.service';
import { LoggerService } from './logger.service';

/**
 * La méthode i18n.t() de ce service retourne une traduction en fonction de la langue courante.
 * La traduction est recherchée dans la propriété `messages`.
 */
@Injectable({ providedIn: 'root' })
export class BaseI18nService {
  /**
   * Messages (JSON) de la langue courante.
   */
  messages: {} = {};

  constructor(protected logger: LoggerService, private langService: LangService) {
    langService.subject.subscribe((code) => this.onLangChange(code));
  }

  /**
   * Retourne la valeur de traduction dans `messages` partir d'un path json.
   * @param jsonPath le chemin dans le fichier de messages json de la langue courante
   * @return le message et si non trouvé le path json
   */
  t(jsonPath: string): string {
    let message = jsonPath2Value(this.messages, jsonPath);
    if (!message) {
      this.logger.warn(`Aucune traduction '${this.langService.lang}' pour '${jsonPath}'`);
      message = jsonPath;
    }
    return message;
  }

  protected onLangChange(code: string) {}
}
