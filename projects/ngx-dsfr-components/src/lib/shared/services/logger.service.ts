import { Injectable } from '@angular/core';
import { isOnBrowser } from '../utils';

/** Levels used for identifying the severity of an event. Levels are organized from most specific to least. */
export enum DsfrLogLevel {
  OFF = 0, // No events will be logged.
  ERROR = 2, // An error in the application, possibly recoverable.
  WARN, // An event that might possible lead to an error.
  INFO, // An event for informational purposes.
  DEBUG, // A general debugging event.
  ALL = 7, // All events should be logged
}

@Injectable({
  providedIn: 'root',
})
/** Les opérations de journalisation de la librairie sont effectuées via ce service. */
export class LoggerService {
  /** Indique à partir de quel niveau on effectue la journalisation, WARN par défaut. */
  private _level = DsfrLogLevel.WARN;

  constructor() {}

  set level(value: DsfrLogLevel) {
    if (!value) return;
    this._level = value;
    this.info(`Configuration du niveau de log : '${this.levelAsString()}'`);
  }

  error(message: string) {
    this.log(DsfrLogLevel.ERROR, message);
  }

  warn(message: string) {
    this.log(DsfrLogLevel.WARN, message);
  }

  info(message: string) {
    this.log(DsfrLogLevel.INFO, message);
  }

  debug(message: string) {
    this.log(DsfrLogLevel.DEBUG, message);
  }

  private log(level: DsfrLogLevel, message: string) {
    if (!isOnBrowser()) return;

    if (level <= this._level) {
      switch (level) {
        case DsfrLogLevel.ERROR:
          console.error(message);
          break;
        case DsfrLogLevel.WARN:
          console.warn(message);
          break;
        case DsfrLogLevel.INFO:
          console.info(message);
          break;
        default:
          console.log(message);
          break;
      }
    }
  }

  private levelAsString() {
    switch (this._level) {
      case DsfrLogLevel.OFF:
        return 'OFF';
      case DsfrLogLevel.ERROR:
        return 'ERROR';
      case DsfrLogLevel.WARN:
        return 'WARN';
      case DsfrLogLevel.INFO:
        return 'INFO';
      case DsfrLogLevel.DEBUG:
        return 'DEBUG';
      case DsfrLogLevel.ALL:
        return 'ALL';
    }
  }
}
