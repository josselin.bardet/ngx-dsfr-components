import { Injectable } from '@angular/core';
import { MESSAGES_EN, MESSAGES_FR } from '../properties';
import { BaseI18nService } from './base-i18n.service';

/** Le service est chargé de maintenir le code langue de l'application et de signaler le changement de langue aux observateurs */
@Injectable({ providedIn: 'root' })
export class I18nService extends BaseI18nService {
  protected override onLangChange(code: string): void {
    this.messages = code === 'fr' ? MESSAGES_FR : MESSAGES_EN;
  }
}
