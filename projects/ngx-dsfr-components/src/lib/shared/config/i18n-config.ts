/**
 * Ce modèle permet de configurer le service i18n.
 * - Répertoire et noms des fichiers pour chaque langue. */
export interface DsfrI18nConfig {
  messagesPath?: string;
  messagesFiles?: { code: string; fileName: string }[];
}
