import { jest } from '@jest/globals';

export function mockAngular() {
  jest.mock('@angular/common', () => {
    return {
      CommonModule: jest.fn(),
    };
  });

  jest.mock('@angular/core', () => {
    return {
      Component: jest.fn(),
      forwardRef: jest.fn(),
      EventEmitter: jest.fn(),
      Injectable: jest.fn(),
      Input: jest.fn(),
      NgModule: jest.fn(),
      OnChanges: jest.fn(),
      OnInit: jest.fn(),
      Output: jest.fn(),
      SimpleChanges: jest.fn(),
      ViewChild: jest.fn(),
      ViewEncapsulation: jest.fn(),
    };
  });

  jest.mock('@angular/router', () => {
    return {
      RouterModule: jest.fn(),
    };
  });

  jest.mock('@angular/forms', () => {
    return {
      AbstractControl: jest.fn(),
      NG_VALIDATORS: jest.fn(),
      NG_VALUE_ACCESSOR: jest.fn(),
      ValidationErrors: jest.fn(),
      Validator: jest.fn(),
    };
  });
}
