import { FormsModule } from '@angular/forms';
import { Meta, StoryFn, moduleMetadata } from '@storybook/angular';
import { DsfrButtonModule } from '../../components/button';
import { DsfrFormCheckboxModule, DsfrFormInputModule, DsfrFormSelectModule } from '../../forms';
import { DsfrFormFieldsetModule } from '../../forms/fieldset';
import { DsfrInputText } from '../../shared/models/input.model';
import { DsfrSelect } from '../../shared/models/select.model';
import { DsfrNameComponent } from './name.component';
import descriptionMd from './name.doc.md';

const meta: Meta = {
  title: 'PATTERNS/Name',
  component: DsfrNameComponent,
  decorators: [
    moduleMetadata({
      imports: [
        DsfrFormFieldsetModule,
        DsfrFormInputModule,
        FormsModule,
        DsfrFormCheckboxModule,
        DsfrButtonModule,
        DsfrFormSelectModule,
      ],
    }),
  ],
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
};
export default meta;

let myModelName: DsfrInputText = { name: 'name', label: 'Nom', value: 'Doe' };
let myModelFirstName: Array<DsfrInputText> = [{ name: 'firstname', label: 'Prénom', value: 'John' }];

/** WithNgModel */
export const WithNgModel: StoryFn<DsfrNameComponent> = (args) => ({
  props: { args, myModelName, myModelFirstName },
  template: `
  <dsfr-name [lastName]="myModelName" [firstNames]="myModelFirstName"></dsfr-name>
        <span>myModelName.value : {{myModelName.value}}</span>
        `,
});

let myModelFirstNameDisabled: Array<DsfrInputText> = [
  {
    name: 'firstname',
    label: 'Prénom',
    value: 'John',
  },
];
/** Prénom désactivé */
export const FirstNameDisabled: StoryFn<DsfrNameComponent> = (args) => ({
  props: { args, myModelName, myModelFirstNameDisabled },
  template: `
  <dsfr-name noFirstName="true" [lastName]="myModelName" [firstNames]="myModelFirstNameDisabled"></dsfr-name>
        <span>myModelName.value : {{myModelName.value}}</span>
        `,
});

let myModelUsualName: DsfrInputText = {
  name: 'UsualName',
  label: "Nom d'usage",
  value: '',
};
/** Ancien nom */
export const UsualName: StoryFn<DsfrNameComponent> = (args) => ({
  props: { args, myModelName, myModelFirstNameDisabled, myModelUsualName },
  template: `
  <dsfr-name noFirstName="true" [lastName]="myModelName" [usualName]="myModelUsualName" [firstNames]="myModelFirstNameDisabled"></dsfr-name>
        <span>myModelName.value : {{myModelName.value}}</span>
        `,
});

let myModelMultipleFirstName: Array<DsfrInputText> = [
  {
    name: 'firstname',
    label: 'Prénom',
    value: 'John',
  },
  {
    name: 'firstname',
    label: 'Prénom',
    value: 'John2',
  },
];
/** Ajout de prénom */
export const AddFirstname: StoryFn<DsfrNameComponent> = (args) => ({
  props: { args, myModelName, myModelMultipleFirstName, addNewFirstName, deleteFirstName },
  template: `
  <dsfr-name (addFirstNameSelect)="addNewFirstName()" (deleteFirstNameSelect)="deleteFirstName($event)" noFirstName="true" [addFirstName]="true" [lastName]="myModelName" [firstNames]="myModelMultipleFirstName"></dsfr-name>`,
});

function addNewFirstName() {
  myModelMultipleFirstName.push({ name: 'given-name', label: 'Prénom', value: '' });
}

function deleteFirstName(index: number) {
  console.log('on y est', index);
  myModelMultipleFirstName.splice(index, 1);
}

let myModelCountry: DsfrSelect = {
  name: 'country',
  label: 'Pays',
  value: '',
  options: [
    { label: 'France', value: 'FR' },
    { label: 'Allemagne', value: 'DE' },
    { label: 'Italie', value: 'IT' },
    { label: 'Espagne', value: 'ES' },
    { label: 'Royaume-Uni', value: 'GB' },
  ],
};
/** Demande d’un nom et d’un prénom à l’international */
export const International: StoryFn<DsfrNameComponent> = (args) => ({
  props: { args, myModelName, myModelFirstName, myModelCountry },
  template: `
  <dsfr-name noFirstName="true" [country]="myModelCountry" [lastName]="myModelName" [firstNames]="myModelFirstName"></dsfr-name>
        `,
});
