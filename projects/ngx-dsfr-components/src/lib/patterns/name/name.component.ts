import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrInputText } from '../../shared/models/input.model';
import { DsfrSelect } from '../../shared/models/select.model';

@Component({
  selector: 'dsfr-name',
  templateUrl: './name.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrNameComponent {
  /**
   * Légende du fieldset.
   */
  @Input() legend: string;

  /**
   * Data du champs name.
   */
  @Input() lastName: DsfrInputText;

  /**
   * Data du champs Firstname.
   */
  @Input() firstNames: DsfrInputText[];

  /**
   * Permet d'afficher l'option "je n'ai pas de prénom".
   */
  @Input() noFirstName = false;

  /**
   * Permet d'indiquer un nom d'usage.
   */
  @Input() usualName: DsfrInputText;

  /**
   * Permet d'afficher un bouton qui ajoute un nouveau champ de de saisi pour un prénom additionnel.
   */
  @Input() addFirstName: false;

  /**
   * Data du select de pays.
   */
  @Input() country: DsfrSelect;

  /**
   * Indique que c'est cliqué et que l'utilisateur doit intègrer un nouvel item dans 'firstnames'.
   */
  @Output() addFirstNameSelect: EventEmitter<string> = new EventEmitter();

  /**
   * Indique que le champs prénom supplémentaire doit être supprimé du tableau 'firstnames'.
   */
  @Output() deleteFirstNameSelect: EventEmitter<number> = new EventEmitter();

  /** @internal */
  noFirstNameModel: boolean;

  /** @internal */
  onAddFirstNameInput(): void {
    this.addFirstNameSelect.emit();
  }

  /** @internal */
  onDeleteFirstNameInput(index: number): void {
    this.deleteFirstNameSelect.emit(index);
  }
}
