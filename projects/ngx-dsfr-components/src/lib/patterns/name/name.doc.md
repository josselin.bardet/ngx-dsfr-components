Le bloc fonctionnel de “demande de nom et prénoms permet d’aider un utilisateur à saisir son nom et son/ses prénom(s).

[Cf. Système Design de l'État](https://www.systeme-de-design.gouv.fr/elements-d-interface/blocs-fonctionnels/nom-et-prenom)

- _Module_ : `DsfrNameModule`
- _Composant_ : `DsfrNameComponent`
- _Tag_ : `dsfr-name`
