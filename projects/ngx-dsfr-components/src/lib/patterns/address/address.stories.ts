import { FormsModule } from '@angular/forms';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryFn } from '@storybook/angular';
import { DsfrFormInputModule } from '../../forms';
import { DsfrFormFieldsetModule } from '../../forms/fieldset';
import { DsfrInputText } from '../../shared/models/input.model';
import { DsfrAddressComponent, DsfrAddressDetails } from './address.component';
import descriptionMd from './address.doc.md';

const meta: Meta = {
  title: 'PATTERNS/Address',
  component: DsfrAddressComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrFormInputModule, DsfrFormFieldsetModule, FormsModule],
    }),
  ],
  parameters: {
    docs: { description: { component: descriptionMd } },
  },
};
export default meta;

const Template: StoryFn<DsfrAddressComponent> = (args) => ({
  props: args,
});

/** Default */
export const Default = Template.bind({});
Default.decorators = [
  componentWrapperDecorator((story) => `<div class="sb-title">Demande d’une adresse postale nationale</div>${story}`),
];

/** Avec champ supplémentaire */
let myAddressSupplement: DsfrInputText = { name: '', label: '', value: '' };
export const WithSupplement = Template.bind({});
WithSupplement.args = {
  addressSupplement: myAddressSupplement,
};
WithSupplement.decorators = [
  componentWrapperDecorator(
    (story) => `<div class="sb-title">Demande d’une adresse postale avec complément optionnel</div>${story}`,
  ),
];

/** Avec lieux-dit */
let myAddressLocality: DsfrInputText = { name: '', label: '', value: '' };
export const WithLocality = Template.bind({});
WithLocality.args = {
  addressSupplement: myAddressSupplement,
  locality: myAddressLocality,
};
WithLocality.decorators = [
  componentWrapperDecorator(
    (story) =>
      `<div class="sb-title">Demande d’une adresse postale nationale + Lieu-dit, commune déléguée ou boîte postale</div>${story}`,
  ),
];

/** Avec compléments d'adresse */
let myAddressDetails: DsfrAddressDetails = { number: { label: '', name: '', value: '' } };
export const WithAddressDetails = Template.bind({});
WithAddressDetails.args = {
  addressDetails: myAddressDetails,
};
WithAddressDetails.decorators = [
  componentWrapperDecorator(
    (story) => `<div class="sb-title">Demande d’une adresse postale nationale + Complément d'adresse</div>${story}`,
  ),
];
