import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrInputText } from '../../shared/models/input.model';

export interface DsfrAddressDetails {
  /** Data du champs "numéro" */
  number?: DsfrInputText;
  /** Data du champs "voie" */
  street?: DsfrInputText;
  /** Data du champs "Bâtiment" */
  building?: DsfrInputText;
  /** Data du champs "Immeuble" */
  block?: DsfrInputText;
  /** Data du champs "Escalier" */
  stare?: DsfrInputText;
  /** Data du champs "numéro d'appartement" */
  apartment?: DsfrInputText;
}

@Component({
  selector: 'dsfr-address',
  templateUrl: './address.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrAddressComponent {
  /**
   * Légende du fieldset.
   */
  @Input() legend = 'Adresse postale';

  /**
   * Data du champs adresse.
   */
  @Input() address: DsfrInputText;

  /**
   * Data du champs complément d'adresse (optionnel).
   */
  @Input() addressSupplement: DsfrInputText;

  /**
   * Data des champs complément d'adresse ("Numéro", "Voie", "Bâtiment", "Immeuble", "Escalier", "Numéro d'appartement").
   */
  @Input() addressDetails: DsfrAddressDetails;
  /**
   * Data du champs code postal.
   */
  @Input() zipCode: DsfrInputText;
  /**
   * Data du champs ville.
   */
  @Input() city: DsfrInputText;
  /**
   * Data du champs lieux-dit/commune déléguée ou boîte postale.
   */
  @Input() locality: DsfrInputText;
}
