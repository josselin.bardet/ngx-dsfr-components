import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryFn, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../components';
import { DsfrFormInputModule, DsfrFormToggleModule } from '../../forms';
import { DsfrFormFieldsetModule } from '../../forms/fieldset';
import { DsfrDateComponent } from './date.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'PATTERNS/Date',
  component: DsfrDateComponent,
  decorators: [
    moduleMetadata({
      imports: [FormsModule, DsfrButtonModule, DsfrFormInputModule, DsfrFormFieldsetModule, DsfrFormToggleModule],
    }),
  ],
  argTypes: {
    value: { control: { type: 'object' } },
    dateChange: { control: { type: 'EventEmitter<Date>' } },
  },
};
export default meta;

type Story = StoryObj<DsfrDateComponent>;

/** Default */
export const Default: Story = {
  decorators: dsfrDecorator("Date vide à l'initialisation"),
};

/** With Date */
export const WithDate: Story = {
  decorators: dsfrDecorator("Date pré-positionnée à l'initialisation"),
  args: {
    value: new Date(),
  },
};

/** Error */
export const Error: Story = {
  decorators: dsfrDecorator("Demande d'une date avec erreur"),
  args: {
    value: new Date(),
    error: ["Un premier message d'erreur", "Un autre message d'erreur (la propriété supportant les tableaux)"],
  },
};

/** WithNgModel */

//let model = { date: new Date('2023-12-05T00:00:00.000Z') };
const date = new Date('2023-08-01T23:00:00.000Z');
let model = { orig: date, date: date };
export const WithNgModel: StoryFn<DsfrDateComponent> = (args) => ({
  props: { args, model },
  template: `
  <dsfr-date [(ngModel)]="model.date"></dsfr-date>
  <p>Date model : {{ model.date }} (ISO = {{ model.date.toISOString() }})</p>
  <p>Date orig : {{ model.orig }} (ISO = {{ model.orig.toISOString() }})</p>
  `,
});
WithNgModel.storyName = 'With NgModel';
WithNgModel.decorators = dsfrDecorator('Mise en oeuvre via NgModel');
