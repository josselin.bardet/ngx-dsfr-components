import { describe, expect, jest, test } from '@jest/globals';

import { mockAngular } from '../../shared/mock/angular.mock';
// Attention de bien initialiser les mocks Angular _AVANT_ d'importer les composants dépendants de Angular
mockAngular();

import { I18nService, LangService } from '../../shared';
import { LoggerService } from '../../shared/services/logger.service';

import { DsfrDateComponent } from './date.component';

jest.mock('../../shared/services/logger.service');

jest.mock('../../shared', () => {
  return {
    I18nService: jest.fn(),
    LangService: jest.fn(),
  };
});

let comp: any;

beforeAll(() => {
  const logger: LoggerService = new LoggerService();
  const langService: LangService = new LangService(logger);
  const i18nService: I18nService = new I18nService(logger, langService);
  comp = new DsfrDateComponent(i18nService);
});

describe('areDatesEqual', () => {
  test('doit retourner vrai pour deux dates équivalentes', () => {
    const d1: Date = new Date('2017-05-04');
    const d2: Date = new Date('2017-05-04');
    expect(comp.areDatesEqual(d1, d2)).toBe(true);
  });
  test('doit retourner faux pour deux dates différentes', () => {
    const d1: Date = new Date('2017-05-04');
    const d2: Date = new Date('2018-06-10');
    expect(comp.areDatesEqual(d1, d2)).toBe(false);
  });
  test('doit retourner vrai pour deux dates équivalentes sur jj/mm/aaaa mais différents sur la partie temps', () => {
    const d1: Date = new Date('2017-05-04T10:40:00');
    const d2: Date = new Date('2017-05-04T10:40:01');
    expect(comp.areDatesEqual(d1, d2)).toBe(true);
  });
});

describe('isAllFilled', () => {
  test('doit retourner faux si jour/mois/année sont null', () => {
    comp.day = null;
    comp.month = null;
    comp.year = null;
    expect(comp.isAllFilled()).toBe(false);
  });
  test('doit retourner vrai si jour/mois/année sont renseignés', () => {
    comp.day = '1';
    comp.month = '2';
    comp.year = '2023';
    expect(comp.isAllFilled()).toBe(true);
  });
  test('doit retourner faux si jour/mois/année sont undefined, empty-string et blank-string', () => {
    comp.day = undefined;
    comp.month = '';
    comp.year = '    ';
    expect(comp.isAllFilled()).toBe(false);
  });
});

describe('isNumber', () => {
  test("doit retourner vrai si c'est un entier natif", () => {
    expect(comp.isNumber(3)).toBe(true);
  });
  test("doit retourner vrai si c'est une chaîne de caractères dénotant en nombre", () => {
    expect(comp.isNumber('3')).toBe(true);
  });
  test("doit retourner faux si c'est une chaîne de caractères qui ne dénote pas une valeur numérique", () => {
    expect(comp.isNumber('foo')).toBe(false);
  });
  test('doit retourner faux pour undefined', () => {
    expect(comp.isNumber(undefined)).toBe(false);
  });
  test('doit retourner faux pour null', () => {
    expect(comp.isNumber(null)).toBe(false);
  });
  test('doit retourner vrai pour zéro', () => {
    expect(comp.isNumber(0)).toBe(true);
  });
  test('doit retourner faux pour zéro', () => {
    expect(comp.isNumber(NaN)).toBe(false);
  });
});

describe('normalizeDate', () => {
  test('doit retourner une date avec fraction horaire à minuit UTC', () => {
    const result: Date = comp.normalizeDate(new Date('2017-05-04T10:40:00.123+02:00'));
    expect(result.toISOString()).toStrictEqual('2017-05-04T00:00:00.000Z');
  });
  test('doit retourner undefined', () => {
    expect(comp.normalizeDate(null)).toBeUndefined();
  });
});

describe('formatTwoDigits', () => {
  test('doit retourner la chaîne "02"', () => {
    expect(comp.formatTwoDigits(2)).toStrictEqual('02');
  });
  test('doit retourner la chaîne "20"', () => {
    expect(comp.formatTwoDigits(20)).toStrictEqual('20');
  });
  test('doit retourner 02', () => {
    expect(comp.formatTwoDigits('02')).toStrictEqual('02');
  });
  test('doit retourner chaîne vide', () => {
    expect(comp.formatTwoDigits(undefined)).toStrictEqual('');
  });
  test('doit retourner 02', () => {
    expect(comp.formatTwoDigits('000002')).toStrictEqual('02');
  });
});

describe('isEmpty', () => {
  test('doit retourner vrai pour un objet vide', () => {
    expect(comp.isEmpty({})).toBe(true);
  });
  test('doit retourner faux', () => {
    expect(comp.isEmpty({ foo: 'bar' })).toBe(false);
  });
  test('doit retourner faux', () => {
    const foo: any = {};
    foo.bar = undefined;
    expect(comp.isEmpty(foo)).toBe(false);
  });
});

describe('getDateObjectFromInputValues', () => {
  test('doit retourner un objet Date correspondant aux inputs du composant ', () => {
    comp.day = '03';
    comp.month = '7';
    comp.year = '2023';
    const date: Date = comp.getDateObjectFromInputValues();
    expect(date.toISOString()).toBe('2023-07-03T00:00:00.000Z');
  });
});

describe('isInputValuesFitDateObj', () => {
  test('doit retourner vrai', () => {
    comp.day = '03';
    comp.month = '7';
    comp.year = '2023';
    expect(comp.isInputValuesFitDateObj()).toBe(true);
  });
  test('doit retourner faux', () => {
    comp.day = '32';
    comp.month = '13';
    comp.year = '2023';
    expect(comp.isInputValuesFitDateObj()).toBe(false);
  });
});

describe('updateInputFields', () => {
  afterEach(() => {
    // restore the spy created with spyOn
    jest.restoreAllMocks();
  });
  test('doit mettre à jour les champs aux valeurs de la date du model', () => {
    comp.value = new Date(2023, 6, 3);
    comp.updateInputFields();
    expect(comp.day).toBe('3');
    expect(comp.month).toBe('7');
    expect(comp.year).toBe('2023');
  });
  test('doit positionner la valeur des tous les champs sur undefined', () => {
    comp.value = 0;
    comp.updateInputFields();
    expect(comp.day).toBeUndefined();
    expect(comp.month).toBeUndefined();
    expect(comp.year).toBeUndefined();
  });
  test('doit réinitialiser les ngControl des champs de saisie', () => {
    comp.value = 0;
    comp.resetOnNull = true;

    comp.fcDay = { reset() {} };
    comp.fcMonth = { reset: jest.fn() };
    comp.fcYear = { reset: jest.fn() };
    const spyFcDay = jest.spyOn(comp.fcDay, 'reset');
    const spyFcMonth = jest.spyOn(comp.fcMonth, 'reset');
    const spyFcYear = jest.spyOn(comp.fcYear, 'reset');

    comp.updateInputFields();

    expect(spyFcDay).toHaveBeenCalledTimes(1);
    expect(spyFcMonth).toHaveBeenCalledTimes(1);
    expect(spyFcYear).toHaveBeenCalledTimes(1);
  });
});
