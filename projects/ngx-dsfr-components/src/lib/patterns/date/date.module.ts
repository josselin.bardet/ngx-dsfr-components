import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormInputModule } from '../../forms';
import { DsfrFormFieldsetModule } from '../../forms/fieldset';
import { DsfrDateComponent } from './date.component';

@NgModule({
  declarations: [DsfrDateComponent],
  exports: [DsfrDateComponent],
  imports: [CommonModule, DsfrFormFieldsetModule, DsfrFormInputModule, FormsModule],
})
export class DsfrDateModule {}
