import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { DsfrButtonModule } from '../../../components';
import { DsfrDateModule } from '../date.module';

@Component({
  selector: 'demo-date-reactiveform',
  templateUrl: './demo-date-reactiveform.html',
  imports: [CommonModule, ReactiveFormsModule, DsfrButtonModule, DsfrDateModule],
})
export class DemoDateReactiveFormComponent {
  @Input() resetOnNull: boolean = false;
  /** @internal  */
  date = new Date();
  /** @internal  */
  fcDate = new FormControl<Date | null>(null, {
    updateOn: 'change',
    validators: [Validators.required, this.myDateValidator() /*this.myFooValidator()*/],
  });
  /** @internal  */
  dateError: any = undefined;

  constructor(private fb: FormBuilder) {
    this.fcDate.statusChanges.subscribe((currentStatus) => {
      console.log('fcDate.statusChanges', currentStatus);
      // if (currentStatus === 'INVALID' && this.fcDate.errors['myCustomError']) {
      //   this.dateError = this.fcDate.errors.myCustomError;
      // } else {
      //   this.dateError = null;
      // }
      // this.dateError = this.fcDate.errors?.['myCustomError'];
    });

    this.fcDate.valueChanges.subscribe((value) => {
      console.log('fcDate.valueChanges', value);
      // if (currentStatus === 'INVALID' && this.fcDate.errors['myCustomError']) {
      //   this.dateError = this.fcDate.errors.myCustomError;
      // } else {
      //   this.dateError = null;
      // }
      // this.dateError = this.fcDate.errors?.['myCustomError'];
    });
  }
  /** @internal  */
  performInvalidDate() {
    const errorDate = new Date();
    errorDate.setDate(this.date.getDate() - 5);
    this.fcDate.setValue(errorDate);
  }
  /** @internal  */
  performValidDate() {
    const validDate = new Date();
    validDate.setDate(new Date().getDate() + 5);
    this.fcDate.setValue(validDate);
  }
  /** @internal  */
  performClearDate() {
    this.fcDate.setValue(null);
  }
  /** @internal  */
  resetFormControl() {
    this.fcDate.reset();
  }
  /** @internal  */
  myDateValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const date = control.value;
      if (date) {
        let today: Date = new Date();
        today.setHours(0, 0, 0, 0);
        let invalid = date < today;
        if (invalid) {
          return { myCustomError: 'La date saisie doit être égale ou postérieure à la date du jour' };
        }
      }

      return null;
    };
  }
  /** @internal  */
  myFooValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return { foo: 'bar' };
    };
  }
}
