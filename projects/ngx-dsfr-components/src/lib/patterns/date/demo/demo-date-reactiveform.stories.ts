import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../../components';
import { DsfrFormToggleModule } from '../../../forms';
import { DsfrDateModule } from '../date.module';
import { DemoDateReactiveFormComponent } from './demo-date-reactiveform';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'PATTERNS/Date',
  component: DemoDateReactiveFormComponent,
  decorators: [
    moduleMetadata({
      imports: [FormsModule, ReactiveFormsModule, DsfrButtonModule, DsfrFormToggleModule, DsfrDateModule],
    }),
  ],
  // argTypes: {
  //   resetOnNull: { control: { type: 'boolean' } },
  // },
};
export default meta;

type Story = StoryObj<DemoDateReactiveFormComponent>;

/** WithReactiveForm */
export const WithReactiveForm: Story = {
  name: 'With ReactiveForm',
  decorators: dsfrDecorator('Mise en oeuvre via FormControl'),
  args: { resetOnNull: false },
};
