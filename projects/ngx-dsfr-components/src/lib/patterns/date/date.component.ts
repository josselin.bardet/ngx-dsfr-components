import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { AbstractControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator } from '@angular/forms';
import { I18nService } from '../../shared/services/i18n.service';

// RPA: import via path complet plutôt que de passer par le barrel ../../shared/index
// ceci pour éviter une erreur à l'exécution des tests Jest :
// TypeError: Class extends value undefined is not a constructor or null
// Erreur visiblement déclenchée par l'import de DefaultValueAccessorComponent depuis le barrel shared qui déclencherait
//  un cycle
import { DefaultValueAccessorComponent } from '../../shared/components/default-value-accessor.component';

@Component({
  selector: 'dsfr-date',
  templateUrl: './date.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrDateComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DsfrDateComponent),
      multi: true,
    },
  ],
})
export class DsfrDateComponent
  extends DefaultValueAccessorComponent<Date | undefined>
  implements OnInit, OnChanges, Validator
{
  /** @internal */
  public static readonly ERROR_REQUIRED = 'required';
  /** @internal */
  public static readonly ERROR_INVALID_FORMAT = 'invalidFormat';
  /** @internal */
  public static readonly ERROR_INVALID_YEAR = 'invalidYear';
  /** @internal */
  public static readonly ERROR_INVALID_MONTH = 'invalidMonth';
  /** @internal */
  public static readonly ERROR_INVALID_DAY = 'invalidDay';
  /** @internal */
  public static readonly ERROR_INVALID_DATE = 'invalidDate';

  private static readonly TIME_UTC = 'T00:00:00.000Z';

  /**
   * Légende du fieldset.
   */
  @Input() legend = '';

  /**
   * Texte de description additionnel du fieldset.
   */
  @Input() hint = '';

  /**
   * Texte de succès.
   */
  @Input() valid: string;

  /**
   * Indique si la date est obligatoire.
   */
  @Input() required = false;

  /**
   * Utilisé pour positionner un attribut `name` sur le champ de formulaire.
   *
   * @deprecated (@since 1.5) Sera supprimé dans la prochaine version majeure.
   */
  @Input() name: string;

  /**
   * Attribut `id` du champ, généré automatiquement par défaut.
   *
   * @deprecated N'est plus utilisé, sera complètement supprimé à l'avenir.
   */
  @Input() id = '';

  /**
   * Réinitialise les champs de saisie (untouched) lorsqu'une valeur nulle est positionnée programmatiquement.
   */
  @Input() resetOnNull = false;

  /**
   * Indique si les champs de saisie doivent être auto-complétés à partir de la date de naissance de l'utilisateur.
   */
  @Input() autocomplete = false;

  /**
   * Signale le changement de date.
   */
  @Output() dateChange: EventEmitter<Date> = new EventEmitter();

  /** @internal */
  @ViewChild('fcDay') fcDay: AbstractControl;

  /** @internal */
  @ViewChild('fcMonth') fcMonth: AbstractControl;

  /** @internal */
  @ViewChild('fcYear') fcYear: AbstractControl;

  /**
   * Modèle de présentation de données internes.
   *
   * @internal
   */
  errorPm: string | string[] | undefined;

  /**
   * Data du champ jour.
   * @internal
   */
  private _day: string | undefined;
  /**
   * Data du champ mois.
   * @internal
   */
  private _month: string | undefined;
  /**
   * Data du champ année.
   * @internal
   */
  private _year: string | undefined;

  /**
   * Texte d'erreur ou liste d'erreurs.
   *
   * @internal
   */
  private _error: string | string[];

  constructor(/** @internal */ public i18n: I18nService) {
    super();
  }

  /**
   * Texte d'erreur ou liste d'erreurs.
   */
  get error(): string | string[] {
    return this._error;
  }

  get day(): string | undefined {
    return this._day;
  }
  get month(): string | undefined {
    return this._month;
  }
  get year(): string | undefined {
    return this._year;
  }

  @Input() set error(error: string | string[]) {
    this._error = error;
    this.fnOnValidatorChange();
  }

  set day(value: string | undefined) {
    if (value && value.trim() !== '') {
      this._day = value;
    } else {
      this._day = undefined;
    }
  }

  set month(value: string | undefined) {
    if (value && value.trim() !== '') {
      this._month = value;
    } else {
      this._month = undefined;
    }
  }

  set year(value: string | undefined) {
    if (value && value.trim() !== '') {
      this._year = value;
    } else {
      this._year = undefined;
    }
  }

  ngOnInit() {
    if (this.error) {
      this.errorPm = this.error;
    }
  }

  ngOnChanges({ value }: SimpleChanges): void {
    if (value) {
      this.updateInputFields();
    }
  }

  /**
   * @internal
   */
  writeValue(value: Date): void {
    super.writeValue(this.normalizeDate(value));
    this.updateInputFields();
    this.dateChange.emit(this.value);
  }

  /**
   * @internal
   */
  validate(control: AbstractControl): ValidationErrors | null {
    const errors: ValidationErrors | null = this.validateInputs();

    this.updateErrorPm(errors);

    return errors;
  }

  /**
   * @internal
   */
  registerOnValidatorChange(fn: () => void): void {
    this.fnOnValidatorChange = fn;
  }

  /**
   * @internal
   */
  onFocusOut() {
    this.fnOnTouched();
    const errors = this.validateInputs();
    if (errors) {
      if (this.isAllCleared()) {
        // si l'utilisateur a tout effacé on positionne le modèle à undefined
        this.performDateChange(undefined);
      } else {
        // force la revalidation pour déclencher la mise à jour des messages d'erreur
        this.fnOnValidatorChange();
      }
    } else if (this.isAllFilled()) {
      // si tous les champs ont été saisis, on déclenche une mise à jour du modèle
      this.performDateChange(this.getDateObjectFromInputValues());
    }
  }

  /**
   * @internal
   */
  private validateInputs(): ValidationErrors | null {
    const errors: any = {};
    if (this.isAllTouched() && !this.isAllFilled()) {
      errors[DsfrDateComponent.ERROR_REQUIRED] = this.i18n.t('date.error.required');
    } else if (
      (this.day && !this.isNumber(this.day)) ||
      (this.month && !this.isNumber(this.month)) ||
      (this.year && !this.isNumber(this.year))
    ) {
      errors[DsfrDateComponent.ERROR_INVALID_FORMAT] = this.i18n.t('date.error.invalid.format');
    } else {
      if (this.day && (+this.day < 1 || +this.day > 31)) {
        errors[DsfrDateComponent.ERROR_INVALID_DAY] = this.i18n.t('date.error.invalid.day');
      }
      if (this.month && (+this.month < 1 || +this.month > 12)) {
        errors[DsfrDateComponent.ERROR_INVALID_MONTH] = this.i18n.t('date.error.invalid.month');
      }
      if (this.isEmpty(errors) && this.isAllFilled() && !this.isInputValuesFitDateObj()) {
        errors[DsfrDateComponent.ERROR_INVALID_DATE] = this.i18n.t('date.error.invalid.date');
      }
    }

    return this.isEmpty(errors) ? null : errors;
  }

  /**
   * Modifie le modèle de date et notifie le changement à l'extérieur.
   *
   * @internal
   */
  private performDateChange(newDate: Date | undefined) {
    if (!this.areDatesEqual(this.value, newDate)) {
      this.value = newDate;
      this.dateChange.emit(this.value);
    }
  }

  /**
   * Renvoie vrai si les deux dates présentent les mêmes valeurs jour/mois/année, faux sinon.
   *
   * @internal
   */
  private areDatesEqual(d1: Date | undefined, d2: Date | undefined) {
    return d1 === d2 || d1?.toISOString().substring(0, 10) === d2?.toISOString().substring(0, 10);
  }

  /**
   * Compile le modèle de présentation des erreurs qui est transmis au fieldset en charge d'afficher les message
   * d'erreur.
   *
   * @internal
   */
  private updateErrorPm(validationErrors: ValidationErrors | null) {
    let allErrors: string[] = [];
    // gestion des erreurs internes
    if (validationErrors) {
      allErrors = allErrors.concat(Object.values(validationErrors).filter((err) => typeof err === 'string'));
    }
    // gestion des erreurs utilisateurs
    if (this.error) {
      if (Array.isArray(this.error)) {
        allErrors = allErrors.concat(this.error);
      } else if (this.error.length > 0) {
        allErrors.push(this.error);
      }
    }
    // on met à jour la vue avec toutes les erreurs (internes + externes)
    this.errorPm = allErrors.length > 0 ? allErrors : undefined;
  }

  /**
   * Mets à jour la valeurs des champs de saisie à partir de la valeur du modèle.
   *
   * @internal
   */
  private updateInputFields() {
    if (this.value) {
      this._day = String(this.value.getDate());
      this._month = String(this.value.getMonth() + 1); // le mois est représenté par un index de 0 à 11
      this._year = String(this.value.getFullYear());
    } else {
      this._day = undefined;
      this._month = undefined;
      this.year = undefined;
      // gestion de la réinitialisation des champs (untouched)
      if (this.resetOnNull) {
        this.fcDay?.reset();
        this.fcMonth?.reset();
        this.fcYear?.reset();
      }
    }
  }

  /**
   * Renvoie vrai si tous les champs de saisie ont été touchés, faux sinon.
   *
   * @internal
   */
  private isAllTouched() {
    return this.fcDay?.touched && this.fcMonth?.touched && this.fcYear?.touched;
  }

  /**
   * Renvoie vrai si tous les champs de saisie ont été renseignés, faux sinon.
   *
   * @internal
   */
  private isAllFilled() {
    return !!this.day && !!this.month && !!this.year;
  }

  /**
   * Renvoie vrai si tous les champs de saisie ont été effacés.
   *
   * @internal
   */
  private isAllCleared() {
    return this.isAllTouched() && this.day === undefined && this.month === undefined && this.year === undefined;
  }

  /**
   * Renvoie vrai si la valeur passée en paramètre est numérique, faux sinon.
   *
   * @internal
   */
  private isNumber(value: any) {
    return !isNaN(parseInt(value));
  }

  /**
   * Retourne vrai si l'objet Date construit à partir des valeurs récupérées depuis les champs de saisie correspond bien
   * à la date signifiée par les champs de saisie.
   *
   * En cas de dépassement des limites supportées, le contructeur Date() va "décaler" et fournir une date "incrémentée"
   * sur l'année et le mois suivant selon la nature du "dépassement".
   *
   * Cette méthode permet de vérifier que la date indiquée dans le s champs de saisie ne soit pas sujette à un décalage.
   * Par exemple : saisir le 29/02/2023 provoquerait un décalage sur le 01/03/2023 car février 2023 ne comporte que
   * 28 jours.
   *
   * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/Date
   *
   * @internal
   */
  private isInputValuesFitDateObj(): boolean {
    const dateObj = this.getDateObjectFromInputValues();
    let dDate = dateObj.getDate();
    let mDate = dateObj.getMonth() + 1; // le mois est représenté par un index de 0 à 11
    let yDate = dateObj.getFullYear();

    return dDate === Number(this.day) && mDate === Number(this.month) && yDate === Number(this.year);
  }

  /**
   * Retourne un objet Date depuis les valeurs présentes dans les champs de saisie.
   *
   * @internal
   */
  private getDateObjectFromInputValues(): Date {
    const dateString =
      [this.year, this.formatTwoDigits(this.month), this.formatTwoDigits(this.day)].join('-') +
      DsfrDateComponent.TIME_UTC;

    return new Date(dateString);
  }

  /**
   * Retourne une date équivalente à la date passée en paramètre mais dont la fraction temporelle est réinitialisée à
   * minuit.
   *
   * @internal
   */
  private normalizeDate(date: Date): Date | undefined {
    if (!date) return undefined;

    return new Date(date.toISOString().substring(0, 10) + DsfrDateComponent.TIME_UTC);
  }

  /**
   * Permet de formater la valeur numérique transmise en tant que chaîne de caractères sur deux digits.
   *
   * @internal
   */
  private formatTwoDigits(value: string | undefined): string {
    if (value) {
      return ('0' + value).slice(-2);
    }

    return '';
  }

  /**
   * Retourne vrai si l'objet ne contient aucune clé, faux sinon.
   *
   * @internal
   */
  private isEmpty(obj: any): boolean {
    return obj && obj.constructor === Object && Object.keys(obj).length === 0;
  }

  /**
   * Permet de stocker la fonction pouvant être appelée pour forcer une revalidation.
   */
  private fnOnValidatorChange = () => {};
}
