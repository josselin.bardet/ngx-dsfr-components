/* Public API Surface of ngx-dsfr */
export * from './lib/components';
export * from './lib/components/follow'; // Ici afin d'éviter une erreur de dépendances cycliques pendant la compilation
export * from './lib/forms';
export * from './lib/pages';
export * from './lib/patterns';

// shared
export * from './lib/shared/config';
export * from './lib/shared/directives';
export * from './lib/shared/models';

// Utilisé par les extensions (en plus des modèles)
export * from './lib/shared/components/default-control.component';
export * from './lib/shared/components/default-value-accessor.component';
export * from './lib/shared/services/base-i18n.service'; // export depuis @since 1.8 car utilisé par `ngx-dsfr-ext`
export * from './lib/shared/services/lang.service';
export * from './lib/shared/services/logger.service';
export * from './lib/shared/utils/json-utils';
export * from './lib/shared/utils/uuid-utils';
