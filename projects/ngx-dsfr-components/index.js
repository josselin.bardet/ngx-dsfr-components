#!/usr/bin/env node

const argsParser = require('args-parser');
const { name, version } = require('./package.json');
const { upgradeMain } = require('./scripts/upgrader/main');

function main() {
  const args = argsParser(process.argv);
  if (args.v || args.version) console.log(`${name} ${version}`);
  else if (args.u || args.upgrade) upgradeMain(args?.dir);
}

main();
