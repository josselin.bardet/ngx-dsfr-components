// playwright.config.ts
import { defineConfig } from '@playwright/test';

export default defineConfig({
  // Run all tests in parallel.
  fullyParallel: true,
  // Glob patterns or regular expressions that match test files.
  testMatch: '**/*.e2e.ts',
  use: {
    baseURL: 'http://localhost:6006/',
  },
});
