# @edugouvfr/ngx-dsfr - Guide de migration

Ce guide trace les changements non rétrocompatibles d'une version à l'autre.
_Les renommages (input, output, etc.) ont été également documentés dans le CHANGELOG, se référer au préfixe `break:`._

## Dépréciations depuis la 1.1

Le tableau ci-dessous recense les éléments dépréciés depuis la version 1.1 : _tag_ (ex : `dsfr-checkbox`), _composants_
(nom simple, sans préfixe ni suffixe, exemple Alert pour `DsfrAlertComponent`), _interfaces_ (nom complet), _propriétés_
ou _slots_ (`<ng-content />`). Description des colonnes :

- 'Dépréciation' : composant ou interface impactée par la dépréciation
- 'Version' : version dans laquelle l'élément a été déprécié
- 'Nature' : nature de l'élément déprécié
- 'Element' : élément déprécié
- 'Remplacée par' : élément de remplacement, vide indique que l'élément déprécié est / sera supprimé
- 'Supprimé en' : version dans laquelle sera ou a été supprimé l'élément déprécié

| Dépréciation            | Version | Nature    | Element              | Remplacée par            | Supprimé en |
| ----------------------- | :-----: | --------- | -------------------- | ------------------------ | :---------: |
| Alert                   |   1.7   | propriété | `closable`           | `closeable`              |             |
| Accordion               |   1.8   | propriété | `index`              |                          |     2.0     |
| Accordion               |         | slot      | `content`            |                          |     2.0     |
| Accordion               |         | slot      | `content`            |                          |     2.0     |
| Button                  |   1.1   | propriété | `uppercase`          |                          |     2.0     |
| Button                  |   1.1   | propriété | `loader`             |                          |     2.0     |
| Consent                 |   1.7   | propriété | `rgpdUrl`            | `rgpdLink`               |     2.0     |
| Consent                 |   1.7   | propriété | `title`              | `heading`                |     2.0     |
| Content                 |   1.6   | propriété | `transcription`      | `transcriptionContent`   |     2.0     |
| Content                 |   1.6   | propriété | `transcriptionLabel` | `linkLabel`              |     2.0     |
| Download                |   1.5   | composant |                      | Card, Tile               |             |
| Link                    |   1.5   | propriété | `targetLink`         | `linkTarget`             |     2.0     |
| Pagination              |   1.6   | propriété | `pageSelectEvent`    | `pageSelect`             |     2.0     |
| Pagination              |   1.6   | propriété | `backEvent`          | `backSelect`             |     2.0     |
| PreviousPage            |   1.6   | composant |                      | Pagination               |             |
| SearchBar               |   1.5   | propriété | `id`                 | `inputId`                |     2.0     |
| Tag                     |   1.5   | propriété | `id`                 | `tagId`                  |     2.0     |
| Tile                    |   1.5   | propriété | `imageAlt`           |                          |             |
| Tile                    |   1.5   | propriété | `imagePath`          | `artworkFilePath`        |             |
| Upload                  |   1.4   | propriété | `description`        | `hint`                   |             |
| FormCheckbox            |   1.5   | propriété | `id`                 | `inputId`                |     2.0     |
| FormCheckbox            |   1.1   | propriété | `dsfr-checkbox`      | `dsfr-form-checkbox`     |     1.1     |
| FormInput               |   1.5   | propriété | `id`                 | `inputId`                |     2.0     |
| FormInput               |   1.1   | propriété | `dsfr-input`         | `dsfr-form-input`        |     1.1     |
| FormInput               |   1.2   | propriété | `width`              |                          |     2.0     |
| FormInput               |   1.2   | propriété | `textarea`           | `type`                   |     2.0     |
| FormPassword            |   1.5   | propriété | `id`                 | `inputId`                |     2.0     |
| FormRadio               |   1.5   | propriété | `id`                 | `inputId`                |     2.0     |
| FormRadio               |   1.1   | propriété | `dsfr-radio`         | `dsfr-form-radio`        |     1.1     |
| FormRadio               |   1.1   | propriété | `radios`             | `options`                |     2.0     |
| FormRadioRich           |   1.5   | propriété | `id`                 | `inputId`                |     2.0     |
| FormRadioRich           |   1.1   | propriété | `dsfr-radio-rich`    | `dsfr-form-radio-rich`   |     1.1     |
| FormRadioRich           |   1.2   | propriété | `radioRich`          | `options`                |     2.0     |
| FormSelect              |   1.5   | propriété | `id`                 | `inputId`                |     2.0     |
| FormSelect              |   1.1   | propriété | `dsfr-select`        | `dsfr-form-select`       |     1.1     |
| FormSelect              |   1.2   | propriété | `placeHolder`        | `placeholder`            |     2.0     |
| FormToggle              |   1.5   | propriété | `id`                 | `inputId`                |     2.0     |
| Date                    |   1.5   | propriété | `name`               |                          |     2.0     |
| Date                    |   1.5   | propriété | `id`                 |                          |     2.0     |
| ShareLink               |   1.8   | propriété | `target`             | `linkTarget`             |     2.0     |
| Upload                  |   1.5   | propriété | `id`                 | `inputId`                |     2.0     |
| DsfrTargetLink          |   1.4   | interface |                      | DsfrLinkTarget           |     2.0     |
| DsfrTargetLinkConst     |   1.4   | interface |                      | DsfrLinkTargetConst      |     2.0     |
| DsfrCardBackground      |   1.5   | interface |                      | DsfrPanelBackground      |     2.0     |
| DsfrCardBackgroundConst |   1.5   | interface |                      | DsfrPanelBackgroundConst |     2.0     |
| DsfrCardBorder          |   1.5   | interface |                      | DsfrPanelBorder          |     2.0     |
| DsfrCardBorderConst     |   1.5   | interface |                      | DsfrPanelBorderConst     |     2.0     |
| DsfrHeaderTranslate     |   1.7   | propriété | `currentLangCode`    |                          |     2.0     |
| DsfrHeadingLevel        |   1.8   | enum      | `NONE`               | `undefined`              |             |
| DsfrIcon                |  1.9.2  | interface |                      | `string`                 |     2.0     |
