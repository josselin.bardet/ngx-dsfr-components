import { StorybookConfig } from '@storybook/angular';

const config: StorybookConfig = {
  stories: ['../projects/ngx-dsfr-components/**/*.@(mdx|stories.@(js|jsx|ts|tsx))'],
  addons: [
    '@storybook/addon-essentials',
    '@storybook/addon-interactions',
    '@storybook/addon-links',
    //'storybook-addon-angular-router', workaround to Error: Cannot find module '@storybook/global'
  ],
  babel: async (options) => ({
    ...options,
    compact: true,
  }),
  framework: {
    name: '@storybook/angular',
    options: {},
  },
  features: {
    storyStoreV7: true,
  },
  staticDirs: [
    './assets',
    '../node_modules/@gouvfr/dsfr/dist',
    { from: '../node_modules/@gouvfr/dsfr/dist/artwork/pictograms/system', to: '/artwork/pictograms/environment' },
  ],
  docs: {
    autodocs: true,
  },
};
export default config;
