import { setCompodocJson } from '@storybook/addon-docs/angular';
import docJson from '../documentation.json';

/**
 * Ajout de documentation pour les propriétés dépréciées, et déplacer en bas de la liste.
 *
 * A supprimer quand SB prendra en charge les annotations 'deprecated'.
 *
 * @see https://github.com/storybookjs/storybook/issues/9721
 */
function displayDeprecated(list) {
  return list
    .map((prop) => ({
      ...prop,
      rawdescription: prop.deprecated
        ? `<p>
            <p>${prop.rawdescription ?? ''}</p>
            <p class="deprecated">
              <span class="flag">deprecated</span>
              <span class="message">${prop.deprecationMessage ?? ''}</span>
            </p>
           </p>`
        : prop.rawdescription ?? '',
    }))
    .sort((a, b) => {
      return a.deprecated && !b.deprecated ? 1 : -1;
    });
}

const additionalCompoDoc = (doc) => ({
  ...doc,
  components: doc.components.map((component) => ({
    ...component,
    inputsClass: displayDeprecated(component.inputsClass),
    propertiesClass: displayDeprecated(component.propertiesClass),
    outputsClass: displayDeprecated(component.outputsClass),
  })),
});

setCompodocJson(additionalCompoDoc(docJson));

const preview = {
  parameters: {
    controls: {},
    docs: {
      story: {
        inline: true,
      },
    },
    options: {
      // storySort: {
      //   method: 'alphabetical',
      //   order: ['Introduction', ['Readme', 'Changelog', 'Migration'], 'COMPONENTS', 'FORMS', 'PATTERNS', 'PAGES'],
      // },
      storySort: (a, b) => {
        if (a.id === b.id) return 0;
        if (a.id.startsWith('introduction-readme')) return -1;
        if (b.id.startsWith('introduction-readme')) return 1;
        if (a.id.startsWith('introduction-changelog')) return -1;
        if (b.id.startsWith('introduction-changelog')) return 1;
        if (a.id.startsWith('introduction')) return -1;
        if (b.id.startsWith('introduction')) return 1;
        if (a.id.startsWith('pages')) return 1;
        if (b.id.startsWith('pages')) return -1;
        if (a.title === b.title) {
          if (a.name === 'Docs') return -1;
          if (b.name === 'Docs') return 1;
          if (a.name === 'Default') return -1;
          if (b.name === 'Default') return 1;
          if (a.importPath.includes('/demo/')) return 1;
          if (b.importPath.includes('/demo/')) return -1;
        }
        if (a.title.length > b.title.length && a.title.startsWith(b.title)) return 1;
        if (b.title.length > a.title.length && b.title.startsWith(a.title)) return -1;

        //return a.id.localeCompare(b.id, undefined, { numeric: true });
        return 0;
      },
    },
  },
};
export default preview;
