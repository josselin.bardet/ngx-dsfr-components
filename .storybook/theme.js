import { create } from '@storybook/theming';
import { version } from '../projects/ngx-dsfr-components/package.json';

export default create({
  base: 'light',
  brandTitle: `
    <div>
      <img src="img/marianne.svg" height="16px"/>
    </div>
    <div style="font-size:1.1rem">ngx-dsfr</div>
    <div style="font-size:0.8rem;color:grey">${version}</div>
  `,
  brandUrl: '/',
  // brandImage: "img/marianne.svg",
});
