import { componentWrapperDecorator } from '@storybook/angular';
import { DsfrSizeConst } from '../projects/ngx-dsfr-components/src/lib/shared';

// -- Constantes -------------------------------------------------------------------------------------------------------
export const controlDisable = { table: { disable: true } };

const icons = ['fr-icon-checkbox-circle-line', 'ri-checkbox-circle-line'];
const buttonVariants = ['primary', 'secondary', 'tertiary', 'tertiary-no-outline'];

export const argBoolean = { control: { type: 'inline-radio' }, options: [true, false] };
export const argButtonVariant = { control: { type: 'inline-radio' }, options: buttonVariants };
export const argEventEmitter = { control: { type: 'EventEmitter' } };
export const argHeadingLevel = { control: { type: 'inline-radio' }, options: ['H2', 'H3', 'H4', 'H5', 'H6'] };
export const argIcon = { control: { type: 'select' }, options: ['', ...icons] };
export const argPosition = { control: { type: 'inline-radio' }, options: ['left', 'right'] };
export const argSize = { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) };

// -- Decorators -------------------------------------------------------------------------------------------------------

export function dsfrDecorator(title: string, info?: string): any {
  return [titleDecorator(title, info)];
}

export function titleDecorator(title: string, info?: string): any {
  if (!info) return componentWrapperDecorator((story) => `<div class="sb-title">${title}</div>${story}`);
  return componentWrapperDecorator(
    (story) => `<div class="sb-title">${title}</div><div class="sb-message">${info}</div>${story}`,
  );
}

export const gridDecoratorLG = gridDecorator('LG');
export const gridDecoratorMD = gridDecorator('MD');
export const gridDecoratorSM = gridDecorator('SM');

export function gridDecorator(size: string): any {
  let gridClass: string;
  if (size === 'SM') gridClass = 'fr-col-6 fr-col-md-3 fr-col-lg-3';
  else if (size === 'LG') gridClass = 'fr-col-12 fr-col-md-8 fr-col-lg-6';
  else gridClass = 'fr-col-9 fr-col-md-4 fr-col-lg-4';
  return componentWrapperDecorator(
    (story) => `<div class="fr-mb-6v">
    <div class="fr-grid-row fr-grid-row--gutters">
        <div class="${gridClass}">
      ${story}
    </div>
  </div>
</div>
`,
  );
}

export function bgDecorator(bgcolor: string): any {
  return componentWrapperDecorator((story) => `<div style="background-color: ${bgcolor}">${story}</div>`);
}
